#define _POSIX_C_SOURCE 200809L
#include "utils.h"

#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int size_t_cmp(const void *a, const void *b)
{
	const size_t aa = *((const size_t *) a);
	const size_t bb = *((const size_t *) b);
	return (aa < bb) ? -1 : (aa > bb);
}

bool is_whitespace_line(const char *line)
{
	size_t i = 0;

	while(line[i] != '\0') {
		char ch = line[i];
		if (!isspace(ch))
			return false;

		++i;
	}

	return true;
}

char *rstrip(char *string)
{
	size_t len = strlen(string);
	char *end = string + len - 1;

	while (end >= string && isspace(*end)) {
		end--;
	}
	*(end + 1) = '\0';

	return string;
}

#ifdef _WIN32
ssize_t getline2 (char **lineptr, size_t *n, FILE *stream)
{
	char *read = (*lineptr);
	if (read == NULL) {
		read = malloc(10000*sizeof(char));
		if (read == NULL) {
			return -1;
		}
		*lineptr = read;
		*n = 10000;
	}

	char *result = fgets(read, *n, stream);
	if (result == NULL) {
		return -1;
	}

	size_t len = strlen(read);
	while (len == (*n)-1) {
		read = realloc(read, (*n)*2*sizeof(char));
		if (read == NULL) {
			return -1;
		}
		*lineptr = read;
		result = fgets(read+(*n)-1, (*n)+1, stream);
		(*n) *= 2;
		len = strlen(read);
	}
	return len;
}

char *strndup(const char *s, size_t n)
{
	char *out = calloc(n + 1, sizeof(char));
	strncpy(out, s, n);
	return out;
}
#endif /* _WIN32 */
