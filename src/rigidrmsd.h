#ifndef _MOL_RIGID_RMSD_H_
#define _MOL_RIGID_RMSD_H_

#include "atom_group.h"
#include "quaternion.h"

/**
 * \addtogroup l_rrmsd Rigid RMSD
 * \brief Module to quickly calculate RMSD between different orientations of same rigid body.
 *
 * The code is based on the paper by Popov and Grudinin [1]. In particular, all references to equations
 * are given according to this paper.
 *
 * This library does not implement weighting of the atoms, so all w_i in Eq. (1) and elsewhere are equal to 1.
 * Therefore, the CoM reference frame mentioned in the paper is actually centroid-based here.
 * But for shortness and simplicity it's still called CoM.
 *
 * [1] Popov, P. & Grudinin, S. Rapid determination of RMSDs corresponding to macromolecular rigid body motions. J. Comput. Chem. 35, 950–956 (2014). doi:10.1002/jcc.23569
 *
 * @{
 */


/**
 * Rigid body descriptor used for calculating RMSDs between different positions of the same rigid body.
 */
struct mol_rrmsd_atom_group
{
	struct mol_vector3 centroid;  /**< Centroid of rigid body */
	struct mol_matrix3 inertia; /**< Inertia tensor of rigid body (Eq. 13), in CoM reference frame, scaled by 4/natoms */
	struct mol_vector3 xdiag; /** Diagonal of X matrix of rigid body (Eq. 18), in CoM reference frame, scaled by 4/natoms. Off-diagonal elements can be easily taken from intertia matrix. */
};


/**
 * Create new \ref mol_rrmsd_atom_group.
 *
 * The parameters of rigid body are calculated from \p ag and \p interface.
 * *
 * If \p interface is \c NULL, the returned rigid body will consist of all atoms of \p ag.
 * Otherwize, the returned rigid body will only contain atoms with indices listed in \p interface.
 *
 * \param ag Atom group.
 * \param interface List of atoms to use from \p ag. Can not be \c NULL.
 * \return New rigid body descriptor \ref mol_rrmsd_atom_group.
 */
struct mol_rrmsd_atom_group *mol_rrmsd_atom_group_create(
	const struct mol_atom_group *ag,
	const struct mol_index_list *interface);


void mol_rrmsd_atom_group_destroy(struct mol_rrmsd_atom_group *rag);
void mol_rrmsd_atom_group_free(struct mol_rrmsd_atom_group *rag);


/**
 * Calculate RMSD between original and transformed rigid body.
 *
 * The transformation is given by rotation matrix \p r and translation vector \p tv.
 *
 * See Eq. (25).
 *
 * The code is somewhat analogous to
 * \code
 * mol_atom_group_move_in_copy(ag, ag1, r, tv);
 * return rmsd(ag, ag1);
 * \endcode
 *
 * \param rag Rigid body descriptor.
 * \param r Rotation matrix.
 * \param tv Translation vector.
 * \return RMSD value.
 */
double mol_rrmsd_rmsd(
	const struct mol_rrmsd_atom_group *rag,
	const struct mol_matrix3 *r,
	const struct mol_vector3 *tv);


/**
 * Calculate RMSD between original and transformed rigid body.
 *
 * The transformation is given by quaternion \p q and translation vector \p tv.
 *
 * See Eq. (24) and \ref mol_rrmsd_rmsd.
 *
 * \param rag Rigid body descriptor.
 * \param q Rotation quaternion.
 * \param tv Translation vector.
 * \return RMSD value.
 */
double mol_rrmsd_rmsd_q(
	const struct mol_rrmsd_atom_group *rag,
	const struct mol_quaternion *q,
	const struct mol_vector3 *tv);


/**
 * Calculate RMSD between two different transformations of the \p rag.
 *
 * See Eq. (21), but since we operate in CoM reference frame, C = 0.
 *
 * \code
 * mol_atom_group_move_in_copy(ag, ag1, r1, tv1);
 * mol_atom_group_move_in_copy(ag, ag2, r2, tv2);
 * return rmsd(ag1, ag2);
 * \endcode
 *
 * \param rag Rigid body.
 * \param r1 Rotation matrix of the first transformation.
 * \param tv1 Translation vector of the first transformation.
 * \param r2 Rotation matrix of the second transformation.
 * \param tv2 Translation vector of the second transformation.
 * \return RMSD value.
 */
double mol_rrmsd_rmsd_pairwise(
	const struct mol_rrmsd_atom_group *rag,
	const struct mol_matrix3 *r1,
	const struct mol_vector3 *tv1,
	const struct mol_matrix3 *r2,
	const struct mol_vector3 *tv2);


/**
 * Calculate RMSD between two different transformations of the \p rag.
 *
 * See Eq. (20), but since we operate in CoM reference frame, C = 0.
 *
 * Similar to \ref mol_rrmsd_rmsd_pairwise, but rotations are given by quaternions.
 *
 * \param rag Rigid body.
 * \param q1 Rotation quaternion of the first transformation.
 * \param tv1 Translation vector of the first transformation.
 * \param q2 Rotation quaternion of the second transformation.
 * \param tv2 Translation vector of the second transformation.
 * \return RMSD value.
 */
double mol_rrmsd_rmsd_pairwise_q(
	const struct mol_rrmsd_atom_group *rag,
	const struct mol_quaternion *q1,
	const struct mol_vector3 *tv1,
	const struct mol_quaternion *a2,
	const struct mol_vector3 *tv2);


/**
 * Recalculate rigid body parameters.
 *
 * Analogous to creating a new rigid body with \p ag and \p interface.
 *
 * \param rag
 * \param ag Atomgroup. Can not be \c NULL.
 * \param interface List of atoms to use from \p ag. If \c NULL, all atoms are used.
 */
void mol_rrmsd_atom_group_update(
		struct mol_rrmsd_atom_group *rag,
		const struct mol_atom_group *ag,
		const struct mol_index_list *interface);

/** @}*/

#endif /* _MOL_RIGID_RMSD_H_ */
