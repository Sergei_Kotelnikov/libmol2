#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES
#include "benergy.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>

#define small 0.0000001

void beng(struct mol_atom_group *ag, double *en)
{
	double l, e1;
	struct mol_vector3 dist, dl;
	struct mol_bond *b;
	for (size_t i = 0; i < ag->active_bonds->size; ++i) {
		b = ag->active_bonds->members[i];

		MOL_VEC_SUB(dist, ag->coords[b->ai], ag->coords[b->aj]);

		l = sqrt(MOL_VEC_SQ_NORM(dist));
		e1 = (b->k) * (l - (b->l0));
		(*en) += e1 * (l - (b->l0));
		e1 = -2 * e1 / l;

		MOL_VEC_MULT_SCALAR(dl, dist, e1);
		MOL_VEC_ADD(ag->gradients[b->ai], ag->gradients[b->ai], dl);
		MOL_VEC_SUB(ag->gradients[b->aj], ag->gradients[b->aj], dl);
	}
}

void aeng(struct mol_atom_group *ag, double *en)
{
	static const double DEGRA = M_PI / 180.0;

	double ls10, l10;
	double ls12, l12, l02;
	double dth, sth, e1, e2, e3;
	double yzp, yzm, xzp, xzm, xyp, xym;
	struct mol_angle *ap;
	struct mol_vector3 delta10, delta12;
	struct mol_vector3 delta10sq, delta12sq;

	for (size_t i = 0; i < ag->active_angles->size; i++) {
		ap = ag->active_angles->members[i];

		MOL_VEC_SUB(delta10, ag->coords[ap->a0], ag->coords[ap->a1]);
		MOL_VEC_SUB(delta12, ag->coords[ap->a2], ag->coords[ap->a1]);

		ls10 = MOL_VEC_SQ_NORM(delta10);
		MOL_VEC_MULT(delta10sq, delta10, delta10);
		l10 = sqrt(ls10);

		ls12 = MOL_VEC_SQ_NORM(delta12);
		MOL_VEC_MULT(delta12sq, delta12, delta12);
		l12 = sqrt(ls12);

		l02 = l10 * l12;
		dth = acos((delta10.X*delta12.X + delta10.Y*delta12.Y + delta10.Z*delta12.Z) / l02);
		sth = sin(dth);

		if (sth < small) {
			fprintf(stderr,
				"angle %zu (%zu, %zu, %zu) is close to linear\n",
				i, ap->a0, ap->a1, ap->a2);
			fprintf(stderr,
				"accuracy of forces will be compromised\n");
			sth = small;
		}

		dth -= DEGRA * (ap->th0);
		e1 = (ap->k) * dth;
		(*en) += e1 * dth;
		e1 *= 2.0 / sth / l02;
// 10 bond
		e2 = e1 / ls10;

		yzp = delta10sq.Y + delta10sq.Z;
		yzm = -delta10.Y * delta10.Z;
		xzp = delta10sq.X + delta10sq.Z;
		xzm = -delta10.X * delta10.Z;
		xyp = delta10sq.X + delta10sq.Y;
		xym = -delta10.X * delta10.Y;

		e3 = e2 * (yzp * delta12.X + xym * delta12.Y + xzm * delta12.Z);
		(ag->gradients[ap->a0].X) += e3;
		(ag->gradients[ap->a1].X) -= e3;

		e3 = e2 * (xym * delta12.X + xzp * delta12.Y + yzm * delta12.Z);
		(ag->gradients[ap->a0].Y) += e3;
		(ag->gradients[ap->a1].Y) -= e3;

		e3 = e2 * (xzm * delta12.X + yzm * delta12.Y + xyp * delta12.Z);
		(ag->gradients[ap->a0].Z) += e3;
		(ag->gradients[ap->a1].Z) -= e3;
// 12 bond
		e2 = e1 / ls12;

		yzp = delta12sq.Y + delta12sq.Z;
		yzm = -delta12.Y * delta12.Z;
		xzp = delta12sq.X + delta12sq.Z;
		xzm = -delta12.X * delta12.Z;
		xyp = delta12sq.X + delta12sq.Y;
		xym = -delta12.X * delta12.Y;

		e3 = e2 * (yzp * delta10.X + xym * delta10.Y + xzm * delta10.Z);
		(ag->gradients[ap->a2].X) += e3;
		(ag->gradients[ap->a1].X) -= e3;

		e3 = e2 * (xym * delta10.X + xzp * delta10.Y + yzm * delta10.Z);
		(ag->gradients[ap->a2].Y) += e3;
		(ag->gradients[ap->a1].Y) -= e3;

		e3 = e2 * (xzm * delta10.X + yzm * delta10.Y + xyp * delta10.Z);
		(ag->gradients[ap->a2].Z) += e3;
		(ag->gradients[ap->a1].Z) -= e3;
	}
}

void ieng(struct mol_atom_group *ag, double *en)
{
	static const double PI2 = 2 * M_PI;
	static const double DEGRA = M_PI / 180.0;

	double ds02, ds13, d12;
	double xco, ysi, impan, imp0, dimp, e1, e2;

	struct mol_improper *ip;

	struct mol_vector3 delta01, delta12, delta23, delta02, delta13;
	struct mol_vector3 v02, v13, v03;
	struct mol_vector3 x, y, d, d0, d1;


	for (size_t i = 0; i < ag->active_impropers->size; i++) {
		ip = ag->active_impropers->members[i];

		MOL_VEC_SUB(delta01, ag->coords[ip->a1], ag->coords[ip->a0]);
		MOL_VEC_SUB(delta12, ag->coords[ip->a2], ag->coords[ip->a1]);
		MOL_VEC_SUB(delta23, ag->coords[ip->a3], ag->coords[ip->a2]);
		MOL_VEC_SUB(delta02, ag->coords[ip->a2], ag->coords[ip->a0]);
		MOL_VEC_SUB(delta13, ag->coords[ip->a3], ag->coords[ip->a1]);

		MOL_VEC_CROSS_PROD(v02, delta01, delta12);
		MOL_VEC_CROSS_PROD(v13, delta12, delta23);
		MOL_VEC_CROSS_PROD(v03, v02, v13);
// lengths
		ds02 = MOL_VEC_SQ_NORM(v02);
		ds13 = MOL_VEC_SQ_NORM(v13);
		d12 = sqrt(MOL_VEC_SQ_NORM(delta12));

		xco = MOL_VEC_DOT_PROD(v02, v13);
		ysi = (MOL_VEC_DOT_PROD(delta12, v03)) / d12;
		impan = atan2(ysi, xco);
		imp0 = DEGRA * (ip->psi0);
		dimp = impan - imp0;
		while (dimp > M_PI)
			dimp -= PI2;
		while (dimp < -M_PI)
			dimp += PI2;
		e1 = (ip->k) * dimp;
		(*en) += e1 * dimp;

		e1 *= 2.0 / d12;
		e2 = e1 / ds13;
		e1 /= (-ds02);

		MOL_VEC_CROSS_PROD(x, v02, delta12);
		MOL_VEC_MULT_SCALAR(x, x, e1);

		MOL_VEC_CROSS_PROD(y, v13, delta12);
		MOL_VEC_MULT_SCALAR(y, y, e2);

		MOL_VEC_CROSS_PROD(d, x, delta12);
		MOL_VEC_ADD(ag->gradients[ip->a0], ag->gradients[ip->a0], d);

		MOL_VEC_CROSS_PROD(d0, delta02, x);
		MOL_VEC_CROSS_PROD(d1, y, delta23);
		MOL_VEC_ADD(d, d0, d1);
		MOL_VEC_ADD(ag->gradients[ip->a1], ag->gradients[ip->a1], d);

		MOL_VEC_CROSS_PROD(d0, x, delta01);
		MOL_VEC_CROSS_PROD(d1, delta13, y);
		MOL_VEC_ADD(d, d0, d1);
		MOL_VEC_ADD(ag->gradients[ip->a2], ag->gradients[ip->a2], d);

		MOL_VEC_CROSS_PROD(d, y, delta12);
		MOL_VEC_ADD(ag->gradients[ip->a3], ag->gradients[ip->a3], d);
	}
}

void teng(struct mol_atom_group *ag, double *en)
{
	static const double DEGRA = M_PI / 180.0;

	double ds02, ds13, d12;
	double xco, ysi, toran, tor0, dtor, e1, e2;
	struct mol_dihedral *tp;

	struct mol_vector3 delta01, delta12, delta23, delta02, delta13;
	struct mol_vector3 v02, v13, v03;
	struct mol_vector3 x, y, d, d0, d1;

	for (size_t i = 0; i < ag->active_dihedrals->size; i++) {
		tp = ag->active_dihedrals->members[i];

		MOL_VEC_SUB(delta01, ag->coords[tp->a1], ag->coords[tp->a0]);
		MOL_VEC_SUB(delta12, ag->coords[tp->a2], ag->coords[tp->a1]);
		MOL_VEC_SUB(delta23, ag->coords[tp->a3], ag->coords[tp->a2]);
		MOL_VEC_SUB(delta02, ag->coords[tp->a2], ag->coords[tp->a0]);
		MOL_VEC_SUB(delta13, ag->coords[tp->a3], ag->coords[tp->a1]);

		MOL_VEC_CROSS_PROD(v02, delta01, delta12);
		MOL_VEC_CROSS_PROD(v13, delta12, delta23);
		MOL_VEC_CROSS_PROD(v03, v02, v13);
// lengths
		ds02 = MOL_VEC_SQ_NORM(v02);
		ds13 = MOL_VEC_SQ_NORM(v13);
		d12 = sqrt(MOL_VEC_SQ_NORM(delta12));

		xco = MOL_VEC_DOT_PROD(v02, v13);
		ysi = (MOL_VEC_DOT_PROD(delta12, v03)) / d12;

		toran = atan2(ysi, xco);
		tor0 = DEGRA * (tp->d);
		dtor = tp->n * toran - tor0;

		e1 = tp->k;
		(*en) += e1 * (1.0 + cos(dtor));

		e1 *= (-((int) tp->n) * sin(dtor) / d12);
		e2 = e1 / ds13;
		e1 /= (-ds02);

		MOL_VEC_CROSS_PROD(x, v02, delta12);
		MOL_VEC_MULT_SCALAR(x, x, e1);

		MOL_VEC_CROSS_PROD(y, v13, delta12);
		MOL_VEC_MULT_SCALAR(y, y, e2);

		MOL_VEC_CROSS_PROD(d, x, delta12);
		MOL_VEC_ADD(ag->gradients[tp->a0], ag->gradients[tp->a0], d);

		MOL_VEC_CROSS_PROD(d0, delta02, x);
		MOL_VEC_CROSS_PROD(d1, y, delta23);
		MOL_VEC_ADD(d, d0, d1);
		MOL_VEC_ADD(ag->gradients[tp->a1], ag->gradients[tp->a1], d);

		MOL_VEC_CROSS_PROD(d0, x, delta01);
		MOL_VEC_CROSS_PROD(d1, delta13, y);
		MOL_VEC_ADD(d, d0, d1);
		MOL_VEC_ADD(ag->gradients[tp->a2], ag->gradients[tp->a2], d);

		MOL_VEC_CROSS_PROD(d, y, delta12);
		MOL_VEC_ADD(ag->gradients[tp->a3], ag->gradients[tp->a3], d);
	}
}
