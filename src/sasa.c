#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _USE_MATH_DEFINES
#include "sasa.h"

#include "vector.h"

#include <math.h>
#include <stdio.h>

static int accs_comp(const void *s1, const void *s2)
{
	const float f1 = *(const float *)s1;
	const float f2 = *(const float *)s2;
	if (f1 < f2)
		return -1;
	if (f1 == f2)
		return 0;
	return 1;
}

void mol_mark_all_sa(struct mol_atom_group *ag)
{
	if (ag->surface == NULL) {
		ag->surface = malloc(ag->natoms * sizeof(bool));
	}

	for (size_t i = 0; i < ag->natoms; ++i) {
		ag->surface[i] = true;
	}
}

void compute_surface(struct mol_atom_group *ag, double msur_k)
{
	if (ag->surface == NULL) {
		ag->surface = malloc(ag->natoms * sizeof(bool));
	}

	double r_solv = 1.4 * msur_k;

	double sthresh = 0.0;
	bool cont_acc = true;
	baccs(ag, r_solv, cont_acc, sthresh);
}

void baccs(struct mol_atom_group *ag, double r_solv, bool cont_acc,
	double sthresh)
{
	double *as = calloc(ag->natoms, sizeof(double));

	accs(as, ag, r_solv, cont_acc);

	for (size_t i = 0; i < ag->natoms; ++i) {
		ag->surface[i] = as[i] > sthresh;
	}
	free(as);
}

void accs(double *as, const struct mol_atom_group *ag, float r_solv,
	bool cont_acc)
{
	struct mol_vector3 min, max;

	const int NAC = 5000;	/* max number of atoms in a cube */

/* radii in preset mode */

/* integration increment */
  const float P = 0.01;

  float rmax = 0;

  float pi = M_PI;
  float pix2 = 2.0 * M_PI;
  double ri;
  float xi, yi, zi;

/* eliminate atoms with zero radii */
  int n_at = 0;
  int *restat = malloc(ag->natoms * sizeof(int));
  memset(as, 0, ag->natoms * sizeof(double));
  for (size_t i = 0; i < ag->natoms; i++) {
    double radius = ag->vdw_radius[i];
    if (radius > 0.0)
      restat[n_at++] = i;
  }

/* initialize boundary constants */
  min = ag->coords[restat[0]];
  max = min;

/* allocate general atom related arrays */
  struct mol_vector3 *coords = malloc(n_at * sizeof(struct mol_vector3));
  float *r = malloc(n_at * sizeof(float));
  float *r2 = malloc(n_at * sizeof(float));

/* allocate arrays for neighbouring atoms */
  float *dx = malloc(n_at * sizeof(float));
  float *dy = malloc(n_at * sizeof(float));
  float *d = malloc(n_at * sizeof(float));
  float *dsq = malloc(n_at * sizeof(float));
  float *arcif = malloc(2 * 2 * n_at * sizeof(float));
  int *inov = malloc(n_at * sizeof(int));

/* calculate sizes and dimensions*/
  for (int i = 0; i < n_at; i++) {
    double radius = ag->vdw_radius[restat[i]];

    radius = radius + r_solv;
    r[i] = radius;
    r2[i] = radius * radius;

    if (radius > rmax)
      rmax = radius;

    coords[i] = ag->coords[restat[i]];

    if (min.X > coords[i].X)
      min.X = coords[i].X;
    if (max.X < coords[i].X)
      max.X = coords[i].X;
    if (min.Y > coords[i].Y)
            min.Y = coords[i].Y;
    if (max.Y < coords[i].Y)
            max.Y = coords[i].Y;
    if (min.Z > coords[i].Z)
            min.Z = coords[i].Z;
    if (max.Z < coords[i].Z)
            max.Z = coords[i].Z;
  }
  float dmax = rmax * 2.0;

  int idim = (max.X - min.X) / dmax + 1;
  idim = idim < 3 ? 3 : idim;

  int jidim = (max.Y - min.Y) / dmax + 1;
  jidim = jidim < 3 ? 3 : jidim;
  jidim *= idim;

  int kjidim = (max.Z - min.Z) / dmax + 1;
  kjidim = kjidim < 3 ? 3 : kjidim;
  kjidim *= jidim;	/* total number of cubes */

/* map atoms to adjacent cubes */
/* allocate cubical arrays */

  int *itab = malloc(kjidim * sizeof(int));	/* number of atoms in each cube */
  for (int i = 0; i < kjidim; i++)
    itab[i] = 0;

  int *natm = malloc(NAC * kjidim * sizeof(int));	/* atom index in each cube */

  int *cube = malloc(n_at * sizeof(int));	/* cube number for each atom */

  int j, k, l, m, n, kji;
  struct mol_vector3i cube_coord;

  for (l = 0; l < n_at; l++) {
	  MOL_VEC_SUB(cube_coord, coords[l], min);
	  MOL_VEC_DIV_SCALAR(cube_coord, cube_coord, dmax);
    kji = cube_coord.Z * jidim + cube_coord.Y * idim + cube_coord.X;	/* cube number */
    n = itab[kji] + 1;
    if (n > NAC) {
	    fprintf(stderr,
		    "number of atoms in a cube %d is above the maximum  NAC= %d\n",
		    n, NAC);
      exit(EXIT_FAILURE);
    }
    itab[kji] = n;
    natm[kji * NAC + n - 1] = l;
    cube[l] = kji;
  }

  int ir, io, in, mkji, nm, nzp, karc;
  float area, xr, yr, zr, rr, rrx2, rr2, b, zres, zgrid;
  float rsec2r, rsecr, rsec2n, rsecn;
  float calpha, alpha, beta, ti, tf, arcsum, parea, t, tt;

/* main loop over atoms */
  zi = 1.0 / P + 0.5;
  nzp = zi;		/* number of z planes */

  for (ir = 0; ir < n_at; ir++) {
    kji = cube[ir];
    io = 0;		/* number of neighbouring atoms */
    area = 0.0;
    xr = coords[ir].X;
    yr = coords[ir].Y;
    zr = coords[ir].Z;
    rr = r[ir];
    rrx2 = rr * 2;
    rr2 = r2[ir];
/* loops over neighbouring cubes */
    for (k = -1; k < 2; k++) {
      for (j = -1; j < 2; j++) {
        for (int i = -1; i < 2; i++) {
          mkji = kji + k * jidim + j * idim + i;
          if (mkji < 0)
            continue;
          if (mkji >= kjidim)
            goto esc_cubes;
          nm = itab[mkji];
          if (nm < 1)
            continue;
          for (m = 0; m < nm; m++) {
            in = natm[mkji * NAC + m];
            if (in != ir) {
              xi = xr - coords[in].X;
              yi = yr - coords[in].Y;
              dx[io] = xi;
              dy[io] = yi;
              ri = xi * xi + yi * yi;
              dsq[io] = ri;
              d[io] = sqrtf(ri);
              inov[io] = in;
              io++;
            }
          }
        }
      }
    }

  esc_cubes:
    if (io != 0) {
      zres = rrx2 / nzp;	/* separation between planes */
      zgrid = coords[ir].Z - rr - zres / 2.0;	/* z level */

      for (int i = 0; i < nzp; i++) {
        zgrid += zres;
/* radius of the circle intersection with a z-plane */
        zi = zgrid - zr;
        rsec2r = rr2 - zi * zi;
        rsecr = sqrtf(rsec2r);

        karc = 0;
        for (j = 0; j < io; j++) {
          in = inov[j];
/* radius of the circle for a neighbour */
          zi = zgrid - coords[in].Z;
          rsec2n = r2[in] - zi * zi;
          if (rsec2n <= 0.0)
            continue;
          rsecn = sqrtf(rsec2n);
/* are they close? */
          if (d[j] >= rsecr + rsecn)
            continue;
/* do they intersect? */
          b = rsecr - rsecn;
          if (b <= 0.0) {
            if (d[j] <= -b)
              goto next_plane;
          } else {
            if (d[j] <= b)
              continue;
          }
          calpha =
            (dsq[j] + rsec2r -
              rsec2n) / (2.0 * d[j] * rsecr);
          if (calpha >= 1.0)
            continue;
/* yes, they do */
          alpha = acosf(calpha);
          beta = atan2f(dy[j], dx[j]) + pi;
          ti = beta - alpha;
          tf = beta + alpha;
          if (ti < 0.0)
            ti += pix2;
          if (tf > pix2)
            tf -= pix2;
          arcif[karc] = ti;
          if (tf < ti) {
            arcif[karc + 1] = pix2;
            karc += 2;
            arcif[karc] = 0.0;
          }
          arcif[karc + 1] = tf;
          karc += 2;
        }
/* find the atom accessible surface increment in z-plane */

        karc /= 2;
        if (karc == 0)
          arcsum = pix2;
        else {
          qsort(arcif, karc, 2 * sizeof(arcif[0]),
                (void *) accs_comp);
          arcsum = arcif[0];
          t = arcif[1];
          if (karc > 1) {
            for (k = 2; k < karc * 2;
                 k += 2) {
              if (t < arcif[k])
                arcsum +=
                  (arcif[k] -
                    t);
              tt = arcif[k + 1];
              if (tt > t)
                t = tt;
            }
          }
          arcsum += (pix2 - t);
        }
        parea = arcsum * zres;
        area += parea;
      next_plane:			;
      }
    } else {
      area = pix2 * rrx2;
    }

    ri = rr - r_solv;
    if (cont_acc)
      b = area * ri * ri / rr;
    else
      b = area * rr;
    as[restat[ir]] = b;
  }
/* free all */
  free(r);
  free(r2);
  free(dx);
  free(dy);
  free(d);
  free(dsq);
  free(arcif);
  free(inov);
  free(itab);
  free(natm);
  free(cube);
  free(restat);
  free(coords);
}
