#ifndef _MOL_LISTS_H_
#define _MOL_LISTS_H_

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include "utils.h"
#include "matrix.h"

/* If we do mol_list_destroy(&x), then sometimes gcc emits
 * "the comparison will always evaluate as ‘true’" warning.
 * It would be troublesome to remake this macro into a function, since
 * it will have to deal with all kinds of lists (which is doable,
 * but requires some pointer-casting voodoo).
 * So, we just selectively disable the warning for one line here.
*/
#ifndef mol_list_destroy
#define mol_list_destroy(S) do {		\
_Pragma("GCC diagnostic push")                  \
_Pragma("GCC diagnostic ignored \"-Waddress\"")         \
		if ((S) != NULL) {              \
_Pragma("GCC diagnostic pop")                   \
			free_if_not_null((S)->members); \
		}	                        \
	} while(0);
#endif

#ifndef destroy_mol_list
#define destroy_mol_list(S) _Pragma ("GCC warning \"'destroy_mol_list' is deprecated, use 'mol_list_destroy'\"") mol_list_destroy(S)
#endif

#ifndef mol_list_free
#define mol_list_free(S) do {		\
		mol_list_destroy(S); free_if_not_null(S);	\
	} while(0);
#endif

#ifndef free_mol_list
#define free_mol_list(S) _Pragma ("GCC warning \"'free_mol_list' is deprecated, use 'mol_list_free'\"")  mol_list_free(S)
#endif

#ifndef MOL_STR_LIST_CREATE
#define MOL_STR_LIST_CREATE(...) { .size = sizeof((char *[]){__VA_ARGS__})/sizeof(char *), \
				   .members = ((char *[]){__VA_ARGS__})}
#endif

#ifndef mol_list_init
#define mol_list_init(_list, _size) do {                                    \
		(_list).size = (_size);                                     \
		(_list).members = calloc((_size), sizeof(*(_list).members)); \
	} while (0);
#endif

struct mol_list {
	size_t size;
	void *members;
};

struct mol_index_list {
	size_t size;
	size_t *members;
};

struct mol_bool_list {
	size_t size;
	bool *members;
};

struct mol_int_fast8_t_list {
	size_t size;
	int_fast8_t *members;
};

struct mol_matrix3_list {
	size_t size;
	struct mol_matrix3 *members;
};

struct mol_matrix3f_list {
	size_t size;
	struct mol_matrix3f *members;
};
struct mol_matrix3l_list {
	size_t size;
	struct mol_matrix3l *members;
};

struct mol_quaternion_list {
	size_t size;
	struct mol_quaternion *members;
};
struct mol_quaternionf_list {
	size_t size;
	struct mol_quaternionf *members;
};
struct mol_quaternionl_list {
	size_t size;
	struct mol_quaternionl *members;
};

//a mol symmetry list goes along with an atom group
//if the size is N , the length of members should be N*natoms
struct mol_symmetry_list {
	size_t size;
	struct mol_atom_group *ag;
	size_t *members;
};

/**
 * Assumes index lists a and b are already sorted. Will either malloc or realloc
 * the members array in dst to be big enough to fit the union.
 */
void mol_index_list_union(struct mol_index_list *dst,
			const struct mol_index_list *a, const struct mol_index_list *b,
			bool realloc);

#endif /* _MOL_LISTS_H_ */
