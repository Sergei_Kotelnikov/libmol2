#ifndef _MOL2_PDBQT_H_
#define _MOL2_PDBQT_H_

#define MOL_PDBQT_ATOM_TYPE_METADATA_KEY "autodock_type"

#include "atom_group.h"

enum mol_autodock_atom_type {
	AUTODOCK_ATOM_C = 0,
	AUTODOCK_ATOM_A = 1,
	AUTODOCK_ATOM_N = 2,
	AUTODOCK_ATOM_O = 3,
	AUTODOCK_ATOM_P = 4,
	AUTODOCK_ATOM_S = 5,
	AUTODOCK_ATOM_H = 6,
	AUTODOCK_ATOM_F = 7,
	AUTODOCK_ATOM_I = 8,
	AUTODOCK_ATOM_NA = 9,
	AUTODOCK_ATOM_OA = 10,
	AUTODOCK_ATOM_SA = 11,
	AUTODOCK_ATOM_HD = 12,
	AUTODOCK_ATOM_Mg = 13,
	AUTODOCK_ATOM_Mn = 14,
	AUTODOCK_ATOM_Zn = 15,
	AUTODOCK_ATOM_Ca = 16,
	AUTODOCK_ATOM_Fe = 17,
	AUTODOCK_ATOM_Cl = 18,
	AUTODOCK_ATOM_Br = 19,
	AUTODOCK_ATOM_X = 20,
};


/**
 * Read protein structure from Autodock PDBQT file. Only atom information is read, the tree structure is ignored for now.
 * The Autodock atom types are stored as metadata with \c MOL_PDBQT_ATOM_TYPE_METADATA_KEY key.
 * \param pdbqt_filename The name of file to read.
 * \return New \ref mol_atom_group, or \c NULL on error.
 */
struct mol_atom_group *mol_pdbqt_read(const char *pdbqt_filename);

#endif // _MOL2_PDBQT_H_
