#ifndef _MOL_UTILS_H_
#define _MOL_UTILS_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/**
 * \file utils.h
 */

#ifndef free_if_not_null
#define free_if_not_null(S) do {		\
		if ((S) != NULL) { free((S)); }	\
	} while(0);
#endif

#ifdef _WIN32
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif
#else
#ifndef MIN
#define MIN(a, b) ({				\
		__typeof__ (a) _a = (a);	\
		__typeof__ (b) _b = (b);	\
		_a < _b ? _a : _b; })
#endif

#ifndef MAX
#define MAX(a, b)               \
	({__typeof__ (a) _a = (a);		\
	 __typeof__ (b) _b = (b);		\
	 _a > _b ? _a : _b; })
#endif
#endif //_WIN32

/**
 * Comparator for pointers to \c size_t values.
 *
 * Used to sort arrays of \c size_t values, as \c qsort requires a function with this signature.
 * \param a Pointer to the first \c size_t element.
 * \param b Pointer to the second \c size_t element.
 * \return -1 if \c *a<*b, 0 if \c *a==*b, and 1 if \c *a>*b.
 */
int size_t_cmp(const void *a, const void *b);

/**
 * Check if every character in \p line is a whitespace character.
 * \param line Pointer to a string to check.
 * \return \c true if line is only whitespace characters, \c false otherwise.
 */
bool is_whitespace_line(const char *line);

/**
 * Strip off whitespace characters from the right end of the string.
 *
 * This function finds the first non-whitespace character starting from the end of the string, and
 * writes a null terminator after the character to truncate the string.
 * Does not reallocate any memory.
 * \param string Pointer to a string to shorten.
 * \return Pointer to the same string, but possibly modified as described above.
 */
char *rstrip(char *string);

#ifdef _WIN32
#define getline getline2
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
ssize_t getline2 (char **lineptr, size_t *n, FILE *stream);
char *strndup(const char *s, size_t n);
// Make GCC-specific helper keywords do nothing
#define _Pragma(x)
#define restrict __restrict
#define __attribute__(A)
#endif

#endif /* _UTILS_H_ */
