#ifndef _MOL_MASK_H_
#define _MOL_MASK_H_

#include "atom_group.h"

void mol_atom_group_mask(struct mol_atom_group *target,
                         const struct mol_atom_group *mask,
                         double maskr);

#endif
