#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

#include "pdbqt.h"
#include "pdb.h"
#include <stdio.h>
#include <string.h>
#include <memory.h>

static enum mol_autodock_atom_type _autodock_type_from_str(const char *s)
{
	// We can use gperf to speed things up here if it ever becomes a bottleneck
	static const int ntypes = 20;
	static const char *atom_names[] = {
		"C ", "A ", "N ", "O ", "P ", "S ", "H ", "F ", "I ", "NA",
		"OA", "SA", "HD", "Mg", "Mn", "Zn", "Ca", "Fe", "Cl", "Br"};
	for (int i = 0; i < ntypes; i++) {
		if (!strncmp(atom_names[i], s, 2))
			return i;
	}
	return AUTODOCK_ATOM_X;
}

struct mol_atom_group *mol_pdbqt_read(const char *pdbqt_filename)
{
	// Official docs: http://autodock.scripps.edu/faqs-help/faq/what-is-the-format-of-a-pdbqt-file
	// They say atom charge starts at 70
	// But <https://www.mdanalysis.org/docs/documentation_pages/coordinates/PDBQT.html> says it starts at 66
	// I'm being conservative here, so let's parse only first 66 characters as normal PDB
	static const size_t buffer_size = 91;
	static const size_t pdb_size = 66;

	FILE *fp = fopen(pdbqt_filename, "r");
	if (fp == NULL)
		return NULL;

	char *buffer = calloc(buffer_size, sizeof(char));
	size_t index = 0;

	while (fgets(buffer, buffer_size - 1, fp) != NULL) {
		if (mol_pdb_line_is_atom_or_hetatm(buffer)) {
			index++;
		}
	}

	if (index == 0) {
		free(buffer);
		fclose(fp);
		return NULL;
	}

	struct mol_atom_group *ag = mol_atom_group_create();
	ag->natoms = index;

	mol_atom_group_init_atomic_field(ag, record);
	mol_atom_group_init_atomic_field(ag, atom_name);
	mol_atom_group_init_atomic_field(ag, alternate_location);
	mol_atom_group_init_atomic_field(ag, residue_name);
	mol_atom_group_init_atomic_field(ag, residue_id);
	mol_atom_group_init_atomic_field(ag, coords);
	mol_atom_group_init_atomic_field(ag, occupancy);
	mol_atom_group_init_atomic_field(ag, B);
	mol_atom_group_init_atomic_field(ag, charge);
	mol_atom_group_init_atomic_field(ag, segment_id); // Ignored, all NULLs
	mol_atom_group_init_atomic_field(ag, element); // Ignored, all NULLs
	mol_atom_group_init_atomic_field(ag, formal_charge); // Ignored, all NULLs
	enum mol_autodock_atom_type *autodock_type = calloc(ag->natoms, sizeof(enum mol_autodock_atom_type));
	mol_atom_group_add_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY, autodock_type);

	rewind(fp);
	index = 0;
	while (fgets(buffer, buffer_size - 1, fp) != NULL) {
		if (mol_pdb_line_is_atom_or_hetatm(buffer)) {
			mol_pdb_parse_atom_line(ag, index, buffer, pdb_size);
			ag->charge[index] = atof(buffer + 66);
			autodock_type[index] = _autodock_type_from_str(buffer + 77);
			index++;
		}
	}

	mol_atom_group_create_residue_hash(ag);
	mol_atom_group_create_residue_list(ag);

	free(buffer);
	fclose(fp);
	mol_atom_group_create_residue_list(ag);
	return ag;
}
