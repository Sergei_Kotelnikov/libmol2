#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include "minimize.h"
#include "lbfgs.h"
#include "rigid_body.h"

static void bracket(double *orig, double *dir, double step,
	     int ndim, void *prms,
	     lbfgs_evaluate_t egfun,
	     double *fb, double *la, double *lb, double *lc)
{
	int mindim = ndim;
	double *new = malloc(mindim * sizeof(double));

	double fa, fc, fnew, lnew;
	double G = 1.618034;
	double TooSmall = 1E-10;

	int i, j;

	*la = 0;
	*lb = step;
	fa = egfun(prms, orig, NULL, ndim, step);
	for (i = 0; i < mindim; i++)
		new[i] = orig[i] + step * dir[i];
	*fb = egfun(prms, new, NULL, ndim, step);
	if (fa < *fb) {
		double temp;
		temp = fa;
		fa = *fb;
		*fb = temp;
		temp = *la;
		*la = *lb;
		*lb = temp;
	}

	*lc = *lb + G * (*lb - *la);

	for (i = 0; i < mindim; i++)
		new[i] = orig[i] + *lc * dir[i];

	fc = egfun(prms, new, NULL, ndim, step);

	double parabp1, parabp2;

	while ((fc <= *fb)) {
		parabp1 = (*lb - *la) * (*fb - fc);
		parabp2 = (*lb - *lc) * (*fb - fa);
		if (fabs(parabp1 - parabp2) > TooSmall) {
			lnew = *lb + ((*lb - *lc) * parabp2 -
					(*lb - *la) * parabp1) / (2 * (parabp1 - parabp2));
			if (((*lb - lnew) * (lnew - *lc)) > 0) {
				for (j = 0; j < mindim; j++)
					new[j] = orig[j] + lnew * (dir[j]);
				fnew = egfun(prms, new, NULL, ndim, 0);
				if (fnew < fc) {
					*la = *lb;
					*fb = fnew;
					*lb = lnew;
					free(new);
					return;
				} else if (fnew > *fb) {
					*lc = lnew;
					free(new);
					return;
				}
				lnew = *lc + G * (*lc - *lb);
				for (j = 0; j < mindim; j++)
					new[j] = orig[j] + lnew * (dir[j]);
				fnew = egfun(prms, new, NULL, ndim, 0);
			} else {
				for (j = 0; j < mindim; j++)
					new[j] = orig[j] + lnew * (dir[j]);
				fnew = egfun(prms, new, NULL, ndim, 0);
				if (fnew < fc) {
					*lb = *lc;
					*lc = lnew;
					lnew = *lc + G * (*lc - *lb);
					*fb = fc;
					fc = fnew;
					for (j = 0; j < mindim; j++)
						new[j] =
							orig[j] + lnew * (dir[j]);
					fnew = egfun(prms, new, NULL, ndim, 0);
				}
			}
		} else {
			lnew = *lc + G * (*lc - *lb);
			for (j = 0; j < mindim; j++)
				new[j] = orig[j] + lnew * (dir[j]);
			fnew = egfun(prms, new, NULL, ndim, 0);
		}
		*la = *lb;
		*lb = *lc;
		*lc = lnew;
		fa = *fb;
		*fb = fc;
		fc = fnew;
	}

	free(new);
}

static void brent(double *orig, double *dir,
	   double fb, double la, double lb, double lc,
	   int ndim, void *prms,
	   lbfgs_evaluate_t egfun,
	   double tol, unsigned int maxtimes, double *min, double *f_min)
{
	double lbound1, lbound2;
	unsigned int numIt = 0;
	int doGolden, i;
	int numAct = ndim;
	if (la < lc) {
		lbound1 = la;
		lbound2 = lc;
	} else {
		lbound1 = lc;
		lbound2 = la;
	}
	double lcurmin = lb;
	double lprevmin = lb;
	double lprevmin2 = lb;
	double fcurmin = fb;
	double fprevmin = fb;
	double fprevmin2 = fb;
	double dMoved = 0;
	double dMoved2 = 0;
	double s = 1E-10;
	double lim;
	double par1, par2, lpartial, lnew, fnew;
	double GOLD = 0.381966;
	double *new = malloc(numAct * sizeof(double));
	double denom;
	double lmidpoint = 0.5 * (lbound1 + lbound2);
	double tempdMoved;

	lim = tol * fabs(lcurmin) + s;
	while (fabs(lcurmin - lmidpoint) > (2 * lim - 0.5 * (lbound2 - lbound1))
	       && numIt < maxtimes) {
		lmidpoint = 0.5 * (lbound1 + lbound2);
		lim = tol * fabs(lcurmin) + s;
		doGolden = 1;
		if (fabs(dMoved2) > lim) {
			par1 = (lcurmin - lprevmin) * (fcurmin - fprevmin2);
			par2 = (lcurmin - lprevmin2) * (fcurmin - fprevmin);
			lpartial =
			    ((lcurmin - lprevmin) * par1 -
			     (lcurmin - lprevmin2) * par2);
			denom = 2 * (par1 - par2);

			if (denom > 0)
				lpartial *= -1;
			denom = fabs(denom);
			tempdMoved = dMoved2;
			if (fabs(lpartial) < fabs(0.5 * denom * tempdMoved)
			    && lpartial > denom * (lbound1 - lcurmin)
			    && lpartial < denom * (lbound2 - lcurmin)) {
				dMoved = lpartial / denom;
				lnew = lcurmin + dMoved;
				if (lnew - lbound1 < 2 * lim
				    || lbound2 - lnew < 2 * lim) {
					doGolden = 0;
					if (lmidpoint - lcurmin != 0)
						dMoved =
						    lim * (lmidpoint -
							   lcurmin) /
						    fabs(lmidpoint - lcurmin);
					else
						dMoved = lim;
				}
			}
		}

		if (doGolden != 0) {
			if (lcurmin >= lmidpoint)
				dMoved2 = lbound1 - lcurmin;
			else
				dMoved2 = lbound2 - lcurmin;
			dMoved = GOLD * dMoved2;
		}

		if (fabs(dMoved) > lim)
			lnew = lcurmin + dMoved;
		else {
			if (dMoved != 0)
				lnew = lcurmin + lim * (dMoved / fabs(dMoved));
			else
				lnew = lcurmin;
		}
		for (i = 0; i < numAct; i++)
			new[i] = orig[i] + lnew * dir[i];
		fnew = egfun(prms, new, NULL, ndim, 0);
		if (fnew < fcurmin) {
			if (lnew >= lcurmin)
				lbound1 = lcurmin;
			else
				lbound2 = lcurmin;
			lprevmin2 = lprevmin;
			fprevmin2 = fprevmin;

			lprevmin = lcurmin;
			fprevmin = fcurmin;

			lcurmin = lnew;
			fcurmin = fnew;
		} else {
			if (lnew < lcurmin)
				lbound1 = lnew;
			else
				lbound2 = lnew;
			if (fnew < fprevmin || lprevmin == fcurmin) {
				lprevmin2 = lprevmin;
				fprevmin2 = fprevmin;
				lprevmin = lnew;
				fprevmin = fnew;
			} else if (fnew < fprevmin2 || lprevmin2 == lcurmin
				   || lprevmin2 == lprevmin) {
				lprevmin2 = lnew;
				fprevmin2 = fnew;
			}
		}
		dMoved2 = dMoved;
		dMoved = lcurmin - lprevmin;
		numIt++;
	}
	if (numIt == maxtimes)
		printf("MAXIMUM ITERATIONS REACHED. RETURNING CURRENT BEST\n");
	for (i = 0; i < numAct; i++)
		min[i] = orig[i] + lcurmin * dir[i];

	*f_min = fcurmin;
	free(new);
}

static void dirbrent(double *orig, double *dir,
	      double fb, double la, double lb, double lc,
	      int ndim, void *prms,
	      lbfgs_evaluate_t egfun,
	      double tol, int maxtimes, double *min, double *f_min, double *grad)
{
	double lbound1, lbound2;
	int numIt = 0;
	int doBisect, i, canUse1, canUse2;
	int numAct = ndim;

	if (la < lc) {
		lbound1 = la;
		lbound2 = lc;
	} else {
		lbound1 = lc;
		lbound2 = la;
	}

	double db = 0;

	for (i = 0; i < numAct; i++)
		db += grad[i] * dir[i];

	double lcurmin = lb;
	double lprevmin = lb;
	double lprevmin2 = lb;
	double dcurmin = db;
	double dprevmin = db;
	double dprevmin2 = db;
	double fcurmin = fb;
	double fprevmin = fb;
	double fprevmin2 = fb;
	double distMoved = 0;
	double distMoved2 = 0;
	double s = 1E-10;
	double lim;
	double sec1 = 0, sec2 = 0, lnew, fnew, dnew;
	double *new = malloc(numAct * sizeof(double));
	double lmidpoint;
	double tempdistMoved;

	for (numIt = 0; numIt < maxtimes; numIt++) {
		lmidpoint = 0.5 * (lbound1 + lbound2);
		lim = tol * fabs(lcurmin) + s;

		if (fabs(lcurmin - lmidpoint) <=
		    (2 * lim - 0.5 * (lbound2 - lbound1))) {
			for (i = 0; i < numAct; i++)
				min[i] = orig[i] + lcurmin * dir[i];
			*f_min = fcurmin;
			free(new);
			return;
		}

		doBisect = 1;
		if (fabs(distMoved2) > lim) {
			canUse1 = 0;
			canUse2 = 0;
			if (dprevmin != dcurmin) {
				sec1 =
				    (lprevmin - lcurmin) * dcurmin / (dcurmin -
								      dprevmin);
				canUse1 = 1;
			}

			if (dprevmin2 != dcurmin) {
				sec2 =
				    (lprevmin2 - lcurmin) * dcurmin / (dcurmin -
								       dprevmin2);
				canUse2 = 1;
			}

			tempdistMoved = distMoved2;
			distMoved2 = distMoved;

			if (canUse1 != 0) {
				if (((lbound1 - lcurmin - sec1) * (lcurmin +
								   sec1 -
								   lbound2) <=
				     0) || (dcurmin * sec1 > 0))
					canUse1 = 0;
			}

			if (canUse2 != 0) {
				if (((lbound1 - lcurmin - sec2) * (lcurmin +
								   sec2 -
								   lbound2) <=
				     0) || (dcurmin * sec2 > 0))
					canUse2 = 0;
			}

			if (canUse2 + canUse1 != 0) {
				if (canUse1 + canUse2 == 2) {
					if (fabs(sec1) < fabs(sec2))
						distMoved = sec1;
					else
						distMoved = sec2;
				} else if (canUse1 != 0)
					distMoved = sec1;
				else if (canUse2 != 0)
					distMoved = sec2;
				doBisect = 0;
				if (fabs(distMoved) <=
				    fabs(0.5 * tempdistMoved)) {
					lnew = lcurmin + distMoved;
					if (((lnew - lbound1) < 2 * lim)
					    || ((lbound2 - lnew) < 2 * lim)) {
						if (lcurmin < lmidpoint)
							distMoved = lim;
						else
							distMoved = -lim;
						doBisect = 0;
					}
				} else
					doBisect = 1;
			} else
				doBisect = 1;
		}
		if (doBisect != 0) {
			if (dcurmin > 0)
				distMoved2 = lbound1 - lcurmin;
			else
				distMoved2 = lbound2 - lcurmin;
			distMoved = 0.5 * distMoved2;
		}

		if (fabs(distMoved) >= lim) {
			lnew = lcurmin + distMoved;
			for (i = 0; i < numAct; i++)
				new[i] = orig[i] + lnew * dir[i];
			fnew = egfun(prms, new, grad, ndim, 0);
		} else {
			if (distMoved >= 0)
				lnew = lcurmin + lim;
			else
				lnew = lcurmin - lim;
			for (i = 0; i < numAct; i++)
				new[i] = orig[i] + lnew * dir[i];
			fnew = egfun(prms, new, grad, ndim, 0);

			if (fnew > fcurmin) {
				for (i = 0; i < numAct; i++)
					min[i] = orig[i] + lcurmin * dir[i];
				*f_min = fcurmin;
				free(new);
				return;
			}
		}
		dnew = 0;
		for (i = 0; i < numAct; i++)
			dnew += dir[i] * grad[i];
		if ((fnew <= fcurmin)) {
			if (lnew >= lcurmin)
				lbound1 = lcurmin;
			else
				lbound2 = lcurmin;

			lprevmin2 = lprevmin;
			fprevmin2 = fprevmin;
			dprevmin2 = dprevmin;

			lprevmin = lcurmin;
			fprevmin = fcurmin;
			dprevmin = dcurmin;

			lcurmin = lnew;
			fcurmin = fnew;
			dcurmin = dnew;
		} else {
			if (lnew < lcurmin)
				lbound1 = lnew;
			else
				lbound2 = lnew;
			if (fnew <= fprevmin || lprevmin == lcurmin) {
				lprevmin2 = lprevmin;
				fprevmin2 = fprevmin;
				dprevmin2 = dprevmin;
				lprevmin = lnew;
				fprevmin = fnew;
				dprevmin = dnew;
			} else if (fnew <= fprevmin2 || lprevmin2 == lcurmin
				   || lprevmin2 == lprevmin) {
				lprevmin2 = lnew;
				fprevmin2 = fnew;
				dprevmin2 = dnew;
			}
		}
	}
	if (numIt == maxtimes)
		printf("MAXIMUM ITERATIONS REACHED. RETURNING CURRENT BEST\n");
	for (i = 0; i < numAct; i++)
		min[i] = orig[i] + lcurmin * dir[i];
	*f_min = fcurmin;
	free(new);
}

static void powell(double *orig, double *directions, unsigned int maxIt, double tol,
	    int ndim, void *prms,
	    lbfgs_evaluate_t egfun,
	    double *min, double *f_min)
{
	int jmostdec = 0;  //direction of  the biggest  decrease withing a single directions set
	int numAct = ndim;
	unsigned int i;
	double mostdec = 0;
	double fbrac, fprev, fprev2 = 0, val;
	double la = 0, lb = 0, lc = 0;
	double test;
	double *pcur = malloc(numAct * sizeof(double));  //point of current minimum
	double *prev = malloc(numAct * sizeof(double));  //previous minimum
	double *newdir = malloc(numAct * sizeof(double));  // direction moved in one iteration

	for (int j = 0; j < numAct; j++) {
		prev[j] = orig[j];
		pcur[j] = orig[j];
	}
	val = egfun(prms, pcur, NULL, ndim, 0);

	for (i = 0; i < maxIt; i++)  //loop through once for each direction set(break out if close enough)
	{
		fprev = val;
		mostdec = 0;
		jmostdec = 0;
		for (int j = 0; j < numAct; j++)  //loop through once for each direction
		{
			for (int k = 0; k < numAct; k++)
				newdir[k] = directions[numAct * j + k];	//get direction
			fprev2 = val;
			bracket(pcur, newdir, 1, ndim, prms, egfun, &fbrac, &la,
				&lb, &lc);
			brent(pcur, newdir, fbrac, la, lb, lc, ndim, prms,
			      egfun, 1E-5, 100, pcur, &val);
			if (fabs(fprev2 - val) > mostdec)  //get direction of greatest decrease and greatest decrease
			{
				mostdec = fabs(fprev2 - val);
				jmostdec = j;
			}
		}

		if (2 * fabs(fprev - val) <= tol * (fabs(fprev) + fabs(val)))	//if you reach the desired tolerance end powell
		{
			for (int j = 0; j < numAct; j++)
				min[j] = pcur[j];

			*f_min = val;
			free(pcur);
			free(prev);
			free(newdir);
			return;
		}

		for (int j = 0; j < numAct; j++)
			newdir[j] = 2 * pcur[j] - prev[j];	//get new direction

		fprev2 = egfun(prms, newdir, NULL, ndim, 0);

		for (int j = 0; j < numAct; j++) {
			newdir[j] = pcur[j] - prev[j];
			prev[j] = pcur[j];
		}

		if (fprev2 < fprev) {
			test = 2.0*(fprev - 2.0*val + fprev2) *
				pow((fprev - val - mostdec), 2) -
				mostdec * pow((fprev - fprev2), 2);
			if (test < 0) {	//if it makes sence to the direction set
				bracket(pcur, newdir, 1, ndim, prms, egfun, &fbrac, &la, &lb, &lc);	//minimize in new direction
				brent(pcur, newdir,
					fbrac, la, lb, lc,
					ndim, prms, egfun,
					1E-5, 100, pcur, &val);
				for (int k = 0; k < numAct; k++) {
					directions[jmostdec * numAct + k] =
						directions[(numAct - 1) * (numAct) + k];
					directions[(numAct - 1) * (numAct) + k] = newdir[k];
				}
			}
		}
	}
	printf
	    ("warning maximum number of iterations reached. using current best\n");

	for (int j = 0; j < numAct; j++)
		min[j] = pcur[j];

	*f_min = val;
	free(pcur);
	free(prev);
	free(newdir);
}

static void dirMin(double *orig, unsigned int maxIt, double tol,
	    int ndim, void *prms,
	    lbfgs_evaluate_t egfun,
	    double *min, double *f_min)
{
	int numAct = ndim;
	int i, j;
	unsigned int it;

	double *dir = malloc(numAct * sizeof(double));
	double *interm = malloc(numAct * sizeof(double));
	double *temp = malloc(numAct * sizeof(double));
	double *curmin = malloc(numAct * sizeof(double));
	double *grad = malloc(numAct * sizeof(double));
	double *mvec = malloc(numAct * sizeof(double));

	for (i = 0; i < numAct; i++)
		curmin[i] = orig[i];

	double val, t1, t2, t3, la, lb, lc, fbrac;
	double s = 1E-5;
	double fprev;
	fprev = egfun(prms, orig, grad, ndim, 0);
	double dnorm = 0, maxnorm = 1.5;

	for (i = 0; i < numAct; i++) {
		interm[i] = -grad[i];
		temp[i] = interm[i];
		dir[i] = interm[i];
	}

	for (it = 0; it < maxIt; it++) {
		dnorm = 0;
		for (i = 0; i < numAct; i++)
			dnorm += dir[i] * dir[i];
		dnorm = sqrt(dnorm);
		if (dnorm > maxnorm)
			for (i = 0; i < numAct; i++)
				dir[i] *= maxnorm / dnorm;

		bracket(curmin, dir, 1,
			ndim, prms, egfun, &fbrac, &la, &lb, &lc);

		dirbrent(curmin, dir, fbrac, la, lb, lc,
			 ndim, prms, egfun,
			 1E-10, 100, mvec, &val, grad);

		if (2 * fabs(val - fprev) <=
		    tol * (fabs(fprev) + fabs(val) + s)) {
			for (j = 0; j < numAct; j++)
				min[j] = mvec[j];
			*f_min = val;
			free(dir);
			free(mvec);
			free(interm);
			free(temp);
			return;
		}

		for (j = 0; j < numAct; j++) {
			curmin[j] = mvec[j];
			dir[j] = grad[j];
		}
		fprev = val;
		t1 = 0;
		t2 = 0;
		for (j = 0; j < numAct; j++) {
			t1 += pow(interm[j], 2);
			t2 += (dir[j] + interm[j]) * dir[j];
		}

		if (t1 == 0) {
			for (j = 0; j < numAct; j++)
				min[j] = curmin[j];
			*f_min = val;
			free(dir);
			free(interm);
			free(temp);
			free(mvec);
			return;
		}

		t3 = t2 / t1;

		for (j = 0; j < numAct; j++) {
			interm[j] = -dir[j];
			temp[j] = interm[j] + t3 * temp[j];
			dir[j] = temp[j];
		}
	}
	printf("maximum num of iterations reached displaying current min\n");
	for (j = 0; j < numAct; j++)
		min[j] = curmin[j];
	*f_min = val;
	free(dir);
	free(interm);
	free(temp);
	free(mvec);
	return;
}

int lbfgs_new_debug_mode = 0;
/*
	the value of the lbfgs_new_debug_mode:
		0 : print nothing
		1 : At each step print the iteration number and the value of the function and gnorm and xnorm and the stepsize and the number of function evaluation
		2 : At each step print the gnorm and xnorm and print the g and x vector the stepsize and the number of function evaluation for the corresponding iteration
*/

static int progress(void *instance, const lbfgsfloatval_t *x,
	const lbfgsfloatval_t *g, const lbfgsfloatval_t fval,
	const lbfgsfloatval_t fnorm, const lbfgsfloatval_t gnorm,
	const lbfgsfloatval_t step, int n, int k, int ls)
{
	if (lbfgs_new_debug_mode > 1 && instance != NULL) {
		fprintf(stderr, "Instance: %p\n", instance);
	}

	if ((lbfgs_new_debug_mode > 0) && (k == 1))
	{
		fprintf(stderr, "*************************************************\n");
		fprintf(stderr, "number of variables = %d\n ", n);
	}
	if ((lbfgs_new_debug_mode > 0) && (k != 1))
	{
		fprintf(stderr, "Iteration %d:\n", k);
		fprintf(stderr, " f =  %.3f   xnorm = %.3f gnorm =  %.3f stepsize = %.3f nfun = %d \n",
			fval, fnorm, gnorm, step, ls);
		fprintf(stderr, "\n");
	}

	if (lbfgs_new_debug_mode == 2) {
		int i;
		fprintf(stderr, "vector x : ");
		for (i = 0; i < n; i++)
			fprintf(stderr, "%f ", x[i]);
		fprintf(stderr, "\n");
		fprintf(stderr, "vector g :");
		for (i = 0; i < n; i++)
			fprintf(stderr, "%f ", g[i]);
		fprintf(stderr, "\n");
	}

	return 0;
}


/* Minimizes atomgroup with specified parameter set
 minimization types; 0 = LBFGS, 1- Conjugate gradients, 2 -Powell 3 -New implementation of LBFGS
*/
void mol_minimize_ag(enum mol_min_method min_type, unsigned int maxIt, double tol,
		struct mol_atom_group *ag, void *minprms,
		lbfgs_evaluate_t egfun)
{
	int ndim = ag->active_atoms->size * 3;
	if (min_type == MOL_RIGID) {
		ndim = 6;
	}


	double *xyz = malloc(ndim * sizeof(double));
	double *minv = malloc(ndim * sizeof(double));

	struct mol_rigid_body rigidbody;
	double *origin;
	if (min_type == MOL_RIGID) {
		origin = calloc(3 * ag->active_atoms->size, sizeof(double));
		rigidbody.origin = origin;
		mol_atom_group_to_rigid_body(&rigidbody, ag);
	} else {
		mol_atom_group_get_actives(xyz, ag);
	}

	double f_min;

	if (min_type == MOL_CONJUGATE_GRADIENTS)
		dirMin(xyz, maxIt, tol, ndim, minprms, egfun, minv, &f_min);
	if (min_type == MOL_POWELL) {
		double *directions = malloc(ndim * ndim * sizeof(double));
		int i;
		for (i = 0; i < ndim * ndim; i++)
			directions[i] = 0;
		for (i = 0; i < ndim; i++)
			directions[i * ndim + i] = 1;
		powell(xyz, directions, maxIt, tol, ndim, minprms, egfun, minv,
		       &f_min);
		free(directions);
	}

	if (min_type == MOL_LBFGS) {
		mol_atom_group_get_actives(minv, ag);
		lbfgs_parameter_t param = {
			6, tol, 0, 1e-5,
			maxIt, LBFGS_LINESEARCH_MORETHUENTE, 40,
			1e-20, 1e20, 1e-4, 0.9, 0.9, 1.0e-16,
			0.0, 0, -1,
		};

		lbfgs(ndim, minv, &f_min, egfun, progress, minprms, &param);
	}

	if (min_type == MOL_RIGID) {
		lbfgs_parameter_t param={
			5, tol, 0, 1e-5,
			maxIt, LBFGS_LINESEARCH_MORETHUENTE, 40,
			1e-20, 1e20, 1e-4, 0.9, 0.9, 1.0e-16,
			0.0, 0, -1,
                };

		lbfgs(ndim,minv,&f_min,egfun,progress,minprms,&param);
	}

	if (min_type == MOL_RIGID) {
		mol_rigid_body_to_atom_group(minv, ag, &rigidbody);
		free(origin);
	} else {
		mol_atom_group_set_actives(ag, minv);
	}
	free(xyz);
	free(minv);
}
