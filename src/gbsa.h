#ifndef _MOL_GBSA_H_
#define _MOL_GBSA_H_

#include "atom_group.h"
#include "benergy.h"
#include "nbenergy.h"

struct acesetup {
	size_t ntypes;
	int nbsize;		//Size of nblist
	double efac;
	int *list0123;
	double *eself;
	double *rborn;
	double *swarr;
	double *dswarr;
	double *darr;
	//d(rb)/d(eself)
	double *dbrdes;

	struct mol_vector3 *vsf;
	struct mol_vector3 *vf;

	double *diarr;
	double *lwace, *rsolv, *vsolv, *s2ace, *uace, *wace, *hydr;
	int n0123;
};

//Initialize ace data types
void ace_ini(struct mol_atom_group *ag, struct acesetup *ac_s);
//Update ace lists once fixedlist was updated
void ace_fixedupdate(struct mol_atom_group *ag, struct agsetup *ags,
		struct acesetup *ac_s);
//Update nblst once nblist is updated
void ace_updatenblst(const struct agsetup *const restrict ags,
		struct acesetup *const restrict ac_s);
//Calculate ace energy and gradients
void aceeng(struct mol_atom_group* ag, double *en,
	struct acesetup* ac_s, struct agsetup* ags);
void aceeng_nonpolar(struct mol_atom_group* ag, double* en,
		struct acesetup* ac_s, struct agsetup* ags);
void aceeng_polar(struct mol_atom_group* ag, double* en,
		struct acesetup* ac_s, struct agsetup* ags);
//Free ace data
void destroy_acesetup(struct acesetup *ac_s);
void free_acesetup(struct acesetup *ac_s);
#endif
