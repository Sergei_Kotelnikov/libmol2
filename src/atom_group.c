#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "atom_group.h"
#include "utils.h"
#include "vector.h"
#include "transform.h"
#ifdef _WIN32
#define strdup _strdup
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

#define __CALLOC_IF_NN(ag, field, old_ag) do {	\
	if (old_ag->field)			\
		ag->field = calloc(old_ag->natoms, sizeof((old_ag->field[0]))); \
} while(0)

#define __REALLOC_IF_NN(ag, field) do {		\
		if (ag->field)			\
			ag->field = realloc(ag->field, ag->natoms * sizeof((ag->field[0]))); \
} while(0)

static kh_inline khint_t __mol_residue_id_hash_func(struct mol_residue_id id)
{
	return id.insertion * 29 + id.chain * 22303 + id.residue_seq;
}

khint_t mol_residue_id_equal(struct mol_residue_id a,
			     struct mol_residue_id b)
{
	return (a.residue_seq == b.residue_seq) &&
		(a.chain == b.chain) &&
		(a.insertion == b.insertion);
}

int mol_residue_id_cmp(const void *a,
		       const void *b)
{
	const struct mol_residue_id *ia = (const struct mol_residue_id *) a;
	const struct mol_residue_id *ib = (const struct mol_residue_id *) b;

	if (ia->chain < ib->chain)
		return -1;
	else if (ia->chain > ib->chain)
		return 1;

	if (ia->residue_seq < ib->residue_seq)
		return -1;
	else if (ia->residue_seq > ib->residue_seq)
		return 1;

	if (ia->insertion < ib->insertion)
		return -1;
	else if (ia->insertion > ib->insertion)
		return 1;

	return 0;
}

static int mol_residue_cmp(const void *a,
			   const void *b)
{
	const struct mol_residue *ia = *((const struct mol_residue *const *) a);
	const struct mol_residue *ib = *((const struct mol_residue *const *) b);

	return mol_residue_id_cmp(&ia->residue_id, &ib->residue_id);
}

bool mol_atom_group_create_residue_hash(struct mol_atom_group *ag)
{
	if (ag == NULL) {
		fprintf(stderr, "[Error] Could not create a residue hash table:\n");
		fprintf(stderr, "        Atom group is NULL\n");
		return false;
	}
	if (ag->residues != NULL) {
		kh_destroy(RESI_HASH, ag->residues);
	}
	ag->residues = kh_init(RESI_HASH);
	for (size_t index = 0; index < ag->natoms; index++) {
		struct mol_residue *residue = find_residue(ag,
			ag->residue_id[index].chain,
		        ag->residue_id[index].residue_seq,
		        ag->residue_id[index].insertion);
		if (residue == NULL) {
			int ret;
			khiter_t key;
			key = kh_put(RESI_HASH, ag->residues, ag->residue_id[index], &ret);
			if (key != kh_end(ag->residues)) {
				residue = &kh_value(ag->residues, key);
				memset(&kh_value(ag->residues, key), 0, sizeof(struct mol_residue));
				residue->residue_id = ag->residue_id[index];
				residue->atom_start = index;
				residue->atom_end = index;
				residue->rotamer = 0;
				memset(residue->name, 0, 4); // Residue name is 4 bytes long, including \0
				if (ag->residue_name[index] != NULL) {
					strncpy(residue->name,
					        ag->residue_name[index], 3);
				}
			} else {
				fprintf(stderr, "Could not insert into residue hash table\n");
				return false;
			}
		}
		residue->atom_start = MIN(index, residue->atom_start);
		residue->atom_end = MAX(index, residue->atom_end);
	}
	return true;
}

void mol_atom_group_create_residue_list(struct mol_atom_group *ag)
{
	if (ag->residue_list != NULL) {
		return;
	}

	ag->nresidues = kh_size(ag->residues);
	ag->residue_list = calloc(ag->nresidues, sizeof(struct mol_residue *));

	size_t i = 0;
	khiter_t key;
	for (key = kh_begin(ag->residues); key != kh_end(ag->residues); ++key) {
		if (!kh_exist(ag->residues, key))
			continue;

		ag->residue_list[i] = &kh_value(ag->residues, key);
		i++;
	}

	qsort(ag->residue_list, ag->nresidues, sizeof(struct mol_residue *),
	      mol_residue_cmp);
}

__KHASH_IMPL(RESI_HASH, , struct mol_residue_id, struct mol_residue, 1,
	__mol_residue_id_hash_func, mol_residue_id_equal);

struct mol_atom_group *mol_atom_group_create(void)
{
	return calloc(1, sizeof(struct mol_atom_group));
}

struct mol_atom_group_list *mol_atom_group_list_create(size_t n)
{
	struct mol_atom_group_list *list =
		malloc(sizeof(struct mol_atom_group_list));
	list->size = n;
	list->members = calloc(n, sizeof(struct mol_atom_group));
	return list;
}

// Issue #31
// The piece of code in this function is repeated in
// several places across the library. At the very least:
//	in mol_atom_group_join()
//	in mol_atom_group_read_psf()
//	in mol_read_json_v1().
// In the future, we should interface it (or put in a hidden header),
// and replace the relevant routines with a function call.

void mol_atom_group_fill_geometry_lists(struct mol_atom_group *ag)
{
	// Issue #31
	// TODO: If geometry lists are not allocated, allocate them and get sizes

	for (size_t i = 0; i < ag->natoms; i++) {
		// First use the sizes to allocate the member arrays, then
		// zero out all the sizes so we can use them as an index later

		if (ag->bond_lists != NULL) {
			ag->bond_lists[i].members = calloc(ag->bond_lists[i].size,
							   sizeof(struct mol_bond *));
			ag->bond_lists[i].size = 0;
		}

		if (ag->angle_lists != NULL) {
			ag->angle_lists[i].members = calloc(ag->angle_lists[i].size,
							    sizeof(struct mol_angle *));
			ag->angle_lists[i].size = 0;
		}

		if (ag->dihedral_lists != NULL) {
			ag->dihedral_lists[i].members = calloc(ag->dihedral_lists[i].size,
							       sizeof(struct mol_dihedral *));
			ag->dihedral_lists[i].size = 0;
		}

		if (ag->improper_lists != NULL) {
			ag->improper_lists[i].members = calloc(ag->improper_lists[i].size,
							       sizeof(struct mol_improper *));
			ag->improper_lists[i].size = 0;
		}
	}

	if (ag->bond_lists != NULL) {
		for (size_t i = 0; i < ag->nbonds; ++i) {
			struct mol_bond *bond = &ag->bonds[i];
			size_t ai = bond->ai;
			size_t aj = bond->aj;

			ag->bond_lists[ai].members[ag->bond_lists[ai].size++] = bond;
			ag->bond_lists[aj].members[ag->bond_lists[aj].size++] = bond;
		}
	}

	if (ag->angle_lists != NULL) {
		for (size_t i = 0; i < ag->nangles; ++i) {
			struct mol_angle *angle = &ag->angles[i];
			size_t a0 = angle->a0;
			size_t a1 = angle->a1;
			size_t a2 = angle->a2;

			ag->angle_lists[a0].members[ag->angle_lists[a0].size++] = angle;
			ag->angle_lists[a1].members[ag->angle_lists[a1].size++] = angle;
			ag->angle_lists[a2].members[ag->angle_lists[a2].size++] = angle;
		}
	}

	if (ag->dihedral_lists != NULL ) {
		for (size_t i = 0; i < ag->ndihedrals; ++i) {
			struct mol_dihedral *dihedral = &ag->dihedrals[i];
			size_t a0 = dihedral->a0;
			size_t a1 = dihedral->a1;
			size_t a2 = dihedral->a2;
			size_t a3 = dihedral->a3;

			ag->dihedral_lists[a0].members[ag->dihedral_lists[a0].size++] = dihedral;
			ag->dihedral_lists[a1].members[ag->dihedral_lists[a1].size++] = dihedral;
			ag->dihedral_lists[a2].members[ag->dihedral_lists[a2].size++] = dihedral;
			ag->dihedral_lists[a3].members[ag->dihedral_lists[a3].size++] = dihedral;
		}
	}

	if (ag->improper_lists != NULL) {
		for (size_t i = 0; i < ag->nimpropers; ++i) {
			struct mol_improper *improper = &ag->impropers[i];
			size_t a0 = improper->a0;
			size_t a1 = improper->a1;
			size_t a2 = improper->a2;
			size_t a3 = improper->a3;

			ag->improper_lists[a0].members[ag->improper_lists[a0].size++] = improper;
			ag->improper_lists[a1].members[ag->improper_lists[a1].size++] = improper;
			ag->improper_lists[a2].members[ag->improper_lists[a2].size++] = improper;
			ag->improper_lists[a3].members[ag->improper_lists[a3].size++] = improper;
		}
	}

}

// Some magic metaprogramming macros to cut down the number of lines in join_atom_groups.
// Uses the concatenation ability of the C preprocessor.
#define __JOIN_FIELD(field, _type) do {					\
		if (A->field != NULL || B->field != NULL) {		\
			AB->field = calloc(AB->natoms, sizeof(_type));	\
			if (A->field != NULL) memmove(AB->field, A->field, A->natoms*sizeof(_type)); \
			if (B->field != NULL) memmove(&AB->field[A->natoms], B->field, B->natoms*sizeof(_type)); \
		}							\
	} while (0)

#define __JOIN_STR_FIELD(field, _type) do {				\
		if (A->field != NULL || B->field != NULL) {		\
			AB->field = calloc(AB->natoms, sizeof(_type));	\
			if (A->field != NULL) {				\
				for (size_t i = 0; i < A->natoms; i++) { \
					if (A->field[i] != NULL)	\
						AB->field[i] =		\
							strdup(A->field[i]); \
				}					\
			}						\
			if (B->field != NULL) {				\
				for (size_t i = 0; i < B->natoms; i++) { \
					if (B->field[i] != NULL)	\
						AB->field[i + A->natoms] = \
							strdup(B->field[i]); \
				}					\
			}						\
		}							\
	} while (0)

#define __JOIN_GEOMETRY(T) do {                                         \
		if (A->n ## T ## s != 0 || B->n ## T ## s != 0) {	\
			AB->n ## T ## s = A->n ## T ## s + B->n ## T ## s; \
			AB->T ## s = calloc(AB->n ## T ## s, sizeof(struct mol_ ## T)); \
			AB->T ## _lists = calloc(AB->natoms, sizeof(struct mol_ ## T ## _list)); \
			memmove(AB->T ## s, A->T ## s, A->n ## T ## s * sizeof(struct mol_ ## T)); \
			memmove(&AB->T ## s[A->n ## T ## s], B->T ## s, B->n ## T ## s * sizeof(struct mol_ ## T)); \
			if (A->T ## _lists != NULL ) {			\
				memmove(AB->T ## _lists, A->T ## _lists, A->natoms * sizeof(struct mol_ ## T ## _list)); \
			}						\
			if (B->T ## _lists != NULL ) {			\
				memmove(&AB->T ## _lists[A->natoms], B->T ## _lists, B->natoms * sizeof(struct mol_ ## T ## _list)); \
			}						\
		}							\
	} while (0)

struct mol_atom_group *mol_atom_group_join(
	const struct mol_atom_group *A,
	const struct mol_atom_group *B)
{
	struct mol_atom_group *AB = calloc(1, sizeof(struct mol_atom_group));

	AB->natoms = A->natoms + B->natoms;

	__JOIN_FIELD(coords, struct mol_vector3);
	__JOIN_FIELD(gradients, struct mol_vector3);

	__JOIN_STR_FIELD(element, char *);
	__JOIN_STR_FIELD(atom_name, char *);
	__JOIN_STR_FIELD(residue_name, char *);
	__JOIN_STR_FIELD(segment_id, char *);
	__JOIN_STR_FIELD(formal_charge, char *);
	__JOIN_FIELD(residue_id, struct mol_residue_id);

	__JOIN_FIELD(occupancy, double);
	__JOIN_FIELD(B, double);
	__JOIN_FIELD(vdw_radius, double);
	__JOIN_FIELD(vdw_radius03, double);
	__JOIN_FIELD(pwpot_id, int);
	__JOIN_FIELD(eps, double);
	__JOIN_FIELD(eps03, double);
	__JOIN_FIELD(ace_volume, double);
	__JOIN_FIELD(charge, double);
	__JOIN_FIELD(record, enum mol_atom_record);

	if (A->seqres != NULL || B->seqres != NULL) {
		size_t A_len =0;
		size_t B_len =0;
		if (A->seqres != NULL) {
			A_len = strlen(A->seqres);
		}
		if (B->seqres != NULL) {
			B_len = strlen(B->seqres);
		}
		size_t AB_len = A_len + B_len + 2;
		AB->seqres = calloc(AB_len, sizeof(char));
		if (A->seqres != NULL) {
			strcat(AB->seqres, A->seqres);
		}
		//if both A and B have seqres data, separate them with a /
		if (A->seqres != NULL && B->seqres != NULL) {
			strcat(AB->seqres, "/");
		}
		if (B->seqres != NULL) {
			strcat(AB->seqres, B->seqres);
		}
	}
	__JOIN_FIELD(alternate_location, char);

	__JOIN_GEOMETRY(bond);
	__JOIN_GEOMETRY(angle);
	__JOIN_GEOMETRY(dihedral);
	__JOIN_GEOMETRY(improper);

	// Adjust bond/angle/dihedral/improper indices for B
	for (size_t i = A->nbonds; i < AB->nbonds; i++) {
		struct mol_bond *bond = &AB->bonds[i];
		bond->ai += A->natoms;
		bond->aj += A->natoms;
	}

	for (size_t i = A->nangles; i < AB->nangles; i++) {
		struct mol_angle *angle = &AB->angles[i];
		angle->a0 += A->natoms;
		angle->a1 += A->natoms;
		angle->a2 += A->natoms;
	}

	for (size_t i = A->ndihedrals; i < AB->ndihedrals; i++) {
		struct mol_dihedral *dihedral = &AB->dihedrals[i];
		dihedral->a0 += A->natoms;
		dihedral->a1 += A->natoms;
		dihedral->a2 += A->natoms;
		dihedral->a3 += A->natoms;
	}

	for (size_t i = A->nimpropers; i < AB->nimpropers; i++) {
		struct mol_improper *improper = &AB->impropers[i];
		improper->a0 += A->natoms;
		improper->a1 += A->natoms;
		improper->a2 += A->natoms;
		improper->a3 += A->natoms;
	}

	// Fill in geometry lists
	for (size_t i = 0; i < AB->natoms; i++) {
		// Zero out all the sizes so we can use them as an index later

		if (AB->bond_lists != NULL) {
			AB->bond_lists[i].members = calloc(AB->bond_lists[i].size,
							   sizeof(struct mol_bond *));
			AB->bond_lists[i].size = 0;
		}

		if (AB->angle_lists != NULL) {
			AB->angle_lists[i].members = calloc(AB->angle_lists[i].size,
							    sizeof(struct mol_angle *));
			AB->angle_lists[i].size = 0;
		}

		if (AB->dihedral_lists != NULL) {
			AB->dihedral_lists[i].members = calloc(AB->dihedral_lists[i].size,
							       sizeof(struct mol_dihedral *));
			AB->dihedral_lists[i].size = 0;
		}

		if (AB->improper_lists != NULL) {
			AB->improper_lists[i].members = calloc(AB->improper_lists[i].size,
							       sizeof(struct mol_improper *));
			AB->improper_lists[i].size = 0;
		}
	}

	if (AB->bond_lists != NULL) {
		for (size_t i = 0; i < AB->nbonds; ++i) {
			struct mol_bond *bond = &AB->bonds[i];
			size_t ai = bond->ai;
			size_t aj = bond->aj;

			AB->bond_lists[ai].members[AB->bond_lists[ai].size++] = bond;
			AB->bond_lists[aj].members[AB->bond_lists[aj].size++] = bond;
		}
	}

	if (AB->angle_lists != NULL) {
		for (size_t i = 0; i < AB->nangles; ++i) {
			struct mol_angle *angle = &AB->angles[i];
			size_t a0 = angle->a0;
			size_t a1 = angle->a1;
			size_t a2 = angle->a2;

			AB->angle_lists[a0].members[AB->angle_lists[a0].size++] = angle;
			AB->angle_lists[a1].members[AB->angle_lists[a1].size++] = angle;
			AB->angle_lists[a2].members[AB->angle_lists[a2].size++] = angle;
		}
	}

	if (AB->dihedral_lists != NULL ) {
		for (size_t i = 0; i < AB->ndihedrals; ++i) {
			struct mol_dihedral *dihedral = &AB->dihedrals[i];
			size_t a0 = dihedral->a0;
			size_t a1 = dihedral->a1;
			size_t a2 = dihedral->a2;
			size_t a3 = dihedral->a3;

			AB->dihedral_lists[a0].members[AB->dihedral_lists[a0].size++] = dihedral;
			AB->dihedral_lists[a1].members[AB->dihedral_lists[a1].size++] = dihedral;
			AB->dihedral_lists[a2].members[AB->dihedral_lists[a2].size++] = dihedral;
			AB->dihedral_lists[a3].members[AB->dihedral_lists[a3].size++] = dihedral;
		}
	}

	if (AB->improper_lists != NULL) {
		for (size_t i = 0; i < AB->nimpropers; ++i) {
			struct mol_improper *improper = &AB->impropers[i];
			size_t a0 = improper->a0;
			size_t a1 = improper->a1;
			size_t a2 = improper->a2;
			size_t a3 = improper->a3;

			AB->improper_lists[a0].members[AB->improper_lists[a0].size++] = improper;
			AB->improper_lists[a1].members[AB->improper_lists[a1].size++] = improper;
			AB->improper_lists[a2].members[AB->improper_lists[a2].size++] = improper;
			AB->improper_lists[a3].members[AB->improper_lists[a3].size++] = improper;
		}
	}

	__JOIN_FIELD(ftypen, size_t);
	__JOIN_STR_FIELD(ftype_name, char *);
	// This is questionable decision, but here we make sure the atom type numbers from A and B
	// don't intersect. Since we have no explicit mapping type_number <-> type_name, we can not
	// reliably "merge" atom type numbers from A and B. So we concatenate and shift them.
	AB->num_atom_types = A->num_atom_types + B->num_atom_types;
	if (A->ftypen != NULL && B->ftype_name != NULL) {
		const size_t d = A->num_atom_types;
		for (size_t i = 0; i < B->natoms; i++)
			AB->ftypen[i + A->natoms] += d;
	}

	mol_atom_group_create_residue_hash(AB);

	return AB;
}

#undef __JOIN_FIELD
#undef __JOIN_GEOMETRY


#define __COPY_FIELD(dst, src, field) do {							\
		if (dst->field && src->field)							\
			memmove(dst->field, src->field, src->natoms * sizeof(*(src->field)));	\
	} while(0)

#define __COPY_STR_FIELD(dst, src, field) do {					\
		if (dst->field && src->field) {					\
			for (size_t i = 0; i < src->natoms; ++i) {		\
				if (src->field[i] != NULL)			\
					dst->field[i] = strdup(src->field[i]);	\
				else						\
					dst->field[i] = NULL;			\
			}							\
		}								\
	} while(0)

struct mol_atom_group* mol_atom_group_copy(const struct mol_atom_group *ag)
{
	struct mol_atom_group* new_ag = mol_atom_group_create();
	// Initialize atomic fields (except bond/angle/.. lists)
	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	// Copy atomic fields (except bond/angle/.. lists)
	new_ag->natoms = ag->natoms;
	__COPY_FIELD(new_ag, ag, coords);
	__COPY_FIELD(new_ag, ag, gradients);
	__COPY_STR_FIELD(new_ag, ag, element);
	__COPY_STR_FIELD(new_ag, ag, atom_name);
	__COPY_FIELD(new_ag, ag, alternate_location);
	__COPY_FIELD(new_ag, ag, residue_id);
	__COPY_STR_FIELD(new_ag, ag, residue_name);
	__COPY_FIELD(new_ag, ag, occupancy);
	__COPY_FIELD(new_ag, ag, B);
	__COPY_STR_FIELD(new_ag, ag, segment_id);
	__COPY_STR_FIELD(new_ag, ag, formal_charge);
	__COPY_FIELD(new_ag, ag, vdw_radius);
	__COPY_FIELD(new_ag, ag, vdw_radius03);
	__COPY_FIELD(new_ag, ag, eps);
	__COPY_FIELD(new_ag, ag, eps03);
	__COPY_FIELD(new_ag, ag, ace_volume);
	__COPY_FIELD(new_ag, ag, charge);

	__COPY_FIELD(new_ag, ag, pwpot_id);
	__COPY_FIELD(new_ag, ag, fixed);
	__COPY_FIELD(new_ag, ag, ftypen);
	__COPY_STR_FIELD(new_ag, ag, ftype_name);
	__COPY_FIELD(new_ag, ag, mask);
	__COPY_FIELD(new_ag, ag, surface);
	__COPY_FIELD(new_ag, ag, attraction_level);
	__COPY_FIELD(new_ag, ag, backbone);
	__COPY_FIELD(new_ag, ag, record);
	__COPY_FIELD(new_ag, ag, hbond_prop);
	__COPY_FIELD(new_ag, ag, hbond_base_atom_id);

	// Copy seqres
	if (ag->seqres != NULL)
		new_ag->seqres = strdup(ag->seqres);

	// Copy geometry
	if (ag->bonds != NULL) {
		new_ag->nbonds = ag->nbonds;
		new_ag->bonds = calloc(ag->nbonds, sizeof(struct mol_bond));
		memmove(new_ag->bonds, ag->bonds, ag->nbonds * sizeof(struct mol_bond));

		new_ag->bond_lists = calloc(ag->natoms, sizeof(struct mol_bond_list));
		// Here we’re naively copying the pointers to the lists.
		// That’s okay: we rebuild them later in mol_atom_group_fill_geometry_lists
		memmove(new_ag->bond_lists, ag->bond_lists, ag->natoms * sizeof(struct mol_bond_list));
	}

	if (ag->angles != NULL) {
		new_ag->nangles = ag->nangles;
		new_ag->angles = calloc(ag->nangles, sizeof(struct mol_angle));
		memmove(new_ag->angles, ag->angles, ag->nangles * sizeof(struct mol_angle));

		new_ag->angle_lists = calloc(ag->natoms, sizeof(struct mol_angle_list));
		// Here we’re naively copying the pointers to the lists.
		// That’s okay: we rebuild them later in mol_atom_group_fill_geometry_lists
		memmove(new_ag->angle_lists, ag->angle_lists, ag->natoms * sizeof(struct mol_angle_list));
	}

	if (ag->dihedrals != NULL) {
		new_ag->ndihedrals = ag->ndihedrals;
		new_ag->dihedrals = calloc(ag->ndihedrals, sizeof(struct mol_dihedral));
		memmove(new_ag->dihedrals, ag->dihedrals, ag->ndihedrals * sizeof(struct mol_dihedral));

		new_ag->dihedral_lists = calloc(ag->natoms, sizeof(struct mol_dihedral_list));
		// Here we’re naively copying the pointers to the lists.
		// That’s okay: we rebuild them later in mol_atom_group_fill_geometry_lists
		memmove(new_ag->dihedral_lists, ag->dihedral_lists, ag->natoms * sizeof(struct mol_dihedral_list));
	}

	if (ag->impropers != NULL) {
		new_ag->nimpropers = ag->nimpropers;
		new_ag->impropers = calloc(ag->nimpropers, sizeof(struct mol_improper));
		memmove(new_ag->impropers, ag->impropers, ag->nimpropers * sizeof(struct mol_improper));

		new_ag->improper_lists = calloc(ag->natoms, sizeof(struct mol_improper_list));
		// Here we’re naively copying the pointers to the lists.
		// That’s okay: we rebuild them later in mol_atom_group_fill_geometry_lists
		memmove(new_ag->improper_lists, ag->improper_lists, ag->natoms * sizeof(struct mol_improper_list));

	}
	mol_atom_group_fill_geometry_lists(new_ag);

	// Active atom/bond/etc lists are not copied,
	// but reconstructed based on the new_ag->fixed atomic field
	if (ag->fixed != NULL) {
		new_ag->active_atoms = calloc(1, sizeof(struct mol_index_list));
		new_ag->active_bonds = calloc(1, sizeof(struct mol_bond_list));
		new_ag->active_angles = calloc(1, sizeof(struct mol_angle_list));
		new_ag->active_dihedrals = calloc(1, sizeof(struct mol_dihedral_list));
		new_ag->active_impropers = calloc(1, sizeof(struct mol_improper_list));

		mol_fixed_update_active_lists(new_ag);
	}

	// Copy number of atom types
	new_ag->num_atom_types = ag->num_atom_types;

	// Set up residue hash table and create residue list
	if (ag->residues != NULL)
		mol_atom_group_create_residue_hash(new_ag);
	if (ag->residue_list != NULL)
		mol_atom_group_create_residue_list(new_ag);

	return new_ag;
}

#undef __COPY_FIELD
#undef __COPY_STR_FIELD


void mol_atom_group_destroy(struct mol_atom_group *ag)
{
	if (ag != NULL) {
		free_if_not_null(ag->coords);
		free_if_not_null(ag->gradients);
		free_if_not_null(ag->vdw_radius);
		free_if_not_null(ag->vdw_radius03);
		free_if_not_null(ag->eps);
		free_if_not_null(ag->eps03);
		free_if_not_null(ag->ace_volume);
		free_if_not_null(ag->charge);

		free_if_not_null(ag->seqres);
		free_if_not_null(ag->residue_id);

		if (ag->element != NULL || ag->atom_name != NULL ||
			ag->residue_name != NULL ||
			ag->formal_charge != NULL ||
			ag->bond_lists != NULL ||
			ag->angle_lists != NULL ||
			ag->dihedral_lists != NULL ||
			ag->improper_lists != NULL ||
			ag->ftype_name != NULL) {

			for (size_t i = 0; i < ag->natoms; ++i) {
				if (ag->element != NULL)
					free_if_not_null(ag->element[i]);
				if (ag->atom_name != NULL)
					free_if_not_null(ag->atom_name[i]);
				if (ag->residue_name != NULL)
					free_if_not_null(ag->residue_name[i]);
				if (ag->formal_charge != NULL)
					free_if_not_null(ag->formal_charge[i]);
				if (ag->bond_lists != NULL)
					free_if_not_null(ag->bond_lists[i].members);
				if (ag->angle_lists != NULL)
					free_if_not_null(ag->angle_lists[i].members);
				if (ag->dihedral_lists != NULL)
					free_if_not_null(ag->dihedral_lists[i].members);
				if (ag->improper_lists != NULL)
					free_if_not_null(ag->improper_lists[i].members);
				if (ag->ftype_name != NULL)
					free_if_not_null(ag->ftype_name[i]);
				if (ag->segment_id != NULL)
					free_if_not_null(ag->segment_id[i]);
			}
		}

		free_if_not_null(ag->element);
		free_if_not_null(ag->atom_name);
		free_if_not_null(ag->residue_name);
		free_if_not_null(ag->formal_charge);
		free_if_not_null(ag->ftypen);
		free_if_not_null(ag->ftype_name);

		free_if_not_null(ag->alternate_location);
		free_if_not_null(ag->occupancy);
		free_if_not_null(ag->B);
		free_if_not_null(ag->segment_id);

		free_if_not_null(ag->mask);
		free_if_not_null(ag->surface);
		free_if_not_null(ag->attraction_level);
		free_if_not_null(ag->pwpot_id);
		free_if_not_null(ag->backbone);

		free_if_not_null(ag->record);

		free_if_not_null(ag->residue_list);
		if (ag->residues != NULL) {
			kh_destroy(RESI_HASH, ag->residues);
		}
		if (ag->metadata != NULL) {
			kh_destroy(STR, ag->metadata);
		}

		free_if_not_null(ag->fixed);
		free_if_not_null(ag->bonds);
		free_if_not_null(ag->angles);
		free_if_not_null(ag->dihedrals);
		free_if_not_null(ag->impropers);

		free_if_not_null(ag->bond_lists);
		free_if_not_null(ag->angle_lists);
		free_if_not_null(ag->dihedral_lists);
		free_if_not_null(ag->improper_lists);

		if (ag->active_atoms != NULL)
			free_if_not_null(ag->active_atoms->members);
		if (ag->active_bonds != NULL)
			free_if_not_null(ag->active_bonds->members);
		if (ag->active_angles != NULL)
			free_if_not_null(ag->active_angles->members);
		if (ag->active_dihedrals != NULL)
			free_if_not_null(ag->active_dihedrals->members);
		if (ag->active_impropers != NULL)
			free_if_not_null(ag->active_impropers->members);
		free_if_not_null(ag->active_atoms);
		free_if_not_null(ag->active_bonds);
		free_if_not_null(ag->active_angles);
		free_if_not_null(ag->active_dihedrals);
		free_if_not_null(ag->active_impropers);

		free_if_not_null(ag->hbond_prop);
		free_if_not_null(ag->hbond_base_atom_id);
	}
}

void mol_atom_group_free(struct mol_atom_group *ag)
{
	if (ag != NULL) {
		mol_atom_group_destroy(ag);
		free(ag);
	}
}

void mol_atom_group_get_actives(double * restrict xyz,
				const struct mol_atom_group * restrict ag)
{
	for (unsigned int i = 0; i < ag->active_atoms->size; ++i) {
		xyz[i*3]     = ag->coords[ag->active_atoms->members[i]].X;
		xyz[i*3 + 1] = ag->coords[ag->active_atoms->members[i]].Y;
		xyz[i*3 + 2] = ag->coords[ag->active_atoms->members[i]].Z;
	}
}

void mol_atom_group_set_actives(struct mol_atom_group * restrict ag,
				const double * restrict xyz)
{
	for (unsigned int i = 0; i < ag->active_atoms->size; ++i) {
		ag->coords[ag->active_atoms->members[i]].X = xyz[i*3];
		ag->coords[ag->active_atoms->members[i]].Y = xyz[i*3 + 1];
		ag->coords[ag->active_atoms->members[i]].Z = xyz[i*3 + 2];
	}
}

void mol_atom_group_list_destroy(struct mol_atom_group_list *ag_list)
{
	if (ag_list != NULL) {
		for (size_t i = 0; i < ag_list->size; ++i) {
			mol_atom_group_destroy(&ag_list->members[i]);
		}

		free(ag_list->members);
	}
}

void mol_atom_group_list_free(struct mol_atom_group_list *ag_list)
{
	if (ag_list != NULL) {
		mol_atom_group_list_destroy(ag_list);
		free(ag_list);
	}
}

#define __COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, field) do {		\
		if (dst->field && src->field)				\
			dst->field[dst_ind] = src->field[src_ind];	\
	} while(0)

#define __COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, field) do {	\
		if (dst->field && src->field && src->field[src_ind]) {	\
			dst->field[dst_ind] = strdup(src->field[src_ind]); \
		}							\
	} while(0)

void copy_atom(struct mol_atom_group *dst, size_t dst_ind,
	const struct mol_atom_group *src, size_t src_ind)
{
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, coords);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, gradients);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, element);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, atom_name);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, alternate_location);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, residue_id);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, residue_name);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, occupancy);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, B);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, segment_id);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, formal_charge);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, vdw_radius);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, vdw_radius03);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, eps);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, eps03);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, ace_volume);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, charge);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, pwpot_id);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, fixed);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, ftypen);
	__COPY_ATOM_STR_FIELD(dst, dst_ind, src, src_ind, ftype_name);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, mask);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, surface);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, attraction_level);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, backbone);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, record);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, hbond_prop);
	__COPY_ATOM_FIELD(dst, dst_ind, src, src_ind, hbond_base_atom_id);
}

#undef __COPY_ATOM_FIELD
#undef __COPY_ATOM_STR_FIELD

void centroid(struct mol_vector3 *center,
	      const struct mol_atom_group *ag)
{
	MOL_VEC_SET_SCALAR((*center), 0.0);

	for (size_t i = 0; i < ag->natoms; ++i) {
		MOL_VEC_ADD((*center), (*center), ag->coords[i]);
	}

	MOL_VEC_DIV_SCALAR((*center), (*center), ag->natoms);
}

void center_of_extrema(struct mol_vector3 *center,
		const struct mol_atom_group *ag)
{
	struct mol_vector3 min = ag->coords[0];
	struct mol_vector3 max = ag->coords[0];

	for (size_t i = 0; i < ag->natoms; ++i) {
		min.X = fmin(min.X, ag->coords[i].X);
		min.Y = fmin(min.Y, ag->coords[i].Y);
		min.Z = fmin(min.Z, ag->coords[i].Z);
		max.X = fmax(max.X, ag->coords[i].X);
		max.Y = fmax(max.Y, ag->coords[i].Y);
		max.Z = fmax(max.Z, ag->coords[i].Z);
	}

	MOL_VEC_ADD((*center), min, max);
	MOL_VEC_DIV_SCALAR((*center), (*center), 2.0);
}

double mol_atom_group_diameter(const struct mol_atom_group *ag)
{
	double diameter = 0.0;
	double dist;
	for (size_t i = 0; i < ag->natoms; ++i) {
		for (size_t j = i + 1; j < ag->natoms; ++j) {
			dist = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[i], ag->coords[j]);
			if (dist > diameter) {
				diameter = dist;
			}
		}
	}
	return sqrt(diameter);
}

void mol_atom_group_bounding_box(
	struct mol_vector3 *min, struct mol_vector3 *max,
	const struct mol_atom_group *ag)
{
	*min = ag->coords[0];
	*max = ag->coords[0];

	for (size_t i = 1; i < ag->natoms; ++i) {
		min->X = fmin(min->X, ag->coords[i].X);
		min->Y = fmin(min->Y, ag->coords[i].Y);
		min->Z = fmin(min->Z, ag->coords[i].Z);

		max->X = fmax(max->X, ag->coords[i].X);
		max->Y = fmax(max->Y, ag->coords[i].Y);
		max->Z = fmax(max->Z, ag->coords[i].Z);
	}
}

/*  Mixed implementation of bouncing ball and Ritter's algorithm
 */
double mol_atom_group_bounding_sphere(struct mol_vector3 *center,
		const struct mol_atom_group *ag)
{
	double max_dist = 0.0;
	struct mol_vector3 point1 = {INFINITY, INFINITY, INFINITY};
	struct mol_vector3 point2 = ag->coords[0];

	//find point1 furthest from the first point in vector
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[0], ag->coords[i]);
		if (dist > max_dist) {
			max_dist = dist;
			point1 = ag->coords[i];
		}
	}

	//now find the furthest point from point1 and store it as point2
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist = MOL_VEC_EUCLIDEAN_DIST_SQ(point1, ag->coords[i]);
		if (dist > max_dist) {
			max_dist = dist;
			point2 = ag->coords[i];
		}
	}

	// initial guess at center halfway between point1 and point2
	MOL_VEC_ADD(*center, point1, point2);
	MOL_VEC_DIV_SCALAR(*center, *center, 2.0);

	double rad = sqrt(max_dist)/2.0;
	double rad_sq = rad * rad;

	for (size_t i = 0; i < ag->natoms; i++) {
		double dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(*center, ag->coords[i]);
		if (dist_sq > rad_sq) {
			double dist = sqrt(dist_sq);

			/* new radius is radius + (dist-rad)/6.0)
			 * This includes the outside point and moves the sphere in
			 * the direction of this point, potentially bouncing out
			 * points that were previously inside the sphere, but giving
			 * a smaller radius
			 */
			double rad_new = (5.0*rad+dist)/6.0;

			/* Derivation of equation used:
			 * new center is rad_new from outside point on
			 * the vector from that point to the old center
			 * center_new = point - rad_new * ((point-center)/dist)
			 *
			 * regroup to give
			 * center_new = ((dist-rad_new) * point + rad_new * center)/dist
			 */

			MOL_VEC_MULT_SCALAR(*center, *center, rad_new);
			struct mol_vector3  temp;
			MOL_VEC_MULT_SCALAR(temp, ag->coords[i], dist-rad_new);

			MOL_VEC_ADD(*center, *center, temp);

			MOL_VEC_DIV_SCALAR(*center, *center, dist);

			rad = rad_new;
			rad_sq = rad * rad;
		}
	}

	/* a pass of Ritter's algorithm to ensure all points are in the sphere */
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(*center, ag->coords[i]);
		if (dist_sq > rad_sq) {
			double dist = sqrt(dist_sq);

			/* new radius is radius + (dist-rad)/2.0)
			 * This radius guarantees the old sphere is inside the new
			 * sphere, so our new sphere encloses everything.
			 */
			double rad_new = (rad+dist)/2;

			MOL_VEC_MULT_SCALAR(*center, *center, rad_new);
			struct mol_vector3  temp;
			MOL_VEC_MULT_SCALAR(temp, ag->coords[i], dist-rad_new);

			MOL_VEC_ADD(*center, *center, temp);

			MOL_VEC_DIV_SCALAR(*center, *center, dist);

			rad = rad_new;
			rad_sq = rad * rad;
		}
	}
	return 2.0 * rad;
}

void mol_atom_group_translate(struct mol_atom_group *restrict ag,
			const struct mol_vector3 *restrict tv)
{
	mol_vector3_translate_list(ag->coords, ag->natoms, tv);
}

void mol_atom_group_rotate(struct mol_atom_group *restrict ag,
			const struct mol_matrix3 *restrict r)
{
	mol_vector3_rotate_list(ag->coords, ag->natoms, r);
}

void mol_atom_group_move(struct mol_atom_group *restrict ag,
			const struct mol_matrix3 *restrict r,
			const struct mol_vector3 *restrict tv)
{
	mol_vector3_move_list(ag->coords, ag->natoms, r, tv);
}

void mol_atom_group_move_in_copy(const struct mol_atom_group *src,
			struct mol_atom_group *dest,
			const struct mol_matrix3 *r,
			const struct mol_vector3 *tv)
{
	if (src == NULL || dest == NULL || dest->coords == NULL) {
		fprintf(stderr, "src and dest pointers must be malloced\n");
		exit(EXIT_FAILURE);
	}

	for (size_t atomi = 0; atomi < src->natoms; atomi++) {
		double X = src->coords[atomi].X;
		double Y = src->coords[atomi].Y;
		double Z = src->coords[atomi].Z;
		dest->coords[atomi].X = r->m11 * X + r->m12 * Y + r->m13 * Z + tv->X;
		dest->coords[atomi].Y = r->m21 * X + r->m22 * Y + r->m23 * Z + tv->Y;
		dest->coords[atomi].Z = r->m31 * X + r->m32 * Y + r->m33 * Z + tv->Z;
	}
}

struct mol_residue *find_residue(const struct mol_atom_group *ag,
				const char chain,
				const int16_t residue_seq,
				const char insertion)
{
	if (ag == NULL || ag->residues == NULL)
		return NULL;

	struct mol_residue_id resi_id = {residue_seq, chain, insertion};
	khiter_t key;

	khash_t(RESI_HASH) *h = ag->residues;
	key = kh_get(RESI_HASH, h, resi_id);

	if (key == kh_end(h))
		return NULL;

	return &kh_value(h, key);
}

ssize_t mol_find_atom(const struct mol_atom_group *ag,
		const char chain,
		const int16_t residue_seq,
		const char insertion,
		const char* atom_name)
{
	struct mol_residue *residue = find_residue(ag, chain, residue_seq, insertion);

	if (residue == NULL) {
		return -1;
	}

	for (size_t i = residue->atom_start; i <= residue->atom_end; i++) {
		char * name = ag->atom_name[i];
		if (strcmp(atom_name, name) == 0) {
			return i;
		}
	}

	//try comparison with stripped whitespace
	while (isspace(atom_name[0])) {
		atom_name++;
	}
	char *l_name = strdup(atom_name);
	l_name = rstrip(l_name);
	for (size_t i = residue->atom_start; i <= residue->atom_end; i++) {
		char * name = ag->atom_name[i];
		while (isspace(name[0])) {
			name++;
		}
		name = strdup(name);
		name = rstrip(name);
		int res = strcmp(l_name, name);
		free(name);
		if (res == 0) {
			free(l_name);
			return i;
		}
	}
	free(l_name);
	return -1;
}

struct mol_residue *mol_atom_residue(const struct mol_atom_group *ag,
				size_t atom_index)
{
	if (atom_index >= ag->natoms) {
		return NULL;
	}
	return find_residue(ag,
			ag->residue_id[atom_index].chain,
			ag->residue_id[atom_index].residue_seq,
			ag->residue_id[atom_index].insertion);
}

void print_residues(const struct mol_atom_group *ag)
{
	if (ag->residues == NULL)
		return;

	khiter_t key;

	for (key = kh_begin(ag->residues); key != kh_end(ag->residues); ++key) {
		if (!kh_exist(ag->residues, key))
			continue;
		printf("%c%d%c: %zu %zu\n",
			kh_key(ag->residues, key).chain,
			kh_key(ag->residues, key).residue_seq,
			kh_key(ag->residues, key).insertion,
			kh_value(ag->residues, key).atom_start,
			kh_value(ag->residues, key).atom_end);
	}
}

void mol_atom_group_add_metadata(struct mol_atom_group *ag, const char *key, void *data)
{
	if (ag->metadata == NULL) {
		ag->metadata = kh_init(STR);
	}
	int ret;
	khiter_t val = kh_put(STR, ag->metadata, key, &ret);

	if (val == kh_end(ag->metadata)) {
		fprintf(stderr, "Could not insert %s into ag metadata hash\n", key);
	} else {
		kh_value(ag->metadata, val) = data;
	}
}

void *mol_atom_group_fetch_metadata(const struct mol_atom_group *ag, const char *key)
{
	if (ag->metadata == NULL) {
		return NULL;
	}

	khash_t(STR) *h = ag->metadata;
	khiter_t val = kh_get(STR, h, key);

	if (val == kh_end(h)) {
		return NULL;
	}

	return kh_value(h, val);
}

bool mol_atom_group_has_metadata(const struct mol_atom_group *ag, const char *key)
{
	return (mol_atom_group_fetch_metadata(ag, key) != NULL);
}

void mol_atom_group_delete_metadata(struct mol_atom_group *ag, const char *key)
{
	if (!mol_atom_group_has_metadata(ag, key)) {
		return;
	}

	khash_t(STR) *h = ag->metadata;
	khiter_t val = kh_get(STR, h, key);

	kh_del(STR, h, val);
}

void mol_atom_group_join_atomic_metadata(
		struct mol_atom_group *ag,
		const struct mol_atom_group *A,
		const struct mol_atom_group *B,
		const char *key,
		const size_t element_size)
{
	// The element_size is given in bytes.
	// We use "char*" (as sizeof(char) == 1) to do a bit of pointer arithmetic in memcpy below.
	// "char*" does not relate to the actual content of the arrays.
	const char *dataA = mol_atom_group_fetch_metadata(A, key);
	const char *dataB = mol_atom_group_fetch_metadata(B, key);
	if (dataA == NULL && dataB == NULL)
		return;
	char *data = calloc(ag->natoms, element_size);
	if (dataA)
		memcpy(data, dataA, A->natoms * element_size);
	if (dataB)
		memcpy(data + A->natoms * element_size, dataB, B->natoms * element_size);
	mol_atom_group_add_metadata(ag, key, data);
}

static void repack_mol_atom_group(struct mol_atom_group *ag) {
	MOL_FOR_ATOMIC_FIELDS(__REALLOC_IF_NN, ag);
}

struct lenstring {
	size_t len;
	char *string;
};

struct mol_atom_group *mol_extract_atom_types(struct mol_atom_group *ag,
					const struct mol_list *types)
{
	struct mol_atom_group *new_ag = calloc(1, sizeof(struct mol_atom_group));

	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	struct lenstring *members = malloc(types->size * sizeof(struct lenstring));

	for (size_t type_i = 0; type_i < types->size; type_i++) {
		char *type = ((char**) (types->members))[type_i];
		while (isspace(type[0])) {
			++type;
		}
		size_t len = strlen(type);
		while (isspace(type[len - 1])) {
			--len;
		}

		members[type_i].len = len;
		members[type_i].string = strdup(type);
	}

	for (size_t i = 0; i < ag->natoms; ++i) {
		const char * name = ag->atom_name[i];
		while (isspace(name[0])) {
			++name;
		}
		size_t name_len = strlen(name);
		while (isspace(name[name_len - 1])) {
			--name_len;
		}

		for (size_t type_i = 0; type_i < types->size; type_i++) {
			size_t len = members[type_i].len;
			char *type = members[type_i].string;

			if (len == name_len && strncmp(type, name, len) == 0) {
				copy_atom(new_ag, new_ag->natoms, ag, i);
				++new_ag->natoms;
			}
		}
	}

	repack_mol_atom_group(new_ag);

	for (size_t type_i = 0; type_i < types->size; type_i++) {
		free(members[type_i].string);
	}
	free(members);

	return new_ag;
}

struct mol_atom_group *mol_extract_atom_type(struct mol_atom_group *ag,
					const char *type)
{
	struct mol_atom_group *new_ag =
		calloc(1, sizeof(struct mol_atom_group));
	while (isspace(type[0])) {
		++type;
	}
	size_t len = strlen(type);
	while (isspace(type[len - 1])) {
		--len;
	}

	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; ++i) {
		const char * name = ag->atom_name[i];
		while (isspace(name[0])) {
			++name;
		}
		size_t name_len = strlen(name);
		while (isspace(name[name_len - 1])) {
			--name_len;
		}

		if (len == name_len && strncmp(type, name, len) == 0) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

struct mol_atom_group *mol_extract_atom_type_prefix(struct mol_atom_group *ag,
					const char *type)
{
	struct mol_atom_group *new_ag =
		calloc(1, sizeof(struct mol_atom_group));
	while (isspace(type[0])) {
		++type;
	}
	size_t len = strlen(type);
	while (isspace(type[len - 1])) {
		--len;
	}

	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; ++i) {
		const char * name = ag->atom_name[i];
		while (isspace(name[0])) {
			++name;
		}

		if (strncmp(type, name, len) == 0) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

struct mol_atom_group *mol_remove_atom_type(struct mol_atom_group *ag,
				const char *type)
{
	struct mol_atom_group *new_ag = calloc(1, sizeof(struct mol_atom_group));
	while (isspace(type[0])) {
		++type;
	}
	size_t len = strlen(type);
	while (isspace(type[len - 1])) {
		--len;
	}

	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; ++i) {
		const char * name = ag->atom_name[i];
		while (isspace(name[0])) {
			++name;
		}
		size_t name_len = strlen(name);
		while (isspace(name[name_len - 1])) {
			--name_len;
		}

		if (len != name_len || strncmp(type, name, len) != 0) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

struct mol_atom_group *mol_extract_chain(const struct mol_atom_group *ag,
		const char chain)
{
	struct mol_atom_group *new_ag = calloc(1, sizeof(struct mol_atom_group));

	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; ++i) {
		if (ag->residue_id[i].chain == chain) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

struct mol_atom_group *mol_atom_group_select(
	const struct mol_atom_group *ag,
	bool (*keep)(const struct mol_atom_group *ag,
			     size_t index,
			     void *params),
	void *func_params)
{
	struct mol_atom_group *new_ag = calloc(1, sizeof(struct mol_atom_group));
	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; i++) {
		if (keep(ag, i, func_params)) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

struct mol_atom_group *mol_atom_group_filter(
	const struct mol_atom_group *ag,
	bool (*remove)(const struct mol_atom_group *ag,
			      size_t index,
			      void *params),
	void *func_params)
{
	struct mol_atom_group *new_ag = calloc(1, sizeof(struct mol_atom_group));
	MOL_FOR_ATOMIC_FIELDS(__CALLOC_IF_NN, new_ag, ag);

	for (size_t i = 0; i < ag->natoms; i++) {
		if (!remove(ag, i, func_params)) {
			copy_atom(new_ag, new_ag->natoms, ag, i);
			++new_ag->natoms;
		}
	}

	repack_mol_atom_group(new_ag);

	return new_ag;
}

bool *mol_atom_group_test_foreach(
	const struct mol_atom_group *ag,
	bool (*test_func)(const struct mol_atom_group *ag,
			 size_t index,
			 void *params),
	void *func_params)
{
	bool *results = calloc(ag->natoms, sizeof(int));

	for (size_t i = 0; i < ag->natoms; i++) {
		results[i] = test_func(ag, i, func_params);
	}

	return results;
}

bool mol_is_backbone(const struct mol_atom_group *ag,
			size_t index,
			__attribute__((unused)) void *params)
{
	if (ag->record != NULL && ag->record[index] != MOL_ATOM) {
		return false;
	}
	const char *atom_name = ag->atom_name[index];
	return !strcmp(atom_name, " N  ") ||
	       !strcmp(atom_name, " CA ") ||
	       !strcmp(atom_name, " C  ") ||
	       !strcmp(atom_name, " O  ");
}

bool mol_is_sidechain(const struct mol_atom_group *ag,
			size_t index,
			__attribute__((unused)) void *params)
{
	if (ag->record != NULL && ag->record[index] != MOL_ATOM) {
		return false;
	}
	const char *atom_name = ag->atom_name[index];
	return strcmp(atom_name, " N  ") &&
	       strcmp(atom_name, " CA ") &&
	       strcmp(atom_name, " C  ") &&
	       strcmp(atom_name, " O  ") &&
	       strcmp(atom_name, " H  ") &&
	       strcmp(atom_name, " HT1") &&
	       strcmp(atom_name, " HT2") &&
	       strcmp(atom_name, " HT3") &&
	       strcmp(atom_name, " OXT");
}

void mol_zero_gradients(struct mol_atom_group *ag)
{
	memset(ag->gradients, 0, sizeof(struct mol_vector3) * ag->natoms);
}

void mol_fixed_init(struct mol_atom_group *ag)
{
	mol_atom_group_init_atomic_field(ag, fixed);
	mol_atom_group_init_atomic_field(ag, gradients);
	ag->active_atoms = calloc(1, sizeof(struct mol_index_list));
	ag->active_bonds = calloc(1, sizeof(struct mol_bond_list));
	ag->active_angles = calloc(1, sizeof(struct mol_angle_list));
	ag->active_dihedrals = calloc(1, sizeof(struct mol_dihedral_list));
	ag->active_impropers = calloc(1, sizeof(struct mol_improper_list));
}

void mol_fixed_update_active_lists(struct mol_atom_group *ag)
{
	size_t m;

	if (ag->active_atoms->size > 0) {
		free_if_not_null(ag->active_atoms->members);
		ag->active_atoms->members = NULL;
		ag->active_atoms->size = 0;
	}

	if (ag->active_bonds->size > 0) {
		free_if_not_null(ag->active_bonds->members);
		ag->active_bonds->members = NULL;
		ag->active_bonds->size = 0;
	}

	if (ag->active_angles->size > 0) {
		free_if_not_null(ag->active_angles->members);
		ag->active_angles->members = NULL;
		ag->active_angles->size = 0;
	}

	if (ag->active_dihedrals->size > 0) {
		free_if_not_null(ag->active_dihedrals->members);
		ag->active_dihedrals->members = NULL;
		ag->active_dihedrals->size = 0;
	}

	if (ag->active_impropers->size > 0) {
		free_if_not_null(ag->active_dihedrals->members);
		ag->active_dihedrals->members = NULL;
		ag->active_impropers->size = 0;
	}

// atoms
	m = 0;
	ag->active_atoms->members = malloc(ag->natoms * sizeof(size_t));
	for (size_t i = 0; i < ag->natoms; i++) {
		if (!ag->fixed[i]) {
			ag->active_atoms->members[m++] = i;
		}
	}
	ag->active_atoms->size = m;
	if (m > 0) {
		ag->active_atoms->members =
			realloc(ag->active_atoms->members, m * sizeof(size_t *));
	} else {
		free(ag->active_atoms->members);
		ag->active_atoms->members = NULL;
	}

// bonds
	m = 0;
	ag->active_bonds->members = malloc(ag->nbonds * sizeof(struct mol_bond *));
	for (size_t i = 0; i < ag->nbonds; i++) {
		size_t ai = ag->bonds[i].ai;
		size_t aj = ag->bonds[i].aj;
		if (ag->fixed[aj] && ag->fixed[ai])
			continue;
		ag->active_bonds->members[m++] = &(ag->bonds[i]);
	}
	ag->active_bonds->size = m;
	if (m > 0) {
		ag->active_bonds->members =
			realloc(ag->active_bonds->members, m * sizeof(struct mol_bond *));
	} else {
		free(ag->active_bonds->members);
		ag->active_bonds->members = NULL;
	}

// angles
	m = 0;
	ag->active_angles->members = malloc(ag->nangles * sizeof(struct mol_angle *));
	for (size_t i = 0; i < ag->nangles; i++) {
		size_t a0 = ag->angles[i].a0;
		size_t a1 = ag->angles[i].a1;
		size_t a2 = ag->angles[i].a2;
		if (ag->fixed[a0] && ag->fixed[a1] && ag->fixed[a2])
			continue;
		ag->active_angles->members[m++] = &(ag->angles[i]);
	}
	ag->active_angles->size = m;
	if (m > 0) {
		ag->active_angles->members =
			realloc(ag->active_angles->members, m * sizeof(struct mol_angle *));
	} else {
		free(ag->active_angles->members);
		ag->active_angles->members = NULL;
	}

// dihedrals
	m = 0;
	ag->active_dihedrals->members =
		malloc(ag->ndihedrals * sizeof(struct mol_dihedral *));
	for (size_t i = 0; i < ag->ndihedrals; i++) {
		size_t a0 = ag->dihedrals[i].a0;
		size_t a1 = ag->dihedrals[i].a1;
		size_t a2 = ag->dihedrals[i].a2;
		size_t a3 = ag->dihedrals[i].a3;
		if (ag->fixed[a0] && ag->fixed[a1] && ag->fixed[a2] && ag->fixed[a3])
			continue;
		ag->active_dihedrals->members[m++] = &(ag->dihedrals[i]);
	}
	ag->active_dihedrals->size = m;
	if (m > 0) {
		ag->active_dihedrals->members =
			realloc(ag->active_dihedrals->members,
				m * sizeof(struct mol_dihedral *));
	} else {
		free(ag->active_dihedrals->members);
		ag->active_dihedrals->members = NULL;
	}

// impropers
	m = 0;
	ag->active_impropers->members = malloc(ag->nimpropers * sizeof(struct mol_improper *));
	for (size_t i = 0; i < ag->nimpropers; i++) {
		size_t a0 = ag->impropers[i].a0;
		size_t a1 = ag->impropers[i].a1;
		size_t a2 = ag->impropers[i].a2;
		size_t a3 = ag->impropers[i].a3;
		if (ag->fixed[a0] && ag->fixed[a1] && ag->fixed[a2] && ag->fixed[a3])
			continue;
		ag->active_impropers->members[m++] = &(ag->impropers[i]);
	}
	ag->active_impropers->size = m;
	if (m > 0) {
		ag->active_impropers->members =
			realloc(ag->active_impropers->members,
				m * sizeof(struct mol_improper *));
	} else {
		free(ag->active_impropers->members);
		ag->active_impropers->members = NULL;
	}
}

void mol_fixed_update(struct mol_atom_group *ag, size_t nlist, size_t *list)
{
	memset(ag->fixed, 0, ag->natoms*sizeof(bool));
	for (size_t i = 0; i < nlist; i++) {
                ag->fixed[list[i]] = true;
	}
	mol_fixed_update_active_lists(ag);
}

void mol_fixed_update_with_list(struct mol_atom_group* ag, const struct mol_index_list* fixed_list)
{
	mol_fixed_update(ag, fixed_list->size, fixed_list->members);
}

bool *mol_gaps(struct mol_atom_group *ag)
{
	const double gap_cutoff = 1.6; //max allowed distance between C and N
	const double gap_cutoff_sq = gap_cutoff * gap_cutoff;
	size_t sequence_len_guess = ag->residues->n_occupied;
	size_t len = 0;
	bool *gaps = malloc(sequence_len_guess * sizeof(bool));
	for (size_t i = 0; i < ag->natoms;) {
		if (len == sequence_len_guess) {
			sequence_len_guess *= 1.5;
			gaps = realloc(gaps, sequence_len_guess
					   * sizeof(bool));
		}

		struct mol_residue *res_cur = mol_atom_residue(ag, i);
		size_t cur_i = i;
		i = res_cur->atom_end + 1;
		struct mol_residue *res_next = mol_atom_residue(ag, i);
		if (res_next == NULL) {
			gaps[len++] = true;
			continue;
		}

		//find C in res_cur
		bool found_C = false;
		struct mol_vector3 C_coords = {INFINITY, INFINITY, INFINITY};
		for (size_t j = res_cur->atom_start; j <= res_cur->atom_end; j++) {
			if (strcmp(ag->atom_name[j], " C  ") == 0) {
				found_C = true;
				C_coords = ag->coords[j];
			}
		}
		//find N in res_next
		bool found_N = false;
		struct mol_vector3 N_coords = {INFINITY, INFINITY, INFINITY};
		for (size_t j = res_next->atom_start; j <= res_next->atom_end; j++) {
			if (strcmp(ag->atom_name[j], " N  ") == 0) {
				found_N = true;
				N_coords = ag->coords[j];
			}
		}

		if (!found_C || !found_N) {
			//TODO: smarter effort to determine if there is no
			//gap based on other atom positions
			gaps[len++] = true;
			fprintf(stderr, "Did not find C or N %zu\n", cur_i);
			continue;
		}

		double dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(C_coords, N_coords);
		if (dist_sq > gap_cutoff_sq) {
			gaps[len++] = true;
		} else {
			gaps[len++] = false;
		}
	}

	gaps = realloc(gaps, len * sizeof(bool));

	return gaps;

}

#undef __CALLOC_IF_NN
#undef __REALLOC_IF_NN

//deprecated
struct mol_atom_group *new_mol_atom_group(void)
{
	fprintf(stderr, "[Warning] Library function 'new_mol_atom_group()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_create()' instead.\n");
	return mol_atom_group_create();
}

struct mol_atom_group_list *new_mol_atom_groups(size_t n)
{
	fprintf(stderr, "[Warning] Library function 'new_mol_atom_groups()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_list_create()' instead.\n");
	return mol_atom_group_list_create(n);
}

void destroy_mol_atom_group(struct mol_atom_group *ag)
{
	fprintf(stderr, "[Warning] Library function 'destroy_mol_atom_group()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_destroy()' instead.\n");
	mol_atom_group_destroy(ag);
}

void free_mol_atom_group(struct mol_atom_group *ag)
{
	fprintf(stderr, "[Warning] Library function 'free_mol_atom_group()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_free()' instead.\n");
	mol_atom_group_free(ag);
}

void destroy_mol_atom_group_list(struct mol_atom_group_list *ag_list)
{
	fprintf(stderr, "[Warning] Library function 'destroy_mol_atom_group_list()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_list_destroy()' instead.\n");
	mol_atom_group_list_destroy(ag_list);
}

void free_mol_atom_group_list(struct mol_atom_group_list *ag_list)
{
	fprintf(stderr, "[Warning] Library function 'free_mol_atom_group_list()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_atom_group_list_free()' instead.\n");
	mol_atom_group_list_free(ag_list);
}

void zero_grads(struct mol_atom_group *ag)
{
	fprintf(stderr, "[Warning] Library function 'zero_grads()' has been deprecated.\n");
	fprintf(stderr, "[Warning] Use 'mol_zero_gradients()' instead.\n");
	mol_zero_gradients(ag);
}
