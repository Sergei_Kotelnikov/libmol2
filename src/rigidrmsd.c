#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "rigidrmsd.h"
#include "transform.h"

//! If the RMSD^2 is between this value and zero, round it to zero. Otherwize, take a square root and let the user deal with NaN.
static const double NEG_RMSD_THRESHOLD = -1e-2;


struct mol_rrmsd_atom_group *mol_rrmsd_atom_group_create(
		const struct mol_atom_group *ag,
		const struct mol_index_list *interface)
{
	if (ag == NULL) {
		fprintf(stderr, "[error] mol_rrmsd_atom_group_create must have non-NULL parameter ag\n");
		exit(EXIT_FAILURE);
	}
	struct mol_rrmsd_atom_group *rag = calloc(1, sizeof(struct mol_rrmsd_atom_group));
	mol_rrmsd_atom_group_update(rag, ag, interface);
	return rag;
}


void mol_rrmsd_atom_group_destroy(struct mol_rrmsd_atom_group *rag)
{
	if (rag) {
		// Do nothing
	}
}


void mol_rrmsd_atom_group_free(struct mol_rrmsd_atom_group *rag)
{
	if (rag != NULL) {
		mol_rrmsd_atom_group_destroy(rag);
		free(rag);
	}
}


/**
 * Find the vector from centroid of original molecule to centroid of transformed molecule.
 *
 * See Eq. (22), with minor optimization.
 */
static void _change_frame(
		struct mol_vector3 *c_com,
		const struct mol_vector3 *c_world,
		const struct mol_matrix3 *r,
		const struct mol_vector3 *tv)
{
	struct mol_matrix3 rm;
	MOL_MATRIX_COPY(rm, *r);
	rm.m11 -= 1;
	rm.m22 -= 1;
	rm.m33 -= 1;
	MOL_VEC_COPY(*c_com, *c_world);
	mol_vector3_move(c_com, &rm, tv);
}


/**
 * Find the vector from centroid of original molecule to centroid of transformed molecule.
 *
 * See Eq. (23).
 */
static void _change_frame_q(
		struct mol_vector3 *c_com,
		const struct mol_vector3 *c_world,
		const struct mol_quaternion *q,
		const struct mol_vector3 *tv)
{
	MOL_VEC_COPY(*c_com, *c_world);
	mol_vector3_rotate_by_quaternion(c_com, q);
	MOL_VEC_ADD(*c_com, *c_com, *tv);
	MOL_VEC_SUB(*c_com, *c_com, *c_world);
}


/**
 * Calculate rotational component of RMSD^2 in CoM reference frame.
 *
 * See Eq. (25)
 */
static double _rotational_component(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_matrix3 *r)
{
	double rmsd2 = 0;
	rmsd2 += (1-r->m11)*rag->xdiag.X;
	rmsd2 += (1-r->m22)*rag->xdiag.Y;
	rmsd2 += (1-r->m33)*rag->xdiag.Z;

	rmsd2 += (r->m12+r->m21)*rag->inertia.m12;
	rmsd2 += (r->m13+r->m31)*rag->inertia.m13;
	rmsd2 += (r->m23+r->m32)*rag->inertia.m23;

	return rmsd2 / 2.0;  // xdiag and intertia are already scaled
}


/**
 * Calculate rotational component of RMSD^2 in CoM reference frame.
 *
 * See Eq. (24)
 */
static double _rotational_component_q(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_quaternion *q)
{
	double rmsd2 = 0;
	rmsd2 += q->X*q->Y*rag->inertia.m12;
	rmsd2 += q->X*q->Z*rag->inertia.m31;
	rmsd2 += q->Y*q->Z*rag->inertia.m23;

	rmsd2 *= 2.0;

	rmsd2 += q->X*q->X*rag->inertia.m11;
	rmsd2 += q->Y*q->Y*rag->inertia.m22;
	rmsd2 += q->Z*q->Z*rag->inertia.m33;

	return rmsd2;
}


static double _safe_sqrt(const double x)
{
	if (x >= 0) {
		return sqrt(x);
	} else if (x > NEG_RMSD_THRESHOLD) {
		return 0;
	} else {
		fprintf(stderr, "[error] Obtained negative RMSD^2\n");
		exit(EXIT_FAILURE);
	}
}


double mol_rrmsd_rmsd(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_matrix3 *r,
		const struct mol_vector3 *tv)
{
	struct mol_vector3 c_com;
	_change_frame(&c_com, &rag->centroid, r, tv);

	double rmsd2 = _rotational_component(rag, r);

	//translational component
	rmsd2 += MOL_VEC_SQ_NORM(c_com);

	return _safe_sqrt(rmsd2);
}


double mol_rrmsd_rmsd_q(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_quaternion *q,
		const struct mol_vector3 *tv)
{
	struct mol_vector3 c_com;
	_change_frame_q(&c_com, &rag->centroid, q, tv);

	// rotational component
	double rmsd2 = _rotational_component_q(rag, q);

	// translational component
	rmsd2 += MOL_VEC_SQ_NORM(c_com);

	return _safe_sqrt(rmsd2);
}


// Calculate B.T * A
#define _RMSD_TWO_MATRICES(DST, A, B) do                                 \
{                                                                        \
	(DST).m11 = (A).m11*(B).m11 + (A).m21*(B).m21 + (A).m31*(B).m31; \
	(DST).m22 = (A).m12*(B).m12 + (A).m22*(B).m22 + (A).m32*(B).m32; \
	(DST).m33 = (A).m13*(B).m13 + (A).m23*(B).m23 + (A).m33*(B).m33; \
	                                                                 \
	(DST).m12 = (A).m11*(B).m12 + (A).m21*(B).m22 + (A).m31*(B).m32; \
	(DST).m21 = (A).m12*(B).m11 + (A).m22*(B).m21 + (A).m32*(B).m31; \
	                                                                 \
	(DST).m13 = (A).m11*(B).m13 + (A).m21*(B).m23 + (A).m31*(B).m33; \
	(DST).m31 = (A).m13*(B).m11 + (A).m23*(B).m21 + (A).m33*(B).m31; \
	                                                                 \
	(DST).m23 = (A).m12*(B).m13 + (A).m22*(B).m23 + (A).m32*(B).m33; \
	(DST).m32 = (A).m13*(B).m12 + (A).m23*(B).m22 + (A).m33*(B).m32; \
} while(0)


double mol_rrmsd_rmsd_pairwise(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_matrix3 *r1,
		const struct mol_vector3 *tv1,
		const struct mol_matrix3 *r2,
		const struct mol_vector3 *tv2)
{
	struct mol_vector3 c1_com, c2_com;

	_change_frame(&c1_com, &rag->centroid, r1, tv1);
	_change_frame(&c2_com, &rag->centroid, r2, tv2);

	struct mol_matrix3 r;
	_RMSD_TWO_MATRICES(r, *r1, *r2);

	double rmsd2 = _rotational_component(rag, &r);

	// translational component
	rmsd2 += MOL_VEC_EUCLIDEAN_DIST_SQ(c1_com, c2_com);

	return _safe_sqrt(rmsd2);
}
#undef _RMSD_TWO_MATRICES


double mol_rrmsd_rmsd_pairwise_q(
		const struct mol_rrmsd_atom_group *rag,
		const struct mol_quaternion *q1,
		const struct mol_vector3 *tv1,
		const struct mol_quaternion *q2,
		const struct mol_vector3 *tv2)
{
	struct mol_vector3 c1_com, c2_com;

	_change_frame_q(&c1_com, &rag->centroid, q1, tv1);
	_change_frame_q(&c2_com, &rag->centroid, q2, tv2);

	struct mol_quaternion q, q2_inv;
	mol_quaternion_inverse(q2, &q2_inv);
	mol_quaternion_product(&q2_inv, q1, &q);

	double rmsd2 = _rotational_component_q(rag, &q);

	// translational component
	rmsd2 += MOL_VEC_EUCLIDEAN_DIST_SQ(c1_com, c2_com);

	return _safe_sqrt(rmsd2);
}


/**
 * Calculate centroid of \p ag (with optional \p interface) and save it to \rag.
 */
static void _update_centroid(
		struct mol_rrmsd_atom_group *rag,
		const struct mol_atom_group *ag,
		const struct mol_index_list *interface)
{
	if (interface == NULL) {
		centroid(&rag->centroid, ag);
	} else {
		MOL_VEC_SET_SCALAR(rag->centroid, 0);
		const size_t *members = interface->members;
		const size_t natoms = interface->size;

		for (size_t i = 0; i < natoms; i++)
			MOL_VEC_ADD(rag->centroid, rag->centroid, ag->coords[members[i]]);

		MOL_VEC_DIV_SCALAR(rag->centroid, rag->centroid, natoms);
	}
}


/**
 * Calculate inertia tensor and xdiag of \p ag (with optional \p interface) and save it to \rag.
 *
 * See Eqs. (13), (18).
 *
 * Both I and X are computed in CoM reference frame.
 * They are scaled by 4/natoms to speed up later calculations.
 */
static void _update_inertia(
		struct mol_rrmsd_atom_group *rag,
		const struct mol_atom_group *ag,
		const struct mol_index_list *interface)
{
	const bool interface_mode = (interface != NULL);

	memset(&rag->inertia, 0, sizeof(struct mol_matrix3));
	memset(&rag->xdiag, 0, sizeof(struct mol_vector3));

	const struct mol_vector3 *coords = ag->coords;

	size_t natoms;
	if (interface_mode)
		natoms = interface->size;
	else
		natoms = ag->natoms;

	for (size_t i = 0; i < natoms; i++) {
		struct mol_vector3 coords_i;

		if (interface_mode)
			MOL_VEC_COPY(coords_i, coords[interface->members[i]]);
		else
			MOL_VEC_COPY(coords_i, coords[i]);

		MOL_VEC_SUB(coords_i, coords_i, rag->centroid);

		rag->xdiag.X += coords_i.X*coords_i.X;
		rag->xdiag.Y += coords_i.Y*coords_i.Y;
		rag->xdiag.Z += coords_i.Z*coords_i.Z;

		rag->inertia.m12 -= coords_i.X*coords_i.Y;
		rag->inertia.m13 -= coords_i.X*coords_i.Z;
		rag->inertia.m23 -= coords_i.Y*coords_i.Z;
	}

	MOL_VEC_MULT_SCALAR(rag->xdiag, rag->xdiag, 4.0 / natoms);

	rag->inertia.m11 = rag->xdiag.Y + rag->xdiag.Z;
	rag->inertia.m22 = rag->xdiag.X + rag->xdiag.Z;
	rag->inertia.m33 = rag->xdiag.X + rag->xdiag.Y;

	rag->inertia.m12 *= 4.0 / natoms;
	rag->inertia.m13 *= 4.0 / natoms;
	rag->inertia.m23 *= 4.0 / natoms;

	rag->inertia.m21 = rag->inertia.m12;
	rag->inertia.m31 = rag->inertia.m13;
	rag->inertia.m32 = rag->inertia.m23;
}


void mol_rrmsd_atom_group_update(
		struct mol_rrmsd_atom_group *rag,
		const struct mol_atom_group *ag,
		const struct mol_index_list *interface)
{
	_update_centroid(rag, ag, interface);
	_update_inertia(rag, ag, interface);
}
