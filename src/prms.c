#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "atom_group.h"
#include "prms.h"

#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define VERSION_KEY "version"
#define ATOM_KEY "atom"
#define PWPOT_KEY "pwpot"
#define PWPOT_R_KEY "pwpot.r"

static mol_version mol_prms_version(FILE *fp);
static void mol_read_atoms_prms(struct mol_prms *prms, FILE *fp);
static void mol_read_pwpot_prms(struct mol_prms *prms, FILE *fp);

static size_t count_lines(FILE *fp, const char *line_key)
{
	size_t nlines = 0;

	char *line = NULL;
	size_t len = 0;
	char *key;

	rewind(fp);
	while (getline(&line, &len, fp) != -1) {
		if (is_whitespace_line(line))
			continue;

		key = strtok(line, " \t");

		if (strcmp(key, line_key) == 0) {
			++nlines;
		}
	}

	free_if_not_null(line);
	rewind(fp);

	return nlines;
}

static int count_nsubatoms(FILE *fp)
{
	int nsubatoms = 0;

	char *line = NULL;
	char *tmp_line;
	size_t len = 0;
	int n;

	rewind(fp);
	while (getline(&line, &len, fp) != -1) {
		if (is_whitespace_line(line))
			continue;

		// Hack to get around the fact that PWPOT_KEY will also match "pwpot.r"
		if (strncmp(line, PWPOT_KEY, strlen(PWPOT_KEY)) == 0 &&
		    line[strlen(PWPOT_KEY)] != '.') {
			tmp_line = line;

			if (sscanf(tmp_line, "%*s %*f %n", &n) != 0) {
				exit(EXIT_FAILURE);
			}

			tmp_line += n;

			while (sscanf(tmp_line, "%*f %n",
				      &n) == 0) {
				nsubatoms += 1;
				tmp_line += n;
			}

			break;
		}
	}

	free_if_not_null(line);
	rewind(fp);

	return nsubatoms;
}

static int comp_mol_atom_prm(const void *a1, const void *a2)
{
	const struct mol_atom_prm *atom1 = (const struct mol_atom_prm *) a1;
	const struct mol_atom_prm *atom2 = (const struct mol_atom_prm *) a2;

	int cmp1 = strcmp(atom1->typemaj, atom2->typemaj);

	return (cmp1 != 0) ? cmp1 : strcmp(atom1->typemin, atom2->typemin);
}

static int atomid(const struct mol_prms *prms, const char *typemaj, const char *typemin)
{
	struct mol_atom_prm atomkey;
	struct mol_atom_prm *atomres;

	while (typemaj[0] == ' ') {
		typemaj++;
	}
	while (typemin[0] == ' ') {
		typemin++;
	}

	strncpy(atomkey.typemaj, typemaj, 5);
	strncpy(atomkey.typemin, typemin, 5);

	while (atomkey.typemaj[strlen(atomkey.typemaj)-1] == ' ') {
		atomkey.typemaj[strlen(atomkey.typemaj)-1] = '\0';
	}

	while (atomkey.typemin[strlen(atomkey.typemin)-1] == ' ') {
		atomkey.typemin[strlen(atomkey.typemin)-1] = '\0';
	}

	atomres = bsearch(&atomkey, prms->atoms, prms->natoms,
			sizeof(struct mol_atom_prm), comp_mol_atom_prm);

	if (atomres == NULL) {
		fprintf(stderr, "typemaj %s\n", atomkey.typemaj);
		fprintf(stderr, "typemin %s\n", atomkey.typemin);
		return -1;
	} else {
		return atomres->id;
	}
}

struct mol_prms *mol_prms_read(const char *path)
{
	FILE *fp = fopen(path, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", path);
		return NULL;
	}

	struct mol_prms *prms = calloc(1, sizeof(struct mol_prms));
	if (prms == NULL) {
		goto close_file_out;
	}

	prms->version = mol_prms_version(fp);

	mol_read_atoms_prms(prms, fp);
	mol_read_pwpot_prms(prms, fp);

close_file_out:
	fclose(fp);

	return prms;
}

static mol_version mol_prms_version(FILE *fp)
{
	if (fp == NULL) {
		return NULL;
	}

	mol_version version_str = calloc(128, sizeof(char));

	char *line = NULL;
	size_t len = 0;
	bool found_version = false;

	while (getline(&line, &len, fp) != -1) {
		// relying on C string constant concatenation
		if (sscanf(line, VERSION_KEY " %80s", version_str) < 1) {
			continue;
		} else {
			found_version = true;
			break;
		}
	}

	free_if_not_null(line);

	if (!found_version) {
		free_if_not_null(version_str);
		return NULL;
	}

	version_str = realloc(version_str, (strlen(version_str)+1) * sizeof(char));
	rewind(fp);

	return version_str;
}

static void mol_read_atoms_prms(struct mol_prms *prms, FILE *fp)
{
	if (fp == NULL) {
		return;
	}

	char *line = NULL;
	size_t len = 0;

	prms->natoms = count_lines(fp, ATOM_KEY);

	prms->atoms = calloc(prms->natoms, sizeof(struct mol_atom_prm));

	size_t atomsi = 0;
	while (getline(&line, &len, fp) != -1) {
		if (is_whitespace_line(line))
			continue;

		if (strncmp(line, ATOM_KEY, strlen(ATOM_KEY)) == 0) {
			struct mol_atom_prm *atom_prm = &prms->atoms[atomsi];

			if (sscanf(line, "%*s %d %7s %7s %d %lf %lf",
					&atom_prm->id,
					atom_prm->typemaj,
					atom_prm->typemin,
					&atom_prm->subid,
					&atom_prm->r,
					&atom_prm->q) < 6) {
				return;
			}

			++atomsi;
		}
		if (atomsi == prms->natoms) {
			break;
		}
	}

	free_if_not_null(line);
	rewind(fp);

	qsort(prms->atoms, prms->natoms, sizeof(struct mol_atom_prm),
		comp_mol_atom_prm);
	for (size_t i = 0; i < prms->natoms; ++i) {
		prms->atoms[i].id = i;
	}
}

static void mol_read_pwpot_prms(struct mol_prms *prms, FILE *fp)
{
	if (fp == NULL) {
		return;
	}

	struct mol_pwpot_prm *pwpot = calloc(1, sizeof(struct mol_pwpot_prm));

	pwpot->k = count_lines(fp, PWPOT_KEY);
	prms->nsubatoms = count_nsubatoms(fp);
	pwpot->lambdas = malloc(sizeof(float) * pwpot->k);
	pwpot->Xs = malloc(sizeof(float) * prms->nsubatoms * pwpot->k);
	pwpot->eng = malloc(sizeof(float *) * prms->nsubatoms);
	pwpot->eng_data = calloc(sizeof(float), prms->nsubatoms * prms->nsubatoms);
	for (int i = 0; i < prms->nsubatoms; ++i) {
		pwpot->eng[i] = &pwpot->eng_data[i*prms->nsubatoms];
	}

	char *line = NULL;
	size_t len = 0;
	char *tmp_line;

	int n;
	size_t lambdasi = 0;
	size_t Xsi = 0;

	while (getline(&line, &len, fp) != -1) {
		if (is_whitespace_line(line))
			continue;

		if (strncmp(line, PWPOT_R_KEY, strlen(PWPOT_R_KEY)) == 0) {
			sscanf(line, "%*s %f %f",
				&pwpot->r1,
				&pwpot->r2);
		} else if (strncmp(line, PWPOT_KEY, strlen(PWPOT_KEY)) == 0) {
			tmp_line = line;

			if (sscanf(tmp_line, "%*s %f %n",
					&pwpot->lambdas[lambdasi], &n) != 1) {
				exit(EXIT_FAILURE);
			}

			++lambdasi;
			tmp_line += n;

			for (int j = 0; j < prms->nsubatoms; ++j) {
				sscanf(tmp_line, "%f %n",
					&pwpot->Xs[(Xsi * prms->nsubatoms) + j], &n);

				tmp_line += n;
			}

			Xsi++;
		}
	}

	free_if_not_null(line);

	for (int i = 0; i < prms->nsubatoms; ++i) {
		for (int j = 0; j < prms->nsubatoms; j++) {
			for (size_t k = 0; k < pwpot->k; ++k) {
				pwpot->eng[i][j] +=
					pwpot->lambdas[k] *
					pwpot->Xs[(k * prms->nsubatoms) + i] *
					pwpot->Xs[(k * prms->nsubatoms) + j];
			}
		}
	}

	rewind(fp);

	prms->pwpot = pwpot;
}

void mol_prms_destroy(struct mol_prms *prms)
{
	if (prms != NULL) {
		free_if_not_null(prms->atoms);
		if (prms->pwpot != NULL) {
			free_if_not_null(prms->pwpot->lambdas);
			free_if_not_null(prms->pwpot->Xs);
			free_if_not_null(prms->pwpot->eng);
			free_if_not_null(prms->pwpot->eng_data);
			free(prms->pwpot);
		}
		free_if_not_null(prms->version);
	}
}

void mol_prms_free(struct mol_prms *prms)
{
	mol_prms_destroy(prms);
	free_if_not_null(prms);
}

void print_prms(const struct mol_prms *prms)
{
	// We call this pragma's to temporarily disable deprecation warning here.
	// This function and fprint_prms are "twins", and there's nothing bad that one
	// calls the other.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	fprint_prms(stdout, prms);         /* no diagnostic for this one */
#pragma GCC diagnostic pop
}

void fprint_prms(FILE *fp, const struct mol_prms *prms)
{
	if (fp == NULL)
		return;

	fprintf(fp, "version: %s\n", prms->version);
	fprintf(fp, "== atoms ==\n");
	fprintf(fp, "natoms: %zu\n", prms->natoms);
	fprintf(fp, "nsubatoms: %zu\n", prms->pwpot->k);
	fprintf(fp, "\n");

	fprintf(fp, "== pwpot ==\n");
	fprintf(fp, "r1: %f\t r2: %f\n", prms->pwpot->r1, prms->pwpot->r2);
	fprintf(fp, "k: %zu\n", prms->pwpot->k);
	fprintf(fp, "\n");
}

void mol_atom_group_add_prms(struct mol_atom_group *ag, const struct mol_prms *prms)
{
	if (ag->charge == NULL) {
		ag->charge = malloc(ag->natoms * sizeof(double));
	}
	if (ag->vdw_radius == NULL) {
		ag->vdw_radius = malloc(ag->natoms * sizeof(double));
	}
	if (ag->pwpot_id == NULL) {
		ag->pwpot_id = malloc(ag->natoms * sizeof(int));
	}

	for (size_t i = 0; i < ag->natoms; i++) {
		int id = atomid(prms, ag->residue_name[i], ag->atom_name[i]);
		if (id == -1) {
			fprintf(stderr, "prm type not found for atom %zu\n", i);
			ag->charge[i] = 0.0;
			ag->vdw_radius[i] = 0.0;
			ag->pwpot_id[i] = -1;
			continue;
		}

		ag->charge[i] = prms->atoms[id].q;
		ag->vdw_radius[i] = prms->atoms[id].r;
		ag->pwpot_id[i] = prms->atoms[id].subid;
	}
}


struct mol_prms *read_prms(const char *path)
{
	fprintf(stderr, "[Warning] Library function 'read_prms()' has been deprecated. Use 'mol_prms_read()' instead\n");
	return mol_prms_read(path);
}


void destroy_prms(struct mol_prms *prms)
{
	fprintf(stderr, "[Warning] Library function 'destroy_prms()' has been deprecated. Use 'mol_prms_destroy()' instead\n");
	mol_prms_destroy(prms);
}

void free_prms(struct mol_prms *prms)
{
	fprintf(stderr, "[Warning] Library function 'free_prms()' has been deprecated. Use 'mol_prms_free()' instead\n");
	mol_prms_free(prms);
}