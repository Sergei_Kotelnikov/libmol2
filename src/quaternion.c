#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define _USE_MATH_DEFINES
#include "quaternion.h"
#include "utils.h"

#include <math.h>
#include <stdio.h>
#include <stdbool.h>

struct mol_quaternion *mol_quaternion_create(void)
{
	return calloc(1, sizeof(struct mol_quaternion));
}

struct mol_quaternionf *mol_quaternionf_create(void)
{
	return calloc(1, sizeof(struct mol_quaternionf));
}

struct mol_quaternionl *mol_quaternionl_create(void)
{
	return calloc(1, sizeof(struct mol_quaternionl));
}

struct mol_quaternion_list *mol_quaternion_list_create(const size_t n)
{
	struct mol_quaternion_list *list =
	     malloc(sizeof(struct mol_quaternion_list));
	list->size = n;
	list->members = calloc(n, sizeof(struct mol_quaternion));
	return list;
}

struct mol_quaternionf_list *mol_quaternionf_list_create(const size_t n)
{
	struct mol_quaternionf_list *list =
	     malloc(sizeof(struct mol_quaternionf_list));
	list->size = n;
	list->members = calloc(n, sizeof(struct mol_quaternionf));
	return list;
}

struct mol_quaternionl_list *mol_quaternionl_list_create(const size_t n)
{
	struct mol_quaternionl_list *list =
	     malloc(sizeof(struct mol_quaternionl_list));
	list->size = n;
	list->members = calloc(n, sizeof(struct mol_quaternionl));
	return list;
}

static struct mol_quaternion_list *list_from_file(const char *filename,
						   const bool normalize)
{
	struct mol_quaternion_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "[Error] No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = mol_quaternion_list_create(nrots);

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		sscanf(line, "%lf %lf %lf %lf",
		       &rotset->members[i].W,
		       &rotset->members[i].X,
		       &rotset->members[i].Y,
		       &rotset->members[i].Z);
		if (normalize) {
			mol_quaternion_norm(&(rotset->members[i]), rotset->members[i]);
		}
		++i;
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}

struct mol_quaternion_list *mol_quaternion_list_from_file(const char
							    *filename)
{
	return list_from_file(filename, true);
}

struct mol_quaternion_list *mol_quaternion_list_from_file_unnormalized(const char
									 *filename)
{
	return list_from_file(filename, false);
}

static struct mol_quaternionf_list *listf_from_file(const char *filename,
						   const bool normalize)
{
	struct mol_quaternionf_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "[Error] No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = mol_quaternionf_list_create(nrots);

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		sscanf(line, "%f %f %f %f",
		       &rotset->members[i].W,
		       &rotset->members[i].X,
		       &rotset->members[i].Y,
		       &rotset->members[i].Z);
		if (normalize) {
			mol_quaternionf_norm(&(rotset->members[i]), rotset->members[i]);
		}
		++i;
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}

struct mol_quaternionf_list *mol_quaternionf_list_from_file(const char
							    *filename)
{
	return listf_from_file(filename, true);
}

struct mol_quaternionf_list *mol_quaternionf_list_from_file_unnormalized(const char
									 *filename)
{
	return listf_from_file(filename, false);
}

static struct mol_quaternionl_list *listl_from_file(const char *filename,
						   const bool normalize)
{
	struct mol_quaternionl_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "[Error] No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = mol_quaternionl_list_create(nrots);

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		sscanf(line, "%Lf %Lf %Lf %Lf",
		       &rotset->members[i].W,
		       &rotset->members[i].X,
		       &rotset->members[i].Y,
		       &rotset->members[i].Z);
		if (normalize) {
			mol_quaternionl_norm(&(rotset->members[i]), rotset->members[i]);
		}
		++i;
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}

struct mol_quaternionl_list *mol_quaternionl_list_from_file(const char
							    *filename)
{
	return listl_from_file(filename, true);
}

struct mol_quaternionl_list *mol_quaternionl_list_from_file_unnormalized(const char
									 *filename)
{
	return listl_from_file(filename, false);
}

void mol_quaternion_free(struct mol_quaternion *q)
{
	free(q);
}

void mol_quaternionf_free(struct mol_quaternionf *q)
{
	free(q);
}

void mol_quaternionl_free(struct mol_quaternionl *q)
{
	free(q);
}

double mol_quaternion_length(const struct mol_quaternion q)
{
	return hypot(hypot(q.W, q.X), hypot(q.Y, q.Z));
}

float mol_quaternionf_length(const struct mol_quaternionf q)
{
	return hypotf(hypotf(q.W, q.X), hypotf(q.Y, q.Z));
}

long double mol_quaternionl_length(const struct mol_quaternionl q)
{
	return hypotl(hypotl(q.W, q.X), hypotl(q.Y, q.Z));
}

void mol_quaternion_norm(struct mol_quaternion *p,
			 const struct mol_quaternion q)
{
	double l = mol_quaternion_length(q);
	p->W = q.W / l;
	p->X = q.X / l;
	p->Y = q.Y / l;
	p->Z = q.Z / l;
}

void mol_quaternionf_norm(struct mol_quaternionf *p,
			  const struct mol_quaternionf q)
{
	float l = mol_quaternionf_length(q);
	p->W = q.W / l;
	p->X = q.X / l;
	p->Y = q.Y / l;
	p->Z = q.Z / l;
}

void mol_quaternionl_norm(struct mol_quaternionl *p,
			  const struct mol_quaternionl q)
{
	long double l = mol_quaternionl_length(q);
	p->W = q.W / l;
	p->X = q.X / l;
	p->Y = q.Y / l;
	p->Z = q.Z / l;
}

/*
 * mol_matrix3_from_quaternion and  mol_quaternion_from_matrix3
 * are derived from this identity based on a normalized quaternion:
 *
 *     | 1-2y^2-2z^2    2xy-2zw      2xz+2yw   |
 * R = |   2xy+2zw    1-2x^2-2z^2    2yz-2xw   |
 *     |   2xz-2yw      2yz+2xw    1-2x^2-2y^2 |
 */
void mol_matrix3_from_quaternion(struct mol_matrix3 *m,
				 const struct mol_quaternion q)
{
	//here I intentionally use the unsafe method of squares rather than
	//hypot. If you wish to not risk overflow, you should normalize first
	double l2 = q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z;
	double s = (l2 == 0.0) ? 0.0 : 2.0 / l2;
	double wx = s * q.W * q.X;
	double wy = s * q.W * q.Y;
	double wz = s * q.W * q.Z;
	double xx = s * q.X * q.X;
	double xy = s * q.X * q.Y;
	double xz = s * q.X * q.Z;
	double yy = s * q.Y * q.Y;
	double yz = s * q.Y * q.Z;
	double zz = s * q.Z * q.Z;

	m->m11 = 1.0 - (yy + zz);
	m->m12 = xy - wz;
	m->m13 = xz + wy;
	m->m21 = xy + wz;
	m->m22 = 1.0 - (xx + zz);
	m->m23 = yz - wx;
	m->m31 = xz - wy;
	m->m32 = yz + wx;
	m->m33 = 1.0 - (xx + yy);
}

void mol_matrix3f_from_quaternionf(struct mol_matrix3f *m,
				   const struct mol_quaternionf q)
{
	float l2 = q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z;
	float s = (l2 == 0.0f) ? 0.0f : 2.0f / l2;
	float wx = s * q.W * q.X;
	float wy = s * q.W * q.Y;
	float wz = s * q.W * q.Z;
	float xx = s * q.X * q.X;
	float xy = s * q.X * q.Y;
	float xz = s * q.X * q.Z;
	float yy = s * q.Y * q.Y;
	float yz = s * q.Y * q.Z;
	float zz = s * q.Z * q.Z;

	m->m11 = 1.0f - (yy + zz);
	m->m12 = xy - wz;
	m->m13 = xz + wy;
	m->m21 = xy + wz;
	m->m22 = 1.0f - (xx + zz);
	m->m23 = yz - wx;
	m->m31 = xz - wy;
	m->m32 = yz + wx;
	m->m33 = 1.0f - (xx + yy);
}

void mol_matrix3l_from_quaternionl(struct mol_matrix3l *m,
				   const struct mol_quaternionl q)
{
	long double l2 = q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z;
	long double s = (l2 == 0.0l) ? 0.0l : 2.0l / l2;
	long double wx = s * q.W * q.X;
	long double wy = s * q.W * q.Y;
	long double wz = s * q.W * q.Z;
	long double xx = s * q.X * q.X;
	long double xy = s * q.X * q.Y;
	long double xz = s * q.X * q.Z;
	long double yy = s * q.Y * q.Y;
	long double yz = s * q.Y * q.Z;
	long double zz = s * q.Z * q.Z;

	m->m11 = 1.0l - (yy + zz);
	m->m12 = xy - wz;
	m->m13 = xz + wy;
	m->m21 = xy + wz;
	m->m22 = 1.0l - (xx + zz);
	m->m23 = yz - wx;
	m->m31 = xz - wy;
	m->m32 = yz + wx;
	m->m33 = 1.0l - (xx + yy);
}

/*
 * mol_quaternion_from_matrix3 assumes the matrix
 * has the properties of a rotation matrix
 *
 * If there is a reason to believe the matrix is not
 * orthogonal, you should implement Version 3 of
 * http://dx.doi.org/10.2514/2.4654
 */
void mol_quaternion_from_matrix3(struct mol_quaternion *q,
				 const struct mol_matrix3 m)
{
	double trace = m.m11 + m.m22 + m.m33;

	if (trace >= 0.0) {
		double w2 = sqrt(1.0 + trace);
		double s = 0.5 / w2;

		q->W = 0.5 * w2;
		q->X = (m.m32 - m.m23) * s;
		q->Y = (m.m13 - m.m31) * s;
		q->Z = (m.m21 - m.m12) * s;
		goto norm;
	}
	//trace negative, use techniques to reduce chance of sqrt(small number)
	//if calculating sqrt(small number, s will approach 0.5/0.0 with issues
	if ((m.m11 > m.m22) && (m.m11 > m.m33)) {	//m11 biggest diagonal element
		double x2 = sqrt(1.0 + m.m11 - m.m22 - m.m33);
		double s = 0.5 / x2;

		q->W = (m.m32 - m.m23) * s;
		q->X = 0.5 * x2;
		q->Y = (m.m12 + m.m21) * s;
		q->Z = (m.m13 + m.m31) * s;
		goto norm;
	}

	if (m.m22 > m.m33) {	//m22 biggest diagonal element
		double y2 = sqrt(1.0 + m.m22 - m.m11 - m.m33);
		double s = 0.5 / y2;

		q->W = (m.m13 - m.m31) * s;
		q->X = (m.m12 + m.m21) * s;
		q->Y = 0.5 * y2;
		q->Z = (m.m23 + m.m32) * s;
		goto norm;
	}
	//m33 biggest diagonal element
	double z2 = sqrt(1.0 + m.m33 - m.m11 - m.m22);
	double s = 0.5 / z2;

	q->W = (m.m21 - m.m12) * s;
	q->X = (m.m13 + m.m31) * s;
	q->Y = (m.m23 + m.m32) * s;
	q->Z = 0.5 * z2;

norm:
	mol_quaternion_norm(q,*q);
}

void mol_quaternionf_from_matrix3f(struct mol_quaternionf *q,
				   const struct mol_matrix3f m)
{
	float trace = m.m11 + m.m22 + m.m33;

	if (trace >= 0.0f) {
		float w2 = sqrtf(1.0f + trace);
		float s = 0.5f / w2;

		q->W = 0.5f * w2;
		q->X = (m.m32 - m.m23) * s;
		q->Y = (m.m13 - m.m31) * s;
		q->Z = (m.m21 - m.m12) * s;
		goto norm;
	}
	//trace negative, use techniques to reduce chance of sqrt(small number)
	//if calculating sqrt(small number, s will approach 0.5/0.0 with issues
	if ((m.m11 > m.m22) && (m.m11 > m.m33)) {	//m11 biggest diagonal element
		float x2 = sqrtf(1.0f + m.m11 - m.m22 - m.m33);
		float s = 0.5f / x2;

		q->W = (m.m32 - m.m23) * s;
		q->X = 0.5f * x2;
		q->Y = (m.m12 + m.m21) * s;
		q->Z = (m.m13 + m.m31) * s;
		goto norm;
	}

	if (m.m22 > m.m33) {	//m22 biggest diagonal element
		float y2 = sqrtf(1.0f + m.m22 - m.m11 - m.m33);
		float s = 0.5f / y2;

		q->W = (m.m13 - m.m31) * s;
		q->X = (m.m12 + m.m21) * s;
		q->Y = 0.5f * y2;
		q->Z = (m.m23 + m.m32) * s;
		goto norm;
	}
	//m33 biggest diagonal element
	float z2 = sqrtf(1.0f + m.m33 - m.m11 - m.m22);
	float s = 0.5f / z2;

	q->W = (m.m21 - m.m12) * s;
	q->X = (m.m13 + m.m31) * s;
	q->Y = (m.m23 + m.m32) * s;
	q->Z = 0.5f * z2;

norm:
	mol_quaternionf_norm(q,*q);
}

void mol_quaternionl_from_matrix3l(struct mol_quaternionl *q,
				   const struct mol_matrix3l m)
{
	long double trace = m.m11 + m.m22 + m.m33;

	if (trace >= 0.0l) {
		long double w2 = sqrtl(1.0l + trace);
		long double s = 0.5l / w2;

		q->W = 0.5l * w2;
		q->X = (m.m32 - m.m23) * s;
		q->Y = (m.m13 - m.m31) * s;
		q->Z = (m.m21 - m.m12) * s;
		goto norm;
	}
	//trace negative, use techniques to reduce chance of sqrt(small number)
	//if calculating sqrt(small number, s will approach 0.5/0.0 with issues
	if ((m.m11 > m.m22) && (m.m11 > m.m33)) {	//m11 biggest diagonal element
		long double x2 = sqrtl(1.0l + m.m11 - m.m22 - m.m33);
		long double s = 0.5l / x2;

		q->W = (m.m32 - m.m23) * s;
		q->X = 0.5l * x2;
		q->Y = (m.m12 + m.m21) * s;
		q->Z = (m.m13 + m.m31) * s;
		goto norm;
	}

	if (m.m22 > m.m33) {	//m22 biggest diagonal element
		long double y2 = sqrtl(1.0l + m.m22 - m.m11 - m.m33);
		long double s = 0.5l / y2;

		q->W = (m.m13 - m.m31) * s;
		q->X = (m.m12 + m.m21) * s;
		q->Y = 0.5l * y2;
		q->Z = (m.m23 + m.m32) * s;
		goto norm;
	}
	//m33 biggest diagonal element
	long double z2 = sqrtl(1.0l + m.m33 - m.m11 - m.m22);
	long double s = 0.5l / z2;

	q->W = (m.m21 - m.m12) * s;
	q->X = (m.m13 + m.m31) * s;
	q->Y = (m.m23 + m.m32) * s;
	q->Z = 0.5l * z2;

norm:
	mol_quaternionl_norm(q,*q);
}


void mol_quaternion_from_axis_angle(struct mol_quaternion *q, const struct mol_vector3 *axis, const double angle)
{
	const double s = sin(angle/2);
	q->W = cos(angle/2);
	q->X = s * axis->X;
	q->Y = s * axis->Y;
	q->Z = s * axis->Z;
}

void mol_quaternion_to_axis_angle(struct mol_vector3 *axis, double *angle, const struct mol_quaternion *q)
{
	const double w = sqrt(MOL_VEC_SQ_NORM(*q));
	*angle = 2*atan2(w, q->W);
	if (*angle > M_PI) {
		*angle -= 2*M_PI;
	}
	MOL_VEC_COPY(*axis, *q); // Copy X, Y, Z
	if (w != 0) {
		MOL_VEC_DIV_SCALAR(*axis, *axis, w);
	} else { // If the rotation axis is undefined, set it to some arbitrary value.
		axis->X = 1;
		axis->Y = 0;
		axis->Z = 0;
	}
}

//
void mol_quaternion_scaled(const struct mol_quaternion *q, double a,
		struct mol_quaternion *q_scaled)
{
	q_scaled->W = a * q->W;
	q_scaled->X = a * q->X;
	q_scaled->Y = a * q->Y;
	q_scaled->Z = a * q->Z;
}

void mol_quaternion_product(const struct mol_quaternion *qa,
		const struct mol_quaternion *qb, struct mol_quaternion *product)
{
	product->W = qa->W * qb->W - qa->X * qb->X - qa->Y * qb->Y - qa->Z * qb->Z;
	product->X = qa->W * qb->X + qa->X * qb->W + qa->Y * qb->Z - qa->Z * qb->Y;
	product->Y = qa->W * qb->Y + qa->Y * qb->W + qa->Z * qb->X - qa->X * qb->Z;
	product->Z = qa->W * qb->Z + qa->Z * qb->W + qa->X * qb->Y - qa->Y * qb->X;
}

void mol_quaternion_conjugate(const struct mol_quaternion *q,
		struct mol_quaternion *qc)
{
	qc->W = q->W;
	qc->X = -q->X;
	qc->Y = -q->Y;
	qc->Z = -q->Z;
}

void mol_quaternion_inverse(const struct mol_quaternion *q,
		struct mol_quaternion *q_inv)
{
	struct mol_quaternion qc;
	struct mol_quaternion Norm;
	mol_quaternion_conjugate(q, &qc);
	mol_quaternion_product(q, &qc, &Norm);
	mol_quaternion_scaled(&qc, 1 / (Norm.W), q_inv);
}
