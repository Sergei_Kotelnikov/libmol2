#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "pdb.h"
#include "utils.h"

#include "vector.h"
#include "utils.h"
#include "sequence.h"

#include <stdio.h>
#ifndef _WIN32
#include <unistd.h>
#endif
#include <string.h>

const char *ATOM_RECORD_FORMAT =
	"%6s%5d %4s%c%3.3s %c%4hd%c   %8.3f%8.3f%8.3f%6.2f%6.2f      %4s%2s%2s\n";

void mol_pdb_parse_atom_line(
		struct mol_atom_group *ag,
		const size_t atom_index,
		const char *line,
		const ssize_t line_length)
{
	/* PDB spec for ATOM line ( http://www.wwpdb.org/documentation/format33/sect9.html#ATOM )
	Record Format

	COLUMNS        DATA  TYPE    FIELD        DEFINITION
	-------------------------------------------------------------------------------------
	 1 -  6        Record name   "ATOM  "
	 7 - 11        Integer       serial       Atom  serial number.
	13 - 16        Atom          name         Atom name.
	17             Character     altLoc       Alternate location indicator.
	18 - 20        Residue name  resName      Residue name.
	21             Residue suffix             Libmol extension adding a suffix to residue name.
	22             Character     chainID      Chain identifier.
	23 - 26        Integer       resSeq       Residue sequence number.
	27             AChar         iCode        Code for insertion of residues.
	31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
	39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
	47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
	55 - 60        Real(6.2)     occupancy    Occupancy.
	61 - 66        Real(6.2)     tempFactor   Temperature  factor. (default = 0.0)
	73 - 76        LString(4)    segment_id   Segment identifier, left-justified.
	77 - 78        LString(2)    element      Element symbol, right-justified.
	79 - 80        LString(2)    charge       Charge  on the atom.
	*/
	if (atom_index >= ag->natoms) {
		fprintf(stderr, "Unable to read atom #%zu into atomgroup with %zu atoms\n", atom_index, ag->natoms);
		return;
	}
	if (line_length < 54) { // We need at least x, y, z coordinates
		fprintf(stderr, "Line for atom #%zu is too short\n", atom_index);
		return;
	}

	if (strncmp("ATOM  ",line,6) == 0) {
		ag->record[atom_index] = MOL_ATOM;
	} else if (strncmp("HETATM",line,6) == 0) {
		ag->record[atom_index] = MOL_HETATM;
	} else {
		ag->record[atom_index] = MOL_RUNKNOWN;
	}

	ag->atom_name[atom_index] = strndup(line+12, 4);
	ag->alternate_location[atom_index] = line[16];

	ag->residue_name[atom_index] = strndup(line+17, 4);
	if (ag->residue_name[atom_index][3] == ' ') {
		ag->residue_name[atom_index][3] = '\0';
	}

	sscanf(line+21, "%c%4hd%c",
		&ag->residue_id[atom_index].chain,
		&ag->residue_id[atom_index].residue_seq,
		&ag->residue_id[atom_index].insertion);

	sscanf(line+30, "%8lf%8lf%8lf", &ag->coords[atom_index].X, &ag->coords[atom_index].Y,
	                                &ag->coords[atom_index].Z);

	if (line_length >= 60) {
		sscanf(line+54, "%6lf", &ag->occupancy[atom_index]);
	} else {
		ag->occupancy[atom_index] = 1.0;
	}
	if (line_length >= 66) {
		sscanf(line+60, "%6lf", &ag->B[atom_index]);
	} else {
		ag->B[atom_index] = 0.0;
	}
	if (line_length >= 76) {
		ag->segment_id[atom_index] = strndup(line+72, 4);
	} else {
		ag->segment_id[atom_index] = NULL;
	}
	if (line_length >= 78) {
		ag->element[atom_index] = strndup(line+76, 2);
	} else {
		ag->element[atom_index] = NULL;
	}

	if (line_length >= 80) {
		ag->formal_charge[atom_index] = strndup(line+78, 2);
	} else {
		ag->formal_charge[atom_index] = NULL;
	}
}

bool mol_pdb_line_is_atom_or_hetatm(const char *line)
{
	return !strncmp(line, "ATOM  ", 6) || !strncmp(line, "HETATM", 6);
}

static void initialize_pdb_atom_group(struct mol_atom_group *ag, const size_t natoms)
{
	ag->natoms = natoms;
	ag->coords = malloc(sizeof(struct mol_vector3) * natoms);
	ag->element = malloc(sizeof(char*) * natoms);
	ag->atom_name = malloc(sizeof(char*) * natoms);
	ag->alternate_location = malloc(sizeof(char) * natoms);
	ag->residue_id = malloc(sizeof(struct mol_residue_id) * natoms);
	ag->residue_name = malloc(sizeof(char*) * natoms);
	ag->occupancy = malloc(sizeof(double) * natoms);
	ag->B = malloc(sizeof(double) * natoms);
	ag->segment_id = malloc(sizeof(char *) * natoms);
	ag->formal_charge = malloc(sizeof(char*) * natoms);
	ag->record = malloc(sizeof(enum mol_atom_record) * natoms);
}

struct mol_atom_group *mol_read_pdb(const char *pdb_filename)
{
	FILE *fp = fopen(pdb_filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", pdb_filename);
		return NULL;
	}

	struct mol_atom_group *ag = mol_fread_pdb(fp);

	fclose(fp);
	return ag;
}

struct mol_atom_group *mol_fread_pdb(FILE *fp)
{
	struct mol_atom_group *ag = NULL;

	if (fp == NULL) {
		return NULL;
	}

	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	size_t natoms = 0;

	while (getline(&line, &len, fp) != -1) {
		if (mol_pdb_line_is_atom_or_hetatm(line)) {
			natoms++;
		}
	}

	if (natoms == 0) {
		fprintf(stderr, "No atoms found in file\n");
		free_if_not_null(line);
		return NULL;
	}

	rewind(fp);

	ag = mol_atom_group_create();
	initialize_pdb_atom_group(ag, natoms);

	size_t i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		if (mol_pdb_line_is_atom_or_hetatm(line)) {
			mol_pdb_parse_atom_line(ag, i, line, read);
			i++;
		}
	}

	rewind(fp);
	mol_atom_group_create_residue_hash(ag);
	mol_attach_pdb_seqres(ag, fp);
	mol_atom_group_create_residue_list(ag);

	free_if_not_null(line);
	return ag;
}

struct mol_atom_group_list *mol_read_pdb_models(const char *pdb_filename)
{
	struct mol_atom_group_list *list = NULL;

	FILE *fp = fopen(pdb_filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", pdb_filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len=0;
	ssize_t read;

	size_t nmodels = 0;

	while (getline(&line, &len, fp) != -1) {
		if (strncmp("MODEL ", line, 6) == 0) {
			nmodels++;
		}
	}

	if (nmodels == 0) {
		fprintf(stderr, "No models found in %s\n", pdb_filename);
		goto reading_out;
	}

	rewind(fp);

	size_t natoms = 0;

	while (getline(&line, &len, fp) != -1) {
		if (mol_pdb_line_is_atom_or_hetatm(line)) {
			natoms++;
		}
		if (strncmp("ENDMDL", line, 6) == 0) {
			break;
		}
	}

	if (natoms == 0) {
		fprintf(stderr, "No atoms found in first model of %s\n", pdb_filename);
		goto reading_out;
	}

	rewind(fp);

	list = mol_atom_group_list_create(nmodels);
	for (size_t i=0; i<nmodels; i++) {
		initialize_pdb_atom_group(&(list->members[i]), natoms);
	}

	size_t atom_i = 0;
	size_t model_i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		if (mol_pdb_line_is_atom_or_hetatm(line)) {
			mol_pdb_parse_atom_line(&(list->members[model_i]), atom_i, line, read);
			atom_i++;
		}
		if (strncmp("ENDMDL", line, 6) == 0) {
			atom_i = 0;
			model_i++;
		}
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return list;
}

bool mol_write_pdb(const char *pdb_filename, const struct mol_atom_group *ag)
{
	bool close_fp = false;
	FILE *ofp = stdout;
	if (strcmp(pdb_filename, "-")) {
		close_fp = true;
		ofp = fopen(pdb_filename, "w");
		if (ofp == NULL) {
			return false;
		}
	}

	bool success = mol_fwrite_pdb(ofp, ag);

	if (close_fp) {
		fclose(ofp);
	}
	return success;
}

bool mol_fwrite_pdb(FILE *ofp, const struct mol_atom_group *ag)
{
	const char *atom_name;
	char alternate_location;
	const char *residue_name;
	char chain;
	int16_t residue_seq = 0;
	char insertion;
	struct mol_vector3 *coords;
	double occupancy;
	double B;
	const char *segment_id;
	const char *element;
	const char *charge;

	for (size_t i = 0; i < ag->natoms; ++i) {
		atom_name = ag->atom_name == NULL ? "    " : ag->atom_name[i];
		alternate_location =
			ag->alternate_location == NULL ? ' ' : ag->alternate_location[i];
		residue_name =
			ag->residue_name == NULL ? "UNK" : ag->residue_name[i];
		if (ag->residue_id == NULL) {
			chain = 'A';
			residue_seq++;
			insertion = ' ';
		} else {
			chain = ag->residue_id[i].chain;
			residue_seq = ag->residue_id[i].residue_seq;
			insertion = ag->residue_id[i].insertion;
		}
		coords = &ag->coords[i]; // Assume there are always coords
		occupancy = ag->occupancy == NULL ? 1.0 : ag->occupancy[i];
		B = ag->B == NULL ? 0.0 : ag->B[i];
		segment_id =
			(ag->segment_id == NULL || ag->segment_id[i] == NULL) ? " " : ag->segment_id[i];
		element =
			(ag->element == NULL || ag->element[i] == NULL) ? " " : ag->element[i];
		charge =
			(ag->formal_charge == NULL || ag->formal_charge[i] == NULL) ? "  " : ag->formal_charge[i];

		fprintf(ofp, ATOM_RECORD_FORMAT,
			"ATOM  ", i+1, atom_name, alternate_location,
			residue_name, chain, residue_seq, insertion,
			coords->X, coords->Y, coords->Z,
			occupancy, B, segment_id, element, charge);
	}

	return true;
}
