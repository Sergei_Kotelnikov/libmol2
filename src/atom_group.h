#ifndef _MOL_ATOM_GROUP_H_
#define _MOL_ATOM_GROUP_H_


#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#include "vector.h"
#include "matrix.h"
#include "lists.h"
#include "hashes.h"
#include "geometry.h"
#include "khash.h"
#include "utils.h"

/**
    \file atom_group.h
*/

KHASH_DECLARE(RESI_HASH, struct mol_residue_id, struct mol_residue);

enum mol_atom_record {
	MOL_RUNKNOWN,
	MOL_ATOM,
	MOL_HETATM
};

enum mol_atom_hbond_props {
	UNKNOWN_HPROP = 0,       ///< Atom does not participate in any hydrogen bonds.
	HBOND_DONOR = 1,         ///< Atom is a donor.
	HBOND_ACCEPTOR = 2,      ///< Atom is an acceptor; it's hbond_base_atom_id is covalently linked heavy atom.
	DONATABLE_HYDROGEN = 4,  ///< Atom is a donatable hydrogen; it's hbond_base_atom_id is corresponding donor.
};


/** Add hydrogen bond property flag \p prop to atom \p atomi. */
#define MOL_ATOM_HBOND_PROP_ADD(ag, atomi, prop) (ag)->hbond_prop[atomi] |= (prop)
/** Set hydrogen bond property flag \p prop to atom \p atomi, discarding all flags set earlier. */
#define MOL_ATOM_HBOND_PROP_SET(ag, atomi, prop) (ag)->hbond_prop[atomi] = (prop)
/** Return non-zero if atom \p atomi has hydrogen bond property \p prop. It may or may not have other properties. */
#define MOL_ATOM_HBOND_PROP_IS(ag, atomi, prop) ((ag)->hbond_prop[atomi] & (prop))

struct mol_residue_id {
	int16_t residue_seq;
	char chain;
	char insertion;
};

/**
 * Represents a group of atoms. Each field represents a different
   property of an atom. Each field should be a null pointer if the data
   for that field is absent, or a pointer to a list of length
   \c natoms of the proper type.
 */
struct mol_atom_group {
	size_t natoms; //!< Number of atoms in the atom group.

	struct mol_vector3 *coords;
	struct mol_vector3 *gradients;
	char **element;
	char **atom_name;
	char *alternate_location;
	struct mol_residue_id *residue_id;
	char **residue_name;
	double *occupancy;
	double *B;
	char **segment_id;
	char **formal_charge;

	double *vdw_radius;
	double *vdw_radius03;
	double *eps;
	double *eps03;
	double *ace_volume;
	double *charge;

	char *seqres;

	int *pwpot_id;

	size_t nbonds;
	struct mol_bond *bonds;
	struct mol_bond_list *bond_lists;

	size_t nangles;
	struct mol_angle *angles;
	struct mol_angle_list *angle_lists;

	size_t ndihedrals;
	struct mol_dihedral *dihedrals;
	struct mol_dihedral_list *dihedral_lists;

	size_t nimpropers;
	struct mol_improper *impropers;
	struct mol_improper_list *improper_lists;

	size_t nresidues;
	struct mol_residue **residue_list;
	kh_RESI_HASH_t *residues;

	kh_STR_t *metadata;

	bool *fixed;
	struct mol_index_list *active_atoms;
	struct mol_bond_list *active_bonds;
	struct mol_angle_list *active_angles;
	struct mol_dihedral_list *active_dihedrals;
	struct mol_improper_list *active_impropers;

	size_t *ftypen;
	char **ftype_name;

	size_t num_atom_types;

	bool *mask;
	bool *surface;
	double *attraction_level;
	bool *backbone;
	enum mol_atom_record *record;

	enum mol_atom_hbond_props *hbond_prop;
	size_t *hbond_base_atom_id;
};

struct mol_residue {
	struct mol_residue_id residue_id;

	size_t atom_start;
	size_t atom_end;
	size_t rotamer;
	char name[4];
};

struct mol_atom_group_list {
	size_t size;
	struct mol_atom_group *members;
};

// Use this macro to apply a macro to each of the fields of an atom_group
#define MOL_FOR_ATOMIC_FIELDS(macro, ag, ...) do {		\
		macro(ag, coords, ##__VA_ARGS__);		\
		macro(ag, gradients, ##__VA_ARGS__);		\
		macro(ag, element, ##__VA_ARGS__);		\
		macro(ag, atom_name, ##__VA_ARGS__);		\
		macro(ag, alternate_location, ##__VA_ARGS__);	\
		macro(ag, residue_id, ##__VA_ARGS__);		\
		macro(ag, residue_name, ##__VA_ARGS__);	\
		macro(ag, occupancy, ##__VA_ARGS__);		\
		macro(ag, B, ##__VA_ARGS__);			\
		macro(ag, segment_id, ##__VA_ARGS__);			\
		macro(ag, formal_charge, ##__VA_ARGS__);	\
		macro(ag, vdw_radius, ##__VA_ARGS__);		\
		macro(ag, vdw_radius03, ##__VA_ARGS__);	\
		macro(ag, eps, ##__VA_ARGS__);		\
		macro(ag, eps03, ##__VA_ARGS__);	\
		macro(ag, ace_volume, ##__VA_ARGS__);		\
		macro(ag, charge, ##__VA_ARGS__);		\
		macro(ag, pwpot_id, ##__VA_ARGS__);		\
		macro(ag, fixed, ##__VA_ARGS__);		\
		macro(ag, ftypen, ##__VA_ARGS__);		\
		macro(ag, ftype_name, ##__VA_ARGS__);		\
		macro(ag, mask, ##__VA_ARGS__);		\
		macro(ag, surface, ##__VA_ARGS__);		\
		macro(ag, attraction_level, ##__VA_ARGS__);	\
		macro(ag, backbone, ##__VA_ARGS__);		\
		macro(ag, record, ##__VA_ARGS__);		\
		macro(ag, hbond_prop, ##__VA_ARGS__);		\
		macro(ag, hbond_base_atom_id, ##__VA_ARGS__);	\
	} while(0)

#define mol_atom_group_init_atomic_field(ag, field) do {					\
		if (ag->field == NULL)					\
			ag->field = calloc(ag->natoms, sizeof(*(ag->field))); \
	} while(0)

/**
 * Create a new atom group or list of atom groups.
 * \return  Newly allocated atomgroup, all fields initialized to zero.
 */
struct mol_atom_group *mol_atom_group_create(void);
struct mol_atom_group_list *mol_atom_group_list_create(size_t n);

/**
 * Join two atom groups together.
 *
 * Most fields are simply concatenated.
 *
 * If the field is present in only one input atom_group, missing values are set to 0.
 * If the field is \c NULL in both \p A and \p B, it is not allocated and is set to \c NULL in the result.
 * Geometry lists get appropriate index adjustments.
 *
 * Atom types (ftypen) are shifted so they don't intersect between two molecules.
 * E.g., if \p A has 20 atom types, the atom types of B will be incremented by 20.
 * This creates some redundancy if we're joining two molecules with the same forcefield,
 * but safely handles joining two molecules with the different set of atom types: the
 * resulting ftypen would not have any collisions.
 */
struct mol_atom_group *mol_atom_group_join(
	const struct mol_atom_group *A,
	const struct mol_atom_group *B);

/**
 * Create a copy of an atom group data structure.
 * Copies all initialized fields of the original atom group except active lists.
 * All pointers to atom group field members are reassigned to the corresponding
 * field members of the new atom group.
 * Active lists are not copied, but rebuilt based on the *fixed* field of the
 * original atom group if it is not NULL.
 * Does not copy metadata.
 *
 * \param ag Atomgroup to be copied.
 */
struct mol_atom_group *mol_atom_group_copy(const struct mol_atom_group *ag);

void mol_atom_group_free(struct mol_atom_group *ag);
void mol_atom_group_destroy(struct mol_atom_group *ag);
void mol_atom_group_list_free(struct mol_atom_group_list *ag_list);
void mol_atom_group_list_destroy(struct mol_atom_group_list *ag_list);

void mol_atom_group_get_actives(double * restrict xyz,
				const struct mol_atom_group * restrict ag);
void mol_atom_group_set_actives(struct mol_atom_group * restrict ag,
				const double * restrict xyz);

/**
 * Copies over as many fields as possible from atom i in agA to atom j in agB
 */
void copy_atom(struct mol_atom_group *agA, size_t i,
	const struct mol_atom_group *agB, size_t j);

/**
 * Get the centroid, which is the average of all the coordinates.
 * \param[out] center Buffer for the result.
 * \param[in] ag Atomgroup.
 */
void centroid(struct mol_vector3 *center,
	      const struct mol_atom_group *ag);
/**
 * Get the center of extrema, which is the average of the
 * max and min in each of the 3 dimensions
 * \param[out] center Buffer for the result.
 * \param[in] ag Atomgroup.
 */
void center_of_extrema(struct mol_vector3 *center,
		const struct mol_atom_group *ag);

/**
 * Get the maximum distance between any two atoms.
 */
double mol_atom_group_diameter(const struct mol_atom_group *ag);

/**
 * Get the bounding box.
 */
void mol_atom_group_bounding_box(
	struct mol_vector3 *min, struct mol_vector3 *max,
	const struct mol_atom_group *ag);
double mol_atom_group_bounding_sphere(struct mol_vector3 *center,
		const struct mol_atom_group *ag);

/**
 * Translate all atoms in \p ag.
 */
void mol_atom_group_translate(struct mol_atom_group *restrict ag,
			const struct mol_vector3 *restrict tv);

/**
 * Rotate all atoms in \p ag.
 */
void mol_atom_group_rotate(struct mol_atom_group *restrict ag,
			const struct mol_matrix3 *restrict r);

/**
 * Rotate and then translate all atoms in \p ag.
 */
void mol_atom_group_move(struct mol_atom_group *restrict ag,
			const struct mol_matrix3 *restrict r,
			const struct mol_vector3 *restrict tv);

void mol_atom_group_move_in_copy(const struct mol_atom_group *src,
			struct mol_atom_group *dest,
			const struct mol_matrix3 *r,
			const struct mol_vector3 *tv);

khint_t mol_residue_id_equal(struct mol_residue_id a,
			     struct mol_residue_id b);
int mol_residue_id_cmp(const void *a,
		       const void *b);

/**
 * Create \c ag->residues hashtable. If it already exists, the old one is discarded.
 * \param ag Atomgroup.
 * \return True on success, false on failure. The error message is printed to stderr.
 */
bool mol_atom_group_create_residue_hash(struct mol_atom_group *ag);
/**
 * Create \c ag->residue_list based on the \c ag->residues hashtable.
 * If it already exists, no action is taken.
 * \param ag Atomgroup.
 */
void mol_atom_group_create_residue_list(struct mol_atom_group *ag);

struct mol_residue *find_residue(const struct mol_atom_group *ag,
				 const char chain,
				 const int16_t residue_seq,
				 const char insertion);

ssize_t mol_find_atom(const struct mol_atom_group *ag,
		const char chain,
		const int16_t residue_seq,
		const char insertion,
		const char* atom_name);

struct mol_residue *mol_atom_residue(const struct mol_atom_group *ag,
				size_t atom_index);

void print_residues(const struct mol_atom_group *ag);

/**
 * Add metadata to atomgroup. Metadata is just a \c void* pointer that "sticks" to atomgroup and
 * can be retrieved using \ref mol_atom_group_fetch_metadata. It can point to arbitrary data, be it single string,
 * per-atom parameters, or some kind of pairwise tabular potential. It can be used, e.g., for handling some data that's
 * highly specific to some energy function, so it does not warrant including it as a field in \ref mol_atom_group,
 * while still avoiding the hassle of passing this data around as an additional function parameter.
 *
 * Be warned: \ref mol_atom_group_join, \ref mol_atom_group_free, \ref mol_atom_group_select, etc do **not**
 * handle metadata at all. If you use any of those, you should handle metadata manually.
 * See \ref mol_atom_group_join_atomic_metadata.
 *
 * \param ag Atomgroup.
 * \param key String that is used as a handle of this metadata. If metadata with such key already exists, it (the pointer)
 * will be silently replaced, with no deallocation or any other handling.
 * \param data Pointer of \c void* to the data. The data is not getting copied -- only this pointer is actually stored
 * in \p ag.
 */
void mol_atom_group_add_metadata(struct mol_atom_group *ag, const char *key, void *data);
/**
 * Return metadata (stored pointer). You will have to manually cast it to the original type.
 *
 * For more information about metadata see \ref mol_atom_group_add_metadata.
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 * \return The stored pointer. \c NULL if no metadata with such \p key exists in \p ag.
 */
void *mol_atom_group_fetch_metadata(const struct mol_atom_group *ag, const char *key);
/**
 * Check if metadata with given key is present in atomgroup.
 *
 * For more information about metadata see \ref mol_atom_group_add_metadata.
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 * \return True if metadata with \p key was added to \p ag; false otherwise.
 */
bool mol_atom_group_has_metadata(const struct mol_atom_group *ag, const char *key);
/**
 * Remove metadata from atomgroup. It only removes the stored \c void* pointer, but does not deallocate the memory.
 * If you want to avoid memory leaks, you are advised to call \ref mol_atom_group_fetch_metadata and \c free the
 * returned pointer, and only afterwards call \ref mol_atom_group_delete_metadata.
 *
 * For more information about metadata see \ref mol_atom_group_add_metadata.
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 */
void mol_atom_group_delete_metadata(struct mol_atom_group *ag, const char *key);
/**
 * Helper function to join metadata when joining two atomgroups with \ref mol_atom_group_join. It works with any
 * metadata that has equally-sized contiguous-stored per-atom elements. E.g., the array of doubles or some
 * custom structs similar to \ref mol_atom_group.coords or \ref mol_atom_group.vdw_radius. This function does what
 * \ref mol_atom_group_join does for the default fields.
 *
 * It assumes that the metadata with handle \p key in \p A has \c A->natoms*element_size bytes; same with \p B.
 * Then the new memory chunk of \c ag->natoms*element_size bytes is allocated, and data from \p A and \p B is copied
 * to it.
 * If there is no metadata with handle \p key in either \p A or \p B, then the corresponding region of joined
 * metadata is set to 0s. If there's no metadata neither in \p A nor in \p B, then the new metadata is not created.
 *
 * This function is not called automatically. You should call it manually for each metadata record you want to process.
 *
 * For more information about metadata see \ref mol_atom_group_add_metadata.
 *
 * \param ag The joined atomgroup, returned by \c mol_atom_group_join(A, B). Metadata is added to it.
 * \param A First atomgroup.
 * \param B Second atomgroup.
 * \param key Metadata handle.
 * \param element_size The size of individual element.
 */
void mol_atom_group_join_atomic_metadata(
	struct mol_atom_group *ag,
	const struct mol_atom_group *A,
	const struct mol_atom_group *B,
	const char *key,
	const size_t element_size);

struct mol_atom_group *mol_extract_atom_type(
	struct mol_atom_group *ag,
	const char *type);
struct mol_atom_group *mol_extract_atom_type_prefix(
	struct mol_atom_group *ag,
	const char *type);
struct mol_atom_group *mol_remove_atom_type(
	struct mol_atom_group *ag,
	const char *type);

/* extract an atom group with a particular chain */
struct mol_atom_group *mol_extract_chain(const struct mol_atom_group *ag,
					 const char chain);

/**
 * Returns a new atom group where only atoms which satisfy/does not satisfy a
 * certain property are kept.
 */
struct mol_atom_group *mol_atom_group_select(
	const struct mol_atom_group *ag,
	bool (*keep_if_true)(const struct mol_atom_group *ag,
			     size_t index,
			     void *params),
	void *func_params);
struct mol_atom_group *mol_atom_group_filter(
	const struct mol_atom_group *ag,
	bool (*keep_if_false)(const struct mol_atom_group *ag,
			     size_t index,
			     void *params),
	void *func_params);


/**
 * Returns an array of bools where arr[i] is true if
 * test_func(ag, i, func_params) is true, false otherwise.
 */
bool *mol_atom_group_test_foreach(
	const struct mol_atom_group *ag,
	bool (*test_func)(const struct mol_atom_group *ag,
			 size_t index,
			 void *params),
	void *func_params);

struct mol_atom_group *mol_extract_atom_types(struct mol_atom_group *ag,
					const struct mol_list *types);

bool mol_is_backbone(const struct mol_atom_group *ag,
			size_t index, void *params);
bool mol_is_sidechain(const struct mol_atom_group *ag,
			size_t index, void *params);

void mol_zero_gradients(struct mol_atom_group *ag);

/**
   initialize active structures
*/
void mol_fixed_init(struct mol_atom_group *ag);

/**
   update fixed atom index and bonded active structures. list contains indices of fixed atoms
*/
void mol_fixed_update(struct mol_atom_group* ag, size_t nlist, size_t* list);
void mol_fixed_update_with_list(struct mol_atom_group* ag, const struct mol_index_list* fixed_list);
void mol_fixed_update_active_lists(struct mol_atom_group *ag);

bool *mol_gaps(struct mol_atom_group *ag);

//deprecated
struct mol_atom_group *new_mol_atom_group(void) __attribute__ ((deprecated));
struct mol_atom_group_list *new_mol_atom_groups(size_t n) __attribute__ ((deprecated));
void destroy_mol_atom_group(struct mol_atom_group *ag) __attribute__ ((deprecated));
void free_mol_atom_group(struct mol_atom_group *ag) __attribute__ ((deprecated));
void destroy_mol_atom_group_list(struct mol_atom_group_list *ag_list) __attribute__ ((deprecated));
void free_mol_atom_group_list(struct mol_atom_group_list *ag_list) __attribute__ ((deprecated));
void zero_grads(struct mol_atom_group *ag) __attribute__ ((deprecated));



#endif /* _MOL_ATOM_GROUP_H_ */
