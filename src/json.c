/*
Copyright (c) 2013, Acpharis
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
- Neither the name of the author nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#define _POSIX_C_SOURCE 200809L
#include "json.h"

#include <jansson.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

bool mol_read_json_v1(struct mol_atom_group *ag, const json_t *base);
static void parse_json_atom(struct mol_atom_group *ag, const json_t *atom, const size_t index);
static void parse_json_bond(struct mol_atom_group *ag, const json_t *bond, const size_t index);
static void parse_json_angle(struct mol_atom_group *ag, const json_t *angle, const size_t index);
static void parse_json_dihedral(struct mol_atom_group *ag, const json_t *dihedral, const size_t index);
static void parse_json_improper(struct mol_atom_group *ag, const json_t *improper, const size_t index);

struct mol_atom_group *mol_read_json(const char *json_filename)
{
	struct mol_atom_group *ag = NULL;

	json_error_t json_file_error;
	json_t *base = json_load_file(json_filename, 0, &json_file_error);

	if (!base) {
		fprintf(stderr, "error reading json file %s on line %d column %d: %s\n",
			json_filename, json_file_error.line, json_file_error.column, json_file_error.text);
		return NULL;
	}

	if (!json_is_object(base)) {
		fprintf(stderr, "json file not an object: %s\n", json_filename);
		json_decref(base);
		return NULL;
	}

	json_t *version = json_object_get(base, "version");
	if (version == NULL || !json_is_number(version)) {
		fprintf(stderr, "no version or version is not a number: %s\n", json_filename);
		goto cleanup;
	}

	ag = (struct mol_atom_group *) calloc(1, sizeof(struct mol_atom_group));
	if (ag == NULL) {
		goto cleanup;
	}

	if (json_integer_value(version) == 1) {
		if (mol_read_json_v1(ag, base)) {
			goto cleanup;
		} else {
			goto error_out;
		}
	} else {
		goto error_out;
	}

	goto cleanup;
error_out:
	mol_atom_group_free(ag);
	ag = NULL;
cleanup:
	json_decref(base);
	return ag;
}

bool mol_read_json_v1(struct mol_atom_group *ag, const json_t *base)
{
	json_t *atoms, *bonds, *angles, *dihedrals, *impropers;

	atoms = json_object_get(base, "atoms");
	if (!json_is_array(atoms)) {
		fprintf(stderr, "json atoms are not an array\n");
		return false;
	}

	size_t natoms = json_array_size(atoms);
	ag->natoms = natoms;
	MOL_FOR_ATOMIC_FIELDS(mol_atom_group_init_atomic_field, ag);
	for (size_t i = 0; i < natoms; i++) {
		json_t *atom = json_array_get(atoms, i);
		if (!json_is_object(atom)) {
			fprintf(stderr, "atom %zu not an object\n", i);
			continue;
		}

		parse_json_atom(ag, atom, i);
	}

	if (!mol_atom_group_create_residue_hash(ag)) {
		return false;
	}

	bonds = json_object_get(base, "bonds");
	if (!json_is_array(bonds)) {
		fprintf(stderr, "json bonds are not an array\n");
	} else {
		size_t nbonds = json_array_size(bonds);
		ag->nbonds = nbonds;
		ag->bonds = (struct mol_bond *) calloc(nbonds, sizeof(struct mol_bond));
		ag->bond_lists = (struct mol_bond_list *) calloc(natoms, sizeof(struct mol_bond_list));
		for (size_t i = 0; i < nbonds; i++) {
			json_t *bond = json_array_get(bonds, i);
			if (!json_is_object(bond)) {
				fprintf(stderr, "bond %zu not an object\n", i);
				continue;
			}

			parse_json_bond(ag, bond, i);
		}
	}

	angles = json_object_get(base, "angles");
	if (!json_is_array(angles)) {
		fprintf(stderr, "json angles are not an array\n");
	} else {
		size_t nangles = json_array_size(angles);
		ag->nangles = nangles;
		ag->angles = (struct mol_angle *) calloc(nangles, sizeof(struct mol_angle));
		ag->angle_lists = (struct mol_angle_list *) calloc(natoms, sizeof(struct mol_angle_list));
		for (size_t i = 0; i < nangles; i++) {
			json_t *angle = json_array_get(angles, i);
			if (!json_is_object(angle)) {
				fprintf(stderr, "angle %zu not an object\n", i);
				continue;
			}

			parse_json_angle(ag, angle, i);
		}
	}

	dihedrals = json_object_get(base, "torsions");
	if (!json_is_array(dihedrals)) {
		fprintf(stderr, "json dihedrals are not an array\n");
	} else {
		size_t ndihedrals = json_array_size(dihedrals);
		ag->ndihedrals = ndihedrals;
		ag->dihedrals = (struct mol_dihedral *) calloc(ndihedrals, sizeof(struct mol_dihedral));
		ag->dihedral_lists = (struct mol_dihedral_list *) calloc(natoms, sizeof(struct mol_dihedral_list));
		for (size_t i = 0; i < ndihedrals; i++) {
			json_t *dihedral = json_array_get(dihedrals, i);
			if (!json_is_object(dihedral)) {
				fprintf(stderr, "dihedral %zu not an object\n", i);
				continue;
			}

			parse_json_dihedral(ag, dihedral, i);
		}
	}

	impropers = json_object_get(base, "impropers");
	if (!json_is_array(impropers)) {
		fprintf(stderr, "json impropers are not an array\n");
	} else {
		size_t nimpropers = json_array_size(impropers);
		ag->nimpropers = nimpropers;
		ag->impropers = (struct mol_improper *) calloc(nimpropers, sizeof(struct mol_improper));
		ag->improper_lists = (struct mol_improper_list *) calloc(natoms, sizeof(struct mol_improper_list));
		for (size_t i = 0; i < nimpropers; i++) {
			json_t *improper = json_array_get(impropers, i);
			if (!json_is_object(improper)) {
				fprintf(stderr, "improper %zu not an object\n", i);
				continue;
			}

			parse_json_improper(ag, improper, i);
		}
	}

	for (size_t i = 0; i < ag->natoms; ++i) {
		ag->bond_lists[i].members = calloc(ag->bond_lists[i].size,
						sizeof(struct mol_bond *));
		ag->angle_lists[i].members = calloc(ag->angle_lists[i].size,
						sizeof(struct mol_angle *));
		ag->dihedral_lists[i].members = calloc(ag->dihedral_lists[i].size,
						sizeof(struct mol_dihedral *));
		ag->improper_lists[i].members = calloc(ag->improper_lists[i].size,
						sizeof(struct mol_improper *));

		// Zero out all the sizes so we can use them as an index later
		ag->bond_lists[i].size = 0;
		ag->angle_lists[i].size = 0;
		ag->dihedral_lists[i].size = 0;
		ag->improper_lists[i].size = 0;
	}

	for (size_t i = 0; i < ag->nbonds; ++i) {
		struct mol_bond *bond = &ag->bonds[i];
		size_t ai = bond->ai;
		size_t aj = bond->aj;

		ag->bond_lists[ai].members[ag->bond_lists[ai].size++] = bond;
		ag->bond_lists[aj].members[ag->bond_lists[aj].size++] = bond;
	}

	for (size_t i = 0; i < ag->nangles; ++i) {
		struct mol_angle *angle = &ag->angles[i];
		size_t a0 = angle->a0;
		size_t a1 = angle->a1;
		size_t a2 = angle->a2;

		ag->angle_lists[a0].members[ag->angle_lists[a0].size++] = angle;
		ag->angle_lists[a1].members[ag->angle_lists[a1].size++] = angle;
		ag->angle_lists[a2].members[ag->angle_lists[a2].size++] = angle;
	}

	for (size_t i = 0; i < ag->ndihedrals; ++i) {
		struct mol_dihedral *dihedral = &ag->dihedrals[i];
		size_t a0 = dihedral->a0;
		size_t a1 = dihedral->a1;
		size_t a2 = dihedral->a2;
		size_t a3 = dihedral->a3;

		ag->dihedral_lists[a0].members[ag->dihedral_lists[a0].size++] = dihedral;
		ag->dihedral_lists[a1].members[ag->dihedral_lists[a1].size++] = dihedral;
		ag->dihedral_lists[a2].members[ag->dihedral_lists[a2].size++] = dihedral;
		ag->dihedral_lists[a3].members[ag->dihedral_lists[a3].size++] = dihedral;
	}

	for (size_t i = 0; i < ag->nimpropers; ++i) {
		struct mol_improper *improper = &ag->impropers[i];
		size_t a0 = improper->a0;
		size_t a1 = improper->a1;
		size_t a2 = improper->a2;
		size_t a3 = improper->a3;

		ag->improper_lists[a0].members[ag->improper_lists[a0].size++] = improper;
		ag->improper_lists[a1].members[ag->improper_lists[a1].size++] = improper;
		ag->improper_lists[a2].members[ag->improper_lists[a2].size++] = improper;
		ag->improper_lists[a3].members[ag->improper_lists[a3].size++] = improper;
	}

	return true;
}

#define __MOL_JSON_PARSE(AG_FIELD, INDEX, BASE, JSON_VAR, SELECTOR, TYPE, VAL_COPY) do { \
		JSON_VAR = json_object_get(BASE, SELECTOR);		\
		if (JSON_VAR == NULL) break;				\
		if (!json_is_##TYPE(JSON_VAR)) {			\
			fprintf(stderr, "json " SELECTOR " is not type " #TYPE " for atom %zu\n", INDEX); \
		} else {						\
			AG_FIELD[INDEX] = VAL_COPY((json_ ## TYPE ## _value(JSON_VAR))); \
		}							\
	} while(0)
#define __SQRT_NEG(X) sqrt(-X)

static void parse_json_atom(struct mol_atom_group *ag, const json_t *atom, const size_t index) {
	json_t *x, *y, *z;
	x = json_object_get(atom, "x");
	if (!json_is_real(x)) {
		fprintf(stderr, "json coordinate x is not floating point for atom %zu\n", index);
	}
	ag->coords[index].X = json_real_value(x);

	y = json_object_get(atom, "y");
	if (!json_is_real(y)) {
		fprintf(stderr, "json coordinate y is not floating point for atom %zu\n", index);
	}
	ag->coords[index].Y = json_real_value(y);

	z = json_object_get(atom, "z");
	if (!json_is_real(z)) {
		fprintf(stderr, "json coordinate z is not floating point for atom %zu\n", index);
	}
	ag->coords[index].Z = json_real_value(z);

	json_t *element, *atom_name, *resi_name, *backbone, *radius, *radius03,
		*eps, *eps03, *ace_volume, *charge, *pwpot_id, *segment, *residue,
		*alt_loc, *hetatom, *generic, *hb_acceptor, *hb_donor;

	__MOL_JSON_PARSE(ag->element, index, atom, element, "element", string, strdup);
	__MOL_JSON_PARSE(ag->atom_name, index, atom, atom_name, "name", string, strdup);
	__MOL_JSON_PARSE(ag->residue_name, index, atom, resi_name, "residue_name", string, strdup);
	__MOL_JSON_PARSE(ag->backbone, index, atom, backbone, "backbone", boolean, );
	__MOL_JSON_PARSE(ag->vdw_radius, index, atom, radius, "radius", real, );

	__MOL_JSON_PARSE(ag->vdw_radius03, index, atom, radius03, "radius03", real, );
	__MOL_JSON_PARSE(ag->eps, index, atom, eps, "eps", real, __SQRT_NEG);
	__MOL_JSON_PARSE(ag->eps03, index, atom, eps03, "eps03", real, __SQRT_NEG);
	__MOL_JSON_PARSE(ag->ace_volume, index, atom, ace_volume, "ace_volume", real, );
	__MOL_JSON_PARSE(ag->charge, index, atom, charge, "charge", real, );
	__MOL_JSON_PARSE(ag->ftype_name, index, atom, generic, "ftype_name", string, strdup);
	__MOL_JSON_PARSE(ag->ftypen, index, atom, generic, "ftype_index", integer, );
	if (ag->ftypen[index] > ag->num_atom_types) {
		ag->num_atom_types = ag->ftypen[index];
	}

	segment = json_object_get(atom, "segment");
	bool has_segment = segment != NULL && json_string_length(segment) > 0;
	if (has_segment) {
		// Does not support multi-character chains yet
		ag->residue_id[index].chain = json_string_value(segment)[0];
	} else {
		ag->residue_id[index].chain = ' ';
	}

	alt_loc = json_object_get(atom, "alt_loc");
	bool has_alt_loc = alt_loc != NULL && json_string_length(alt_loc) > 0;
	if (has_alt_loc) {
		// Does not support multi-character alternative locations
		ag->alternate_location[index] = json_string_value(alt_loc)[0];
	} else {
		ag->alternate_location[index] = ' ';
	}

	pwpot_id = json_object_get(atom, "acp_type");
	if (pwpot_id != NULL) {
		ag->pwpot_id[index] = json_integer_value(pwpot_id);
	} else {
		ag->pwpot_id[index] = -1;
	}

	residue = json_object_get(atom, "residue");
	if (residue != NULL && json_is_string(residue)) {
		ag->residue_id[index].insertion = ' ';
		sscanf(json_string_value(residue), "%4hd%c",
		       &ag->residue_id[index].residue_seq,
		       &ag->residue_id[index].insertion);
	}

	hetatom = json_object_get(atom, "hetatom");
	if (hetatom != NULL && json_is_boolean(hetatom) && json_is_true(hetatom)) {
		ag->record[index] = MOL_HETATM;
	} else {
		ag->record[index] = MOL_ATOM;
        }

        // We read flags from JSON here, but it's not enough to calculate H-bond potential,
        // we also need to process structure and determine bases for atoms. This is out of
        // scope for this function, though.
	hb_acceptor = json_object_get(atom, "hb_acceptor");
	if (hb_acceptor != NULL && json_is_boolean(hb_acceptor) && json_is_true(hb_acceptor)) {
		MOL_ATOM_HBOND_PROP_ADD(ag, index, HBOND_ACCEPTOR);
	}

	hb_donor = json_object_get(atom, "hb_donor");
	if (hb_donor != NULL && json_is_boolean(hb_donor) && json_is_true(hb_donor)) {
		MOL_ATOM_HBOND_PROP_ADD(ag, index, DONATABLE_HYDROGEN);
	}
}

#undef __SQRT_NEG
#undef __MOL_JSON_PARSE

#define __MOL_JSON_PARSE_GEOMETRY_INDEX(GEO_TYPE, AG_FIELD, INDEX, BASE, JSON_VAR, SELECTOR) do { \
		JSON_VAR = json_object_get(BASE, SELECTOR);		\
		if (!json_is_integer(JSON_VAR)) {			\
			fprintf(stderr, "json " SELECTOR " is not integer " #GEO_TYPE " for %zu\n", INDEX); \
		} else {						\
			json_int_t __I = json_integer_value(JSON_VAR) - 1; \
			ag->GEO_TYPE ## s[INDEX].AG_FIELD = __I;	\
			ag->GEO_TYPE ## _lists[__I].size += 1;	\
		}							\
	} while(0)

static void parse_json_bond(struct mol_atom_group *ag, const json_t *bond, const size_t index)
{
	json_t *atom1, *atom2, *length, *spring_constant, *sdf_type;

	__MOL_JSON_PARSE_GEOMETRY_INDEX(bond, ai, index, bond, atom1, "atom1");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(bond, aj, index, bond, atom2, "atom2");

	length = json_object_get(bond, "length");
	if (!json_is_real(length)) {
		fprintf(stderr,
			"json length is not floating point for bond %zu\n",
			index);
	} else {
		ag->bonds[index].l0 = json_real_value(length);
	}

	spring_constant = json_object_get(bond, "spring_constant");
	if (!json_is_real(spring_constant)) {
		fprintf(stderr,
			"json spring_constant is not floating point for bond %zu\n",
			index);
	} else {
		ag->bonds[index].k = json_real_value(spring_constant);
	}

	sdf_type = json_object_get(bond, "sdf_type");
	if (sdf_type != NULL) {
		if (!json_is_integer(sdf_type)) {
			fprintf(stderr,
				"json sdf_type is not integer for bond %zu\n",
				index);
		} else {
			ag->bonds[index].sdf_type = json_integer_value(sdf_type);
		}
	} else {
		ag->bonds[index].sdf_type = 0;
	}
}

static void parse_json_angle(struct mol_atom_group *ag, const json_t *angle, const size_t index)
{
	json_t *atom1, *atom2, *atom3, *theta, *spring_constant;

	__MOL_JSON_PARSE_GEOMETRY_INDEX(angle, a0, index, angle, atom1, "atom1");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(angle, a1, index, angle, atom2, "atom2");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(angle, a2, index, angle, atom3, "atom3");

	theta = json_object_get(angle, "theta");
	if (!json_is_real(theta)) {
		fprintf(stderr,
			"json theta is not floating point for angle %zu\n",
			index);
	} else {
		ag->angles[index].th0 = json_real_value(theta);
	}

	spring_constant = json_object_get(angle, "spring_constant");
	if (!json_is_real(spring_constant)) {
		fprintf(stderr,
			"json spring_constant is not floating point for angle %zu\n",
			index);
	} else {
		ag->angles[index].k = json_real_value(spring_constant);
	}
}

static void parse_json_dihedral(struct mol_atom_group *ag, const json_t *dihedral, const size_t index)
{
	json_t *atom1, *atom2, *atom3, *atom4, *minima, *delta_constant, *spring_constant;

	__MOL_JSON_PARSE_GEOMETRY_INDEX(dihedral, a0, index, dihedral, atom1, "atom1");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(dihedral, a1, index, dihedral, atom2, "atom2");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(dihedral, a2, index, dihedral, atom3, "atom3");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(dihedral, a3, index, dihedral, atom4, "atom4");

	minima = json_object_get(dihedral, "minima");
	if (!json_is_integer(minima)) {
		fprintf(stderr,
			"json minima is not integer for dihedral %zu\n",
			index);
	} else {
		ag->dihedrals[index].n = json_integer_value(minima);
	}

	delta_constant = json_object_get(dihedral, "delta_constant");
	if (!json_is_real(delta_constant)) {
		fprintf(stderr,
			"json delta_constant is not floating point for dihedral %zu\n",
			index);
	} else {
		ag->dihedrals[index].d = json_real_value(delta_constant);
	}

	spring_constant = json_object_get(dihedral, "spring_constant");
	if (!json_is_real(spring_constant)) {
		fprintf(stderr,
			"json spring_constant is not floating point for dihedral %zu\n",
			index);
	} else {
		ag->dihedrals[index].k = json_real_value(spring_constant);
	}
}

static void parse_json_improper(struct mol_atom_group *ag, const json_t *improper, const size_t index)
{
	json_t *atom1, *atom2, *atom3, *atom4, *psi, *spring_constant;

	__MOL_JSON_PARSE_GEOMETRY_INDEX(improper, a0, index, improper, atom1, "atom1");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(improper, a1, index, improper, atom2, "atom2");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(improper, a2, index, improper, atom3, "atom3");
	__MOL_JSON_PARSE_GEOMETRY_INDEX(improper, a3, index, improper, atom4, "atom4");

	psi = json_object_get(improper, "phi");
	if (!json_is_real(psi)) {
		fprintf(stderr,
			"json psi is not floating point for improper %zu\n",
			index);
	} else {
		ag->impropers[index].psi0 = json_real_value(psi);
	}

	spring_constant = json_object_get(improper, "spring_constant");
	if (!json_is_real(spring_constant)) {
		fprintf(stderr,
			"json spring_constant is not floating point for improper %zu\n",
			index);
	} else {
		ag->impropers[index].k = json_real_value(spring_constant);
	}
}

#undef __MOL_JSON_PARSE_GEOMETRY_INDEX
