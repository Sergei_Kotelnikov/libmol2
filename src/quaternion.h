#ifndef _MOL_QUATERNION_H_
#define _MOL_QUATERNION_H_

#include "matrix.h"
#include "vector.h"

struct mol_quaternion {
	double W;
	double X;
	double Y;
	double Z;
};

struct mol_quaternionf {
	float W;
	float X;
	float Y;
	float Z;
};

struct mol_quaternionl {
	long double W;
	long double X;
	long double Y;
	long double Z;
};

#define MOL_QUATERNION_MULT_SCALAR(DST, U, C) do \
	{                                        \
		(DST).W = (U).W * (C);           \
		MOL_VEC_MULT_SCALAR(DST, U, C);  \
	} while(0)

#define MOL_QUATERNION_ADD(DST, U, V) do \
	{                                \
		(DST).W = (U).W + (V).W; \
		MOL_VEC_ADD(DST, U, V);  \
	} while(0)

#define MOL_QUATERNION_SUB(DST, U, V) do \
	{                                \
		(DST).W = (U).W - (V).W; \
		MOL_VEC_SUB(DST, U, V);  \
	} while(0)

struct mol_quaternion *mol_quaternion_create(void);
struct mol_quaternionf *mol_quaternionf_create(void);
struct mol_quaternionl *mol_quaternionl_create(void);

struct mol_quaternion_list *mol_quaternion_list_create(const size_t n);
struct mol_quaternionf_list *mol_quaternionf_list_create(const size_t n);
struct mol_quaternionl_list *mol_quaternionl_list_create(const size_t n);

struct mol_quaternion_list *mol_quaternion_list_from_file(const char
							    *filename);
struct mol_quaternion_list *mol_quaternion_list_from_file_unnormalized(const char
							    *filename);

struct mol_quaternionf_list *mol_quaternionf_list_from_file(const char
							    *filename);
struct mol_quaternionf_list *mol_quaternionf_list_from_file_unnormalized(const char
							    *filename);
struct mol_quaternionl_list *mol_quaternionl_list_from_file(const char
							    *filename);
struct mol_quaternionl_list *mol_quaternionl_list_from_file_unnormalized(const char
									 *filename);

void mol_quaternion_free(struct mol_quaternion *q);
void mol_quaternionf_free(struct mol_quaternionf *q);
void mol_quaternionl_free(struct mol_quaternionl *q);

double mol_quaternion_length(const struct mol_quaternion q);
float mol_quaternionf_length(const struct mol_quaternionf q);
long double mol_quaternionl_length(const struct mol_quaternionl q);

void mol_quaternion_norm(struct mol_quaternion *p,
			 const struct mol_quaternion q);
void mol_quaternionf_norm(struct mol_quaternionf *p,
			  const struct mol_quaternionf q);
void mol_quaternionl_norm(struct mol_quaternionl *p,
			  const struct mol_quaternionl q);

void mol_matrix3_from_quaternion(struct mol_matrix3 *m,
				 const struct mol_quaternion q);
void mol_matrix3f_from_quaternionf(struct mol_matrix3f *m,
				   const struct mol_quaternionf q);
void mol_matrix3l_from_quaternionl(struct mol_matrix3l *m,
				   const struct mol_quaternionl q);

void mol_quaternion_from_matrix3(struct mol_quaternion *q,
				 const struct mol_matrix3 m);
void mol_quaternionf_from_matrix3f(struct mol_quaternionf *q,
				   const struct mol_matrix3f m);
void mol_quaternionl_from_matrix3l(struct mol_quaternionl *q,
				   const struct mol_matrix3l m);


/**
 * Calculate quaternion describing rotation around \p axis by \p angle.
 * \param q[out] Pointer to the \c mol_quaternion where the result will be stored.
 * \param axis[in] Rotation axis. Must be unit vector.
 * \param angle[in] Angle in radians.
 */
void mol_quaternion_from_axis_angle(struct mol_quaternion *q, const struct mol_vector3 *axis, const double angle);

/**
 * Return axis and angle of the rotation from the quaternion.
 * Out of two possible rotation axes, the one used in XYZ coordinated of \p q is used.
 * For zero-angle rotation, the \p axis is set to some unspecified value (but still has unit length).
 *
 * \param axis[out] Rotation axis. Will be unit vector.
 * \param angle[out] Rotation angle in [-pi; pi] range.
 * \param q[in] Quaternion.
 */
void mol_quaternion_to_axis_angle(struct mol_vector3 *axis, double *angle, const struct mol_quaternion *q);

void mol_quaternion_scaled(const struct mol_quaternion *q, double a,
	struct mol_quaternion *q_scaled);
void mol_quaternion_product(const struct mol_quaternion *qa,
	const struct mol_quaternion *qb, struct mol_quaternion *product);
void mol_quaternion_conjugate(const struct mol_quaternion *q,
	struct mol_quaternion *qc);
void mol_quaternion_inverse(const struct mol_quaternion *q,
	struct mol_quaternion *q_inv);

#endif /* _MOL_QUATERNION_H_ */
