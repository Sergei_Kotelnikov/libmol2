#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "matrix.h"

#include <stdio.h>
#include <string.h>
#include "utils.h"

struct mol_matrix3_list *mol_matrix3_list_from_file(const char *filename)
{
	struct mol_matrix3_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = malloc(sizeof(struct mol_matrix3_list));
	rotset->size = nrots;
	rotset->members = malloc(nrots * sizeof(struct mol_matrix3));

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		//10 membered rotation file
		int ret = sscanf(line, "%*s %lf %lf %lf %lf %lf %lf %lf %lf %lf",
			     &rotset->members[i].m11,
			     &rotset->members[i].m12,
			     &rotset->members[i].m13,
			     &rotset->members[i].m21,
			     &rotset->members[i].m22,
			     &rotset->members[i].m23,
			     &rotset->members[i].m31,
			     &rotset->members[i].m32,
			     &rotset->members[i].m33);
		if (ret != 9) {
			//9 membered rotation file
			ret = sscanf(line, "%lf %lf %lf %lf %lf %lf %lf %lf %lf",
				     &rotset->members[i].m11,
				     &rotset->members[i].m12,
				     &rotset->members[i].m13,
				     &rotset->members[i].m21,
				     &rotset->members[i].m22,
				     &rotset->members[i].m23,
				     &rotset->members[i].m31,
				     &rotset->members[i].m32,
				     &rotset->members[i].m33);
			if (ret != 9) {
				fprintf(stderr, "Badly formatted line: %zu: %s\n",
					i, line);
				free_if_not_null(rotset->members);
				free_if_not_null(rotset);
				rotset = NULL;
				goto reading_out;
			}
		}
		++i;
	}

reading_out:
	free_if_not_null(line);
	fclose(fp);
nofile_out:
	return rotset;
}

struct mol_matrix3f_list *mol_matrix3f_list_from_file(const char *filename)
{
	struct mol_matrix3f_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = malloc(sizeof(struct mol_matrix3f_list));
	rotset->size = nrots;
	rotset->members = malloc(nrots * sizeof(struct mol_matrix3f));

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		//10 membered rotation file
		int ret = sscanf(line, "%*s %f %f %f %f %f %f %f %f %f",
			&rotset->members[i].m11,
			&rotset->members[i].m12,
			&rotset->members[i].m13,
			&rotset->members[i].m21,
			&rotset->members[i].m22,
			&rotset->members[i].m23,
			&rotset->members[i].m31,
			&rotset->members[i].m32,
			&rotset->members[i].m33);
		if (ret != 9) {
			//9 membered rotation file
			ret = sscanf(line, "%f %f %f %f %f %f %f %f %f",
				&rotset->members[i].m11,
				&rotset->members[i].m12,
				&rotset->members[i].m13,
				&rotset->members[i].m21,
				&rotset->members[i].m22,
				&rotset->members[i].m23,
				&rotset->members[i].m31,
				&rotset->members[i].m32,
				&rotset->members[i].m33);
			if (ret != 9) {
				fprintf(stderr, "Badly formatted line: %zu: %s\n",
					i, line);
				free_if_not_null(rotset->members);
				free_if_not_null(rotset);
				rotset = NULL;
				goto reading_out;
			}
		}
		++i;
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}

struct mol_matrix3l_list *mol_matrix3l_list_from_file(const char *filename)
{
	struct mol_matrix3l_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		fprintf(stderr, "path: %s\n", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		++nrots;
	}

	if (nrots == 0) {
		fprintf(stderr, "No rotations found in %s\n", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = malloc(sizeof(struct mol_matrix3l_list));
	rotset->size = nrots;
	rotset->members = malloc(nrots * sizeof(struct mol_matrix3l));

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		//10 membered rotation file
		int ret = sscanf(line, "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
			&rotset->members[i].m11,
			&rotset->members[i].m12,
			&rotset->members[i].m13,
			&rotset->members[i].m21,
			&rotset->members[i].m22,
			&rotset->members[i].m23,
			&rotset->members[i].m31,
			&rotset->members[i].m32,
			&rotset->members[i].m33);
		if (ret != 9) {
			//9 membered rotation file
			ret = sscanf(line, "%Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
				&rotset->members[i].m11,
				&rotset->members[i].m12,
				&rotset->members[i].m13,
				&rotset->members[i].m21,
				&rotset->members[i].m22,
				&rotset->members[i].m23,
				&rotset->members[i].m31,
				&rotset->members[i].m32,
				&rotset->members[i].m33);
			if (ret != 9) {
				fprintf(stderr, "Badly formatted line: %zu: %s\n",
					i, line);
				free_if_not_null(rotset->members);
				free_if_not_null(rotset);
				rotset = NULL;
				goto reading_out;
			}
		}
		++i;
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}

void mol_matrix3_multiply(struct mol_matrix3 *out, const struct mol_matrix3 *r1,
                          const struct mol_matrix3 *r2)
{
	struct mol_matrix3 tmp1;
	struct mol_matrix3 tmp2;
	MOL_MATRIX_COPY(tmp1, *r1);
	MOL_MATRIX_COPY(tmp2, *r2);
	out->m11 = tmp1.m11*tmp2.m11 + tmp1.m21*tmp2.m12 + tmp1.m31*tmp2.m13;
	out->m21 = tmp1.m11*tmp2.m21 + tmp1.m21*tmp2.m22 + tmp1.m31*tmp2.m23;
	out->m31 = tmp1.m11*tmp2.m31 + tmp1.m21*tmp2.m32 + tmp1.m31*tmp2.m33;
	out->m12 = tmp1.m12*tmp2.m11 + tmp1.m22*tmp2.m12 + tmp1.m32*tmp2.m13;
	out->m22 = tmp1.m12*tmp2.m21 + tmp1.m22*tmp2.m22 + tmp1.m32*tmp2.m23;
	out->m32 = tmp1.m12*tmp2.m31 + tmp1.m22*tmp2.m32 + tmp1.m32*tmp2.m33;
	out->m13 = tmp1.m13*tmp2.m11 + tmp1.m23*tmp2.m12 + tmp1.m33*tmp2.m13;
	out->m23 = tmp1.m13*tmp2.m21 + tmp1.m23*tmp2.m22 + tmp1.m33*tmp2.m23;
	out->m33 = tmp1.m13*tmp2.m31 + tmp1.m23*tmp2.m32 + tmp1.m33*tmp2.m33;
}
