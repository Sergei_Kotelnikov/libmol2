.. include:: ../global.rst

mol_quaternion
==============

Quaternions are one way to represent rigid body rotations in 3 dimensions. While they
are not as simple to apply as the rotation matrix representation, it is much simpler
to interpolate between two quaternions than two rotation matrices.

.. type:: struct mol_quaternion
          struct mol_quaternionf
          struct mol_quaternionl

Members
-------
   .. member:: double W
               double X
               double Y
               double Z

      The four components of a quaternion. To be a valid quaternion, the norm must be 1.

Functions
---------

The following group of functions simply allocate memory for
a pointer to one mol quaternion structure, and simultaneously
nullify the contents. It will be helpful to read over the standard
quaternion structure, which is posted in the below link,
to understand what these structures represent in greater detail

Similar to many of the other libraries, these functions are grouped by type,
with doubles floats and longs all handled in the respective order below. Note
that there is no input to the functions, and the output is a pointer to a
mol_quaternion structure with null contents

.. function:: struct mol_quaternion *mol_quaternion_create()
              struct mol_quaternionf *mol_quaternionf_create()
              struct mol_quaternionl *mol_quaternionl_create()

    The functions below deal with initializing multiple mol_quaternion structures
    and placing them into lists:

    The following function is a standard initialization. It takes an input ``n``, which
    denotes the quantity of the mol_quaternion structures we can place into the list.
    The function allocates space for each of these structures and sets the contents
    to NULL, and returns a pointer to the head of the list

.. function:: struct mol_quaternionf_list *mol_quaternion_list_create(const size_t n)
.. function:: struct mol_quaternionf_list *mol_quaternionf_list_create(const size_t n)
.. function:: struct mol_quaternionf_list *mol_quaternionl_list_create(const size_t n)

   These next functions are responsible for instantiating lists based upon the contents
   of an input file. The static version is the crux of the functionality, and works by
   taking an input file that contains rotation information, as well as a boolean that
   denotes whether or not we normalize the mol_quaternion_list extracted from the file.
   The following two functions each call the static function with the normalize variable
   set to true and false, respectively, and return a pointer to either a normalized or
   unnormalized list of mol_quaternions.

.. function:: struct mol_quaternionf_list *mol_quaternion_list_from_file(const char *filename)
              struct mol_quaternionf_list *mol_quaternion_list_from_file_unnormalized(const char *filename)
              struct mol_quaternionf_list *mol_quaternionf_list_from_file(const char *filename)
              struct mol_quaternionf_list *mol_quaternionf_list_from_file_unnormalized(const char *filename)
              struct mol_quaternionf_list *mol_quaternionl_list_from_file(const char *filename)
              struct mol_quaternionf_list *mol_quaternionl_list_from_file_unnormalized(const char *filename)

    This set of functions frees up allocated memory for mol_quaternion structs, usually
    memory declared by the 'mol_quaternion_create()' function set. Each of the three
    supported types (doubles, floats and longs) can be freed from each of these
    respective functions. The inputs to these functions are the mol_quaternion pointers
    ``q``  which subsequently get freed by the function.

.. function:: void mol_quaternion_free(struct mol_quaternion *q)
              void mol_quaternionf_free(struct mol_quaternionf *q)
              void mol_quaternionl_free(struct mol_quaternionl *q)

    The set of the length functions are compatible across each type; double, float
    and long, respectively. These functions each take in mol_quaternion structs of
    the corresponding type and return the length of the 3-dimensional quaternion of
    the appropriate type.

.. function:: double mol_quaternion_length(const struct mol_quaternion q)
              float mol_quaternionf_length(const struct mol_quaternionf q)
              long double mol_quaternionl_length(const struct mol_quaternionl q)

   The norm functions are used quite frequently in this library. They take in two
   inputs; a mol_quaternion structure ``q`` and a pointer to a mol_quaternion
   structure ``p``. The function works by taking the length of q and then normalizes
   the function based upon this length. Then, the adjusted normalized quaternion
   coordinates are stored in p which are available to use once the function has finished

.. function:: void mol_quaternion_norm(struct mol_quaternion *p, const struct mol_quaternion q)
              void mol_quaternionf_norm(struct mol_quaternionf *p, const struct mol_quaternionf q)
              void mol_quaternionl_norm(struct mol_quaternionl *p, const struct mol_quaternionl q)

    The following functions utilize the identities in the following matrix to
    translate from a quaternion to a mol_matrix.

    .. math::

       R =
       \begin{bmatrix}
          1-2y^2-2z^2  &      2xy-2zw  &      2xz+2yw \\
              2xy+2zw  &  1-2x^2-2z^2  &      2yz-2x2 \\
              2xz-2yw  &      2yz+2xw  &  1-2x^2-2y^2
       \end{bmatrix}

    The input to these functions include a pointer to a mol_matrix3, ``m``, and a
    mol_quaternion struct ``p``. According to the identities above, the matrix ``m``
    is then constructed from the quaternion values. Again, the three functions are each
    dedicated to doubles, floats and longs respectively.

.. function:: void mol_matrix3_from_quaternion(struct mol_matrix3 *m, const struct mol_quaternion q)
              void mol_matrix3f_from_quaternion(struct mol_matrix3f *m, const struct mol_quaternionf q)
              void mol_matrix3l_from_quaternion(struct mol_matrix3l *m, const struct mol_quaternionl q)

    The same R identity used in the 'mol_matrix3l_from_quaternion' set is assumed for
    the matrices in the following functions. These next functions perform the inverse
    function as the last set, and return a normalized quaternion from a matrix
    representation. The inputs to the function include a pointer to a mol_quaternion,
    ``q``, and a mol_matrix3 structure, ``m``, that has already been initialized
    with values. It must be noted that the function **assumes** that you input
    proper rotation matrix, and safeguards against negative trace values.

.. function:: void mol_quaternion_from_matrix3(struct mol_quaternion *q, const struct mol_matrix3 m)
              void mol_quaternionf_from_matrix3f(struct mol_quaternionf *q, const struct mol_matrix3f m)
              void mol_quaternionl_from_matrix3l(struct mol_quaternionl *q, const struct mol_matrix3l m)

    The scaled function below simply takes an input pointer to mol_quaternion ``q``,
    and multiplies each element of the quaternion by the input ``a``. It then keeps
    the mol_quaternion structure pointed to by ``q``, and returns the scaled structure
    values in the mol_quaternion structure pointed to by ``q_scaled``

.. function:: void mol_quaternion_scaled(const struct mol_quaternion *q, double a, struct mol_quaternion *q_scaled)

    The product of two quaternions :math:`q = q_0+iq_1+jq_2+kq_3` and :math:`r = r_0+ir_1+jr_2+kr_3`
    corresponds to the rotation obtained by first applying :math:`r`, and then :math:`q`. It is defined
    as follows:

    .. math::

       t = q \times r = t_0+it_1+jt_2+kt_3

    where

    .. math::

       t_0 = (r_0q_0 - r_1q_1 - r_2q_2 - r_3q_3) \\
       t_1 = (r_0q_1 + r_1q_0 - r_2q_3 + r_3q_2) \\
       t_2 = (r_0q_2 + r_1q_3 + r_2q_0 - r_3q_1) \\
       t_3 = (r_0q_3 - r_1q_2 + r_2q_1 + r_3q_0)

    In this functions case, we input pointers to two mol_quaternions ``qa`` and ``qb``,
    and multiply ``qa`` and ``qb`` to get the values in the input mol_quaternion pointer
    ``qc``. **WARNING:** Note that this multiplication is **NOT** commutative, so the order
    of arguments does matter.

.. function:: void mol_quaternion_product(const struct mol_quaternion *qa, const struct mol_quaternion *qb, struct mol_quaternion *product)

    The following function flips the 3D quaternion by simply negating the
    vector components while leaving the scalar W component the same. The inputs
    to the function include a pointer to a mol_quaternion ``qa``, as well as a
    pointer to a mol_quaternion ``qc`` which will contain the conjugated values of ``qa``.

.. function:: void mol_quaternion_conjugate(const struct mol_quaternion *q, struct mol_quaternion *qc)

    The following function calculates the inverse of a quaternion, which corresponds to a rotation which
    would undo the transformation of the input.

.. function:: void mol_quaternion_inverse(const struct mol_quaternion *q, struct mol_quaternion *q_inv)
