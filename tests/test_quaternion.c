#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>
#include <stdbool.h>

#include "quaternion.h"

#define _LDBL_EPSILON pow(1.084,-19)

START_TEST(test_structs)
{
	struct mol_quaternion x = {1.9, 2.3, 2.1, 3.3};

	ck_assert(fabs(x.W - 1.9) <= DBL_EPSILON);
	ck_assert(fabs(x.X - 2.3) <= DBL_EPSILON);
	ck_assert(fabs(x.Y - 2.1) <= DBL_EPSILON);
	ck_assert(fabs(x.Z - 3.3) <= DBL_EPSILON);

	struct mol_quaternion *y = mol_quaternion_create();
	mol_quaternion_free(y);
}
END_TEST

START_TEST(test_structsf)
{
	struct mol_quaternionf x = {1.9F, 2.3F, 2.1F, 3.3F};

	ck_assert(fabsf(x.W - 1.9F) <= FLT_EPSILON);
	ck_assert(fabsf(x.X - 2.3F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Y - 2.1F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Z - 3.3F) <= FLT_EPSILON);

	struct mol_quaternionf *y = mol_quaternionf_create();
	mol_quaternionf_free(y);
}
END_TEST

START_TEST(test_structsl)
{
	struct mol_quaternionl x = {1.9L, 2.3L, 2.1L, 3.3L};

	ck_assert(fabsl(x.W - 1.9L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.X - 2.3L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Y - 2.1L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Z - 3.3L) <= LDBL_EPSILON);

	struct mol_quaternionl *y = mol_quaternionl_create();
	mol_quaternionl_free(y);
}
END_TEST

START_TEST(test_macros)
{
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion q2 = {1.0, 3.0, 5.0, 7.0};
	struct mol_quaternionf q1f = {3.0f, 6.0f, 3.0f, 9.0f};
	struct mol_quaternionf q2f = {1.0f, 3.0f, 5.0f, 7.0f};
	struct mol_quaternionl q1l = {3.0L, 6.0L, 3.0L, 9.0L};
	struct mol_quaternionl q2l = {1.0L, 3.0L, 5.0L, 7.0L};
	struct mol_quaternion q;
	struct mol_quaternionf qf;
	struct mol_quaternionl ql;

	MOL_QUATERNION_ADD(q, q1, q2);
	ck_assert(fabs(q.W - 4) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 9) <= DBL_EPSILON);
	ck_assert(fabs(q.Y - 8) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 16) <= DBL_EPSILON);

	MOL_QUATERNION_ADD(qf, q1f, q2f);
	ck_assert(fabsf(qf.W - 4) <= FLT_EPSILON);
	ck_assert(fabsf(qf.X - 9) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Y - 8) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Z - 16) <= FLT_EPSILON);

	MOL_QUATERNION_ADD(ql, q1l, q2l);
	ck_assert(fabsl(ql.W - 4) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.X - 9) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Y - 8) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Z - 16) <= LDBL_EPSILON);

	MOL_QUATERNION_SUB(q, q1, q2);
	ck_assert(fabs(q.W - 2) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 3) <= DBL_EPSILON);
	ck_assert(fabs(q.Y + 2) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 2) <= DBL_EPSILON);

	MOL_QUATERNION_SUB(qf, q1f, q2f);
	ck_assert(fabsf(qf.W - 2) <= FLT_EPSILON);
	ck_assert(fabsf(qf.X - 3) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Y + 2) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Z - 2) <= FLT_EPSILON);

	MOL_QUATERNION_SUB(ql, q1l, q2l);
	ck_assert(fabsl(ql.W - 2) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.X - 3) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Y + 2) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Z - 2) <= LDBL_EPSILON);

	MOL_QUATERNION_MULT_SCALAR(q, q1, 0.5);
	ck_assert(fabs(q.W - 1.5) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q.Y - 1.5) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 4.5) <= DBL_EPSILON);

	MOL_QUATERNION_MULT_SCALAR(qf, q1f, 0.5f);
	ck_assert(fabsf(qf.W - 1.5f) <= FLT_EPSILON);
	ck_assert(fabsf(qf.X - 3.0f) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Y - 1.5f) <= FLT_EPSILON);
	ck_assert(fabsf(qf.Z - 4.5f) <= FLT_EPSILON);

	MOL_QUATERNION_MULT_SCALAR(ql, q1l, 0.5L);
	ck_assert(fabsl(ql.W - 1.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.X - 3.0L) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Y - 1.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(ql.Z - 4.5L) <= LDBL_EPSILON);
}
END_TEST

START_TEST(test_norm)
{
	struct mol_quaternion x = {1.0, 1.0, 1.0, 1.0};
	struct mol_quaternion dst;
	mol_quaternion_norm(&dst, x);

	ck_assert(fabs(dst.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(dst.X - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(dst.Y - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(dst.Z - 0.5) <= DBL_EPSILON);

	x = (struct mol_quaternion) {1.0, -1.0, -1.0, 1.0};
	mol_quaternion_norm(&dst, x);

	ck_assert(fabs(dst.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(dst.X - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(dst.Y - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(dst.Z - 0.5) <= DBL_EPSILON);

	x = (struct mol_quaternion) {-4.0, 0.0, 0.0, -3.0};
	mol_quaternion_norm(&dst, x);

	ck_assert(fabs(dst.W - (-0.8)) <= DBL_EPSILON);
	ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(dst.Y - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(dst.Z - (-0.6)) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_self_norm)
{
	struct mol_quaternion x = {1.0, 1.0, 1.0, 1.0};

	mol_quaternion_norm(&x, x);

	ck_assert(fabs(x.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(x.X - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(x.Y - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(x.Z - 0.5) <= DBL_EPSILON);

	x = (struct mol_quaternion) {1.0, -1.0, -1.0, 1.0};

	mol_quaternion_norm(&x, x);

	ck_assert(fabs(x.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(x.X - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(x.Y - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(x.Z - 0.5) <= DBL_EPSILON);

	x = (struct mol_quaternion) {-4.0, 0.0, 0.0, -3.0};
	mol_quaternion_norm(&x, x);

	ck_assert(fabs(x.W - (-0.8)) <= DBL_EPSILON);
	ck_assert(fabs(x.X - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(x.Y - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(x.Z - (-0.6)) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_selff_norm)
{
	struct mol_quaternionf x = {1.0F, 1.0F, 1.0F, 1.0F};

	mol_quaternionf_norm(&x, x);

	ck_assert(fabsf(x.W - 0.5F) <= FLT_EPSILON);
	ck_assert(fabsf(x.X - 0.5F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Y - 0.5F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Z - 0.5F) <= FLT_EPSILON);

	x = (struct mol_quaternionf) {1.0F, -1.0F, -1.0F, 1.0F};

	mol_quaternionf_norm(&x, x);

	ck_assert(fabsf(x.W - 0.5F) <= FLT_EPSILON);
	ck_assert(fabsf(x.X - (-0.5F)) <= FLT_EPSILON);
	ck_assert(fabsf(x.Y - (-0.5F)) <= FLT_EPSILON);
	ck_assert(fabsf(x.Z - 0.5F) <= FLT_EPSILON);

	x = (struct mol_quaternionf) {-4.0F, 0.0F, 0.0F, -3.0F};
	mol_quaternionf_norm(&x, x);

	ck_assert(fabsf(x.W - (-0.8F)) <= FLT_EPSILON);
	ck_assert(fabsf(x.X - 0.0F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Y - 0.0F) <= FLT_EPSILON);
	ck_assert(fabsf(x.Z - (-0.6F)) <= FLT_EPSILON);
}
END_TEST

START_TEST(test_selfl_norm)
{
	struct mol_quaternionl x = {1.0L, 1.0L, 1.0L, 1.0L};
	mol_quaternionl_norm(&x, x);

	ck_assert(fabsl(x.W - 0.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.X - 0.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Y - 0.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Z - 0.5L) <= LDBL_EPSILON);

	x = (struct mol_quaternionl) {1.0L, -1.0L, -1.0L, 1.0L};
	mol_quaternionl_norm(&x, x);

	ck_assert(fabsl(x.W - 0.5L) <= LDBL_EPSILON);
	ck_assert(fabsl(x.X - (-0.5L)) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Y - (-0.5L)) <= LDBL_EPSILON);
	ck_assert(fabsl(x.Z - 0.5L) <= LDBL_EPSILON);

	x = (struct mol_quaternionl) {-4.0L, 0.0L, 0.0L, -3.0L};
	mol_quaternionl_norm(&x, x);

	ck_assert(fabsl(x.W - (-0.8L)) <= _LDBL_EPSILON);
	ck_assert(fabsl(x.X - 0.0L) <= _LDBL_EPSILON);
	ck_assert(fabsl(x.Y - 0.0L) <= _LDBL_EPSILON);
	ck_assert(fabsl(x.Z - (-0.6L)) <= _LDBL_EPSILON);
}
END_TEST

START_TEST(test_length)
{
	struct mol_quaternion x = {1.0, 1.0, 1.0, 1.0};
	double len = mol_quaternion_length(x);

	ck_assert(fabs(len - 2.0) <= DBL_EPSILON);

	x = (struct mol_quaternion) {-3.0, 4.0, -12.0, 0.0};
	len = mol_quaternion_length(x);

	ck_assert(fabs(len - 13.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_lengthf)
{
	struct mol_quaternionf x = {1.0F, 1.0F, 1.0F, 1.0F};
	float len = mol_quaternionf_length(x);

	ck_assert(fabsf(len - 2.0F) <= FLT_EPSILON);

	x = (struct mol_quaternionf) {-3.0F, 4.0F, -12.0F, 0.0F};
	len = mol_quaternionf_length(x);

	ck_assert(fabsf(len - 13.0F) <= FLT_EPSILON);
}
END_TEST

START_TEST(test_lengthl)
{
	struct mol_quaternionl x = {1.0L, 1.0L, 1.0L, 1.0L};
	long double len = mol_quaternionl_length(x);

	ck_assert(fabsl(len - 2.0L) <= LDBL_EPSILON);

	x = (struct mol_quaternionl) {-3.0L, 4.0L, -12.0L, 0.0L};
	len = mol_quaternionl_length(x);

	ck_assert(fabsl(len - 13.0L) <= LDBL_EPSILON);
}
END_TEST

START_TEST(test_readf)
{
	struct mol_quaternionf_list *rots = mol_quaternionf_list_from_file_unnormalized("base.qua");

	ck_assert(rots != NULL);
	ck_assert_int_eq(rots->size, 72);
	//0.236268	0.881766	-0.204124	0.353553
	ck_assert(fabsf(rots->members[2].W - 0.236268F) <= FLT_EPSILON);
	ck_assert(fabsf(rots->members[2].X - 0.881766F) <= FLT_EPSILON);
	ck_assert(fabsf(rots->members[2].Y - (-0.204124F)) <= FLT_EPSILON);
	ck_assert(fabsf(rots->members[2].Z - 0.353553F) <= FLT_EPSILON);
	mol_list_free(rots);
}
END_TEST

START_TEST(test_read)
{
	struct mol_quaternion_list *rots = mol_quaternion_list_from_file_unnormalized("base.qua");

	ck_assert(rots != NULL);
	ck_assert_int_eq(rots->size, 72);
	//0.236268	0.881766	-0.204124	0.353553
	ck_assert(fabs(rots->members[2].W - 0.236268) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].X - 0.881766) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].Y - (-0.204124)) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].Z - 0.353553) <= DBL_EPSILON);
	mol_list_free(rots);
}
END_TEST

START_TEST(test_readl)
{
	struct mol_quaternionl_list *rots = mol_quaternionl_list_from_file_unnormalized("base.qua");

	ck_assert(rots != NULL);
	ck_assert_int_eq(rots->size, 72);
	//0.236268	0.881766	-0.204124	0.353553
	ck_assert(fabsl(rots->members[2].W - 0.236268L) <= LDBL_EPSILON);
	ck_assert(fabsl(rots->members[2].X - 0.881766L) <= LDBL_EPSILON);
	ck_assert(fabsl(rots->members[2].Y - (-0.204124L)) <= LDBL_EPSILON);
	ck_assert(fabsl(rots->members[2].Z - 0.353553L) <= LDBL_EPSILON);
	mol_list_free(rots);
}
END_TEST

static bool _quaternion_approx_equal(struct mol_quaternion q1, struct mol_quaternion q2)
{
	return (
		((fabs(q1.W - q2.W) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.X - q2.X) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.Y - q2.Y) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.Z - q2.Z) <= 2.0 * DBL_EPSILON))
		|| //quaternion * -1 = quanterion
		((fabs(q1.W + q2.W) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.X + q2.X) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.Y + q2.Y) <= 2.0 * DBL_EPSILON) &&
		 (fabs(q1.Z + q2.Z) <= 2.0 * DBL_EPSILON))
	);
}

static bool _quaternionf_approx_equal( struct mol_quaternionf q1, struct mol_quaternionf q2)
{
	return (
		((fabsf(q1.W - q2.W) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.X - q2.X) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.Y - q2.Y) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.Z - q2.Z) <= 2.0F * FLT_EPSILON))
		|| //quaternion * -1 = quanterion
		((fabsf(q1.W + q2.W) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.X + q2.X) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.Y + q2.Y) <= 2.0F * FLT_EPSILON) &&
		 (fabsf(q1.Z + q2.Z) <= 2.0F * FLT_EPSILON))
	);
}

static bool _quaternionl_approx_equal(struct mol_quaternionl q1, struct mol_quaternionl q2)
{
	return (
		((fabsl(q1.W - q2.W) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.X - q2.X) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.Y - q2.Y) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.Z - q2.Z) <= 2.0L * LDBL_EPSILON))
		|| //quaternion * -1 = quanterion
		((fabsl(q1.W + q2.W) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.X + q2.X) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.Y + q2.Y) <= 2.0L * LDBL_EPSILON) &&
		 (fabsl(q1.Z + q2.Z) <= 2.0L * LDBL_EPSILON))
	);
}

START_TEST(test_roundtrip_identity)
{
	struct mol_quaternion q1 = {1.0, 0.0, 0.0, 0.0};
	struct mol_matrix3 m;
	mol_matrix3_from_quaternion(&m, q1);
	struct mol_quaternion q2;
	mol_quaternion_from_matrix3(&q2, m);
	ck_assert(_quaternion_approx_equal(q1, q2));
}
END_TEST

START_TEST(test_roundtrip_identityf)
{
	struct mol_quaternionf q1 = {1.0F, 0.0F, 0.0F, 0.0F};
	struct mol_matrix3f m;
	mol_matrix3f_from_quaternionf(&m, q1);
	struct mol_quaternionf q2;
	mol_quaternionf_from_matrix3f(&q2, m);
	ck_assert(_quaternionf_approx_equal(q1, q2));
}
END_TEST

START_TEST(test_roundtrip_identityl)
{
	struct mol_quaternionl q1 = {1.0L, 0.0L, 0.0L, 0.0L};
	struct mol_matrix3l m;
	mol_matrix3l_from_quaternionl(&m, q1);
	struct mol_quaternionl q2;
	mol_quaternionl_from_matrix3l(&q2, m);
	ck_assert(_quaternionl_approx_equal(q1, q2));
}
END_TEST

START_TEST(test_roundtrip)
{
	struct mol_quaternion_list *rots = mol_quaternion_list_from_file("base.qua");

	for (size_t i = 0; i < rots->size; i++) {
		struct mol_matrix3 m;
		mol_matrix3_from_quaternion(&m, rots->members[i]);
		struct mol_quaternion q;
		mol_quaternion_from_matrix3(&q, m);
		ck_assert(_quaternion_approx_equal(rots->members[i], q));
	}

	mol_list_free(rots);
}
END_TEST

START_TEST(test_roundtripf)
{
	struct mol_quaternionf_list *rots = mol_quaternionf_list_from_file("base.qua");

	for (size_t i = 0; i < rots->size; i++) {
		struct mol_matrix3f m;
		mol_matrix3f_from_quaternionf(&m, rots->members[i]);
		struct mol_quaternionf q;
		mol_quaternionf_from_matrix3f(&q, m);
		ck_assert(_quaternionf_approx_equal(rots->members[i], q));
	}

	mol_list_free(rots);
}
END_TEST

START_TEST(test_roundtripl)
{
	struct mol_quaternionl_list *rots = mol_quaternionl_list_from_file("base.qua");

	for (size_t i = 0; i < rots->size; i++) {
		struct mol_matrix3l m;
		mol_matrix3l_from_quaternionl(&m, rots->members[i]);
		struct mol_quaternionl q;
		mol_quaternionl_from_matrix3l(&q, m);
		ck_assert(_quaternionl_approx_equal(rots->members[i], q));
	}

	mol_list_free(rots);
}
END_TEST


START_TEST(test_from_axis_angle)
{
	const struct mol_vector3 axis = {.X = 1 / sqrt(14), .Y = 2 / sqrt(14), .Z = 3 / sqrt(14)};
	const struct mol_quaternion q0 = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const struct mol_quaternion q60 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};
	const struct mol_quaternion q60i = {.W = sqrt(3) / 2, .X = -0.5 / sqrt(14), .Y = -1 / sqrt(14), .Z = -1.5 / sqrt(14)};
	const struct mol_quaternion q420 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};

	struct mol_quaternion q;

	mol_quaternion_from_axis_angle(&q, &axis, 0);
	ck_assert(_quaternion_approx_equal(q, q0));

	mol_quaternion_from_axis_angle(&q, &axis, M_PI / 3);
	ck_assert(_quaternion_approx_equal(q, q60));

	mol_quaternion_from_axis_angle(&q, &axis, -M_PI / 3);
	ck_assert(_quaternion_approx_equal(q, q60i));

	// This one is tricky, as there are actually more than one quaternion for each rotation
	mol_quaternion_from_axis_angle(&q, &axis, 7 * M_PI / 3);
	if (q.W < 0) {
		mol_quaternion_scaled(&q, -1, &q);
	}
	ck_assert(_quaternion_approx_equal(q, q420));
}
END_TEST

START_TEST(test_to_axis_angle)
{
	const struct mol_quaternion q0 = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const struct mol_quaternion q60 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};
	const struct mol_quaternion q60i = {.W = -sqrt(3) / 2, .X = -0.5 / sqrt(14), .Y = -1 / sqrt(14), .Z = -1.5 / sqrt(14)};
	const double eps = 2 * DBL_EPSILON; // Same as _quaternion_approx_equal uses
	struct mol_vector3 v;
	double a;

	// For this case, we don't have a well-defined rotation axis, so we just check that it has length of 1.
	mol_quaternion_to_axis_angle(&v, &a, &q0);
	ck_assert(fabs(a - 0) < eps);
	double len = MOL_VEC_SQ_NORM(v);
	ck_assert(fabs(len - 1) < eps);

	mol_quaternion_to_axis_angle(&v, &a, &q60);
	ck_assert(fabs(a - M_PI / 3) < eps);
	ck_assert(fabs(v.X - 1 / sqrt(14)) < eps);
	ck_assert(fabs(v.Y - 2 / sqrt(14)) < eps);
	ck_assert(fabs(v.Z - 3 / sqrt(14)) < eps);

	mol_quaternion_to_axis_angle(&v, &a, &q60i);
	// The rotation is the same, but we use the opposing axis in quaternion. Therefore, the opposing axis will be returned.
	ck_assert(fabs(a + M_PI / 3) < eps);
	ck_assert(fabs(v.X + 1 / sqrt(14)) < eps);
	ck_assert(fabs(v.Y + 2 / sqrt(14)) < eps);
	ck_assert(fabs(v.Z + 3 / sqrt(14)) < eps);
}
END_TEST

START_TEST(test_scaled)
{
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion scaled = {0, 0, 0, 0};

	double a = 4.0;

	mol_quaternion_scaled(&q1, a, &scaled);

	ck_assert(fabs(scaled.W / 4.0 - q1.W) <= DBL_EPSILON);
	ck_assert(fabs(scaled.X / 4.0 - q1.X) <= DBL_EPSILON);
	ck_assert(fabs(scaled.Y / 4.0 - q1.Y) <= DBL_EPSILON);
	ck_assert(fabs(scaled.Z / 4.0 - q1.Z) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_product)
{
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion q2 = {1.0, 2.0, 3.0, 4.0};

	struct mol_quaternion prod = {0, 0, 0, 0};

	mol_quaternion_product(&q1, &q2, &prod);

	ck_assert(fabs(prod.W - (-54.0)) <= DBL_EPSILON);
	ck_assert(fabs(prod.X - (-3.0)) <= DBL_EPSILON);
	ck_assert(fabs(prod.Y - 6.0) <= DBL_EPSILON);
	ck_assert(fabs(prod.Z - 33.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_conjugate)
{
	struct mol_quaternion q1 = {3.0, -6.0, -3.0, -9.0};
	struct mol_quaternion q2 = {0, 0, 0, 0};

	mol_quaternion_conjugate(&q1, &q2);

	ck_assert(fabs(q2.W - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q2.X - 6.0) <= DBL_EPSILON);
	ck_assert(fabs(q2.Y - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q2.Z - 9.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_inverse)
{
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion q_inv = {0, 0, 0, 0};

	mol_quaternion_inverse(&q1, &q_inv);

	// Conjugate outcome will be: 3.0, -6.0, -3.0, -9.0
	// Product outcome will be: 135.0, 0, 0 ,0
	// Inverse outcome SHOULD be: 3/135, -6/135. -3/135, -9/135

	ck_assert(fabs(q_inv.W - (3.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.X - (-6.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.Y - (-3.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.Z - (-9.0 / 135.0)) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_negtrace)
{
	// Matrix diagonals add up to a negative number
	// This does not have any specific physical meaning, but affects internal workings of the algorithm

	// m11 biggest, rotation by pi around (1, 0, 0)
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat = {1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1.0};

	mol_quaternion_from_matrix3(&q1, trace_neg_mat);

	ck_assert(fabs(q1.W) <= DBL_EPSILON);
	ck_assert(fabs(q1.X - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(q1.Y) <= DBL_EPSILON);
	ck_assert(fabs(q1.Z) <= DBL_EPSILON);

	// m22 biggest, rotation by pi around (0, 1, 0)
	struct mol_quaternion q2 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat2 = {-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0};

	mol_quaternion_from_matrix3(&q2, trace_neg_mat2);

	ck_assert(fabs(q2.W) <= DBL_EPSILON);
	ck_assert(fabs(q2.X) <= DBL_EPSILON);
	ck_assert(fabs(q2.Y - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(q2.Z) <= DBL_EPSILON);

	// m33 biggest, rotation by pi around (0, 0, 1)
	struct mol_quaternion q3 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat3 = {-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0};

	mol_quaternion_from_matrix3(&q3, trace_neg_mat3);

	ck_assert(fabs(q3.W) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.X) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.Y) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.Z - 1.0) <= 2*DBL_EPSILON);
}
END_TEST

START_TEST(test_negtracel)
{
	// m11 biggest
	struct mol_quaternionl q1 = {3.0L, 6.0L, 3.0L, 9.0L};
	struct mol_matrix3l trace_neg_mat = {1.0L, 0.0L, 0.0L, 0.0L, -1.0L, 0.0L, 0.0L, 0.0L, -1.0L};

	mol_quaternionl_from_matrix3l(&q1, trace_neg_mat);

	ck_assert(fabsl(q1.W) <= LDBL_EPSILON);
	ck_assert(fabsl(q1.X - 1.0L) <= LDBL_EPSILON);
	ck_assert(fabsl(q1.Y) <= LDBL_EPSILON);
	ck_assert(fabsl(q1.Z) <= LDBL_EPSILON);

	// m22 biggest
	struct mol_quaternionl q2 = {3.0L, 6.0L, 3.0L, 9.0L};
	struct mol_matrix3l trace_neg_mat2 = {-1.0L, 0.0L, 0.0L, 0.0L, 1.0L, 0.0L, 0.0L, 0.0L, -1.0L};

	mol_quaternionl_from_matrix3l(&q2, trace_neg_mat2);

	ck_assert(fabsl(q2.W) <= LDBL_EPSILON);
	ck_assert(fabsl(q2.X) <= LDBL_EPSILON);
	ck_assert(fabsl(q2.Y - 1.0L) <= LDBL_EPSILON);
	ck_assert(fabsl(q2.Z) <= LDBL_EPSILON);

	// m33 biggest
	struct mol_quaternionl q3 = {3.0L, 6.0L, 3.0L, 9.0L};
	struct mol_matrix3l trace_neg_mat3 = {-1.0L, 0.0L, 0.0L, 0.0L, -1.0L, 0.0L, 0.0L, 0.0L, 1.0L};

	mol_quaternionl_from_matrix3l(&q3, trace_neg_mat3);

	ck_assert(fabsl(q3.W) <= LDBL_EPSILON);
	ck_assert(fabsl(q3.X) <= LDBL_EPSILON);
	ck_assert(fabsl(q3.Y) <= LDBL_EPSILON);
	ck_assert(fabsl(q3.Z - 1.0L) <= LDBL_EPSILON);
}
END_TEST

Suite *quaternion_suite(void)
{
	Suite *suite = suite_create("quaternion");

	TCase *tcase_quaternion = tcase_create("test_quaternion");
	tcase_add_test(tcase_quaternion, test_structs);
	tcase_add_test(tcase_quaternion, test_structsf);
	tcase_add_test(tcase_quaternion, test_structsl);
	tcase_add_test(tcase_quaternion, test_macros);
	tcase_add_test(tcase_quaternion, test_self_norm);
	tcase_add_test(tcase_quaternion, test_selff_norm);
	tcase_add_test(tcase_quaternion, test_selfl_norm);
	tcase_add_test(tcase_quaternion, test_norm);
	tcase_add_test(tcase_quaternion, test_length);
	tcase_add_test(tcase_quaternion, test_lengthf);
	tcase_add_test(tcase_quaternion, test_lengthl);
	tcase_add_test(tcase_quaternion, test_scaled);
	tcase_add_test(tcase_quaternion, test_product);
	tcase_add_test(tcase_quaternion, test_conjugate);
	tcase_add_test(tcase_quaternion, test_inverse);
	tcase_add_test(tcase_quaternion, test_from_axis_angle);
	tcase_add_test(tcase_quaternion, test_to_axis_angle);

	TCase *tcase_reading = tcase_create("test_quaternion_reading");
	tcase_add_test(tcase_reading, test_readf);
	tcase_add_test(tcase_reading, test_read);
	tcase_add_test(tcase_reading, test_readl);

	TCase *tcase_roundtrip = tcase_create("test_quaternion_roundtrip");
	tcase_add_test(tcase_roundtrip, test_roundtrip_identity);
	tcase_add_test(tcase_roundtrip, test_roundtrip_identityf);
	tcase_add_test(tcase_roundtrip, test_roundtrip_identityl);
	tcase_add_test(tcase_roundtrip, test_roundtrip);
	tcase_add_test(tcase_roundtrip, test_roundtripf);
	tcase_add_test(tcase_roundtrip, test_roundtripl);

	TCase *tcase_negtrace = tcase_create("test_quaternion_negtrace");
	tcase_add_test(tcase_negtrace, test_negtrace);
	tcase_add_test(tcase_negtrace, test_negtracel);

	suite_add_tcase(suite, tcase_quaternion);
	suite_add_tcase(suite, tcase_reading);
	suite_add_tcase(suite, tcase_roundtrip);
	suite_add_tcase(suite, tcase_negtrace);

	return suite;
}

int main(void)
{
	Suite *suite = quaternion_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
