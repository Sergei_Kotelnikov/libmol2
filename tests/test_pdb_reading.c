#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "pdb.h"

START_TEST(test_read_single)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);
	//ATOM      9  N  AILE A   2     -10.122  46.639   2.897  0.50  8.70           N+1
	ck_assert_str_eq(ag->atom_name[8], " N  ");
	ck_assert(ag->alternate_location[8] == 'A');
	ck_assert_str_eq(ag->residue_name[8], "ILE");
	ck_assert(ag->residue_id[8].chain == 'A');
	ck_assert_int_eq(ag->residue_id[8].residue_seq, 2);
	ck_assert(ag->residue_id[8].insertion == ' ');
	ck_assert(ag->coords[8].X == -10.122);
	ck_assert(ag->coords[8].Y == 46.639);
	ck_assert(ag->coords[8].Z == 2.897);
	ck_assert(ag->occupancy[8] == 0.50);
	ck_assert(ag->B[8] == 8.70);
	ck_assert_str_eq(ag->element[8], " N");
	ck_assert_str_eq(ag->formal_charge[8], "+1");

	for (size_t i = 0; i < ag->natoms; ++i) {
		struct mol_residue *res = mol_atom_residue(ag, i);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
		ck_assert(ag->residue_id[i].chain == res->residue_id.chain);
		ck_assert(ag->residue_id[i].residue_seq == res->residue_id.residue_seq);
		ck_assert(ag->residue_id[i].insertion == res->residue_id.insertion);
	}

	for (size_t i = 0; i < ag->nresidues - 1; i++) {
		struct mol_residue *a = ag->residue_list[i];
		struct mol_residue *b = ag->residue_list[i+1];

		ck_assert(a != NULL);
		ck_assert(b != NULL);

		ck_assert(mol_residue_id_cmp(&a->residue_id,
					     &b->residue_id) == -1);
	}

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_single_just_coords)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_just_coords.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);
	//ATOM      9  N   ILE A   2     -10.122  46.639   2.897
	ck_assert_str_eq(ag->atom_name[8], " N  ");
	ck_assert(ag->alternate_location[8] == ' ');
	ck_assert_str_eq(ag->residue_name[8], "ILE");
	ck_assert(ag->residue_id[8].chain == 'A');
	ck_assert_int_eq(ag->residue_id[8].residue_seq, 2);
	ck_assert(ag->residue_id[8].insertion == ' ');
	ck_assert(ag->coords[8].X == -10.122);
	ck_assert(ag->coords[8].Y == 46.639);
	ck_assert(ag->coords[8].Z == 2.897);
	ck_assert(ag->occupancy[8] == 1.00);
	ck_assert(ag->B[8] == 0.00);
	ck_assert(ag->element[8] == NULL);
	ck_assert(ag->formal_charge[8] == NULL);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_multiple)
{
	struct mol_atom_group_list *ags = mol_read_pdb_models("1nmr.pdb");
	ck_assert(ags != NULL);
	ck_assert_int_eq(ags->size, 20);
	//ATOM     10  N   SER A   2      21.493  -3.740 -45.185  1.00  0.00           N
	struct mol_atom_group ag = ags->members[2];
	ck_assert_str_eq(ag.atom_name[9], " N  ");
	ck_assert(ag.alternate_location[9] == ' ');
	ck_assert_str_eq(ag.residue_name[9], "SER");
	ck_assert(ag.residue_id[9].chain == 'A');
	ck_assert_int_eq(ag.residue_id[9].residue_seq, 2);
	ck_assert(ag.residue_id[9].insertion == ' ');
	ck_assert(ag.coords[9].X == 21.493);
	ck_assert(ag.coords[9].Y == -3.740);
	ck_assert(ag.coords[9].Z == -45.185);
	ck_assert(ag.occupancy[9] == 1.00);
	ck_assert(ag.B[9] == 0.00);
	ck_assert_str_eq(ag.element[9], " N");
	ck_assert_str_eq(ag.formal_charge[9], "  ");
	mol_atom_group_list_free(ags);

}
END_TEST

START_TEST(test_read_nonexistent)
{
	struct mol_atom_group *ag = mol_read_pdb("nonexistent");
	ck_assert(ag == NULL);
	struct mol_atom_group_list *ags = mol_read_pdb_models("nonexistent");
	ck_assert(ags == NULL);
}
END_TEST

START_TEST(test_read_no_atoms)
{
	struct mol_atom_group *ag = mol_read_pdb("no_atoms.pdb");
	ck_assert(ag == NULL);
	struct mol_atom_group_list *ags = mol_read_pdb_models("models_no_atoms.pdb");
	ck_assert(ags == NULL);
}
END_TEST

START_TEST(test_read_no_models)
{
	struct mol_atom_group_list *ags = mol_read_pdb_models("no_models.pdb");
	ck_assert(ags == NULL);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("pdb_reading");

	TCase *tcase_read = tcase_create("test_read");
	tcase_add_test(tcase_read, test_read_single);
	tcase_add_test(tcase_read, test_read_single_just_coords);
	tcase_add_test(tcase_read, test_read_multiple);

	TCase *tcase_errors = tcase_create("test_read_errors");
	tcase_add_test(tcase_errors, test_read_nonexistent);
	tcase_add_test(tcase_errors, test_read_no_atoms);
	tcase_add_test(tcase_errors, test_read_no_models);

	suite_add_tcase(suite, tcase_read);
	suite_add_tcase(suite, tcase_errors);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
