#include <ctype.h>
#include <string.h>
#include <check.h>
#include <stdio.h>

#include "utils.h"

START_TEST(test_sizecmp)
{
	size_t smaller = 5;
  size_t bigger = 6;

	ck_assert(size_t_cmp(&bigger, &smaller) == 1);
	ck_assert(size_t_cmp(&smaller, &bigger) == -1);

  size_t dupl = 6;

	ck_assert(size_t_cmp(&bigger, &dupl) == 0);
}
END_TEST

START_TEST(test_rstrip)
{
	char spacey_string[] = "8spacesafter   ";

	rstrip(spacey_string);

	ck_assert(strlen(spacey_string) == 12);
	ck_assert(spacey_string[strlen(spacey_string)] == '\0');

	char messy_spaces[] = "thing s	";

	rstrip(messy_spaces);

	ck_assert(strlen(messy_spaces) == 7);
	ck_assert(messy_spaces[strlen(messy_spaces)] == '\0');
}
END_TEST

Suite* utils_test_suite(void)
{
	Suite* suite = suite_create("utils");

	TCase* tcases = tcase_create("test_utils_lib");
	tcase_add_test(tcases, test_sizecmp);
	tcase_add_test(tcases, test_rstrip);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite* suite = utils_test_suite();
	SRunner* runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
