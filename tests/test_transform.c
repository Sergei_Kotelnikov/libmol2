#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <float.h>
#include <math.h>
#include <errno.h>

#include "transform.h"
#include "vector.h"
#include "pdb.h"

/*
  -----------------------------------
  Start of unit tests for transform.c
  -----------------------------------
*/

START_TEST(test_translate_double)
{
	// Checking to see if the function is returning what is expected
	struct mol_vector3 one = {.X = 3.0, .Y = 4.0, .Z = 5.0};
	struct mol_vector3 two = {.X = 5.0, .Y = 6.0, .Z = 7.0};

	mol_vector3_translate(&one, &two);

	ck_assert(fabs(one.X - 8.0) <= DBL_EPSILON);
	ck_assert(fabs(one.Y - 10.0) <= DBL_EPSILON);
	ck_assert(fabs(one.Z - 12.0) <= DBL_EPSILON);

	// Adding test case for corner case of passing a null pointer
	struct mol_vector3* null_vec_ptr = NULL;

	// Reset global variable errno to zero on each iteration to see if null pointer
	// input flags a change in errno value
	errno = 0;
	mol_vector3_translate(&one, null_vec_ptr);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate(null_vec_ptr, &two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate(null_vec_ptr, null_vec_ptr);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_translate_float)
{
	struct mol_vector3f one = {.X = 3.0F, .Y = 4.0F, .Z = 5.0F};
	struct mol_vector3f two = {.X = 5.0F, .Y = 6.0F, .Z = 7.0F};

	mol_vector3f_translate(&one, &two);

	ck_assert(fabsf(one.X - 8.0F) <= FLT_EPSILON);
	ck_assert(fabsf(one.Y - 10.0F) <= FLT_EPSILON);
	ck_assert(fabsf(one.Z - 12.0F) <= FLT_EPSILON);

	struct mol_vector3f* null_vec_ptr = NULL;

	errno = 0;
	mol_vector3f_translate(&one, null_vec_ptr);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate(null_vec_ptr, &two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate(null_vec_ptr, null_vec_ptr);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_translate_long)
{
	struct mol_vector3l one = {.X = 3.0L, .Y = 4.0L, .Z = 5.0L};
	struct mol_vector3l two = {.X = 5.0L, .Y = 6.0L, .Z = 7.0L};

	mol_vector3l_translate(&one, &two);

	ck_assert(fabsl(one.X - 8.0L) <= LDBL_EPSILON);
	ck_assert(fabsl(one.Y - 10.0L) <= LDBL_EPSILON);
	ck_assert(fabsl(one.Z - 12.0L) <= LDBL_EPSILON);

	struct mol_vector3l* null_vec_ptr = NULL;

	errno = 0;
	mol_vector3l_translate(&one, null_vec_ptr);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate(null_vec_ptr, &two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate(null_vec_ptr, null_vec_ptr);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_translate_double_list)
{
        // Start by testing out if array of vectors translates as expected
	struct mol_vector3 input[3] = {
		{.X = 2.0, .Y = 3.0, .Z = 4.0},
		{.X = 3.0, .Y = 4.0, .Z = 5.0},
		{.X = 4.0, .Y = 5.0, .Z = 6.0}
	};

	struct mol_vector3 expected[3] = {
		{.X = 3.0, .Y = 4.0, .Z = 5.0},
		{.X = 4.0, .Y = 5.0, .Z = 6.0},
		{.X = 5.0, .Y = 6.0, .Z = 7.0}
	};

	const struct mol_vector3 vec_one = {.X = 1.0, .Y = 1.0, .Z = 1.0};

	size_t len = 3;

	mol_vector3_translate_list(input, len, &vec_one);

	for (size_t i = 0; i < 3; i++) {
		ck_assert(fabs(input[i].X - expected[i].X) < DBL_EPSILON);
		ck_assert(fabs(input[i].Y - expected[i].Y) < DBL_EPSILON);
		ck_assert(fabs(input[i].Z - expected[i].Z) < DBL_EPSILON);
	}

	// Testing all possible combinations of NULL inputs
	struct mol_vector3* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_vector3* null_vec_two = NULL;

	errno = 0;
	mol_vector3_translate_list(null_vec_one, len_null, &vec_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate_list(null_vec_one, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate_list(input, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate_list(input, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate_list(null_vec_one, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_translate_list(null_vec_one, len, &vec_one);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_translate_float_list)
{
	struct mol_vector3f input[3] = {
		{.X = 2.0F, .Y = 3.0F, .Z = 4.0F},
		{.X = 3.0F, .Y = 4.0F, .Z = 5.0F},
		{.X = 4.0F, .Y = 5.0F, .Z = 6.0F}
	};

	struct mol_vector3f expected[3] = {
		{.X = 3.0F, .Y = 4.0F, .Z = 5.0F},
		{.X = 4.0F, .Y = 5.0F, .Z = 6.0F},
		{.X = 5.0F, .Y = 6.0F, .Z = 7.0F}
	};

	const struct mol_vector3f vec_one = {.X = 1.0F, .Y = 1.0F, .Z = 1.0F};

	size_t len = 3;

	mol_vector3f_translate_list(input, len, &vec_one);

	for (size_t i = 0; i < 3; i++) {
		ck_assert(fabsf(input[i].X - expected[i].X) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Y - expected[i].Y) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Z - expected[i].Z) < FLT_EPSILON);
	}

	struct mol_vector3f* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_vector3f* null_vec_two = NULL;

	errno = 0;
	mol_vector3f_translate_list(null_vec_one, len_null, &vec_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate_list(null_vec_one, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate_list(input, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate_list(input, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate_list(null_vec_one, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_translate_list(null_vec_one, len, &vec_one);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_translate_long_list)
{
	struct mol_vector3l input[3] = {
		{.X = 2.0L, .Y = 3.0L, .Z = 4.0L},
		{.X = 3.0L, .Y = 4.0L, .Z = 5.0L},
		{.X = 4.0L, .Y = 5.0L, .Z = 6.0L}
	};

	struct mol_vector3l expected[3] = {
		{.X = 3.0L, .Y = 4.0L, .Z = 5.0L},
		{.X = 4.0L, .Y = 5.0L, .Z = 6.0L},
		{.X = 5.0L, .Y = 6.0L, .Z = 7.0L}
	};

	const struct mol_vector3l vec_one = {.X = 1.0L, .Y = 1.0L, .Z = 1.0L};

	size_t len = 3;

	mol_vector3l_translate_list(input, len, &vec_one);

	for (size_t i = 0; i < 3; i++) {
		ck_assert(fabsl(input[i].X - expected[i].X) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Y - expected[i].Y) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Z - expected[i].Z) < LDBL_EPSILON);
	}

	struct mol_vector3l* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_vector3l* null_vec_two = NULL;

	errno = 0;
	mol_vector3l_translate_list(null_vec_one, len_null, &vec_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate_list(null_vec_one, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate_list(input, len_null, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate_list(input, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate_list(null_vec_one, len, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_translate_list(null_vec_one, len, &vec_one);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_double)
{
	// Start by assuring the rotate function returns expected results
	struct mol_matrix3 mat_one = {
		.m11 = 3.0, .m12 = 4.0, .m13 = 5.0, .m21 = 6.0, .m22 = 7.0,
		.m23 = 8.0, .m31 = 9.0, .m32 = 10.0, .m33 = 11.0
	};

	struct mol_vector3 vec_one = {.X = 3.0, .Y = 4.0, .Z = 5.0};

	mol_vector3_rotate(&vec_one, &mat_one);

	ck_assert(fabs(50.0 - vec_one.X) < DBL_EPSILON);
	ck_assert(fabs(86.0 - vec_one.Y) < DBL_EPSILON);
	ck_assert(fabs(122.0 - vec_one.Z) < DBL_EPSILON);

	// Tests null pointer edge cases
	struct mol_matrix3* null_mat = NULL;
	struct mol_vector3* null_vec = NULL;

	errno = 0;
	mol_vector3_rotate(null_vec, &mat_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate(&vec_one, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate(null_vec, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_float)
{
	struct mol_matrix3f mat_one = {
		.m11 = 3.0F, .m12 = 4.0F, .m13 = 5.0F, .m21 = 6.0F, .m22 = 7.0F,
		.m23 = 8.0F, .m31 = 9.0F, .m32 = 10.0F, .m33 = 11.0F
	};

	struct mol_vector3f vec_one = {.X = 3.0F, .Y = 4.0F, .Z = 5.0F};

	mol_vector3f_rotate(&vec_one, &mat_one);

	ck_assert(fabsf(50.0F - vec_one.X) < FLT_EPSILON);
	ck_assert(fabsf(86.0F - vec_one.Y) < FLT_EPSILON);
	ck_assert(fabsf(122.0F - vec_one.Z) < FLT_EPSILON);

	struct mol_matrix3f* null_mat = NULL;
	struct mol_vector3f* null_vec = NULL;

	errno = 0;
	mol_vector3f_rotate(null_vec, &mat_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate(&vec_one, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate(null_vec, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_long)
{
	struct mol_matrix3l mat_one = {
		.m11 = 3.0L, .m12 = 4.0L, .m13 = 5.0L, .m21 = 6.0L, .m22 = 7.0L,
		.m23 = 8.0L, .m31 = 9.0L, .m32 = 10.0L, .m33 = 11.0L
	};

	struct mol_vector3l vec_one = {.X = 3.0L, .Y = 4.0L, .Z = 5.0L};

	mol_vector3l_rotate(&vec_one, &mat_one);

	ck_assert(fabsl(50.0L - vec_one.X) < LDBL_EPSILON);
	ck_assert(fabsl(86.0L - vec_one.Y) < LDBL_EPSILON);
	ck_assert(fabsl(122.0L - vec_one.Z) < LDBL_EPSILON);

	struct mol_matrix3l* null_mat = NULL;
	struct mol_vector3l* null_vec = NULL;

	errno = 0;
	mol_vector3l_rotate(null_vec, &mat_one);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate(&vec_one, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate(null_vec, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_double_list)
{
	// Test if the function is returning values as expected
	struct mol_vector3 input[2] = {
		{.X = 2.0, .Y = 3.0, .Z = 4.0},
		{.X = 1.0, .Y = 1.0, .Z = 1.0}
	};

	struct mol_vector3 expected[2] = {
		{.X = 38.0, .Y = 65.0, .Z = 92.0},
		{.X = 12.0, .Y = 21.0, .Z = 30.0}
	};

	struct mol_matrix3 matrix = {
		.m11 = 3.0, .m12 = 4.0, .m13 = 5.0, .m21 = 6.0, .m22 = 7.0,
		.m23 = 8.0, .m31 = 9.0, .m32 = 10.0, .m33 = 11.0
	};

	size_t len = 2;

	mol_vector3_rotate_list(input, len, &matrix);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabs(input[i].X - expected[i].X) < DBL_EPSILON);
		ck_assert(fabs(input[i].Y - expected[i].Y) < DBL_EPSILON);
		ck_assert(fabs(input[i].Z - expected[i].Z) < DBL_EPSILON);
	}

	// Test null input edge cases
	struct mol_vector3* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_matrix3* null_mat = NULL;

	errno = 0;
	mol_vector3_rotate_list(null_vec_one, len, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate_list(null_vec_one, len_null, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate_list(null_vec_one, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate_list(input, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate_list(input, len, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_rotate_list(null_vec_one, len, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_float_list)
{
	struct mol_vector3f input[2] = {
		{.X = 2.0F, .Y = 3.0F, .Z = 4.0F},
		{.X = 1.0F, .Y = 1.0F, .Z = 1.0F}
	};

	struct mol_vector3f expected[2] = {
		{.X = 38.0F, .Y = 65.0F, .Z = 92.0F},
		{.X = 12.0F, .Y = 21.0F, .Z = 30.0F}
	};

	struct mol_matrix3f matrix = {
		.m11 = 3.0F, .m12 = 4.0F, .m13 = 5.0F, .m21 = 6.0F, .m22 = 7.0F,
		.m23 = 8.0F, .m31 = 9.0F, .m32 = 10.0F, .m33 = 11.0F
	};


	size_t len = 2;

	mol_vector3f_rotate_list(input, len, &matrix);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabsf(input[i].X - expected[i].X) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Y - expected[i].Y) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Z - expected[i].Z) < FLT_EPSILON);
	}

	struct mol_vector3f* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_matrix3f* null_mat = NULL;

	errno = 0;
	mol_vector3f_rotate_list(null_vec_one, len, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate_list(null_vec_one, len_null, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate_list(null_vec_one, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate_list(input, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate_list(input, len, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_rotate_list(null_vec_one, len, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_rotate_long_list)
{
	struct mol_vector3l input[2] = {
		{.X = 2.0L, .Y = 3.0L, .Z = 4.0L},
		{.X = 1.0L, .Y = 1.0L, .Z = 1.0L}
	};

	struct mol_vector3l expected[2] = {
		{.X = 38.0L, .Y = 65.0L, .Z = 92.0L},
		{.X = 12.0L, .Y = 21.0L, .Z = 30.0L}
	};

	struct mol_matrix3l matrix = {
		.m11 = 3.0L, .m12 = 4.0L, .m13 = 5.0L, .m21 = 6.0L, .m22 = 7.0L,
		.m23 = 8.0L, .m31 = 9.0L, .m32 = 10.0L, .m33 = 11.0L
	};

	size_t len = 2;

	mol_vector3l_rotate_list(input, len, &matrix);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabsl(input[i].X - expected[i].X) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Y - expected[i].Y) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Z - expected[i].Z) < LDBL_EPSILON);
	}
	struct mol_vector3l* null_vec_one = NULL;
	size_t len_null = 0;
	const struct mol_matrix3l* null_mat = NULL;

	errno = 0;
	mol_vector3l_rotate_list(null_vec_one, len, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate_list(null_vec_one, len_null, &matrix);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate_list(null_vec_one, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate_list(input, len_null, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate_list(input, len, null_mat);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_rotate_list(null_vec_one, len, null_mat);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move)
{
	// Testing regular inputs to see if the function works as expected
	struct mol_matrix3 mat_one = {
		.m11 = 3.0, .m12 = 4.0, .m13 = 5.0, .m21 = 6.0, .m22 = 7.0,
		.m23 = 8.0, .m31 = 9.0, .m32 = 10.0, .m33 = 11.0
	};

	struct mol_vector3 vec_one = {.X = 3.0, .Y = 4.0, .Z = 5.0};
	struct mol_vector3 vec_two = {.X = 13.0, .Y = 14.0, .Z = 15.0};

	mol_vector3_move(&vec_one, &mat_one, &vec_two);

	ck_assert(fabs(63.0 - vec_one.X) < DBL_EPSILON);
	ck_assert(fabs(100.0 - vec_one.Y) < DBL_EPSILON);
	ck_assert(fabs(137.0 - vec_one.Z) < DBL_EPSILON);

	struct mol_matrix3* null_mat = NULL;
	struct mol_vector3* null_vec_one = NULL;
	struct mol_vector3* null_vec_two = NULL;

	// Testing all possible combinations of NULL inputs
	errno = 0;
	mol_vector3_move(null_vec_one, &mat_one, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(null_vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(null_vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(&vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(&vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(&vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move(null_vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move_float)
{
	struct mol_matrix3f mat_one = {
		.m11 = 3.0F, .m12 = 4.0F, .m13 = 5.0F, .m21 = 6.0F, .m22 = 7.0F,
		.m23 = 8.0F, .m31 = 9.0F, .m32 = 10.0F, .m33 = 11.0F
	};

	struct mol_vector3f vec_one = {.X = 3.0F, .Y = 4.0F, .Z = 5.0F};
	struct mol_vector3f vec_two = {.X = 13.0F, .Y = 14.0F, .Z = 15.0F};

	mol_vector3f_move(&vec_one, &mat_one, &vec_two);

	ck_assert(fabsf(63.0F - vec_one.X) < FLT_EPSILON);
	ck_assert(fabsf(100.0F - vec_one.Y) < FLT_EPSILON);
	ck_assert(fabsf(137.0F - vec_one.Z) < FLT_EPSILON);

	struct mol_matrix3f* null_mat = NULL;
	struct mol_vector3f* null_vec_one = NULL;
	struct mol_vector3f* null_vec_two = NULL;

	errno = 0;
	mol_vector3f_move(null_vec_one, &mat_one, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(null_vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(null_vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(&vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(&vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(&vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move(null_vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move_long)
{
	struct mol_matrix3l mat_one = {
		.m11 = 3.0L, .m12 = 4.0L, .m13 = 5.0L, .m21 = 6.0L, .m22 = 7.0L,
		.m23 = 8.0L, .m31 = 9.0L, .m32 = 10.0L, .m33 = 11.0L
	};

	struct mol_vector3l vec_one = {.X = 3.0L, .Y = 4.0L, .Z = 5.0L};
	struct mol_vector3l vec_two = {.X = 13.0L, .Y = 14.0L, .Z = 15.0L};

	mol_vector3l_move(&vec_one, &mat_one, &vec_two);

	ck_assert(fabsl(63.0L - vec_one.X) < LDBL_EPSILON);
	ck_assert(fabsl(100.0L - vec_one.Y) < LDBL_EPSILON);
	ck_assert(fabsl(137.0L - vec_one.Z) < LDBL_EPSILON);

	struct mol_matrix3l* null_mat = NULL;
	struct mol_vector3l* null_vec_one = NULL;
	struct mol_vector3l* null_vec_two = NULL;

	errno = 0;
	mol_vector3l_move(null_vec_one, &mat_one, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(null_vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(null_vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(&vec_one, null_mat, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(&vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(&vec_one, null_mat, &vec_two);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move(null_vec_one, &mat_one, null_vec_two);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move_list)
{
	// First start by testing for proper functionality
	struct mol_vector3 input[2] = {
		{.X = 2.0, .Y = 3.0, .Z = 4.0},
		{.X = 1.0, .Y = 1.0, .Z = 1.0}
	};

	struct mol_vector3 expected[2] = {
		{.X = 39.0, .Y = 66.0, .Z = 93.0},
		{.X = 13.0, .Y = 22.0, .Z = 31.0}
	};

	struct mol_matrix3 matrix = {
		.m11 = 3.0, .m12 = 4.0, .m13 = 5.0, .m21 = 6.0, .m22 = 7.0,
		.m23 = 8.0, .m31 = 9.0, .m32 = 10.0, .m33 = 11.0
	};

	struct mol_vector3 vector_add = {.X = 1.0, .Y = 1.0, .Z = 1.0};

	size_t len = 2;

	mol_vector3_move_list(input, len, &matrix, &vector_add);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabs(input[i].X - expected[i].X) < DBL_EPSILON);
		ck_assert(fabs(input[i].Y - expected[i].Y) < DBL_EPSILON);
		ck_assert(fabs(input[i].Z - expected[i].Z) < DBL_EPSILON);
	}

	// Testing all possible combinations of NULL inputs
	struct mol_vector3* null_array = NULL;
	size_t null_len = 0;
	struct mol_matrix3* null_mat = NULL;
	struct mol_vector3* null_vec = NULL;

	errno = 0;
	mol_vector3_move_list(null_array, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, null_len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(null_array, len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3_move_list(input, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move_list_float)
{
	struct mol_vector3f input[2] = {
		{.X = 2.0F, .Y = 3.0F, .Z = 4.0F},
		{.X = 1.0F, .Y = 1.0F, .Z = 1.0F}
	};

	struct mol_vector3f expected[2] = {
		{.X = 39.0F, .Y = 66.0F, .Z = 93.0F},
		{.X = 13.0F, .Y = 22.0F, .Z = 31.0F}
	};

	struct mol_matrix3f matrix = {
		.m11 = 3.0F, .m12 = 4.0F, .m13 = 5.0F, .m21 = 6.0F, .m22 = 7.0F,
		.m23 = 8.0F, .m31 = 9.0F, .m32 = 10.0F, .m33 = 11.0F
	};

	struct mol_vector3f vector_add = {.X = 1.0F, .Y = 1.0F, .Z = 1.0F};

	size_t len = 2;

	mol_vector3f_move_list(input, len, &matrix, &vector_add);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabsf(input[i].X - expected[i].X) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Y - expected[i].Y) < FLT_EPSILON);
		ck_assert(fabsf(input[i].Z - expected[i].Z) < FLT_EPSILON);
	}

	struct mol_vector3f* null_array = NULL;
	size_t null_len = 0;
	struct mol_matrix3f* null_mat = NULL;
	struct mol_vector3f* null_vec = NULL;

	errno = 0;
	mol_vector3f_move_list(null_array, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, null_len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(null_array, len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3f_move_list(input, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_vector_move_list_long)
{
	struct mol_vector3l input[2] = {
		{.X = 2.0L, .Y = 3.0L, .Z = 4.0L},
		{.X = 1.0L, .Y = 1.0L, .Z = 1.0L}
	};

	struct mol_vector3l expected[2] = {
		{.X = 39.0L, .Y = 66.0L, .Z = 93.0L},
		{.X = 13.0L, .Y = 22.0L, .Z = 31.0L}
	};

	struct mol_matrix3l matrix = {
		.m11 = 3.0L, .m12 = 4.0L, .m13 = 5.0L, .m21 = 6.0L, .m22 = 7.0L,
		.m23 = 8.0L, .m31 = 9.0L, .m32 = 10.0L, .m33 = 11.0L
	};

	struct mol_vector3l vector_add = {.X = 1.0L, .Y = 1.0L, .Z = 1.0L};

	size_t len = 2;

	mol_vector3l_move_list(input, len, &matrix, &vector_add);

	for (size_t i = 0; i < len; i++) {
		ck_assert(fabsl(input[i].X - expected[i].X) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Y - expected[i].Y) < LDBL_EPSILON);
		ck_assert(fabsl(input[i].Z - expected[i].Z) < LDBL_EPSILON);
	}

	struct mol_vector3l* null_array = NULL;
	size_t null_len = 0;
	struct mol_matrix3l* null_mat = NULL;
	struct mol_vector3l* null_vec = NULL;

	errno = 0;
	mol_vector3l_move_list(null_array, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, null_len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(null_array, len, &matrix, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, null_len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, null_len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, null_len, &matrix, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, len, null_mat, null_vec);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, len, null_mat, &vector_add);
	ck_assert(errno == EFAULT);

	errno = 0;
	mol_vector3l_move_list(input, len, &matrix, null_vec);
	ck_assert(errno == EFAULT);
}
END_TEST

START_TEST(test_transform_to_residue)
{
	struct mol_atom_group *rotamer = mol_read_pdb("asn_canonical.pdb");
	struct mol_matrix3_list* rotations = mol_matrix3_list_from_file("rot.prm");

	struct mol_residue *residue = find_residue(rotamer, ' ', 1, ' ');

	struct mol_vector3 translation = {-10, 5, 3};
	struct mol_matrix3 rotation = rotations->members[0];

	mol_atom_group_move(rotamer, &rotation, &translation);

	struct mol_vector3 output_translation = {0};
	struct mol_matrix3 output_rotation = {0};

	mol_get_transform_to_residue(&output_translation, &output_rotation, rotamer, residue);

	ck_assert(fabs(translation.X - output_translation.X) <= DBL_EPSILON);
	ck_assert(fabs(translation.Y - output_translation.Y) <= DBL_EPSILON);
	ck_assert(fabs(translation.Z - output_translation.Z) <= DBL_EPSILON);

	ck_assert(fabs(rotation.m11 - output_rotation.m11) <= 1E-5);
	ck_assert(fabs(rotation.m12 - output_rotation.m12) <= 1E-5);
	ck_assert(fabs(rotation.m13 - output_rotation.m13) <= 1E-5);
	ck_assert(fabs(rotation.m21 - output_rotation.m21) <= 1E-5);
	ck_assert(fabs(rotation.m22 - output_rotation.m22) <= 1E-5);
	ck_assert(fabs(rotation.m23 - output_rotation.m23) <= 1E-5);
	ck_assert(fabs(rotation.m31 - output_rotation.m31) <= 1E-5);
	ck_assert(fabs(rotation.m32 - output_rotation.m32) <= 1E-5);
	ck_assert(fabs(rotation.m33 - output_rotation.m33) <= 1E-5);

	mol_atom_group_free(rotamer);
	mol_list_free(rotations);
}
END_TEST

START_TEST(test_symmetry_transform)
{
	struct mol_matrix3_list *rots = mol_matrix3_list_from_file("symmetry_rot.prm");
	struct mol_vector3 vector = {.X = -0.5, .Y = -0.25, .Z = 0.7384311362463636};

	struct mol_vector3 out_vector;
	struct mol_matrix3 out_rot;

	mol_get_symmetry_transform(&out_vector, &out_rot, &vector, &(rots->members[0]), 3);

	//this is a trimer, so the third subunit should be a zero vector out and identity
	//matrix

	ck_assert(fabs(out_vector.X - 0.0) <= 1E-5);
	ck_assert(fabs(out_vector.Y - 0.0) <= 1E-5);
	ck_assert(fabs(out_vector.Z - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m11 - 1.0) <= 1E-5);
	ck_assert(fabs(out_rot.m12 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m13 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m21 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m22 - 1.0) <= 1E-5);
	ck_assert(fabs(out_rot.m23 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m31 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m32 - 0.0) <= 1E-5);
	ck_assert(fabs(out_rot.m33 - 1.0) <= 1E-5);
}
END_TEST

START_TEST(test_vector3_rotate_by_quaternion)
{
	const struct mol_quaternion q_unit = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const double cos_a = cos(M_PI / 4), sin_a = sin(M_PI / 4); // Rotation by pi/2
	const struct mol_quaternion q_x = {.W = cos_a, .X = sin_a, .Y = 0, .Z = 0};
	const struct mol_quaternion q_x_2pi = {.W = -cos_a, .X = -sin_a, .Y = 0, .Z = 0};
	const struct mol_quaternion q_y = {.W = cos_a, .X = 0, .Y = sin_a, .Z = 0};
	const struct mol_quaternion q_z = {.W = cos_a, .X = 0, .Y = 0, .Z = sin_a};
	const struct mol_vector3 v0 = {.X = 1, .Y = 2, .Z = 3};
	const double eps = 10 * DBL_EPSILON; // Allow some leeway as the rotation is non-trivial operation. But not too much of it.
	struct mol_vector3 v;

	v = v0;
	mol_vector3_rotate_by_quaternion(&v, &q_unit); // Don't rotate
	ck_assert(fabs(v.X - v0.X) <= eps);
	ck_assert(fabs(v.Y - v0.Y) <= eps);
	ck_assert(fabs(v.Z - v0.Z) <= eps);

	v = v0;
	mol_vector3_rotate_by_quaternion(&v, &q_x); // Rotate by pi/2 around X axis: Y and Z change places
	ck_assert(fabs(v.X - v0.X) <= eps);
	ck_assert(fabs(v.Y + v0.Z) <= eps);
	ck_assert(fabs(v.Z - v0.Y) <= eps);

	v = v0;
	mol_vector3_rotate_by_quaternion(&v, &q_y);
	ck_assert(fabs(v.X - v0.Z) <= eps);
	ck_assert(fabs(v.Y - v0.Y) <= eps);
	ck_assert(fabs(v.Z + v0.X) <= eps);

	v = v0;
	mol_vector3_rotate_by_quaternion(&v, &q_z);
	ck_assert(fabs(v.X + v0.Y) <= eps);
	ck_assert(fabs(v.Y - v0.X) <= eps);
	ck_assert(fabs(v.Z - v0.Z) <= eps);

	// Multiplying all elements of quaternion by (-1) should not change anything.
	v = v0;
	mol_vector3_rotate_by_quaternion(&v, &q_x_2pi);
	ck_assert(fabs(v.X - v0.X) <= eps);
	ck_assert(fabs(v.Y + v0.Z) <= eps);
	ck_assert(fabs(v.Z - v0.Y) <= eps);

	// Trying to rotate zero vector
	MOL_VEC_SET_SCALAR(v, 0);
	mol_vector3_rotate_by_quaternion(&v, &q_x);
	ck_assert(fabs(v.X - 0) <= eps);
	ck_assert(fabs(v.Y - 0) <= eps);
	ck_assert(fabs(v.Z - 0) <= eps);
}
END_TEST

/*
  ------------------------------------------
  End of Unit Tests from Transform library
  ------------------------------------------
*/

Suite* transform_test_suite(void)
{
	// Creating test suite and adding all 18 unit tests; 6 for each function of 3 types; double, float, long
	Suite* suite = suite_create("transform");

	TCase* tcases = tcase_create("test_transform");
	tcase_add_test(tcases, test_translate_double);
	tcase_add_test(tcases, test_translate_float);
	tcase_add_test(tcases, test_translate_long);
	tcase_add_test(tcases, test_rotate_double);
	tcase_add_test(tcases, test_rotate_float);
	tcase_add_test(tcases, test_rotate_long);
	tcase_add_test(tcases, test_vector_move);
	tcase_add_test(tcases, test_vector_move_float);
	tcase_add_test(tcases, test_vector_move_long);
	tcase_add_test(tcases, test_translate_double_list);
	tcase_add_test(tcases, test_translate_float_list);
	tcase_add_test(tcases, test_translate_long_list);
	tcase_add_test(tcases, test_rotate_double_list);
	tcase_add_test(tcases, test_rotate_float_list);
	tcase_add_test(tcases, test_rotate_long_list);
	tcase_add_test(tcases, test_vector_move_list);
	tcase_add_test(tcases, test_vector_move_list_float);
	tcase_add_test(tcases, test_vector_move_list_long);

	tcase_add_test(tcases, test_transform_to_residue);
	tcase_add_test(tcases, test_symmetry_transform);

	tcase_add_test(tcases, test_vector3_rotate_by_quaternion);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite* suite = transform_test_suite();
	SRunner* runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
