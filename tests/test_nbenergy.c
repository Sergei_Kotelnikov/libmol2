#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "nbenergy.h"
#include "pdb.h"
#include "json.h"
#include "icharmm.h"

struct mol_atom_group *test_ag;
struct agsetup test_ags;

static const double delta = 1e-6;
static const double tolerance = 1e-5;

#define _ALMOST_ZERO(x) (fabs((x)) < 1e-6)

double _vdwengs03_wrapper(void)
{
	double energy = 0.;
	vdwengs03(0.5, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	return energy;
}

double _vdweng_wrapper(void)
{
	double energy = 0.;
	vdweng(test_ag, &energy, test_ags.nblst);
	return energy;
}
double _elengs03_wrapper(void)
{
	double energy = 0.;
	elengs03(0.5, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	return energy;
}

double _eleng_wrapper(void)
{
	double energy = 0.;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	return energy;
}


void test_grads(const double d, double (*energy_func)(void))
{
	double en, en1, t;
	struct mol_vector3 *fs = malloc(test_ag->natoms * sizeof(struct mol_vector3));

	en = energy_func();

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		t = test_ag->coords[i].X;
		test_ag->coords[i].X = d + t;
		en1 = energy_func();
		test_ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;

		t = test_ag->coords[i].Y;
		test_ag->coords[i].Y = d + t;
		en1 = energy_func();
		test_ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;

		t = test_ag->coords[i].Z;
		test_ag->coords[i].Z = d + t;
		en1 = energy_func();
		test_ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	mol_zero_gradients(test_ag);
	energy_func();
	char msg[256];

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			test_ag->gradients[i].X, test_ag->gradients[i].Y, test_ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(test_ag->gradients[i].X - fs[i].X) < tolerance, msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Y - fs[i].Y) < tolerance, msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Z - fs[i].Z) < tolerance, msg);

	}
	free(fs);
}

void setup_phenol(void)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_phenol(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

void setup_three(void)
{
	test_ag = mol_read_json("propylene_oxide.json");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_three(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

void setup_tip3p(void)
{
	test_ag = mol_read_pdb("water.pdb");
	mol_atom_group_read_geometry(test_ag, "water.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_tip3p(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

void setup_oxetane(void)
{
	test_ag = mol_read_json("oxetane.json");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_oxetane(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

// Test cases
START_TEST(test_phenol_vdwengs03)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _vdwengs03_wrapper);
	double energy = 0.0;
	vdwengs03(1.0, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_vdweng)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _vdweng_wrapper);
	double energy = 0.0;
	vdweng(test_ag, &energy, test_ags.nblst);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_elengs03)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _elengs03_wrapper);
	double energy = 0.0;
	elengs03(1.0, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_eleng)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _eleng_wrapper);
	double energy = 0.0;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST


START_TEST(test_phenol_init_nblist)
{
	static const int list02_ref[] = {0,2, 0,4, 1,3, 1,5, 2,4, 2,6, 3,5, 3,7, 4,6};
	static const int list03_ref[] = {0,3, 1,4, 1,6, 2,5, 2,7, 4,7, 5,6};
	ck_assert_int_eq(test_ags.n02, 9);
	for (int i = 0; i < test_ags.n02*2; i++)
		ck_assert_int_eq(test_ags.list02[i], list02_ref[i]);
	ck_assert_int_eq(test_ags.n03, 7);
	ck_assert_int_eq(test_ags.nf03, 7);
	for (int i = 0; i < test_ags.n03*2; i++) {
		ck_assert_int_eq(test_ags.list03[i], list03_ref[i]);
		ck_assert_int_eq(test_ags.listf03[i], list03_ref[i]);
	}
	ck_assert_int_eq(test_ags.nblst->nfat, 3);
	ck_assert_int_eq(test_ags.nblst->ifat[0], 0); // C1 [0], with O1 [6] and H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[0], 2);
	ck_assert_int_eq(test_ags.nblst->isat[0][0], 6);
	ck_assert_int_eq(test_ags.nblst->isat[0][1], 7);
	ck_assert_int_eq(test_ags.nblst->ifat[1], 1); // C2 [1], with H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[1], 1);
	ck_assert_int_eq(test_ags.nblst->isat[1][0], 7);
	ck_assert_int_eq(test_ags.nblst->ifat[2], 5); // C6 [5], with H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[2], 1);
	ck_assert_int_eq(test_ags.nblst->isat[2][0], 7);
}
END_TEST

START_TEST(test_three_init_nblst)
{
	// 02 list is [(0,2), (0,3)]
	ck_assert_int_eq(test_ags.n02, 2);
	ck_assert_int_eq(test_ags.list02[0], 0);
	ck_assert_int_eq(test_ags.list02[1], 2);
	ck_assert_int_eq(test_ags.list02[2], 0);
	ck_assert_int_eq(test_ags.list02[3], 3);
	// 03 list is empty
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	// nblist is empty
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

START_TEST(test_tip3p_init_nblst)
{
	// TIP3P water has bonds between all atoms (yes, even H-H, although it's somewhat virtual).
	// Therefore, it should have no 02 and 03 lists, as all atoms are 01
	// Coincidentally, we test how well the code works in edge-case of empty lists
	ck_assert_int_eq(test_ags.n02, 0);
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

START_TEST(test_oxetane_init_nblst)
{
	// Oxetane has (well, IS) a 4-atom ring, so it's possible to introduce redundant 0-2 bonds.
	// 02 list is [(0,2), (1,3)]
	ck_assert_int_eq(test_ags.n02, 2);
	ck_assert_int_eq(test_ags.list02[0], 0);
	ck_assert_int_eq(test_ags.list02[1], 2);
	ck_assert_int_eq(test_ags.list02[2], 1);
	ck_assert_int_eq(test_ags.list02[3], 3);
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

// These four test functions are used for both three_membered_ring and tip3p test cases.
START_TEST(test_vdwengs03_zero)
{
	double energy = 0.0;
	vdwengs03(1.0, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_vdweng_zero)
	{
		double energy = 0.0;
		vdweng(test_ag, &energy, test_ags.nblst);
		ck_assert(_ALMOST_ZERO(energy));
	}
END_TEST
START_TEST(test_elengs03_zero)
{
	double energy = 0.0;
	elengs03(0.5, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST


START_TEST(test_eleng_zero)
{
	double energy = 0.0;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

Suite *nbenergy_suite(void)
{
	Suite *suite = suite_create("nbenergy");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 20);
	tcase_add_checked_fixture(tcase, setup_phenol, teardown_phenol);
	tcase_add_test(tcase, test_phenol_init_nblist);
	tcase_add_test(tcase, test_phenol_vdwengs03);
	tcase_add_test(tcase, test_phenol_vdweng);
	tcase_add_test(tcase, test_phenol_elengs03);
	tcase_add_test(tcase, test_phenol_eleng);

	suite_add_tcase(suite, tcase);

	TCase *tcase_three = tcase_create("three_membered_ring");
	tcase_add_checked_fixture(tcase_three, setup_three, teardown_three);
	tcase_add_test(tcase_three, test_three_init_nblst);
	tcase_add_test(tcase_three, test_vdwengs03_zero);
	tcase_add_test(tcase_three, test_vdweng_zero);
	tcase_add_test(tcase_three, test_elengs03_zero);
	tcase_add_test(tcase_three, test_eleng_zero);

	suite_add_tcase(suite, tcase_three);

	TCase *tcase_tip3p = tcase_create("tip3p");
	tcase_add_checked_fixture(tcase_tip3p, setup_tip3p, teardown_tip3p);
	tcase_add_test(tcase_tip3p, test_tip3p_init_nblst);
	tcase_add_test(tcase_tip3p, test_vdwengs03_zero);
	tcase_add_test(tcase_tip3p, test_vdweng_zero);
	tcase_add_test(tcase_tip3p, test_elengs03_zero);
	tcase_add_test(tcase_tip3p, test_eleng_zero);

	suite_add_tcase(suite, tcase_tip3p);


	TCase *tcase_oxetane = tcase_create("oxetane");
	tcase_add_checked_fixture(tcase_oxetane, setup_oxetane, teardown_oxetane);
	tcase_add_test(tcase_oxetane, test_oxetane_init_nblst);

	suite_add_tcase(suite, tcase_oxetane);

	return suite;
}

int main(void)
{
	Suite *suite = nbenergy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
