HEADER    PEPTIDE BINDING PROTEIN                 10-JAN-03   1NMR              
TITLE     SOLUTION STRUCTURE OF C-TERMINAL DOMAIN FROM TRYPANOSOMA              
TITLE    2 CRUZI POLY(A)-BINDING PROTEIN                                        
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: POLY(A)-BINDING PROTEIN;                                   
COMPND   3 CHAIN: A;                                                            
COMPND   4 FRAGMENT: C-TERMINAL DOMAIN;                                         
COMPND   5 ENGINEERED: YES                                                      
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 ORGANISM_SCIENTIFIC: TRYPANOSOMA CRUZI;                              
SOURCE   3 ORGANISM_TAXID: 5693;                                                
SOURCE   4 EXPRESSION_SYSTEM: ESCHERICHIA COLI;                                 
SOURCE   5 EXPRESSION_SYSTEM_TAXID: 562;                                        
SOURCE   6 EXPRESSION_SYSTEM_STRAIN: BL21 GOLD (DE3);                           
SOURCE   7 EXPRESSION_SYSTEM_VECTOR_TYPE: PLASMID;                              
SOURCE   8 EXPRESSION_SYSTEM_PLASMID: PGEX-2T                                   
KEYWDS    ALL HELICAL DOMAIN, PEPTIDE BINDING PROTEIN                           
EXPDTA    SOLUTION NMR                                                          
NUMMDL    20                                                                    
AUTHOR    N.SIDDIQUI,G.KOZLOV,I.D'ORSO,J.F.TREMPE,A.C.C.FRASCH,                 
AUTHOR   2 K.GEHRING                                                            
REVDAT   3   24-FEB-09 1NMR    1       VERSN                                    
REVDAT   2   04-NOV-03 1NMR    1       JRNL                                     
REVDAT   1   09-SEP-03 1NMR    0                                                
JRNL        AUTH   N.SIDDIQUI,G.KOZLOV,I.D'ORSO,J.F.TREMPE,K.GEHRING            
JRNL        TITL   SOLUTION STRUCTURE OF THE C-TERMINAL DOMAIN FROM             
JRNL        TITL 2 POLY(A)-BINDING PROTEIN IN TRYPANOSOMA CRUZI: A              
JRNL        TITL 3 VEGETAL PABC DOMAIN                                          
JRNL        REF    PROTEIN SCI.                  V.  12  1925 2003              
JRNL        REFN                   ISSN 0961-8368                               
JRNL        PMID   12930992                                                     
JRNL        DOI    10.1110/PS.0390103                                           
REMARK   1                                                                      
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : CNS 1.1                                              
REMARK   3   AUTHORS     : BRUNGER                                              
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: THE STRUCTURES ARE BASED ON A TOTAL       
REMARK   3  OF 1170 RESTRAINTS: 974 ARE NOE-DERIVED DISTANCE CONSTRAINTS,       
REMARK   3  78 DIHEDRAL ANGLE RESTRAINTS,49 DISTANCE RESTRAINTS FROM            
REMARK   3  HYDROGEN BONDS AND 69 15N-1H RESIDUAL DIPOLAR COUPLINGS             
REMARK   4                                                                      
REMARK   4 1NMR COMPLIES WITH FORMAT V. 3.15, 01-DEC-08                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY RCSB ON 28-JAN-03.                  
REMARK 100 THE RCSB ID CODE IS RCSB018013.                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 303                                
REMARK 210  PH                             : 6.3                                
REMARK 210  IONIC STRENGTH                 : 50MM PHOSPHATE, 150MM NACL         
REMARK 210  PRESSURE                       : AMBIENT                            
REMARK 210  SAMPLE CONTENTS                : 2MM TCPABC, UNLABELLED, 50MM       
REMARK 210                                   PHOSPHATE BUFFER, 150MM NACL,      
REMARK 210                                   1MM NAN3, PH 6.3, 90% H2O, 10%     
REMARK 210                                   D2O; 2MM TCPABC U-15N, 50MM        
REMARK 210                                   PHOSPHATE BUFFER, 150MM NACL,      
REMARK 210                                   1MM NAN3, PH 6.3, 90% H2O, 10%     
REMARK 210                                   D2O; 2MM TCPABC U-15N,13C,         
REMARK 210                                   50MM PHOSPHATE BUFFER, 150MM       
REMARK 210                                   NACL, 1MM NAN3, PH 6.3, 100%       
REMARK 210                                   D20; 2MM TCPABC U-15N, 50MM        
REMARK 210                                   PHOSPHATE BUFFER, 150MM NACL,      
REMARK 210                                   1MM NAN3, 18MG/ML PF1 PHAGE,       
REMARK 210                                   PH 6.3, 90% H2O, 10% D2O; 2MM      
REMARK 210                                   TCPABC, UNLABELLED, 50MM           
REMARK 210                                   PHOSPHATE BUFFER, 150MM NACL,      
REMARK 210                                   1MM NAN3, PH 6.3, 100% D20         
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D NOESY, 3D_15N-SEPARATED_        
REMARK 210                                   NOESY, HNHA, 3D_13C-SEPARATED_     
REMARK 210                                   NOESY, IPAP-HSQC                   
REMARK 210  SPECTROMETER FIELD STRENGTH    : 500 MHZ, 800 MHZ, 600 MHZ          
REMARK 210  SPECTROMETER MODEL             : DRX, UNITYPLUS, AVANCE             
REMARK 210  SPECTROMETER MANUFACTURER      : BRUKER, VARIAN                     
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : XWINNMR 2.1, GIFA 4.31, XEASY      
REMARK 210                                   1.3.13, ARIA 1.1                   
REMARK 210   METHOD USED                   : SIMULATED ANNEALING                
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 100                                
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 20                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : STRUCTURES WITH THE LEAST          
REMARK 210                                   RESTRAINT VIOLATIONS,              
REMARK 210                                   STRUCTURES WITH THE LOWEST         
REMARK 210                                   ENERGY                             
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 8                   
REMARK 210                                                                      
REMARK 210 REMARK: THIS STRUCTURE WAS DETERMINED USING STANDARD 2D              
REMARK 210  HOMONUCLEAR AND 3D HETERONUCLEAR TECHNIQUES.                        
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM SOLUTION           
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 215 THESE RECORDS ARE MEANINGLESS.                                       
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: CLOSE CONTACTS                                             
REMARK 500                                                                      
REMARK 500 THE FOLLOWING ATOMS ARE IN CLOSE CONTACT.                            
REMARK 500                                                                      
REMARK 500  ATM1  RES C  SSEQI   ATM2  RES C  SSEQI           DISTANCE          
REMARK 500   O    MET A    49     N    LEU A    51              2.14            
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500 CHOLOGY: RAMACHANDRAN REVISITED. STRUCTURE 4, 1395 - 1400            
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 SER A   2       31.44    -96.96                                   
REMARK 500  1 GLN A   9       79.31   -105.14                                   
REMARK 500  1 LEU A  18       53.18   -115.04                                   
REMARK 500  1 ALA A  41       43.78    -78.96                                   
REMARK 500  1 ALA A  42      -39.47   -159.76                                   
REMARK 500  1 LEU A  50      -66.80     23.75                                   
REMARK 500  1 ASP A  54      -70.60    -89.52                                   
REMARK 500  1 LEU A  59      -32.53    -38.17                                   
REMARK 500  1 LEU A  61       52.00   -114.64                                   
REMARK 500  1 LEU A  62       90.34    -29.80                                   
REMARK 500  1 PRO A  65      -72.28    -71.96                                   
REMARK 500  1 LEU A  67      -70.23    -50.75                                   
REMARK 500  1 LEU A  79     -166.49     60.69                                   
REMARK 500  1 ARG A  81       23.31    -68.96                                   
REMARK 500  1 MET A  83      -69.62   -161.95                                   
REMARK 500  2 SER A   3      -68.31   -154.44                                   
REMARK 500  2 THR A  13      -72.12    -66.50                                   
REMARK 500  2 LEU A  15       18.66    -69.67                                   
REMARK 500  2 ALA A  16       88.65    -68.49                                   
REMARK 500  2 ASN A  17      -75.13   -121.92                                   
REMARK 500  2 ILE A  38      -65.19    -92.81                                   
REMARK 500  2 ALA A  41       44.58    -93.90                                   
REMARK 500  2 ALA A  42      -39.08   -159.55                                   
REMARK 500  2 LEU A  50      -66.68     23.02                                   
REMARK 500  2 ASP A  54      -74.78    -87.23                                   
REMARK 500  2 LEU A  59      -36.44    -36.97                                   
REMARK 500  2 LEU A  61       53.95   -116.35                                   
REMARK 500  2 LEU A  62       89.49    -29.28                                   
REMARK 500  2 LEU A  67      -71.13    -55.94                                   
REMARK 500  2 LEU A  79     -171.87     53.99                                   
REMARK 500  2 ARG A  81       17.12    -65.59                                   
REMARK 500  3 ALA A   5       96.60    -56.75                                   
REMARK 500  3 SER A   6      -62.08   -108.57                                   
REMARK 500  3 ASN A  10      -84.42    -68.50                                   
REMARK 500  3 ASN A  17      -67.97   -125.46                                   
REMARK 500  3 ALA A  41       45.53    -77.27                                   
REMARK 500  3 ALA A  42      -44.82   -159.69                                   
REMARK 500  3 LEU A  50      -66.82     24.83                                   
REMARK 500  3 ASP A  54      -63.96    -91.03                                   
REMARK 500  3 LEU A  59      -34.46    -36.94                                   
REMARK 500  3 LEU A  62       88.87    -31.77                                   
REMARK 500  3 LEU A  67      -70.07    -55.26                                   
REMARK 500  3 LEU A  79      -71.11    -54.83                                   
REMARK 500  3 ARG A  81       23.33    -76.01                                   
REMARK 500  3 MET A  83      178.48    -49.41                                   
REMARK 500  4 ALA A  16       46.95    -76.13                                   
REMARK 500  4 ASN A  17      -62.44   -126.45                                   
REMARK 500  4 ILE A  38      -65.05    -91.11                                   
REMARK 500  4 ALA A  41       47.00    -92.82                                   
REMARK 500  4 ALA A  42      -39.63   -159.51                                   
REMARK 500  4 LEU A  50      -67.06     18.62                                   
REMARK 500  4 LEU A  51       70.40    -68.88                                   
REMARK 500  4 ASP A  54      -76.83    -86.44                                   
REMARK 500  4 LEU A  59      -35.84    -36.87                                   
REMARK 500  4 LEU A  62       85.63    -28.91                                   
REMARK 500  4 ASN A  84       35.73    -99.43                                   
REMARK 500  5 SER A   2       46.79   -169.01                                   
REMARK 500  5 LEU A  18       64.49   -119.31                                   
REMARK 500  5 ILE A  38      -65.68    -90.86                                   
REMARK 500  5 ALA A  41       44.52    -88.33                                   
REMARK 500  5 ALA A  42      -40.95   -159.72                                   
REMARK 500  5 LEU A  50      -67.19     18.15                                   
REMARK 500  5 ASP A  54      -69.84    -93.90                                   
REMARK 500  5 LEU A  59      -35.45    -37.63                                   
REMARK 500  5 LEU A  62       92.10    -28.19                                   
REMARK 500  5 LEU A  79     -165.10     65.42                                   
REMARK 500  5 ARG A  81       30.98    -70.61                                   
REMARK 500  5 MET A  83      -47.25   -156.48                                   
REMARK 500  6 SER A   2      172.14     67.79                                   
REMARK 500  6 ALA A   5      -79.84    -61.92                                   
REMARK 500  6 ASN A  10      -81.89    -70.05                                   
REMARK 500  6 ASN A  17      -72.31   -123.99                                   
REMARK 500  6 ILE A  38      -63.67    -91.82                                   
REMARK 500  6 ALA A  41       45.34    -80.68                                   
REMARK 500  6 ALA A  42      -41.18   -159.72                                   
REMARK 500  6 LEU A  50      -66.80     19.24                                   
REMARK 500  6 LEU A  51       73.33    -66.39                                   
REMARK 500  6 ASP A  54      -71.70    -93.79                                   
REMARK 500  6 LEU A  59      -35.65    -36.98                                   
REMARK 500  6 LEU A  61       50.38   -117.01                                   
REMARK 500  6 LEU A  62       87.82    -28.75                                   
REMARK 500  6 LEU A  67      -70.82    -58.69                                   
REMARK 500  6 MET A  83      -69.85    -95.14                                   
REMARK 500  7 GLN A   9       43.42    -87.73                                   
REMARK 500  7 ALA A  16       82.09    -63.05                                   
REMARK 500  7 ASN A  17      -71.24   -126.37                                   
REMARK 500  7 ALA A  41       43.60    -93.04                                   
REMARK 500  7 ALA A  42      -40.16   -159.23                                   
REMARK 500  7 LEU A  50      -65.84     29.13                                   
REMARK 500  7 LEU A  51       67.94    -68.43                                   
REMARK 500  7 ASP A  54      -72.25    -90.95                                   
REMARK 500  7 LEU A  59      -38.12    -36.80                                   
REMARK 500  7 LEU A  61       53.97   -108.79                                   
REMARK 500  7 LEU A  62       91.63    -28.72                                   
REMARK 500  7 PRO A  65      -70.08    -76.93                                   
REMARK 500  7 LEU A  67      -71.00    -55.42                                   
REMARK 500  7 LEU A  79     -167.18     44.83                                   
REMARK 500  7 ARG A  81        5.76    -67.09                                   
REMARK 500  7 MET A  83      -95.81     51.48                                   
REMARK 500  8 GLN A   9       47.56   -100.08                                   
REMARK 500  8 ASN A  10      -83.18    -64.66                                   
REMARK 500  8 LEU A  18       64.37   -102.48                                   
REMARK 500  8 ALA A  41       48.08    -87.57                                   
REMARK 500  8 ALA A  42      -43.76   -159.32                                   
REMARK 500  8 LEU A  50      -67.50     18.74                                   
REMARK 500  8 LEU A  51       65.09    -69.91                                   
REMARK 500  8 ASP A  54      -77.50    -92.10                                   
REMARK 500  8 LEU A  61       54.32   -114.08                                   
REMARK 500  8 LEU A  62       93.97    -28.87                                   
REMARK 500  8 LEU A  67      -70.29    -56.88                                   
REMARK 500  8 LEU A  79     -168.87     54.15                                   
REMARK 500  8 ARG A  81       25.40    -64.95                                   
REMARK 500  8 MET A  83      -68.86   -140.91                                   
REMARK 500  8 ASN A  84      -72.02   -115.06                                   
REMARK 500  9 SER A   2      -12.18   -179.88                                   
REMARK 500  9 SER A   3      -64.47     72.14                                   
REMARK 500  9 ALA A  16       76.02    -62.46                                   
REMARK 500  9 ASN A  17      -62.61   -126.41                                   
REMARK 500  9 ALA A  41       46.23    -92.81                                   
REMARK 500  9 ALA A  42      -36.53   -159.86                                   
REMARK 500  9 LEU A  50      -66.57     21.84                                   
REMARK 500  9 ASP A  54      -72.25    -92.64                                   
REMARK 500  9 LEU A  59      -34.82    -37.03                                   
REMARK 500  9 LEU A  62       87.96    -28.63                                   
REMARK 500  9 MET A  83      -72.16    -59.97                                   
REMARK 500 10 SER A   2      100.12     58.39                                   
REMARK 500 10 SER A   3       80.96     57.28                                   
REMARK 500 10 THR A  13      -70.12    -67.86                                   
REMARK 500 10 LEU A  15       31.00    -83.97                                   
REMARK 500 10 LEU A  18       63.86   -109.84                                   
REMARK 500 10 ALA A  41       45.30    -77.09                                   
REMARK 500 10 ALA A  42      -43.07   -159.50                                   
REMARK 500 10 LEU A  50      -67.03     22.73                                   
REMARK 500 10 LEU A  51       65.61    -69.92                                   
REMARK 500 10 ASP A  54      -72.59    -93.69                                   
REMARK 500 10 LEU A  61       53.77   -119.32                                   
REMARK 500 10 LEU A  62       90.45    -28.91                                   
REMARK 500 10 LEU A  67      -71.46    -49.68                                   
REMARK 500 10 ARG A  81       20.22    -77.20                                   
REMARK 500 11 GLN A   7      -71.11    -91.02                                   
REMARK 500 11 ASN A  10      -73.52    -64.35                                   
REMARK 500 11 ALA A  41       43.81    -80.84                                   
REMARK 500 11 ALA A  42      -42.76   -159.41                                   
REMARK 500 11 LEU A  50      -66.74     19.37                                   
REMARK 500 11 ASP A  54      -73.91    -88.42                                   
REMARK 500 11 LEU A  59      -36.50    -38.76                                   
REMARK 500 11 LEU A  61       56.06   -108.32                                   
REMARK 500 11 LEU A  62       95.79    -30.43                                   
REMARK 500 11 LEU A  67      -70.30    -51.53                                   
REMARK 500 11 ARG A  81       23.55    -73.51                                   
REMARK 500 11 MET A  83      -75.40    -63.86                                   
REMARK 500 12 SER A   2      163.06     58.24                                   
REMARK 500 12 ALA A   5       99.63    -65.32                                   
REMARK 500 12 GLN A   7      -79.58    -89.71                                   
REMARK 500 12 GLN A   9       49.33    -82.17                                   
REMARK 500 12 ASN A  10      -73.78    -74.86                                   
REMARK 500 12 ASN A  17      -61.07   -125.72                                   
REMARK 500 12 ALA A  41       45.09    -87.13                                   
REMARK 500 12 ALA A  42      -40.09   -159.90                                   
REMARK 500 12 LEU A  50      -67.03     20.43                                   
REMARK 500 12 ASP A  54      -66.50    -91.55                                   
REMARK 500 12 GLU A  57      -55.09    -29.79                                   
REMARK 500 12 LEU A  59      -37.35    -36.82                                   
REMARK 500 12 LEU A  62       90.03    -29.00                                   
REMARK 500 12 LEU A  67      -70.35    -57.90                                   
REMARK 500 13 SER A   2      -73.67     66.13                                   
REMARK 500 13 ALA A   5       39.91    -81.81                                   
REMARK 500 13 ASN A  10      -78.26    -66.47                                   
REMARK 500 13 ALA A  16       29.92    -76.06                                   
REMARK 500 13 ILE A  38      -65.07    -91.20                                   
REMARK 500 13 ALA A  41       43.19    -81.17                                   
REMARK 500 13 ALA A  42      -39.33   -159.90                                   
REMARK 500 13 LEU A  50      -66.44     20.15                                   
REMARK 500 13 LEU A  59      -34.19    -39.26                                   
REMARK 500 13 LEU A  62       93.52    -28.61                                   
REMARK 500 13 LEU A  67      -70.21    -55.77                                   
REMARK 500 13 LEU A  79     -165.28     56.03                                   
REMARK 500 13 ASN A  80        3.15    -68.85                                   
REMARK 500 13 ARG A  81       25.61    -71.04                                   
REMARK 500 13 MET A  83     -165.44   -114.71                                   
REMARK 500 14 LEU A   4     -178.70    -64.09                                   
REMARK 500 14 ALA A   5       92.00    -69.33                                   
REMARK 500 14 GLN A   9       37.82    -92.16                                   
REMARK 500 14 THR A  13      -77.19    -74.47                                   
REMARK 500 14 LEU A  15       26.23    -74.09                                   
REMARK 500 14 LEU A  18       56.54   -107.10                                   
REMARK 500 14 ILE A  38      -66.85    -91.78                                   
REMARK 500 14 ALA A  41       45.52    -92.82                                   
REMARK 500 14 ALA A  42      -44.41   -159.00                                   
REMARK 500 14 LEU A  50      -66.77     26.48                                   
REMARK 500 14 LEU A  51       72.55    -68.84                                   
REMARK 500 14 ASP A  54      -70.45    -90.70                                   
REMARK 500 14 LEU A  61       62.91   -113.13                                   
REMARK 500 14 LEU A  62       82.60    -33.60                                   
REMARK 500 14 LEU A  79     -170.14     54.85                                   
REMARK 500 14 ARG A  81       25.39    -66.22                                   
REMARK 500 15 SER A   2      -65.56   -154.93                                   
REMARK 500 15 SER A   3      -68.72   -100.68                                   
REMARK 500 15 ALA A  41       44.56    -86.18                                   
REMARK 500 15 ALA A  42      -38.37   -159.42                                   
REMARK 500 15 LEU A  50      -66.79     23.09                                   
REMARK 500 15 ASP A  54      -68.74    -95.43                                   
REMARK 500 15 LEU A  59      -35.20    -37.62                                   
REMARK 500 15 LEU A  61       54.71   -112.57                                   
REMARK 500 15 LEU A  62       89.34    -28.78                                   
REMARK 500 15 LEU A  67      -70.86    -58.08                                   
REMARK 500 15 LEU A  79      -77.46    -53.73                                   
REMARK 500 15 ASN A  84       30.37    -88.23                                   
REMARK 500 16 ALA A   5       34.81    -93.58                                   
REMARK 500 16 ASN A  10      -80.28    -65.66                                   
REMARK 500 16 ALA A  41       47.82    -88.81                                   
REMARK 500 16 ALA A  42      -42.12   -159.69                                   
REMARK 500 16 LEU A  50      -66.05     20.90                                   
REMARK 500 16 ASP A  54      -73.46    -86.50                                   
REMARK 500 16 LEU A  62       88.62    -29.09                                   
REMARK 500 16 LEU A  67      -71.09    -56.96                                   
REMARK 500 16 LEU A  79     -122.20    -61.54                                   
REMARK 500 16 ARG A  81       19.80    -68.87                                   
REMARK 500 16 MET A  83      -82.81    -82.53                                   
REMARK 500 16 ASN A  84      -84.58   -105.92                                   
REMARK 500 17 SER A   6      -68.21    -91.07                                   
REMARK 500 17 ALA A  16       83.62    -59.20                                   
REMARK 500 17 ASN A  17      -76.60   -125.23                                   
REMARK 500 17 ILE A  38      -64.80    -92.95                                   
REMARK 500 17 ALA A  41       49.78    -92.28                                   
REMARK 500 17 ALA A  42      -43.80   -158.77                                   
REMARK 500 17 LEU A  50      -67.34     14.94                                   
REMARK 500 17 ASP A  54      -72.27    -88.20                                   
REMARK 500 17 LEU A  59      -37.59    -36.55                                   
REMARK 500 17 LEU A  62       94.28    -28.63                                   
REMARK 500 17 LEU A  67      -70.24    -56.37                                   
REMARK 500 17 LEU A  79     -166.88     55.88                                   
REMARK 500 17 ARG A  81       33.68    -67.92                                   
REMARK 500 18 SER A   2       81.79     61.79                                   
REMARK 500 18 SER A   3       48.67   -145.08                                   
REMARK 500 18 GLN A   7       34.23    -88.56                                   
REMARK 500 18 LEU A  15       20.78    -71.31                                   
REMARK 500 18 ASN A  17      -61.29   -125.75                                   
REMARK 500 18 ALA A  41       46.08    -94.00                                   
REMARK 500 18 ALA A  42      -42.23   -160.21                                   
REMARK 500 18 LEU A  50      -66.25     23.18                                   
REMARK 500 18 LEU A  51       65.49    -66.55                                   
REMARK 500 18 ASP A  54      -71.78    -94.92                                   
REMARK 500 18 LEU A  59      -35.73    -38.74                                   
REMARK 500 18 LEU A  61       57.76   -112.42                                   
REMARK 500 18 LEU A  62       92.07    -28.62                                   
REMARK 500 18 LEU A  79     -162.49     59.37                                   
REMARK 500 18 ASN A  80        7.91    -68.72                                   
REMARK 500 18 ARG A  81       22.42    -71.97                                   
REMARK 500 18 MET A  83      -68.08   -162.34                                   
REMARK 500 18 ASN A  84       57.81   -113.60                                   
REMARK 500 19 ALA A  16       65.06    -69.67                                   
REMARK 500 19 ASN A  17      -63.40   -126.25                                   
REMARK 500 19 ALA A  41       48.56    -93.24                                   
REMARK 500 19 ALA A  42      -44.55   -159.51                                   
REMARK 500 19 LEU A  50      -67.47     18.48                                   
REMARK 500 19 ASP A  54      -72.55    -92.87                                   
REMARK 500 19 LEU A  59      -34.05    -37.66                                   
REMARK 500 19 LEU A  61       55.71   -113.57                                   
REMARK 500 19 LEU A  62       93.09    -28.80                                   
REMARK 500 19 LEU A  67      -70.38    -57.08                                   
REMARK 500 19 LEU A  79     -165.76     60.44                                   
REMARK 500 19 ASN A  80        0.67    -65.93                                   
REMARK 500 19 ARG A  81       22.73    -73.11                                   
REMARK 500 20 SER A   3      112.47   -167.02                                   
REMARK 500 20 SER A   6      -82.63   -110.22                                   
REMARK 500 20 ALA A  16       79.78    -66.17                                   
REMARK 500 20 ASN A  17      -73.24   -125.73                                   
REMARK 500 20 ALA A  41       49.36    -93.58                                   
REMARK 500 20 ALA A  42      -40.86   -159.13                                   
REMARK 500 20 LEU A  50      -66.79     16.28                                   
REMARK 500 20 ASP A  54      -69.84    -95.11                                   
REMARK 500 20 LEU A  59      -37.08    -38.24                                   
REMARK 500 20 LEU A  61       58.30   -118.32                                   
REMARK 500 20 LEU A  62       91.69    -28.24                                   
REMARK 500 20 LEU A  79      -75.77    -57.48                                   
REMARK 500 20 ARG A  81       21.06    -72.56                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
DBREF  1NMR A    3    85  UNP    Q27335   Q27335_TRYCR   468    550             
SEQADV 1NMR GLY A    1  UNP  Q27335              CLONING ARTIFACT               
SEQADV 1NMR SER A    2  UNP  Q27335              CLONING ARTIFACT               
SEQRES   1 A   85  GLY SER SER LEU ALA SER GLN GLY GLN ASN LEU SER THR          
SEQRES   2 A   85  VAL LEU ALA ASN LEU THR PRO GLU GLN GLN LYS ASN VAL          
SEQRES   3 A   85  LEU GLY GLU ARG LEU TYR ASN HIS ILE VAL ALA ILE ASN          
SEQRES   4 A   85  PRO ALA ALA ALA ALA LYS VAL THR GLY MET LEU LEU GLU          
SEQRES   5 A   85  MET ASP ASN GLY GLU ILE LEU ASN LEU LEU ASP THR PRO          
SEQRES   6 A   85  GLY LEU LEU ASP ALA LYS VAL GLN GLU ALA LEU GLU VAL          
SEQRES   7 A   85  LEU ASN ARG HIS MET ASN VAL                                  
HELIX    1   1 LEU A   11  ALA A   16  1                                   6    
HELIX    2   2 LEU A   18  ALA A   37  1                                  20    
HELIX    3   3 ASN A   39  LEU A   50  1                                  12    
HELIX    4   4 MET A   53  ASN A   60  1                                   8    
HELIX    5   5 THR A   64  LEU A   79  1                                  16    
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
MODEL        1                                                                  
ENDMDL                                                                          
MODEL        2                                                                  
ENDMDL                                                                          
MODEL        3                                                                  
ENDMDL                                                                          
MODEL        4                                                                  
ENDMDL                                                                          
MODEL        5                                                                  
ENDMDL                                                                          
MASTER      374    0    0    5    0    0    0    625800   20    0    7          
END                                                                             
