#include <check.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>

#include "pdb.h"
#include "prms.h"

START_TEST(test_read_prms)
{
	struct mol_prms *prms = mol_prms_read("test.prm");
	ck_assert(prms != NULL);

	// atom prms
	ck_assert_str_eq(prms->version, "0.0.6");
	ck_assert(prms->natoms == 40);
	ck_assert(prms->atoms != NULL);
	ck_assert_str_eq(prms->atoms[0].typemaj, "ALA");
	ck_assert_str_eq(prms->atoms[1].typemaj, "ALA");
	ck_assert_str_eq(prms->atoms[0].typemin, "C");
	ck_assert_str_eq(prms->atoms[1].typemin, "CA");
	ck_assert(prms->atoms[0].subid == 2);
	ck_assert(prms->atoms[1].subid == 1);
	ck_assert(prms->atoms[0].r == 1.870);
	ck_assert(prms->atoms[1].r == 2.265);
	ck_assert(prms->atoms[0].q == 0.600);
	ck_assert(prms->atoms[1].q == 0.100);
	ck_assert(prms->pwpot != NULL);

	// pwpot prms
	ck_assert(prms->pwpot->k == 18);
	ck_assert(prms->pwpot->r1 < DBL_EPSILON);
	ck_assert(fabsf(prms->pwpot->r2 - 6.5f) < FLT_EPSILON);
	ck_assert(fabsf(prms->pwpot->lambdas[1] - 6.607402f) < FLT_EPSILON);
	ck_assert(fabsf(prms->pwpot->Xs[1] - -0.1926657f) < FLT_EPSILON);

	float test_eng = 0.0f;
	for (size_t i = 0; i < prms->pwpot->k; i++) {
		test_eng += prms->pwpot->lambdas[i] *
			prms->pwpot->Xs[i*prms->pwpot->k] *
			prms->pwpot->Xs[i*prms->pwpot->k];
	}
	ck_assert(fabsf(prms->pwpot->eng[0][0] - test_eng) < FLT_EPSILON);

	test_eng = 0.0f;
	for (size_t i = 0; i < prms->pwpot->k; i++) {
		test_eng += prms->pwpot->lambdas[i] *
			prms->pwpot->Xs[i*prms->pwpot->k + 1] *
			prms->pwpot->Xs[i*prms->pwpot->k + 2];
	}
	ck_assert(fabsf(prms->pwpot->eng[1][2] - test_eng) < FLT_EPSILON);

	for (size_t i = 0; i < prms->pwpot->k; i++) {
		for (size_t j = 0; j < prms->pwpot->k; j++) {
			ck_assert(fabsf(prms->pwpot->eng[i][j] - prms->pwpot->eng[j][i]) <= FLT_EPSILON);
		}
	}
	mol_prms_free(prms);
}
END_TEST

START_TEST(test_read_nonexistent)
{
	struct mol_prms *prms = mol_prms_read("nonexistent");
	ck_assert(prms == NULL);
}
END_TEST


START_TEST(test_add_prms)
{
	struct mol_prms *prms = mol_prms_read("test.prm");
	struct mol_atom_group *ag = mol_read_pdb("ala_gapped.pdb");
	ck_assert(prms != NULL);
	ck_assert(ag != NULL);
	mol_atom_group_add_prms(ag, prms);

	ck_assert(ag->charge != NULL);
	ck_assert(ag->vdw_radius != NULL);
	ck_assert(ag->pwpot_id != NULL);

	ck_assert(fabs(ag->charge[0] + 0.4) < DBL_EPSILON); // ALA, N
	ck_assert(fabs(ag->charge[6] - 0.0) < DBL_EPSILON); // ALA, HA
	ck_assert(fabs(ag->charge[54] - 0.0) < DBL_EPSILON); // ALA, CB
	ck_assert(fabs(ag->vdw_radius[0] - 1.83) < DBL_EPSILON); // ALA, N
	ck_assert(fabs(ag->vdw_radius[6] - 0.0) < DBL_EPSILON); // ALA, HA
	ck_assert(fabs(ag->vdw_radius[54] - 2.165) < DBL_EPSILON); // ALA, CB
	ck_assert_int_eq(ag->pwpot_id[0], 0); // ALA, N
	ck_assert_int_eq(ag->pwpot_id[6], -1); // ALA, HA
	ck_assert_int_eq(ag->pwpot_id[54], 5); // ALA, CB
}
END_TEST

Suite *prms_suite(void)
{
	Suite *suite = suite_create("prms");

	TCase *tcase_prms = tcase_create("test_prms");
	tcase_add_test(tcase_prms, test_read_prms);
	tcase_add_test(tcase_prms, test_read_nonexistent);
	tcase_add_test(tcase_prms, test_add_prms);

	suite_add_tcase(suite, tcase_prms);

	return suite;
}

int main(void)
{
	Suite *suite = prms_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
