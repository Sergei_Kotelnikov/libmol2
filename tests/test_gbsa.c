#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "gbsa.h"
#include "pdb.h"
#include "icharmm.h"

static struct mol_atom_group *test_ag;
static struct agsetup test_ags;
static struct acesetup test_ac_s;

static const double delta = 0.00001;
static const double tolerance = 0.1;

void test_acegrads(struct mol_atom_group *ag, struct agsetup *ags,
		   struct acesetup *ac_s, double d,
		   void (*energy) (struct mol_atom_group * ag, double *en,
				   struct acesetup * ac_s,
				   struct agsetup * ags))
{
	double en, en1, t;
	struct mol_vector3 *fs = malloc(ag->natoms * sizeof(struct mol_vector3));

	en = 0;
	energy(ag, &en, ac_s, ags);

	for (size_t i = 0; i < ag->natoms; ++i) {
		en1 = 0;
		t = ag->coords[i].X;
		ag->coords[i].X = d + t;
		energy(ag, &en1, ac_s, ags);
		ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;

		en1 = 0;
		t = ag->coords[i].Y;
		ag->coords[i].Y = d + t;
		energy(ag, &en1, ac_s, ags);
		ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;

		en1 = 0;
		t = ag->coords[i].Z;
		ag->coords[i].Z = d + t;
		energy(ag, &en1, ac_s, ags);
		ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	en = 0;
	mol_zero_gradients(ag);
	energy(ag, &en, ac_s, ags);
	char msg[256];

	for (size_t i = 0; i < ag->natoms; ++i) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - fs[i].X) < tolerance, msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - fs[i].Y) < tolerance, msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - fs[i].Z) < tolerance, msg);

	}
	free(fs);
}

void setup(void)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
	ace_ini(test_ag, &test_ac_s);
	test_ac_s.efac = 0.5;
	ace_fixedupdate(test_ag, &test_ags, &test_ac_s);
	ace_updatenblst(&test_ags, &test_ac_s);
}

void teardown(void)
{
	destroy_acesetup(&test_ac_s);
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

// Test cases
START_TEST(test_gbsa)
{
	mol_zero_gradients(test_ag);
	test_acegrads(test_ag, &test_ags, &test_ac_s, delta, aceeng);
}
END_TEST
START_TEST(test_gbsa_nonpolar)
{
	mol_zero_gradients(test_ag);
	test_acegrads(test_ag, &test_ags, &test_ac_s, delta, aceeng_nonpolar);
}
END_TEST
START_TEST(test_gbsa_polar)
{
	mol_zero_gradients(test_ag);
	test_acegrads(test_ag, &test_ags, &test_ac_s, delta, aceeng_polar);
}
END_TEST
START_TEST(test_ace_ini_no_volumes)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");
	test_ag->ace_volume = NULL; // Volumes field is present in PRM. Remove it here.

	// Preamble
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);

	// This line should fail
	ace_ini(test_ag, &test_ac_s);
}
END_TEST

Suite *gbsa_suite(void)
{
	Suite *suite = suite_create("gbsa");

#ifndef _WIN32
	TCase *tcase_init = tcase_create("test_init");
	tcase_add_exit_test(tcase_init, test_ace_ini_no_volumes, EXIT_FAILURE);
	suite_add_tcase(suite, tcase_init);
#endif /* _WIN32 */

	TCase *tcase = tcase_create("test_energy");
	tcase_set_timeout(tcase, 300);
	tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_add_test(tcase, test_gbsa);
	tcase_add_test(tcase, test_gbsa_nonpolar);
	tcase_add_test(tcase, test_gbsa_polar);
	suite_add_tcase(suite, tcase);


	return suite;
}

int main(void)
{
	Suite *suite = gbsa_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
