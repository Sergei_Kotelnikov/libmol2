#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "lists.h"


START_TEST(test_init)
{
	struct mol_index_list a;
	struct mol_bool_list *b = calloc(1, sizeof(struct mol_bool_list));
	mol_list_init(a, 10);
	mol_list_init(*b, 10);
	ck_assert_int_eq(a.size, 10);
	ck_assert(a.members != NULL);
	ck_assert_int_eq(b->size, 10);
	ck_assert(b->members != NULL);

	for (int i = 0; i < 10; i++) {
		ck_assert_int_eq(a.members[i], 0);
		ck_assert_int_eq(b->members[i], 0);
	}
}
END_TEST

START_TEST(test_union)
{
	struct mol_index_list *a = calloc(1, sizeof(struct mol_index_list));
	struct mol_index_list *b = calloc(1, sizeof(struct mol_index_list));

	size_t a_members[] = {1, 3, 4};
	size_t b_members[] = {2, 3, 5, 6, 7};

	a->size = 3;
	a->members = a_members;
	b->size = 5;
	b->members = b_members;

	struct mol_index_list *c = calloc(1, sizeof(struct mol_index_list));
        c->members = malloc((a->size + b->size)*sizeof(size_t));
	mol_index_list_union(c, a, b, true);

	ck_assert(c->size == 7);
	size_t correct[] = {1, 2, 3, 4, 5, 6, 7};
	for (size_t i = 0; i < c->size; ++i) {
		ck_assert(c->members[i] = correct[i]);
	}
	free(a);
	free(b);
	mol_list_free(c);
}
END_TEST

START_TEST(test_union_a_empty)
{
	struct mol_index_list *a = calloc(1, sizeof(struct mol_index_list));
	struct mol_index_list *b = calloc(1, sizeof(struct mol_index_list));

	size_t b_members[] = {2, 3, 5, 6, 7};

	a->size = 0;
	a->members = NULL;
	b->size = 5;
	b->members = b_members;

	struct mol_index_list *c = calloc(1, sizeof(struct mol_index_list));
        c->members = malloc((a->size + b->size)*sizeof(size_t));
	mol_index_list_union(c, a, b, true);

	ck_assert(c->size == 5);
	size_t correct[] = {2, 3, 5, 6, 7};
	for (size_t i = 0; i < c->size; ++i) {
		ck_assert(c->members[i] = correct[i]);
	}
	free(a);
	free(b);
	mol_list_free(c);
}
END_TEST

START_TEST(test_union_b_empty)
{
	struct mol_index_list *a = calloc(1, sizeof(struct mol_index_list));
	struct mol_index_list *b = calloc(1, sizeof(struct mol_index_list));

	size_t a_members[] = {1, 3, 4};

	a->size = 3;
	a->members = a_members;
	b->size = 0;
	b->members = NULL;

	struct mol_index_list *c = calloc(1, sizeof(struct mol_index_list));
        c->members = malloc((a->size + b->size)*sizeof(size_t));
	mol_index_list_union(c, a, b, true);

	ck_assert(c->size == 3);
	size_t correct[] = {1, 3, 4};
	for (size_t i = 0; i < c->size; ++i) {
		ck_assert(c->members[i] = correct[i]);
	}
	free(a);
	free(b);
	mol_list_free(c);
}
END_TEST

Suite *lists_suite(void)
{
	Suite *suite = suite_create("lists");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_init);
	tcase_add_test(tcase, test_union);
	tcase_add_test(tcase, test_union_a_empty);
	tcase_add_test(tcase, test_union_b_empty);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = lists_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
