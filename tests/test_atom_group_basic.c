#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <string.h>

#include "atom_group.h"
#include "pdb.h"

START_TEST(test_new)
{
	struct mol_atom_group *ag = mol_atom_group_create();
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 0);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_metadata)
{
	struct mol_atom_group *ag = mol_atom_group_create();
	int sequence[7] = { 0, 1, 2, 3, 4, 5, 6 };
	mol_atom_group_add_metadata(ag, "sequence", sequence);

	int *sequence2 = (int *)mol_atom_group_fetch_metadata(ag, "sequence");

	for (size_t i = 0; i < 7; i++) {
		ck_assert(sequence[i] == sequence2[i]);
	}

	bool seq_present = mol_atom_group_has_metadata(ag, "sequence");
	ck_assert(seq_present == true);

	bool bac_present = mol_atom_group_has_metadata(ag, "bacon");
	ck_assert(bac_present == false);

	mol_atom_group_delete_metadata(ag, "sequence");
	seq_present = mol_atom_group_has_metadata(ag, "sequence");
	ck_assert(seq_present == false);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_find_atom1)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ssize_t res = mol_find_atom(ag, 'A', 107, ' ', " O  ");
	ck_assert(res == 822);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_find_atom2)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ssize_t res = mol_find_atom(ag, 'A', 107, ' ', "O");
	ck_assert(res == 822);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_find_missing_atom1)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ssize_t res = mol_find_atom(ag, 'A', 107, ' ', "CB33333");
	ck_assert(res == -1);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_find_missing_atom2)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ssize_t res = mol_find_atom(ag, 'A', 108, ' ', " O  ");
	ck_assert(res == -1);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_join_strings)
{
	struct mol_atom_group *A = mol_read_pdb("1rei.pdb");
	struct mol_atom_group *B = mol_read_pdb("1rei_just_coords.pdb");

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	struct mol_atom_group *BA = mol_atom_group_join(B, A);

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	ck_assert_str_eq(AB->formal_charge[8], "+1");
	ck_assert_str_eq(BA->formal_charge[1715], "+1");
	ck_assert_str_eq(AB->formal_charge[0], "  ");
	ck_assert_str_eq(BA->formal_charge[1707], "  ");
	ck_assert(AB->formal_charge[1707] == NULL);
	ck_assert(BA->formal_charge[0] == NULL);
	mol_atom_group_free(A);
	mol_atom_group_free(B);
	mol_atom_group_free(AB);
	mol_atom_group_free(BA);
}
END_TEST

START_TEST(test_copy)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_A_prep.pdb");
	ck_assert(ag != NULL);
	ck_assert(ag->natoms == 1013);

	// Manually set up some of the fields that can not be initialized from this pdb

	// set up formal charges
	mol_atom_group_init_atomic_field(ag, formal_charge);
	const char *dummy_charge = "2+";
	for (size_t i = 0; i < ag->natoms; ++i) {
		ag->formal_charge[i] = calloc(3, sizeof(char));
		strcpy(ag->formal_charge[i], dummy_charge);
	}

	// set up mask
	mol_atom_group_init_atomic_field(ag, mask);
	for (size_t i = 0; i < ag->natoms; ++i)
		ag->mask[i] = (i < ag->natoms / 2) ? false : true;

	// set up surface
	mol_atom_group_init_atomic_field(ag, surface);
	for (size_t i = 0; i < ag->natoms; ++i)
		ag->surface[i] = (i < ag->natoms / 2) ? false : true;

	// set up attraction level
	mol_atom_group_init_atomic_field(ag, attraction_level);
	for (size_t i = 0; i < ag->natoms; ++i)
		ag->attraction_level[i] = (i < ag->natoms / 2) ? 1.2: 0.0;

	// set up backbone
	mol_atom_group_init_atomic_field(ag, backbone);
	for (size_t i = 0; i < ag->natoms; ++i)
		ag->backbone[i] = mol_is_backbone(ag, i, NULL);

	// set up seqres
	ag->seqres = calloc(5, sizeof(char));
	sprintf(ag->seqres, "TEST");


	struct mol_atom_group *new_ag = mol_atom_group_copy(ag);
	ck_assert(new_ag != NULL);

	ck_assert(new_ag->natoms == 1013);
	ck_assert(new_ag->coords != NULL);
	ck_assert(new_ag->element != NULL);
	ck_assert(new_ag->alternate_location != NULL);
	ck_assert(new_ag->residue_id != NULL);
	ck_assert(new_ag->residue_name != NULL);
	ck_assert(new_ag->occupancy != NULL);
	ck_assert(new_ag->B != NULL);
	ck_assert(new_ag->segment_id != NULL);
	ck_assert(new_ag->formal_charge != NULL);
	ck_assert(new_ag->mask != NULL);
	ck_assert(new_ag->surface != NULL);
	ck_assert(new_ag->attraction_level != NULL);
	ck_assert(new_ag->backbone != NULL);
	ck_assert(new_ag->record != NULL);

	ck_assert(new_ag->vdw_radius == NULL);
	ck_assert(new_ag->vdw_radius03 == NULL);
	ck_assert(new_ag->eps == NULL);
	ck_assert(new_ag->eps03 == NULL);
	ck_assert(new_ag->ace_volume == NULL);
	ck_assert(new_ag->charge == NULL);
	ck_assert(new_ag->pwpot_id == NULL);
	ck_assert(new_ag->fixed == NULL);
	ck_assert(new_ag->ftypen == NULL);
	ck_assert(new_ag->ftype_name == NULL);

	ck_assert(new_ag->coords != ag->coords);
	ck_assert(new_ag->element != ag->element);
	ck_assert(new_ag->alternate_location != ag->alternate_location);
	ck_assert(new_ag->residue_id != ag->residue_id);
	ck_assert(new_ag->residue_name != ag->residue_name);
	ck_assert(new_ag->occupancy != ag->occupancy);
	ck_assert(new_ag->B != ag->B);
	ck_assert(new_ag->segment_id != ag->segment_id);
	ck_assert(new_ag->formal_charge != ag->formal_charge);
	ck_assert(new_ag->mask != ag->mask);
	ck_assert(new_ag->surface != ag->surface);
	ck_assert(new_ag->attraction_level != ag->attraction_level);
	ck_assert(new_ag->backbone != ag->backbone);
	ck_assert(new_ag->record != ag->record);


	for (size_t i = 0; i < ag->natoms; ++i) {
		ck_assert(MOL_VEC_EQ(new_ag->coords[i], ag->coords[i]));

		// gradients 		tested in test_icharmm

		ck_assert(new_ag->element[i] != ag->element[i]);
		ck_assert_str_eq(new_ag->element[i], ag->element[i]);

		ck_assert(new_ag->atom_name[i] != ag->atom_name[i]);
		ck_assert_str_eq(new_ag->atom_name[i], ag->atom_name[i]);

		ck_assert(new_ag->alternate_location[i] == ag->alternate_location[i]);

		ck_assert(mol_residue_id_cmp(
					&new_ag->residue_id[i],
					&ag->residue_id[i]) == 0);

		ck_assert(new_ag->residue_name[i] != ag->residue_name[i]);

		ck_assert_str_eq(new_ag->residue_name[i], ag->residue_name[i]);

		ck_assert(new_ag->occupancy[i] == ag->occupancy[i]);

		ck_assert(new_ag->B[i] == ag->B[i]);

		ck_assert(new_ag->segment_id[i] != ag->segment_id[i]);
		ck_assert_str_eq(new_ag->segment_id[i], ag->segment_id[i]);

		ck_assert(new_ag->formal_charge[i] != ag->formal_charge[i]);
		ck_assert_str_eq(new_ag->formal_charge[i], ag->formal_charge[i]);

		// vdw_radius		tested in test_icharmm
		// vdw_radius03		tested in test_icharmm
		// eps			tested in test_icharmm
		// eps03		tested in test_icharmm
		// ace_volume		tested in test_icharmm
		// charge		tested in test_icharmm
		// pwpot_id		tested in test_icharmm
		// fixed		tested in test_icharmm
		// ftypen		tested in test_icharmm
		// ftype_name		tested in test_icharmm


		ck_assert(new_ag->mask[i] == ag->mask[i]);
		ck_assert(new_ag->surface[i] == ag->surface[i]);
		ck_assert(new_ag->attraction_level[i] == ag->attraction_level[i]);
		ck_assert(new_ag->backbone[i] == ag->backbone[i]);
		ck_assert(new_ag->record[i] == ag->record[i]);

		// hbond_prop 		tested in test_icharmm
		// hbond_base_atom_id	tested in test_icharmm

		struct mol_residue* new_res;
		struct mol_residue* old_res;
		new_res = mol_atom_residue(new_ag, i);
		old_res = mol_atom_residue(ag, i);

		ck_assert(mol_residue_id_cmp(
				&new_res->residue_id,
				&old_res->residue_id) == 0);
	}

	ck_assert(new_ag->seqres != NULL);
	ck_assert_str_eq(new_ag->seqres, ag->seqres);

	// This may be an overkill, but whatever
	ck_assert(new_ag->residue_list != NULL);
	ck_assert(new_ag->nresidues == ag->nresidues);
	for (size_t i = 0; i < new_ag->nresidues; ++i) {
		ck_assert(new_ag->residue_list[i]->atom_end   == ag->residue_list[i]->atom_end);
		ck_assert(new_ag->residue_list[i]->atom_start == ag->residue_list[i]->atom_start);

		ck_assert_str_eq(new_ag->residue_list[i]->name, ag->residue_list[i]->name);

		ck_assert(new_ag->residue_list[i]->rotamer    == ag->residue_list[i]->rotamer);
		ck_assert(mol_residue_id_cmp(
				&new_ag->residue_list[i]->residue_id,
				&ag->residue_list[i]->residue_id) == 0);
	}
}
END_TEST

START_TEST(test_create_residue_hash)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");

	// Check that initially everything works
	ck_assert(find_residue(ag, 'A', 1, ' ') != NULL);
	ck_assert(find_residue(ag, 'A', 2, ' ') != NULL);

	// Modify insertion of the first residue
	for (size_t i = 0; i < 8; i++) {
		ag->residue_id[i].insertion = 'P';
	}
	bool ret = mol_atom_group_create_residue_hash(ag);
	ck_assert(ret == true);
	ck_assert(find_residue(ag, 'A', 1, ' ') == NULL); // Unable to find anymore
	ck_assert(find_residue(ag, 'A', 1, 'P') != NULL);
	ck_assert(find_residue(ag, 'A', 2, ' ') != NULL); // Is still okay

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_join_residue_hash)
{
	struct mol_atom_group *A = mol_read_pdb("1rei.pdb");
	struct mol_atom_group *B = mol_read_pdb("1rei_just_coords.pdb");
	for (size_t i = 0; i < B->natoms; i++) {
		B->residue_id[i].chain += 2; // A -> C, B -> D
	}

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	struct mol_atom_group *BA = mol_atom_group_join(B, A);

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	struct mol_residue *r;

	r = find_residue(AB, 'A', 1, ' '); // First chain in joined atomgroup
	ck_assert(r != NULL);
	ck_assert(r->atom_start == 0);
	ck_assert(r->atom_end == 7);

	r = find_residue(AB, 'D', 1, ' '); // Fourth chain in joined atomgroup
	ck_assert(r != NULL);
	ck_assert(r->atom_start == 2534);
	ck_assert(r->atom_end == 2541);

	r = find_residue(AB, 'A', 404, ' ');
	ck_assert(r == NULL);

	r = find_residue(BA, 'A', 1, ' ');  // Third chain in joined atomgroup
	ck_assert(r != NULL);
	ck_assert(r->atom_start == 1707);
	ck_assert(r->atom_end == 1714);

	r = find_residue(BA, 'D', 1, ' '); // Second chain in joined atomgroup
	ck_assert(r != NULL);
	ck_assert(r->atom_start == 827);
	ck_assert(r->atom_end == 834);

	r = find_residue(BA, 'A', 404, ' ');
	ck_assert(r == NULL);

	mol_atom_group_free(A);
	mol_atom_group_free(B);
	mol_atom_group_free(AB);
	mol_atom_group_free(BA);
}
END_TEST

START_TEST(test_join_atomic_metadata)
{
	struct mol_atom_group *A = mol_read_pdb("1rei.pdb");
	struct mol_atom_group *B = mol_read_pdb("1rei_just_coords.pdb");

	// Joining two atomgroups with no metadata

	struct mol_atom_group *AB_no_metadata = mol_atom_group_join(A, B);
	mol_atom_group_join_atomic_metadata(AB_no_metadata, A, B, "key", sizeof(size_t));

	ck_assert(AB_no_metadata != NULL);
	ck_assert(mol_atom_group_has_metadata(AB_no_metadata, "key") == false);

	// Joining atomgroups when only one has metadata

	size_t *dataA = calloc(A->natoms, sizeof(size_t));
	for (size_t i = 0; i < A->natoms; i++)
		dataA[i] = i;
	mol_atom_group_add_metadata(A, "key", dataA);

	struct mol_atom_group *AB_one_metadata = mol_atom_group_join(A, B);
	mol_atom_group_join_atomic_metadata(AB_one_metadata, A, B, "key", sizeof(size_t));
	struct mol_atom_group *BA_one_metadata = mol_atom_group_join(B, A);
	mol_atom_group_join_atomic_metadata(BA_one_metadata, B, A, "key", sizeof(size_t));
	size_t *dataAB_1 = (size_t *) mol_atom_group_fetch_metadata(AB_one_metadata, "key");
	size_t *dataBA_1 = (size_t *) mol_atom_group_fetch_metadata(BA_one_metadata, "key");

	ck_assert(AB_one_metadata != NULL);
	ck_assert(BA_one_metadata != NULL);

	ck_assert_int_eq(dataAB_1[8], 8);
	ck_assert_int_eq(dataAB_1[1706], 1706);
	ck_assert_int_eq(dataAB_1[1707], 0);
	ck_assert_int_eq(dataAB_1[1717], 0);
	ck_assert_int_eq(dataAB_1[3413], 0);

	ck_assert_int_eq(dataBA_1[8], 0);
	ck_assert_int_eq(dataBA_1[1706], 0);
	ck_assert_int_eq(dataBA_1[1707], 0);
	ck_assert_int_eq(dataBA_1[1717], 10);
	ck_assert_int_eq(dataBA_1[3413], 1706);

	// Joining atomgroups when both have metadata

	size_t *dataB = calloc(B->natoms, sizeof(size_t));
	for (size_t i = 0; i < B->natoms; i++)
		dataB[i] = i + 100000;
	mol_atom_group_add_metadata(B, "key", dataB);

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	mol_atom_group_join_atomic_metadata(AB, A, B, "key", sizeof(size_t));
	struct mol_atom_group *BA = mol_atom_group_join(B, A);
	mol_atom_group_join_atomic_metadata(BA, B, A, "key", sizeof(size_t));

	size_t *dataAB = (size_t *) mol_atom_group_fetch_metadata(AB, "key");
	size_t *dataBA = (size_t *) mol_atom_group_fetch_metadata(BA, "key");

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	ck_assert_int_eq(dataAB[8], 8);
	ck_assert_int_eq(dataAB[1706], 1706);
	ck_assert_int_eq(dataAB[1707], 100000);
	ck_assert_int_eq(dataAB[1717], 100010);
	ck_assert_int_eq(dataAB[3413], 101706);

	ck_assert_int_eq(dataBA[8], 100008);
	ck_assert_int_eq(dataBA[1706], 101706);
	ck_assert_int_eq(dataBA[1707], 0);
	ck_assert_int_eq(dataBA[1717], 10);
	ck_assert_int_eq(dataBA[3413], 1706);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("atom_group");

	TCase *tcase_new = tcase_create("test_new");
	tcase_add_test(tcase_new, test_new);

	TCase *tcase_metadata = tcase_create("test_metadata");
	tcase_add_test(tcase_metadata, test_metadata);
	tcase_add_test(tcase_metadata, test_join_atomic_metadata);

	TCase *tcase_join = tcase_create("test_join");
	tcase_add_test(tcase_join, test_join_strings);

	TCase *tcase_copy = tcase_create("test_copy");
	tcase_add_test(tcase_copy, test_copy);

	TCase *tcase_find_atom = tcase_create("test_find_atom");
	tcase_add_test(tcase_find_atom, test_find_atom1);
	tcase_add_test(tcase_find_atom, test_find_atom2);
	tcase_add_test(tcase_find_atom, test_find_missing_atom1);
	tcase_add_test(tcase_find_atom, test_find_missing_atom2);

	TCase *tcase_residues = tcase_create("test_residues");
	tcase_add_test(tcase_residues, test_create_residue_hash);
	tcase_add_test(tcase_residues, test_join_residue_hash);

	suite_add_tcase(suite, tcase_new);
	suite_add_tcase(suite, tcase_metadata);
	suite_add_tcase(suite, tcase_join);
	suite_add_tcase(suite, tcase_copy);
	suite_add_tcase(suite, tcase_find_atom);
	suite_add_tcase(suite, tcase_residues);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
