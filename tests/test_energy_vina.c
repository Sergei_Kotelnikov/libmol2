#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "atom_group.h"
#include "pdbqt.h"
#include "energy_vina.h"

struct mol_atom_group *test_ag_rec, *test_ag_lig, *test_ag;
size_t lig_nbranch;


const double tolerance_strict = 1e-9;

#ifndef ck_assert_double_eq_tol
#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));
#endif

#ifndef ck_assert_double_eq_def
#define ck_assert_double_eq_def(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);
#endif

static void test_vina_grads_intra(const struct mol_vina_params *vp, const double d, const double tolerance)
{
	double en, en1, t;
	struct mol_vector3 *fs = malloc(test_ag_lig->natoms * sizeof(struct mol_vector3));

	en = 0;
	mol_vina_energy_intra(test_ag_lig, &en, NULL, vp);

	for (size_t i = 0; i < test_ag_lig->natoms; ++i) {
		en1 = 0;
		t = test_ag_lig->coords[i].X;
		test_ag_lig->coords[i].X = d + t;
		mol_vina_energy_intra(test_ag_lig, &en1, NULL, vp);
		test_ag_lig->coords[i].X = t;
		fs[i].X = (en - en1) / d;

		en1 = 0;
		t = test_ag_lig->coords[i].Y;
		test_ag_lig->coords[i].Y = d + t;
		mol_vina_energy_intra(test_ag_lig, &en1, NULL, vp);
		test_ag_lig->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;

		en1 = 0;
		t = test_ag_lig->coords[i].Z;
		test_ag_lig->coords[i].Z = d + t;
		mol_vina_energy_intra(test_ag_lig, &en1, NULL, vp);
		test_ag_lig->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	en = 0;
	mol_zero_gradients(test_ag_lig);
	mol_vina_energy_intra(test_ag_lig, &en1, NULL, vp);
	char msg[256];

	for (size_t i = 0; i < test_ag_lig->natoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        test_ag_lig->gradients[i].X, test_ag_lig->gradients[i].Y, test_ag_lig->gradients[i].Z,
		        fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(test_ag_lig->gradients[i].X - fs[i].X) < tolerance, msg);
		ck_assert_msg(fabs(test_ag_lig->gradients[i].Y - fs[i].Y) < tolerance, msg);
		ck_assert_msg(fabs(test_ag_lig->gradients[i].Z - fs[i].Z) < tolerance, msg);

	}
	free(fs);
}

static void test_vina_grads_inter(const struct mol_vina_params *vp, const double d, const double tolerance)
{
	// For the sake of speed, we test only every other atom
	const size_t stride = 37; // Some relatively large and not-nice number, that's less than the number of atoms in the ligand
	double en, en1, t;
	struct mol_vector3 *fs = malloc(test_ag->natoms * sizeof(struct mol_vector3));

	en = 0;
	mol_vina_energy_inter(test_ag, &en, NULL, vp);

	for (size_t i = 0; i < test_ag->natoms; i+=stride) {
		en1 = 0;
		t = test_ag->coords[i].X;
		test_ag->coords[i].X = d + t;
		mol_vina_energy_inter(test_ag, &en1, NULL, vp);
		test_ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;

		en1 = 0;
		t = test_ag->coords[i].Y;
		test_ag->coords[i].Y = d + t;
		mol_vina_energy_inter(test_ag, &en1, NULL, vp);
		test_ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;

		en1 = 0;
		t = test_ag->coords[i].Z;
		test_ag->coords[i].Z = d + t;
		mol_vina_energy_inter(test_ag, &en1, NULL, vp);
		test_ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	en = 0;
	mol_zero_gradients(test_ag);
	mol_vina_energy_inter(test_ag, &en1, NULL, vp);
	char msg[256];

	for (size_t i = 0; i < test_ag->natoms; i+=stride) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        test_ag->gradients[i].X, test_ag->gradients[i].Y, test_ag->gradients[i].Z,
		        fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(test_ag->gradients[i].X - fs[i].X) < tolerance, msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Y - fs[i].Y) < tolerance, msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Z - fs[i].Z) < tolerance, msg);

	}
	free(fs);
}


START_TEST(test_vina_get_num_branches_from_pdbqt)
{
	// Should be equal to the number of active torsions
	ck_assert_int_eq(mol_vina_get_num_branches_from_pdbqt("cats.pdbqt"), 9);
	ck_assert_int_eq(mol_vina_get_num_branches_from_pdbqt("bace-lig.pdbqt"), 8);
	ck_assert_int_eq(mol_vina_get_num_branches_from_pdbqt("bace.pdbqt"), 0);
}
END_TEST

START_TEST(test_vina_free_energy)
{
	ck_assert_double_eq_def(mol_vina_free_energy(1.0, 0, NULL), 1.0);
	ck_assert_double_eq_def(mol_vina_free_energy(10.0, 0, NULL), 10.0);
	ck_assert_double_eq_def(mol_vina_free_energy(10.0, 1, NULL), 9.44768815070952);
	ck_assert_double_eq_def(mol_vina_free_energy(10.0, 5, NULL), 7.73814129846011);
	// Check that changing weights work
	struct mol_vina_params vp = { .weights = { 0 } };
	vp.weights.branch = 1;
	ck_assert_double_eq_def(mol_vina_free_energy(10.0, 5, &vp), 1.6666666666667);
}
END_TEST

START_TEST(test_vina_update_box)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag_lig, 0, test_ag_lig->natoms - 1, true, 0);
	bool ret;
	// No buffer: just max and min of ligand coordinates
	ret = mol_vina_update_box(&vp, test_ag_lig, 0, false);
	ck_assert(ret == true);
	ck_assert_double_eq_def(vp.box_min.X, 9.747);
	ck_assert_double_eq_def(vp.box_min.Y, 36.130);
	ck_assert_double_eq_def(vp.box_min.Z, -12.528);
	ck_assert_double_eq_def(vp.box_max.X, 22.370);
	ck_assert_double_eq_def(vp.box_max.Y, 45.079);
	ck_assert_double_eq_def(vp.box_max.Z, -4.334);

	ret = mol_vina_update_box(&vp, test_ag_lig, 4, false);
	ck_assert(ret == true);
	ck_assert_double_eq_def(vp.box_min.X, 5.747);
	ck_assert_double_eq_def(vp.box_min.Y, 32.130);
	ck_assert_double_eq_def(vp.box_min.Z, -16.528);
	ck_assert_double_eq_def(vp.box_max.X, 26.370);
	ck_assert_double_eq_def(vp.box_max.Y, 49.079);
	ck_assert_double_eq_def(vp.box_max.Z, -0.334);

	// Now we have the atomgroup with receptor. It should not affect anything
	vp = mol_vina_params_init(test_ag,
		test_ag_rec->natoms, test_ag_rec->natoms + test_ag_lig->natoms - 1, true, 0);
	ret = mol_vina_update_box(&vp, test_ag, 4, false);
	ck_assert(ret == true);
	ck_assert_double_eq_def(vp.box_min.X, 5.747);
	ck_assert_double_eq_def(vp.box_min.Y, 32.130);
	ck_assert_double_eq_def(vp.box_min.Z, -16.528);
	ck_assert_double_eq_def(vp.box_max.X, 26.370);
	ck_assert_double_eq_def(vp.box_max.Y, 49.079);
	ck_assert_double_eq_def(vp.box_max.Z, -0.334);

	// Move the first atom to the lower left corner of the box, and last atom to the upper right. They are still
	// within the box, so lazy update should not do anything.
	test_ag->coords[vp.lig_index_first].X = 5.8;
	test_ag->coords[vp.lig_index_first].Y = 32.2;
	test_ag->coords[vp.lig_index_first].Z = -16.4;
	test_ag->coords[vp.lig_index_last].X = 26.3;
	test_ag->coords[vp.lig_index_last].Y = 49.0;
	test_ag->coords[vp.lig_index_last].Z = -0.4;
	ret = mol_vina_update_box(&vp, test_ag, 4, true);
	ck_assert(ret == false);
	ck_assert_double_eq_def(vp.box_min.X, 5.747);
	ck_assert_double_eq_def(vp.box_min.Y, 32.130);
	ck_assert_double_eq_def(vp.box_min.Z, -16.528);
	ck_assert_double_eq_def(vp.box_max.X, 26.370);
	ck_assert_double_eq_def(vp.box_max.Y, 49.079);
	ck_assert_double_eq_def(vp.box_max.Z, -0.334);

	// But as soon as the fist atom is moved outside of the box, the lazy update should do
	test_ag->coords[vp.lig_index_first].X = 5.7;
	ret = mol_vina_update_box(&vp, test_ag, 4, true);
	ck_assert(ret == true);
	ck_assert_double_eq_def(vp.box_min.X, 1.7);
	ck_assert_double_eq_def(vp.box_min.Y, 28.2);
	ck_assert_double_eq_def(vp.box_min.Z, -20.4);
	ck_assert_double_eq_def(vp.box_max.X, 30.3);
	ck_assert_double_eq_def(vp.box_max.Y, 53.0);
	ck_assert_double_eq_def(vp.box_max.Z, 3.6);
}
END_TEST

START_TEST(test_vina_update_inter_list)
{
	// We have only ligand, so we should not have any receptor atoms here.
	struct mol_vina_params vp = mol_vina_params_init(test_ag,
		test_ag_rec->natoms, test_ag_rec->natoms + test_ag_lig->natoms - 1, true, -1.0);

	// Nothing falls into the box
	MOL_VEC_SET_SCALAR(vp.box_min, 20000);
	MOL_VEC_SET_SCALAR(vp.box_max, 20001);
	mol_vina_update_inter_list(&vp, test_ag);
	ck_assert_int_eq(vp.rec_list.size, 0);

	// Everything falls into the box
	mol_vina_update_box(&vp, test_ag, 10000, false);
	mol_vina_update_inter_list(&vp, test_ag);
	ck_assert_int_eq(vp.rec_list.size, 3597);
}
END_TEST

START_TEST(test_vina_params_init_all_receptor)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag,
		test_ag_rec->natoms, test_ag_rec->natoms + test_ag_lig->natoms - 1, true, -1.0);
	ck_assert_int_eq(vp.lig_index_first, test_ag_rec->natoms);
	ck_assert_int_eq(vp.lig_index_last, test_ag_rec->natoms + test_ag_lig->natoms - 1);
	ck_assert(vp.cutoff_sq == 8*8);
	// Atom types are computed for all atoms anyway
	ck_assert_int_eq(vp.atom_types[0], XS_ATOM_N_A); // First receptor atom
	ck_assert_int_eq(vp.atom_types[3597], XS_ATOM_N_D); // First ligand atom
	// By default, all atoms are in a box
	ck_assert_int_eq(vp.rec_list.size, 3597);
	ck_assert_int_eq(vp.rec_list.members[0], 0);
	ck_assert_int_eq(vp.rec_list.members[3596], 3596);

	// Exclusions for the first atom (N2)
	const int excl_0[] = {
		0, // self
		4, 5, 21, // 0-1
		6, 2, 22, 8, // 0-2
		7, 1, 9, 27, 23, 25 // 0-3
	};
	bool excl_mask[41]; // 41 == test_ag_lig->natoms
	memset(excl_mask, 0, sizeof(excl_mask));
	for (size_t i = 0; i < sizeof(excl_0)/sizeof(excl_0[0]); i++)
		excl_mask[excl_0[i]] = true;
	for (size_t i = 0; i < test_ag_lig->natoms; i++) {
		ck_assert_msg(excl_mask[i] == vp.exclmat[0*test_ag_lig->natoms + i], "Wrong exclusion flag for pair (0, %zu)", i);
		ck_assert_msg(excl_mask[i] == vp.exclmat[i*test_ag_lig->natoms + 0], "Wrong exclusion flag for pair (%zu, 0)", i);
	}

	// Exclusions for the last atom (C6)
	const int excl_40[] = {
		40, // self
		38, // 0-1
		39, 34, // 0-2
		35, 33 // 0-3
	};
	memset(excl_mask, 0, sizeof(excl_mask));
	for (size_t i = 0; i < sizeof(excl_40)/sizeof(excl_40[0]); i++)
		excl_mask[excl_40[i]] = true;
	for (size_t i = 0; i < test_ag_lig->natoms; i++) {
		ck_assert_msg(excl_mask[i] == vp.exclmat[40*test_ag_lig->natoms + i], "Wrong exclusion flag for pair (40, %zu)", i);
		ck_assert_msg(excl_mask[i] == vp.exclmat[i*test_ag_lig->natoms + 40], "Wrong exclusion flag for pair (%zu, 40)", i);
	}
}
END_TEST

START_TEST(test_vina_params_init_small_box)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag,
		test_ag_rec->natoms, test_ag_rec->natoms + test_ag_lig->natoms - 1, true, 4);
	ck_assert_int_eq(vp.lig_index_first, test_ag_rec->natoms);
	ck_assert_int_eq(vp.lig_index_last, test_ag_rec->natoms + test_ag_lig->natoms - 1);
	ck_assert(vp.cutoff_sq == 8*8);
	// Atom types are computed for all atoms anyway
	ck_assert_int_eq(vp.atom_types[0], XS_ATOM_N_A);
	ck_assert_int_eq(vp.atom_types[3597], XS_ATOM_N_D);
	/**
	 * We don't compare with the very exact value to avoid too tight dependency on the algorithm.
	 * Our atoms hsould be within 8A of the box, so we check that their number is more than the number of
	 * atoms within 8A of any atom in the box (lower bound), and that their number is greater than the number
	 * of atoms is a box extended by 8A in each direction.
	 *
	 * Upper bound: all the atoms within box expanded by cutoff in all direction.
	 * PyMOL>select box, bace & x > 5.747 & y > 32.130 & z > -16.528 & x < 26.370 & y < 49.079 & z < -1.334
	 * Selector: selection "box" defined with 248 atoms.
	 * PyMOL>select box_ext, bace & x > -2.253 & y > 24.130 & z > -24.528 & x < 34.370 & y < 57.079 & z < 7.666
	 * Selector: selection "box_ext" defined with 1504 atoms.
	 * PyMOL>select needed, bace within 8 of box
	 * Selector: selection "needed" defined with 1090 atoms.
	 */
	ck_assert_int_ge(vp.rec_list.size, 1090);
	ck_assert_int_le(vp.rec_list.size, 1504);
}
END_TEST


START_TEST(test_vina_params_init_no_receptor)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag_lig, 0, test_ag_lig->natoms - 1, true, 4);
	ck_assert_int_eq(vp.lig_index_first, 0);
	ck_assert_int_eq(vp.lig_index_last, test_ag_lig->natoms - 1);
	ck_assert(vp.cutoff_sq == 8*8);
	// Atom types are computed for all atoms anyway
	ck_assert_int_eq(vp.atom_types[0], XS_ATOM_N_D);
	ck_assert_int_eq(vp.atom_types[40], XS_ATOM_C);
	// We have only ligand, so no receptor
	ck_assert_int_eq(vp.rec_list.size, 0);
	// We have buffer of 4 A
	ck_assert_double_eq_def(vp.box_min.X, 5.747);
	ck_assert_double_eq_def(vp.box_min.Y, 32.130);
	ck_assert_double_eq_def(vp.box_min.Z, -16.528);
	ck_assert_double_eq_def(vp.box_max.X, 26.370);
	ck_assert_double_eq_def(vp.box_max.Y, 49.079);
	ck_assert_double_eq_def(vp.box_max.Z, -0.334);
}
END_TEST

START_TEST(test_inter_real)
{
	// Ligand does not move, so we don't need any buffer
	struct mol_vina_params vp = mol_vina_params_init(test_ag,
		test_ag_rec->natoms, test_ag_rec->natoms + test_ag_lig->natoms - 1, true, 0);

	mol_zero_gradients(test_ag);

	double energy = 0.0;
	struct mol_vina_components energy_components = { 0 };

	mol_vina_energy_inter(test_ag, &energy, &energy_components, &vp);
	double efree = mol_vina_free_energy(energy, lig_nbranch, &vp);
	double en1 = (
		energy_components.gauss1 * (-0.035579) +
		energy_components.gauss2 * (-0.005156) +
		energy_components.repuls * (0.840245) +
		energy_components.hydrph * (-0.035069) +
		energy_components.hydrbn * (-0.587439)) /
		(1.0 + (0.05846) * lig_nbranch);
	// Check for self-consistency
	ck_assert_double_eq_def(en1, efree);
	static double energy_tol = 1e-6;
	// Compare with v_alone implementation
	//     The calculation of ehydrbn was corrected (some energy was errorneously added to hydrph)
	// The energy values are the same as original AutoDock Vina 1.1.2:
	//     vina --receptor bace.pdbqt --ligand bace-lig.pdbqt --score_only
	//     Intermolecular contributions to the terms, before weighting:
	//     gauss 1     : 129.39879
	//     gauss 2     : 1927.65987
	//     repulsion   : 6.33964
	//     hydrophobic : 60.45408
	//     Hydrogen    : 6.99608
	ck_assert_double_eq_tol(efree, -10.524006, energy_tol);
	ck_assert_double_eq_tol(energy, -15.445874, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss1, 129.398794, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss2, 1927.659872, energy_tol);
	ck_assert_double_eq_tol(energy_components.repuls, 6.339644, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrph, 60.454078, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrbn, 6.996079, energy_tol);
	ck_assert_double_eq_tol(energy_components.branch, 0.0, energy_tol);

	// Test gradients
	test_vina_grads_inter(&vp, 1e-6, 1e-5);

	// Test weighting
	energy = 0;
	vp.weights.gauss1 = vp.weights.gauss2 = vp.weights.repuls = vp.weights.hydrbn = vp.weights.hydrph = 1;
	mol_vina_energy_inter(test_ag, &energy, &energy_components, &vp);
	ck_assert_double_eq_tol(energy, 2130.848467, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss1, 129.398794, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss2, 1927.659872, energy_tol);
	ck_assert_double_eq_tol(energy_components.repuls, 6.339644, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrph, 60.454078, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrbn, 6.996079, energy_tol);
	ck_assert_double_eq_tol(energy_components.branch, 0.0, energy_tol);
}
END_TEST

START_TEST(test_intra_real)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag_lig, 0, test_ag_lig->natoms - 1, true, -1.0);
	mol_zero_gradients(test_ag_lig);

	double energy = 0.0;
	struct mol_vina_components energy_components;

	mol_vina_energy_intra(test_ag_lig, &energy, &energy_components, &vp);
	double efree = mol_vina_free_energy(energy, lig_nbranch, &vp);
	double en1 = (
	             energy_components.gauss1 * (-0.035579) +
	             energy_components.gauss2 * (-0.005156) +
	             energy_components.repuls * (0.840245) +
	             energy_components.hydrph * (-0.035069) +
	             energy_components.hydrbn * (-0.587439)) /
                     (1.0 + (0.05846) * lig_nbranch);
	// Check for self-consistency
	ck_assert_double_eq_def(en1, efree);
	static double energy_tol = 1e-6;
	// Compare with v_alone implementation
	//     The calculation of ehydrbn was corrected (some energy was errorneously added to hydrph)
	//     Exclusion lists were fixed (indexing errors when writing to diagonal elements)
	// The energy DIFFERS from original AutoDock Vina 1.1.2:
	//     We don't exclude "fixed" bonds from energy calculation
	//     And the overall the bond list differs slightly due to different algorithm for deducing bonds from structure
	ck_assert_double_eq_tol(efree, -1.297241, energy_tol);
	ck_assert_double_eq_tol(energy, -1.903935, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss1, 22.839597, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss2, 210.832883, energy_tol);
	ck_assert_double_eq_tol(energy_components.repuls, 0.625017, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrph, 15.097026, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrbn, 0.000000, energy_tol);
	ck_assert_double_eq_tol(energy_components.branch, 0.0, energy_tol);

	// Test gradients
	test_vina_grads_intra(&vp, 1e-6, 1e-4);

	// Test weighting
	energy = 0;
	vp.weights.gauss1 = vp.weights.gauss2 = vp.weights.repuls = vp.weights.hydrbn = vp.weights.hydrph = 1;
	mol_vina_energy_intra(test_ag_lig, &energy, &energy_components, &vp);
	ck_assert_double_eq_tol(energy, 249.394523, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss1, 22.839597, energy_tol);
	ck_assert_double_eq_tol(energy_components.gauss2, 210.832883, energy_tol);
	ck_assert_double_eq_tol(energy_components.repuls, 0.625017, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrph, 15.097026, energy_tol);
	ck_assert_double_eq_tol(energy_components.hydrbn, 0.000000, energy_tol);
	ck_assert_double_eq_tol(energy_components.branch, 0.0, energy_tol);
}
END_TEST


START_TEST(test_vina_components_scale)
{
	struct mol_vina_params vp = mol_vina_params_init(test_ag_lig, 0, test_ag_lig->natoms - 1, true, -1.0);
	mol_zero_gradients(test_ag_lig);

	static double energy_tol = 1e-6;
	double energy = 0.0;

	// Default weights
	mol_vina_energy_intra(test_ag_lig, &energy, NULL, &vp);
	ck_assert_double_eq_tol(energy, -1.903935, energy_tol);

	// Scale by 100
	mol_vina_components_scale(&vp.weights, 100);
	ck_assert_double_eq_def(vp.weights.gauss1, -3.5579);
	ck_assert_double_eq_def(vp.weights.gauss2, -0.5156);
	ck_assert_double_eq_def(vp.weights.repuls, 84.0245);
	ck_assert_double_eq_def(vp.weights.hydrph, -3.5069);
	ck_assert_double_eq_def(vp.weights.hydrbn, -58.7439);
	ck_assert_double_eq_def(vp.weights.branch, 0.05846); // Not changed

	energy = 0;
	mol_vina_energy_intra(test_ag_lig, &energy, NULL, &vp);
	ck_assert_double_eq_tol(energy, -190.3935, energy_tol*100);

	// Scale by -1
	mol_vina_components_scale(&vp.weights, -1);
	ck_assert_double_eq_def(vp.weights.gauss1, 3.5579);
	ck_assert_double_eq_def(vp.weights.gauss2, 0.5156);
	ck_assert_double_eq_def(vp.weights.repuls, -84.0245);
	ck_assert_double_eq_def(vp.weights.hydrph, 3.5069);
	ck_assert_double_eq_def(vp.weights.hydrbn, 58.7439);
	ck_assert_double_eq_def(vp.weights.branch, 0.05846); // Not changed

	energy = 0;
	mol_vina_energy_intra(test_ag_lig, &energy, NULL, &vp);
	ck_assert_double_eq_tol(energy, 190.3935, energy_tol*100);

}
END_TEST


void setup_real(void)
{
	test_ag_rec = mol_pdbqt_read("bace.pdbqt");
	test_ag_lig = mol_pdbqt_read("bace-lig.pdbqt");

	test_ag_rec->gradients = calloc(test_ag_rec->natoms, sizeof(struct mol_vector3));
	test_ag_lig->gradients = calloc(test_ag_lig->natoms, sizeof(struct mol_vector3));
	lig_nbranch = mol_vina_get_num_branches_from_pdbqt("bace-lig.pdbqt");

	test_ag = mol_atom_group_join(test_ag_rec, test_ag_lig);
	mol_atom_group_join_atomic_metadata(test_ag, test_ag_rec, test_ag_lig, MOL_PDBQT_ATOM_TYPE_METADATA_KEY, sizeof(enum mol_autodock_atom_type));
}

void teardown_real(void)
{
	mol_atom_group_free(test_ag_rec);
	mol_atom_group_free(test_ag_lig);
	mol_atom_group_free(test_ag);
}


Suite *lists_suite(void)
{
	Suite *suite = suite_create("energy_vina");

	TCase *tcase_util = tcase_create("utilities");
	tcase_add_test(tcase_util, test_vina_get_num_branches_from_pdbqt);
	tcase_add_test(tcase_util, test_vina_free_energy);
	suite_add_tcase(suite, tcase_util);

	TCase *tcase_real = tcase_create("real");
	tcase_add_checked_fixture(tcase_real, setup_real, teardown_real);
	tcase_set_timeout(tcase_real, 100);
	tcase_add_test(tcase_real, test_vina_params_init_all_receptor);
	tcase_add_test(tcase_real, test_vina_params_init_small_box);
	tcase_add_test(tcase_real, test_vina_params_init_no_receptor);
	tcase_add_test(tcase_real, test_inter_real);
	tcase_add_test(tcase_real, test_intra_real);
	tcase_add_test(tcase_real, test_vina_update_box);
	tcase_add_test(tcase_real, test_vina_update_inter_list);
	tcase_add_test(tcase_real, test_vina_components_scale);
	suite_add_tcase(suite, tcase_real);

	return suite;
}

int main(void)
{
	Suite *suite = lists_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
