#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "pdbqt.h"

START_TEST(test_read_small_molecule)
{
	// File produced by AutoDock
	struct mol_atom_group *ag = mol_pdbqt_read("cats.pdbqt");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 46);

	ck_assert_str_eq(ag->atom_name[9], " C15");
	ck_assert(ag->coords[9].X == -3.058);
	ck_assert(ag->coords[9].Y == 11.560);
	ck_assert(ag->coords[9].Z == -8.826);
	ck_assert(ag->occupancy[9] == 1.00);
	ck_assert(ag->B[9] == 0.00);
	ck_assert(ag->charge[9] == 0.158);
	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
			mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	ck_assert_int_eq(autodock_type[9], AUTODOCK_ATOM_C);

	for (size_t i = 0; i < ag->natoms; ++i) {
		ck_assert(ag->alternate_location[i] == ' ');
		ck_assert_str_eq(ag->residue_name[i], "X00");
		ck_assert(ag->residue_id[i].chain == ' ');
		ck_assert_int_eq(ag->residue_id[i].residue_seq, 1);
		ck_assert(ag->residue_id[i].insertion == ' ');

		struct mol_residue *res = mol_atom_residue(ag, i);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
		ck_assert(ag->residue_id[i].chain == res->residue_id.chain);
		ck_assert(ag->residue_id[i].residue_seq == res->residue_id.residue_seq);
		ck_assert(ag->residue_id[i].insertion == res->residue_id.insertion);
	}

	ck_assert(ag->residue_list[0] != NULL);
	ck_assert_int_eq(ag->residue_list[0]->atom_start, 0);
	ck_assert_int_eq(ag->residue_list[0]->atom_end, 45);
	ck_assert_int_eq(ag->residue_list[0]->residue_id.residue_seq, 1);
	ck_assert_int_eq(mol_residue_id_cmp(&ag->residue_list[0]->residue_id, &ag->residue_id[0]), 0);

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_protein)
{
	// File produced by Babel.
	// For the testing, edited alternate location, occupancy, and beta for atom 560
	struct mol_atom_group *ag = mol_pdbqt_read("1rei.pdbqt");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);
	//ATOM    560  N  AILE A  2      -10.122  46.639   2.897  0.50  8.70    -0.197 NA
	ck_assert_str_eq(ag->atom_name[559], " N  ");
	ck_assert(ag->alternate_location[559] == 'A');
	ck_assert_str_eq(ag->residue_name[559], "ILE");
	ck_assert(ag->residue_id[559].chain == 'A');
	ck_assert_int_eq(ag->residue_id[559].residue_seq, 2);
	ck_assert(ag->residue_id[559].insertion == ' ');
	ck_assert(ag->coords[559].X == -10.122);
	ck_assert(ag->coords[559].Y == 46.639);
	ck_assert(ag->coords[559].Z == 2.897);
	ck_assert(ag->occupancy[559] == 0.50);
	ck_assert(ag->B[559] == 8.70);
	ck_assert(ag->charge[559] == -0.197);

	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
			mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	ck_assert_int_eq(autodock_type[559], AUTODOCK_ATOM_NA);

	for (size_t i = 0; i < ag->natoms; i++) {
		struct mol_residue *res = mol_atom_residue(ag, i);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
		ck_assert(ag->residue_id[i].chain == res->residue_id.chain);
		ck_assert(ag->residue_id[i].residue_seq == res->residue_id.residue_seq);
		ck_assert(ag->residue_id[i].insertion == res->residue_id.insertion);
	}

	for (size_t i = 0; i < ag->nresidues - 1; i++) {
		struct mol_residue *a = ag->residue_list[i];
		struct mol_residue *b = ag->residue_list[i+1];

		ck_assert(a != NULL);
		ck_assert(b != NULL);

		ck_assert(mol_residue_id_cmp(&a->residue_id,
		                             &b->residue_id) == -1);
	}
}
END_TEST

START_TEST(test_read_nonexistent)
{
	struct mol_atom_group *ag = mol_pdbqt_read("nonexistent");
	ck_assert(ag == NULL);
}
END_TEST

START_TEST(test_read_no_atoms)
{
	// It's pdb, not pdbqt. But since there are no atoms, what's the difference?
	struct mol_atom_group *ag = mol_pdbqt_read("no_atoms.pdb");
	ck_assert(ag == NULL);
}
END_TEST


Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("pdbqt_reading");

	TCase *tcase_read = tcase_create("test_read");
	tcase_add_test(tcase_read, test_read_small_molecule);
	tcase_add_test(tcase_read, test_read_protein);

	TCase *tcase_errors = tcase_create("test_read_errors");
	tcase_add_test(tcase_errors, test_read_nonexistent);
	tcase_add_test(tcase_errors, test_read_no_atoms);

	suite_add_tcase(suite, tcase_read);
	suite_add_tcase(suite, tcase_errors);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
