#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "pdb.h"
#include "icharmm.h"
#include "rmsd.h"
#include "vector.h"

#define TOLERANCE 1e-8

START_TEST(test_rmsd)
{
	struct mol_matrix3_list *rots = mol_matrix3_list_from_file("rot.prm");
	struct mol_vector3 tv1 = {1.0, 1.0, 1.0};
	struct mol_vector3 tv2 = {2.0, 2.0, 2.0};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");
	struct mol_atom_group *ag2 = mol_read_pdb("1rei.pdb");

	struct mol_vector3 center;
	centroid(&center, ag1);
	MOL_VEC_MULT_SCALAR(center, center, -1);

	mol_atom_group_translate(ag1, &center);
	mol_atom_group_translate(ag2, &center);

	mol_atom_group_move(ag1, &rots->members[0], &tv1);
	mol_atom_group_move(ag2, &rots->members[1], &tv2);

	double r = atom_group_rmsd(ag1, ag2);

	ck_assert(fabs(r - 3.0855179599e+01) <= TOLERANCE);

	mol_list_free(rots);
	mol_atom_group_free(ag1);
	mol_atom_group_free(ag2);
}
END_TEST

START_TEST(test_indices)
{
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");

	struct mol_index_list *indices = atom_group_all_indices(ag1);

	double distance = index_rmsd(ag1->coords, ag1->coords, indices);

	ck_assert(distance == 0);

	mol_list_free(indices);
	mol_atom_group_free(ag1);
}
END_TEST

START_TEST(test_rmsd_sym)
{
	struct mol_atom_group *ag1 = mol_read_pdb("butane.pdb");
	struct mol_atom_group *ag2 = mol_read_pdb("butane2.pdb");
	mol_atom_group_read_geometry(ag1, "butane.psf", "small.prm", "small.rtf");
	struct mol_symmetry_list *symmetry =  mol_detect_symmetry(ag1);

	double r = mol_atom_group_rmsd_sym(ag1, ag2, symmetry);

	ck_assert(fabs(r - 1.0) <= TOLERANCE);

	mol_atom_group_free(ag1);
	mol_atom_group_free(ag2);
}
END_TEST

Suite *rmsd_suite(void)
{
	Suite *suite = suite_create("rmsd");

	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_rmsd);
	tcase_add_test(tcase, test_indices);
	tcase_add_test(tcase, test_rmsd_sym);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = rmsd_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
