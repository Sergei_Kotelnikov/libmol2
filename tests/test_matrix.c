#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "matrix.h"

// Test cases
START_TEST(test_rotate1)
{
  struct mol_matrix3 rot = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};

  //tests input and output matrices being at same location in memory
  mol_matrix3_multiply(&rot, &rot, &rot);

  ck_assert(fabs(rot.m11 - 30.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m12 - 36.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m13 - 42.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m21 - 66.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m22 - 81.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m23 - 96.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m31 - 102.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m32 - 126.0) <= DBL_EPSILON);
  ck_assert(fabs(rot.m33 - 150.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_rotate2)
{
  struct mol_matrix3 rot1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  struct mol_matrix3 rot2 = {-1.0, -2.0, -3.0, -4.0, -5.0, -6.0, -7.0, -8.0, -9.0};
  struct mol_matrix3 rot3;

  mol_matrix3_multiply(&rot3, &rot1, &rot2);

  ck_assert(fabs(rot3.m11 + 30.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m12 + 36.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m13 + 42.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m21 + 66.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m22 + 81.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m23 + 96.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m31 + 102.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m32 + 126.0) <= DBL_EPSILON);
  ck_assert(fabs(rot3.m33 + 150.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_rotate_random)
{
	const double eps = 1e-9;
	const struct mol_matrix3 rot1 = {
		.m11 = 0.825406, .m12 = 0.535188, .m13 = 0.058269,
		.m21 = 0.100526, .m22 = 0.196159, .m23 = 0.584842,
		.m31 = 0.926538, .m32 = 0.860059, .m33 = 0.224466};
	const struct mol_matrix3 rot2 = {
		.m11 = 0.836150, .m12 = 0.061400, .m13 = 0.142246,
		.m21 = 0.221859, .m22 = 0.795132, .m23 = 0.274921,
		.m31 = 0.606147, .m32 = 0.975627, .m33 = 0.228991};
	struct mol_matrix3 rot3;
	const struct mol_matrix3 rotg = {
		.m11 = 0.844219080935, .m12 = 0.533073842879, .m13 = 0.277888198603,
		.m21 = 0.482074678255, .m22 = 0.732732240322, .m23 = 0.202151204257,
		.m31 = 1.101595970883, .m32 = 0.959744956170, .m33 = 0.419645298493}; // rot1 * rot2

	mol_matrix3_multiply(&rot3, &rot2, &rot1);

	ck_assert(fabs(rot3.m11 - rotg.m11) <= eps);
	ck_assert(fabs(rot3.m12 - rotg.m12) <= eps);
	ck_assert(fabs(rot3.m13 - rotg.m13) <= eps);
	ck_assert(fabs(rot3.m21 - rotg.m21) <= eps);
	ck_assert(fabs(rot3.m22 - rotg.m22) <= eps);
	ck_assert(fabs(rot3.m23 - rotg.m23) <= eps);
	ck_assert(fabs(rot3.m31 - rotg.m31) <= eps);
	ck_assert(fabs(rot3.m32 - rotg.m32) <= eps);
	ck_assert(fabs(rot3.m33 - rotg.m33) <= eps);
}
END_TEST

Suite *matrix_suite(void)
{
	Suite *suite = suite_create("matrix");

	TCase *tcase = tcase_create("test_mol_matrix3_multiply");
	tcase_add_test(tcase, test_rotate1);
	tcase_add_test(tcase, test_rotate2);
	tcase_add_test(tcase, test_rotate_random);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = matrix_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
