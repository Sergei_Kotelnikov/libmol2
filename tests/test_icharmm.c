#include <stdio.h>
#include <check.h>
#include <math.h>

#include "atom_group.h"
#include "pdb.h"
#include "icharmm.h"

// Test cases
START_TEST(test_read_ff_charm)
{
	struct mol_atom_group *ag = mol_read_pdb("phenol.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 8);

	bool retval = mol_atom_group_read_geometry(ag, "phenol.psf", "small.prm", "small.rtf");
	ck_assert(retval);

	ck_assert(ag->bonds != NULL);
	ck_assert_int_eq(ag->nbonds, 8);
	ck_assert_int_eq(ag->nangles, 9);
	ck_assert_int_eq(ag->ndihedrals, 10);
	ck_assert_int_eq(ag->nimpropers, 1);

	ck_assert(ag->bonds[0].k == 478.4);
	ck_assert(ag->angles[0].k == 67.18);
	ck_assert(ag->dihedrals[0].k == 3.625);
	ck_assert(ag->impropers[0].k == 1.1);

	ck_assert(ag->ftypen != NULL);
	ck_assert(ag->ace_volume != NULL);
	ck_assert(ag->eps != NULL);
	ck_assert(ag->eps03 != NULL);
	ck_assert(ag->vdw_radius != NULL);
	ck_assert(ag->vdw_radius03 != NULL);
	ck_assert(ag->charge != NULL);
	ck_assert(ag->hbond_prop != NULL);
	ck_assert(ag->hbond_base_atom_id != NULL);

	// First atom is of type CA
	ck_assert(ag->ftypen[0] == 6);
	ck_assert(ag->ace_volume[0] == 18.583);
	ck_assert(fabs(ag->eps[0] - sqrt(0.086)) < 1E-6);
	ck_assert(fabs(ag->eps03[0] - sqrt(0.0430)) < 1E-6);
	ck_assert(ag->vdw_radius[0] == 1.908);
	ck_assert(ag->vdw_radius03[0] == 1.908);
	ck_assert(ag->charge[0] == -0.033);

	ck_assert_int_eq(ag->bond_lists[2].size, 2);
	ck_assert_int_eq(ag->angle_lists[2].size, 4);
	ck_assert_int_eq(ag->dihedral_lists[2].size, 6);
	ck_assert_int_eq(ag->improper_lists[2].size, 1);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_ff_charm_big_ftype)
{
	// The test is the same as test_read_ff_charm, but now we have very big atom type numbers, just to check that
	//   we handle them correctly (we had related bug).
	struct mol_atom_group *ag = mol_read_pdb("phenol.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 8);

	bool retval = mol_atom_group_read_geometry(ag, "phenol_big_ftype.psf", "small.prm", "small_big_ftype.rtf");
	ck_assert(retval);

	ck_assert(ag->bonds != NULL);
	ck_assert_int_eq(ag->nbonds, 8);
	ck_assert_int_eq(ag->nangles, 9);
	ck_assert_int_eq(ag->ndihedrals, 10);
	ck_assert_int_eq(ag->nimpropers, 1);

	ck_assert(ag->bonds[0].k == 478.4);
	ck_assert(ag->angles[0].k == 67.18);
	ck_assert(ag->dihedrals[0].k == 3.625);
	ck_assert(ag->impropers[0].k == 1.1);

	// First atom is of type CA
	ck_assert(ag->ftypen[0] == 996);
	ck_assert(ag->ace_volume[0] == 18.583);
	ck_assert(fabs(ag->eps[0] - sqrt(0.086)) < 1E-6);
	ck_assert(fabs(ag->eps03[0] - sqrt(0.0430)) < 1E-6);
	ck_assert(ag->vdw_radius[0] == 1.908);
	ck_assert(ag->vdw_radius03[0] == 1.908);
	ck_assert(ag->charge[0] == -0.033);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_donors_acceptors)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_A_prep.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1013);

	bool retval = mol_atom_group_read_psf(ag, "1rei_A_prep.psf");
	ck_assert(retval);

	ck_assert(ag->hbond_prop != NULL);
	ck_assert(ag->hbond_base_atom_id != NULL);
	ck_assert_int_eq(ag->hbond_prop[0], HBOND_DONOR);
	ck_assert_int_eq(ag->hbond_prop[1], DONATABLE_HYDROGEN);
	ck_assert_int_eq(ag->hbond_base_atom_id[1], 0);
	ck_assert_int_eq(ag->hbond_prop[5], HBOND_ACCEPTOR);
	ck_assert_int_eq(ag->hbond_base_atom_id[5], 4);
	ck_assert_int_eq(ag->hbond_prop[43], HBOND_DONOR | HBOND_ACCEPTOR);
	ck_assert_int_eq(ag->hbond_base_atom_id[44], 43);

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_duplicate_dihedrals)
{
	struct mol_atom_group *ag = mol_read_pdb("butane.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 4);

	bool retval = mol_atom_group_read_geometry(ag, "butane.psf", "small.prm", "small.rtf");
	ck_assert(retval);

	ck_assert(ag->bonds != NULL);
	ck_assert_int_eq(ag->nbonds, 3);
	ck_assert_int_eq(ag->nangles, 2);
	ck_assert_int_eq(ag->ndihedrals, 3);
	ck_assert_int_eq(ag->nimpropers, 0);

	ck_assert(ag->dihedrals[0].k == 0.18);
	ck_assert(ag->dihedrals[1].k == 0.25);
	ck_assert(ag->dihedrals[2].k == 0.2);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_join)
{
	struct mol_atom_group *A = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(A, "phenol.psf", "small.prm", "small.rtf");

	struct mol_atom_group *B = mol_read_pdb("butane.pdb");
	mol_atom_group_read_geometry(B, "butane.psf", "small.prm", "small.rtf");

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	struct mol_atom_group *BA = mol_atom_group_join(B, A);

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	ck_assert_int_eq(AB->natoms, (A->natoms + B->natoms));
	ck_assert_int_eq(AB->nbonds, (A->nbonds + B->nbonds));
	ck_assert_int_eq(AB->nangles, (A->nangles + B->nangles));
	ck_assert_int_eq(AB->ndihedrals, (A->ndihedrals + B->ndihedrals));
	ck_assert_int_eq(AB->nimpropers, (A->nimpropers + B->nimpropers));

	ck_assert_int_eq(BA->natoms, (A->natoms + B->natoms));
	ck_assert_int_eq(BA->nbonds, (A->nbonds + B->nbonds));
	ck_assert_int_eq(BA->nangles, (A->nangles + B->nangles));
	ck_assert_int_eq(BA->ndihedrals, (A->ndihedrals + B->ndihedrals));
	ck_assert_int_eq(BA->nimpropers, (A->nimpropers + B->nimpropers));

	ck_assert_int_eq(AB->num_atom_types, A->num_atom_types + B->num_atom_types);
	ck_assert_int_eq(AB->ftypen[0], A->ftypen[0]);
	ck_assert_int_eq(AB->ftypen[A->natoms], B->ftypen[0] + A->num_atom_types);
	ck_assert(!strcmp(AB->ftype_name[0], A->ftype_name[0]));
	ck_assert(!strcmp(AB->ftype_name[A->natoms], B->ftype_name[0]));

	ck_assert_int_eq(BA->num_atom_types, A->num_atom_types + B->num_atom_types);
	ck_assert_int_eq(BA->ftypen[0], B->ftypen[0]);
	ck_assert_int_eq(BA->ftypen[B->natoms], A->ftypen[0] + B->num_atom_types);
	ck_assert(!strcmp(BA->ftype_name[0], B->ftype_name[0]));
	ck_assert(!strcmp(BA->ftype_name[B->natoms], A->ftype_name[0]));

	mol_atom_group_free(A);
	mol_atom_group_free(B);
	mol_atom_group_free(AB);
	mol_atom_group_free(BA);
}
END_TEST

START_TEST(test_join_no_geometry)
{
	struct mol_atom_group *A = mol_read_pdb("phenol.pdb");
	struct mol_atom_group *B = mol_read_pdb("butane.pdb");

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	struct mol_atom_group *BA = mol_atom_group_join(B, A);

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	ck_assert_int_eq(AB->natoms, (A->natoms + B->natoms));
	ck_assert_int_eq(BA->natoms, (A->natoms + B->natoms));
	mol_atom_group_free(A);
	mol_atom_group_free(B);
	mol_atom_group_free(AB);
	mol_atom_group_free(BA);
}
END_TEST

START_TEST(test_join_half_geometry)
{
	struct mol_atom_group *A = mol_read_pdb("phenol.pdb");
	struct mol_atom_group *B = mol_read_pdb("butane.pdb");
	mol_atom_group_read_geometry(B, "butane.psf", "small.prm", "small.rtf");

	struct mol_atom_group *AB = mol_atom_group_join(A, B);
	struct mol_atom_group *BA = mol_atom_group_join(B, A);

	ck_assert(AB != NULL);
	ck_assert(BA != NULL);

	ck_assert_int_eq(AB->natoms, (A->natoms + B->natoms));
	ck_assert_int_eq(AB->nbonds, (A->nbonds + B->nbonds));
	ck_assert_int_eq(AB->nangles, (A->nangles + B->nangles));
	ck_assert_int_eq(AB->ndihedrals, (A->ndihedrals + B->ndihedrals));
	ck_assert_int_eq(AB->nimpropers, (A->nimpropers + B->nimpropers));

	ck_assert_int_eq(BA->natoms, (A->natoms + B->natoms));
	ck_assert_int_eq(BA->nbonds, (A->nbonds + B->nbonds));
	ck_assert_int_eq(BA->nangles, (A->nangles + B->nangles));
	ck_assert_int_eq(BA->ndihedrals, (A->ndihedrals + B->ndihedrals));
	ck_assert_int_eq(BA->nimpropers, (A->nimpropers + B->nimpropers));

	ck_assert(AB->vdw_radius[0] == 0.0);
	ck_assert(BA->vdw_radius[0] == 1.9080);

	ck_assert_int_eq(AB->num_atom_types, B->num_atom_types);
	ck_assert_int_eq(AB->ftypen[0], 0);
	ck_assert_int_eq(AB->ftypen[A->natoms], B->ftypen[0]);
	ck_assert(AB->ftype_name[0] == NULL);
	ck_assert(!strcmp(AB->ftype_name[A->natoms], B->ftype_name[0]));

	ck_assert_int_eq(BA->num_atom_types, B->num_atom_types);
	ck_assert_int_eq(BA->ftypen[0], B->ftypen[0]);
	ck_assert_int_eq(BA->ftypen[B->natoms], 0);
	ck_assert(!strcmp(BA->ftype_name[0], B->ftype_name[0]));
	ck_assert(BA->ftype_name[B->natoms] == NULL);

	mol_atom_group_free(A);
	mol_atom_group_free(B);
	mol_atom_group_free(AB);
	mol_atom_group_free(BA);
}
END_TEST

static bool cmp_bonds(const struct mol_bond* a, const struct mol_bond* b)
{
	return  (a->ai == b->ai) &&
		(a->aj == b->aj) &&
		(a->k  == b->k) &&
		(a->l  == b->l) &&
		(a->l0 == b->l0) &&
		(a->sdf_type == b->sdf_type);

}

static bool cmp_angles(const struct mol_angle* a, const struct mol_angle* b)
{
	return  (a->a0 == b->a0) &&
		(a->a1 == b->a1) &&
		(a->a2 == b->a2) &&
		(a->k  == b->k) &&
		(a->th == b->th) &&
		(a->th0 == b->th0);
}

static bool cmp_dihedrals(const struct mol_dihedral* a, const struct mol_dihedral* b)
{
	return  (a->a0 == b->a0) &&
		(a->a1 == b->a1) &&
		(a->a2 == b->a2) &&
		(a->a3 == b->a3) &&
		(a->chi == b->chi) &&
		(a->d == b->d) &&
		(a->k == b->k) &&
		(a->n == b->n);
}

static bool cmp_impropers(const struct mol_improper* a, const struct mol_improper* b)
{
	return  (a->a0 == b->a0) &&
		(a->a1 == b->a1) &&
		(a->a2 == b->a2) &&
		(a->a3 == b->a3) &&
		(a->k == b->k) &&
		(a->psi == b->psi) &&
		(a->psi0 == b->psi0);
}

#define __CMP_LISTS(listA, listB, member_comparator) do {\
		ck_assert((listA).size == (listB).size);\
		for (size_t _i = 0; _i < (listA).size; ++_i) {\
			ck_assert(member_comparator((listA).members[_i], (listB).members[_i]));\
		}\
	} while(0);

START_TEST(test_copy)
{
	struct mol_atom_group *ag = mol_read_pdb("phenol.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 8);
	mol_atom_group_read_geometry(ag, "phenol.psf", "small.prm", "small.rtf");

	// manually set up gradients
	mol_atom_group_init_atomic_field(ag, gradients);
	for (size_t i = 0; i < ag->natoms; ++i) {
		ag->gradients[i].X = 0.1 * i;
		ag->gradients[i].Y = 0.2 * i;
		ag->gradients[i].Z = 0.3 * i;
	}

	// manually set up pwpot_id
	mol_atom_group_init_atomic_field(ag, pwpot_id);
	for (size_t i = 0; i < ag->natoms; ++i)
		ag->pwpot_id[i] = (i < ag->natoms / 2) ? 2 : 3;

	// manually set up hbond_prop and hbond_base_atom_id (to complete gibberish)
	mol_atom_group_init_atomic_field(ag, hbond_prop);
	for (size_t i = 0; i < ag->natoms; ++i) {
		MOL_ATOM_HBOND_PROP_SET(ag, i, HBOND_DONOR);
		ag->hbond_base_atom_id[i] = (i + 1) % ag->natoms;
	}

	// set up fixed atoms
	mol_fixed_init(ag);
	struct mol_index_list* fixed_list = calloc(1, sizeof(struct mol_index_list));
	fixed_list->size = 3;
	fixed_list->members = calloc(fixed_list->size, sizeof(size_t));
	// Fix C4, O1, H6
	fixed_list->members[0] = 3;
	fixed_list->members[1] = 6;
	fixed_list->members[2] = 7;
	mol_fixed_update_with_list(ag, fixed_list);

	struct mol_atom_group* new_ag;
	new_ag = mol_atom_group_copy(ag);

	ck_assert(new_ag != NULL);

	// Test forcefield-related atomic fields
	ck_assert(new_ag->natoms == ag->natoms);
	for (size_t i = 0; i < ag->natoms; ++i) {
		ck_assert(MOL_VEC_EQ(new_ag->gradients[i],ag->gradients[i]));
		ck_assert(new_ag->vdw_radius[i] == ag->vdw_radius[i]);
		ck_assert(new_ag->vdw_radius03[i] == ag->vdw_radius03[i]);
		ck_assert(new_ag->eps[i] == ag->eps[i]);
		ck_assert(new_ag->eps03[i] == ag->eps03[i]);
		ck_assert(new_ag->ace_volume[i] == ag->ace_volume[i]);
		ck_assert(new_ag->charge[i] == ag->charge[i]);
		ck_assert(new_ag->pwpot_id[i] == ag->pwpot_id[i]);
		ck_assert(new_ag->fixed[i] == ag->fixed[i]);
		ck_assert(new_ag->ftypen[i] == ag->ftypen[i]);
		ck_assert_str_eq(new_ag->ftype_name[i], ag->ftype_name[i]);
		ck_assert(new_ag->hbond_prop[i] == ag->hbond_prop[i]);
		ck_assert(new_ag->hbond_base_atom_id[i] == ag->hbond_base_atom_id[i]);
	}

	// Test geometry
	ck_assert(new_ag->nbonds == ag->nbonds);
	for (size_t i = 0; i < ag->nbonds; ++i)
		ck_assert(cmp_bonds(&new_ag->bonds[i], &ag->bonds[i]));

	ck_assert(new_ag->nangles == ag->nangles);
	for (size_t i = 0; i < ag->nangles; ++i)
		ck_assert(cmp_angles(&new_ag->angles[i], &ag->angles[i]));

	ck_assert(new_ag->ndihedrals == ag->ndihedrals);
	for (size_t i = 0; i < ag->ndihedrals; ++i)
		ck_assert(cmp_dihedrals(&new_ag->dihedrals[i], &ag->dihedrals[i]));

	ck_assert(new_ag->nimpropers == ag->nimpropers);
	for (size_t i = 0; i < ag->nimpropers; ++i)
		ck_assert(cmp_impropers(&new_ag->impropers[i], &ag->impropers[i]));

	// Test geometry lists
	for (size_t i = 0; i < ag->natoms; ++i) {
		__CMP_LISTS(new_ag->bond_lists[i],     ag->bond_lists[i],     cmp_bonds);
		__CMP_LISTS(new_ag->angle_lists[i],    ag->angle_lists[i],    cmp_angles);
		__CMP_LISTS(new_ag->dihedral_lists[i], ag->dihedral_lists[i], cmp_dihedrals);
		__CMP_LISTS(new_ag->improper_lists[i], ag->improper_lists[i], cmp_impropers);
	}

	// Test the correctness of fixed/active atoms/bonds/etc lists.
	for (size_t i = 0; i < ag->natoms; ++i)
		ck_assert(new_ag->fixed[i] == ag->fixed[i]);

	__CMP_LISTS((*new_ag->active_bonds),     (*ag->active_bonds),     cmp_bonds);
	__CMP_LISTS((*new_ag->active_angles),    (*ag->active_angles),    cmp_angles);
	__CMP_LISTS((*new_ag->active_dihedrals), (*ag->active_dihedrals), cmp_dihedrals);
	__CMP_LISTS((*new_ag->active_impropers), (*ag->active_impropers), cmp_impropers);
}
END_TEST

#undef __CMP_LISTS

Suite *icharmm_suite(void)
{
	Suite *suite = suite_create("icharmm");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 10);
	tcase_add_test(tcase, test_read_ff_charm);
	tcase_add_test(tcase, test_read_ff_charm_big_ftype);
	tcase_add_test(tcase, test_duplicate_dihedrals);
	tcase_add_test(tcase, test_join);
	tcase_add_test(tcase, test_join_no_geometry);
	tcase_add_test(tcase, test_join_half_geometry);
	tcase_add_test(tcase, test_donors_acceptors);
	tcase_add_test(tcase, test_copy);


	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = icharmm_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
