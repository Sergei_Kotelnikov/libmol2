#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>
#else
#include <io.h>
#endif
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "pdb.h"

START_TEST(test_write_pdb)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	char fname[] = "tempfileXXXXXX";
#ifdef _WIN32
	char* tmp_filename = mktemp(fname);
	FILE *tmpfile = fopen(tmp_filename, "w+");
#else
	int tmp_file_descriptor = mkstemp(fname);
	ck_assert(tmp_file_descriptor != -1);
	FILE *tmpfile = fdopen(tmp_file_descriptor, "w+");
#endif /* _WIN32 */
	ck_assert(tmpfile != NULL);

	mol_fwrite_pdb(tmpfile, ag);
	mol_atom_group_free(ag);

	rewind(tmpfile);
	ag = mol_fread_pdb(tmpfile);
	fclose(tmpfile);
#ifndef _WIN32
	close(tmp_file_descriptor);
#endif /* _WIN32 */
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);
	//ATOM      9  N  AILE A   2     -10.122  46.639   2.897  0.50  8.70           N+1
	ck_assert_str_eq(ag->atom_name[8], " N  ");
	ck_assert(ag->alternate_location[8] == 'A');
	ck_assert_str_eq(ag->residue_name[8], "ILE");
	ck_assert(ag->residue_id[8].chain == 'A');
	ck_assert_int_eq(ag->residue_id[8].residue_seq, 2);
	ck_assert(ag->residue_id[8].insertion == ' ');
	ck_assert(ag->coords[8].X == -10.122);
	ck_assert(ag->coords[8].Y == 46.639);
	ck_assert(ag->coords[8].Z == 2.897);
	ck_assert(ag->coords[8].Z == 2.897);
	ck_assert(ag->occupancy[8] == 0.50);
	ck_assert(ag->B[8] == 8.70);
	ck_assert_str_eq(ag->element[8], " N");
	ck_assert_str_eq(ag->formal_charge[8], "+1");

	for (size_t i = 0; i < ag->natoms; ++i) {
		struct mol_residue *res = find_residue(ag,
						ag->residue_id[i].chain,
						ag->residue_id[i].residue_seq,
						ag->residue_id[i].insertion);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
	}

	unlink(fname);
	mol_atom_group_free(ag);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("pdb_writing");

	TCase *tcase_write = tcase_create("test_write");
	tcase_add_test(tcase_write, test_write_pdb);

	suite_add_tcase(suite, tcase_write);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
