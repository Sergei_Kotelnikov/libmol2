libmol2
-------

Will contain code described in the [library proposal](https://bitbucket.org/bu-structure/library-proposal).

## Dependencies

* CMake and C compiler
* Jansson, gperf
* [Check](http://check.sourceforge.net/) for running tests
* Lcov for generating code coverage on tests

**Ubuntu 16.04 / 18.04**

    sudo apt install build-essential git cmake libjansson4 libjansson-dev gperf check lcov

**Mac OS**

Install [Homebrew](https://brew.sh/), then run:

    brew install gperf jansson check

## Getting started

First, install dependencies. Then download this repository and go to its root:

    git clone https://bitbucket.org/bu-structure/libmol2.git && cd libmol2

Now, create separate build directory and run CMake:

    mkdir build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=YES -CMAKE_INSTALL_PREFIX=$HOME ..

If it does not complain about anything, you can finally compile the library and run tests:

    make && make test

The tests should all pass.

If you want to install the library to your home directory (`~/lib`, `~/include`, etc), just run

    make install

### Customizing your build

If you want to customize your build, run `ccmake ..` on the build directory and
change the variables in the nice console interface. Alternatively, you can just pass
them as arguments to the `cmake` command — like we did with `CMAKE_BUILD_TYPE` and
`BUILD_TESTS` earlier.

The most common variables you may want to change are:

* `CMAKE_BUILD_TYPE` — we recommend `Release` for normal use, and `Debug` for development.
`RelWithDebInfo` is a nice middle ground, that works almost as fast as release version, but
still contains debug symbols.

* `CMAKE_INSTALL_PREFIX` — choose installation directory (for `make install`).

* `BUILD_TESTS` — set to `YES` to enable tests, and to `NO` if you don't need them.

* `USE_COVERAGE` — set up test coverage measurement. See below.

### Measuring test coverage

Build code with `USE_COVERAGE=YES`. Then to run tests and simultaneously generate code coverage
run `make coverage` on the build directory. The Lcov tool generates HTML
files showing coverage statistics.
Open `./code_coverage/index.html` in your browser to view the coverage report.

### Contributing

The description of the code style is available in `CONTRIBUTING.md`.
